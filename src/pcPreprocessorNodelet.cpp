/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Ziegler
 * @date 08.04.2019
 * @file pcPreprocessorNodelet.cpp
 * @brief Nodelet for processing a point cloud so that it can be used as an input for the pair feature
 * matching algorithm for object recognition.
 * 
 * Currently this Nodelet subsamples a point cloud and estimates surface normals on the subsampled
 * cloud in a very simple way. Any normals already present in the dataset will be overridden / deleted.
 */

// project
#include <pcDownsampler.hpp>
#include <pcNormalEstimator.hpp>
#include <boundingSphere.hpp>
#include <planeRemoval.hpp>
#include <unorganizedEdgeDetection.hpp>
#include <boundaryOrientation.hpp>
#include <nodeletWithStatistics.hpp>

// ROS code
#include <ros/ros.h>
#include <pluginlib/class_list_macros.h> // for nodelets

// dynamic reconfigure functionality
#include <dynamic_reconfigure/server.h>
#include <pf_matching_core/preprocessorConfig.h>

// ROS messages
#include <sensor_msgs/PointCloud2.h>
#include <pf_matching_core/ProcessedClouds.h>
#include <std_msgs/String.h>

// PCL
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/features/boundary.h>
//#include <pcl/io/pcd_io.h>   // MFZ added for debugging, TODO: remove this afterwards

// other
#include <string>
#include <boost/timer/timer.hpp>
#include <ctime>



namespace pf_matching{
namespace ROS{
    
    
/**
 * @class PCpreprocessorSettings
 * @brief Class for reading and storing the node's settings
 */
class PCpreprocessorSettings{
    public:

        // topics
        std::string input_topic;                    //!< ROS-topic for the input cloud
        std::string output_topic_processed_clouds;  //!< ROS-topic for publishing oriented surface points
        std::string output_topic_statistics;        //!< general statistics topic

        /**
         * @brief Read the static node settings from the private node handle and check them for plausability.
         * @param privateNh private node handle
         * @return true if reading was successfull, else false
         */
        bool readSettings(const ros::NodeHandle& privateNh){
            
            bool readOK = true;
            
            // get topics
            privateNh.param("input_topic",                   input_topic, std::string("point_cloud_raw"));
            privateNh.param("output_topic_processed_clouds", output_topic_processed_clouds, std::string("processed_clouds"));
            privateNh.param("output_topic_statistics",       output_topic_statistics, std::string("pf_statistics"));
            
            // check for help command
            bool printHelp;
            privateNh.param("help", printHelp, true);
            if(printHelp){
                printHelpMsg();
            }
            
            return true;
            
        }
    private:
        /**
         * @brief Print out a nice help message on how to use the node.
         */
        void printHelpMsg(){
            ROS_INFO_STREAM("\nHelp and parameters for pcPreprocessorNodelet\n"
                << "-------------------------------------\n"
                << "(To hide this message, start the node / nodelet with _help:=false)\n\n"
                << "Available static parameters (set via _<pname>:=<value>):\n"
                << "--------------------------------------------------------\n"
                << "input_topic:                   topic with raw point cloud \n"
                << "output_topic_processed_clouds: topic for publishing the oriented points\n"
                << "output_topic_statistics:       topic publishing general statistics about node operation\n"
                << "\n"
                << "This nodelet takes an input point cloud and performs the following processing:\n"
                << " * downsample the cloud to a managable size\n"
                << " * estimate surface normals\n"
                << " * extract boundary information\n"
                << " * downsample oriented surface and boundary points to desired point distance\n"
                << " * perform post-processing steps such as removal of the biggest plane or NaNs\n"
                << "\n"
                << "Algorithm parameters are available through the dynamic_reconfigure interface."
                );
        }
};
    
/**
 * @class PCpreprocessorNodelet
 * @brief nodelet for performing various preprocessing steps on a point cloud to make it usable
 *        for matching point pair features
 */
class PCpreprocessorNodelet : public NodeletWithStatistics{
    
    public:
        typedef pcl::PointNormal                   internalPointType; //!< point type used in this nodelet
        typedef pcl::PointCloud<internalPointType> internalCloudType; //!< cloud type used in this nodelet

    private:
    
        /**
         * @brief Setup of the nodelet.
         */
        virtual void onInit(){
            
            // general node handle, manager's namespace, usually for topic access
            ros::NodeHandle& nh = getNodeHandle(); // manager namespace, for topics
            ros::NodeHandle& private_nh = getPrivateNodeHandle(); // node's private namespace, 
                                                                  // for parameters
            
            // read settings
            bool readOK = settings.readSettings(private_nh);
            if(!readOK){
                exit(EXIT_FAILURE);
            }

            // set up publishers for the surface and edge points, in rare cases, advertising fails
            pubPCsOut = nh.advertise<pf_matching_core::ProcessedClouds>(settings.output_topic_processed_clouds, 10, true);
            if (!pubPCsOut){
                NODELET_ERROR_STREAM("nh.advertise returned an empty publisher. Shutting down.");
                exit(EXIT_FAILURE);
            }
            initStatisticsPublisher(nh, settings.output_topic_statistics);
            
            // set up subscriber to the topic for the input cloud
            subPCin = nh.subscribe(settings.input_topic, 10, &PCpreprocessorNodelet::processCloud, this);
            
            // set up dynamic reconfigure callback
            dynReconfigServer.setCallback(boost::bind(&PCpreprocessorNodelet::dynamicReconfigureCallback, this, _1, _2));
            
        }
        
        /**
         * @brief process an incoming point cloud and publish the results
         * @param msg_in message containing the point cloud
         */
        void processCloud(const sensor_msgs::PointCloud2ConstPtr& msg_in){
            NODELET_INFO("Got new input cloud. Processing...");
            
            if(!got_initial_config){
                NODELET_WARN("Did not get an initial set of dynamic reconfigure parameters. Doing nothing.");
                return;
            }
            
            // extract message content to local storage
            internalCloudType::Ptr cloudInPtr(new internalCloudType);
            pcl::fromROSMsg(*msg_in, *cloudInPtr);
            std::string cloudName = cloudInPtr->header.frame_id;
            publishStatistics("n_points_input", cloudInPtr->points.size(),
                              config.publish_statistics, cloudName);
            NODELET_INFO_STREAM("Cloud size before processing: " << cloudInPtr->points.size());
            

            // determine desired distance between points
            float d_points_abs;
            float cloudDiameter = 0.0;
            bTimer.start();
//			Eigen::Vector4f boundingSphere;
//			tools::welzlBoundingSphere(*cloudInPtr, boundingSphere);
//			cloudDiameter = (float)(2 * boundingSphere[3]);
            if(config.d_points_is_relative){
                
                // distance calculation
            	Eigen::Vector4f boundingSphere;
				tools::welzlBoundingSphere(*cloudInPtr, boundingSphere);
				cloudDiameter = (float)(2 * boundingSphere[3]);
                d_points_abs =  cloudDiameter * config.d_points;
                
                // output
                NODELET_DEBUG_STREAM("Calculated absolute point distance: " << d_points_abs << "");
                publishStatistics("d_points_calculation_time_ms", bTimer.elapsed().wall / 1e6,
                                  config.publish_statistics, cloudName);
                publishStatistics("cloud_diameter", cloudDiameter, config.publish_statistics, cloudName);
                publishStatistics("d_points_abs", d_points_abs, config.publish_statistics,
                                  cloudName);
            }
            else{
                d_points_abs = config.d_points;
                publishStatistics("cloud_diameter", cloudDiameter, config.publish_statistics, cloudName);
                publishStatistics("d_points_abs", d_points_abs, config.publish_statistics,
                                  cloudName);
            }
            
            
            // set up worker classes
            preDownsampler.setPointDistances(config.pre_downsample_dist_ratio * d_points_abs);
            normalEstimator.setSearchRadius (d_points_abs);
            postDownsampler.setPointDistances(d_points_abs);
            
            // remove largest plane (e.g. ground plane) if requested
            if(config.remove_largest_plane && config.remove_plane_first){
                removePlane(cloudInPtr, cloudName, d_points_abs);
            }

            // remove NaNs if requested
			// do this before pre-downsampling too (not only at the end of processCloud() and after normal-estimation ).
			// why? because NAN-Points and points with NAN-Normals might cause the preDownsampler to produce
            // a lot of invalid points and points with invalid normals. This reduces the number of valid points
            // that the normalEstimator can use. Because before MLS-normal estimation, those invalid points are removed.
			if(config.remove_NaNs){
				std::vector<int> tmp; // dummy vector
				pcl::removeNaNNormalsFromPointCloud(*cloudInPtr, *cloudInPtr, tmp);
				pcl::removeNaNFromPointCloud(*cloudInPtr, *cloudInPtr, tmp);
			}

            // pre-downsampling
            bTimer.start();
            internalCloudType::Ptr cloudPreDownsampledPtr(new internalCloudType);
            preDownsampler.downsample<internalPointType>(cloudInPtr, cloudPreDownsampledPtr);
            
            NODELET_INFO_STREAM("pre-downsampling done in " << bTimer.elapsed().wall / 1e6 << " ms");
            publishStatistics("pre-downsample_time_ms", bTimer.elapsed().wall / 1e6,
                              config.publish_statistics, cloudName);
            publishStatistics("n_points_after_pre-downsample", cloudPreDownsampledPtr->points.size(),
                              config.publish_statistics, cloudName);
            
            // normal estimation
            bTimer.start();
            internalCloudType::Ptr cloudNormalsPtr(new internalCloudType);
            normalEstimator.estimateNormals<internalPointType, internalPointType>(cloudPreDownsampledPtr,
                                                                                  cloudNormalsPtr,
                                                                                  config.flip_normals);
 
            NODELET_INFO_STREAM("normal estimation done in " << bTimer.elapsed().wall / 1e6 << " ms");
            publishStatistics("normal_estimation_time_ms", bTimer.elapsed().wall / 1e6,
                              config.publish_statistics, cloudName);
            publishStatistics("n_points_after_normal_estimation", cloudNormalsPtr->points.size(),
                              config.publish_statistics, cloudName);
            

            // remove NaNs if requested
            // do this after normal estimation too (not only at the end of processCloud() ).
            // why? because in case that the normalEstimator produces points with NAN-Normals,
            // those NAN-Normals need to be filtered before final down-sampling. Otherwise they
            // can cause the postDownsampler to produce a lot of points with invalid normals
            // (because it calculates the average normals too. NAN-normals might jeopardize these calculations),
            // which finally will be removed by the final NAN-removal.
			if(config.remove_NaNs){
				std::vector<int> tmp; // dummy vector
				pcl::removeNaNNormalsFromPointCloud(*cloudNormalsPtr, *cloudNormalsPtr, tmp);
				pcl::removeNaNFromPointCloud(*cloudNormalsPtr, *cloudNormalsPtr, tmp);
			}


            // post-downsampling
            bTimer.start();
            internalCloudType::Ptr cloudPostDownsampledPtr(new internalCloudType);
            postDownsampler.downsample<internalPointType>(cloudNormalsPtr, cloudPostDownsampledPtr);
            
            NODELET_INFO_STREAM("post-downsampling done in " << bTimer.elapsed().wall / 1e6 << " ms");
            publishStatistics("post-downsample_time_ms", bTimer.elapsed().wall / 1e6,
                              config.publish_statistics, cloudName);
                              
            // normalize normals (required for unorganized edge detection)
            for(auto& p : cloudPostDownsampledPtr->points){
                p.getNormalVector3fMap().normalize();
            }

            // extract boundaries
            internalCloudType::Ptr surfacePtr = cloudPostDownsampledPtr; // keep everything as surface
            internalCloudType::Ptr edgesPtr(new internalCloudType);
            if(config.extract_curvature_edges || config.extract_boundary){
                
                bTimer.start();
                
                extractBoundary(cloudPostDownsampledPtr, edgesPtr, 2 * d_points_abs);
                publishStatistics("boundary_extraction_time_ms", bTimer.elapsed().wall / 1e6,
                                  config.publish_statistics, cloudName);
                NODELET_INFO_STREAM("boundary extraction done in " << bTimer.elapsed().wall / 1e6 << " ms");
            }
            
            // remove largest plane (e.g. ground plane) if requested
            if(config.remove_largest_plane && !config.remove_plane_first){
                removePlane(surfacePtr, cloudName, d_points_abs);
            }
            
            // remove NaNs if requested
            if(config.remove_NaNs){
                
                bTimer.start();
                std::vector<int> tmp; // dummy vector
                pcl::removeNaNNormalsFromPointCloud(*surfacePtr, *surfacePtr, tmp);
                pcl::removeNaNFromPointCloud(       *surfacePtr, *surfacePtr, tmp);
                pcl::removeNaNNormalsFromPointCloud(*edgesPtr, *edgesPtr, tmp);
                pcl::removeNaNFromPointCloud(       *edgesPtr, *edgesPtr, tmp);
                
                publishStatistics("NaN_removal_time_ms", bTimer.elapsed().wall / 1e6,
                                  config.publish_statistics, cloudName);
            }
            
            // create a new message for publishing
            pf_matching_core::ProcessedClouds::Ptr msgOut(new pf_matching_core::ProcessedClouds);

            // add the clouds
            pcl::toROSMsg(*surfacePtr, msgOut->surface);
            pcl::toROSMsg(*edgesPtr, msgOut->edges); // edge cloud is empty so far
            
            // keep time and frame_id of the original message
            msgOut->surface.header = msg_in->header;
            msgOut->edges.header = msg_in->header;

            // MFZ 30.08.2018 added cloudDiameter info to processed_cloud msg:
            // Why? Because the matcher nodelet needs the model diameter, if d_dist is a
            // relative value, so that it can calculate d_dist_abs with it. Originally, the matcher
            // calculated the model diameter from the subsampled model cloud. Less points = different model diameter.
            // Therefore it differed unpredictably from the cloud diameter that was calculated here.
            // Hence d_dist_abs and d_points_abs of the model were differing unpredictably.
            // There was no defined ratio, such as 1:1, not even a rule like d_dist_abs < d_points_abs was observed.
            // --> That is why we make the erroneous diameter-calculation in matcher-nodelet obsolete, by publishing it here:
            msgOut->cloudDiameter = cloudDiameter;

            // TODO:: MFZ 10.07.2018 delete this after debugging
//            std::time_t timestamp = std::time(0);
//            std::string fileName = "MY_SYSTEM_downsampled_cloud_" + msg_in->header.frame_id + "_" + std::to_string (timestamp) + ".pcd";
//            pcl::io::savePCDFileASCII ("/home/markus/Documents/" + fileName, *surfacePtr);

            // publish message
            pubPCsOut.publish(msgOut);
            int n_surface = surfacePtr->size();
            int n_edges = edgesPtr->size();
            NODELET_INFO_STREAM("Point clouds with " << n_surface << " surface points and "
                                << n_edges << " edge points "
                                << "published to topic '" + pubPCsOut.getTopic() + "'.");
            publishStatistics("n_points_surface", n_surface,
                              config.publish_statistics, cloudName);
            publishStatistics("n_points_edges"  , n_edges, 
                              config.publish_statistics, cloudName);
        }
        
        /**
         * @brief extract the boundary from a point cloud
         * @param[in,out] cloudPtr original point cloud with estimated normals of length 1,
         *                might be modified to remove NaN-points
         * @param[out] edgesPtr found boundaries
         * @param[in] searchRadius radius around points for neighbour search
         */
        void extractBoundary(internalCloudType::Ptr& cloudPtr,
                             internalCloudType::Ptr& edgesPtr,
                             const float searchRadius){
            
            // remove NaNs
            std::vector<int> tmp; // dummy vector
            pcl::removeNaNNormalsFromPointCloud(*cloudPtr, *cloudPtr, tmp);
            pcl::removeNaNFromPointCloud(       *cloudPtr, *cloudPtr, tmp);
            
            // make kdTree of input cloud
            typename pcl::search::KdTree<internalPointType>::Ptr treePtr(new pcl::search::KdTree<internalPointType>);
            treePtr->setInputCloud(cloudPtr);

            // extract high curvature edges
            if(config.extract_curvature_edges){
                extensions::UnorganizedEdgeDetection<internalPointType> ued;
                ued.setInputCloud(cloudPtr);
                ued.setSearchMethod(treePtr);
                ued.setRadiusSearch(searchRadius);
                ued.setCurvatureThresholds(config.curvature_lower_thresh,
                                           config.curvature_upper_thresh);
                std::vector<int> edgesIndices; // just dummy
                ued.compute(edgesPtr, edgesIndices);
            }
            
            // extract boundary points (i.e. occluding edges, occluded edges, scan edges)
            if(config.extract_boundary){
                pcl::PointCloud<pcl::Boundary> isBoundary;
                pcl::BoundaryEstimation<internalPointType, internalPointType, pcl::Boundary> est;
                est.setInputCloud(cloudPtr);
                est.setInputNormals(cloudPtr);
                est.setRadiusSearch(searchRadius);
                est.setAngleThreshold(config.boundary_angle_thresh_in_pi * M_PI);
                est.setSearchMethod(treePtr);
                est.compute(isBoundary);
                
                // copy into separate point cloud
                internalCloudType::Ptr boundariesPtr(new internalCloudType);
                for(int i = 0; i < isBoundary.size(); i++){
                    if(isBoundary.points[i].boundary_point){
                        boundariesPtr->push_back(cloudPtr->points[i]);
                    }
                }
                
                // orient boundary points
                if(boundariesPtr->size() > 0){
                    extensions::BoundaryOrientation<internalPointType> bo;
                    bo.setRadiusSearch(searchRadius);
                    bo.orientPoints(boundariesPtr);
                }
                
                // concatenate to edge points
                *edgesPtr += *boundariesPtr;
            }
        }
        
        /**
         * @brief remove the biggest plane from the cloud according to the values set in config
         * @param cloudPtr cloud to remove plane from
         * @param cloudName name of the cloud for output /  statistics
         * @param d_points_abs absolut point distance for cases if inlier threshold is relative
         */
        void removePlane(typename internalCloudType::Ptr& cloudPtr, std::string& cloudName,
                         float d_points_abs){
                
            boost::timer::cpu_timer localTimer;
            localTimer.start();
                
            // determine threshold value
            float inlierThresh;
            if(config.plane_inlier_threshold_is_relative){
                inlierThresh = d_points_abs * config.plane_inlier_threshold;
            }
            else{
                inlierThresh = config.plane_inlier_threshold;
            }
            
            // remove
            pre_processing::removeBiggestPlane<internalPointType>(cloudPtr, cloudPtr, inlierThresh);
            
            // output
            NODELET_INFO_STREAM("plane removed in " << localTimer.elapsed().wall / 1e6 << " ms");
            publishStatistics("plane_removal_time_ms",
                              localTimer.elapsed().wall / 1e6,
                              config.publish_statistics, cloudName);
            publishStatistics("n_points_after_plane_removal", cloudPtr->points.size(),
                              config.publish_statistics, cloudName);
        }
        
        /**
         * @brief callback for changing dynamic reconfigure parameters, copies the parameters in
         * the appropriate place.
         * @param config_in the new configuration
         * @param level bitfield specifying which parameters changed (currently unused)

         */
        void dynamicReconfigureCallback(pf_matching_core::preprocessorConfig &config_in, uint32_t level){
        
            NODELET_DEBUG_STREAM("Got new dynamic reconfigure configuration.");
            
            config = config_in;
            
            preDownsampler.setMethod(static_cast<pre_processing::PCdownsampler::DownsampleMethod>(config_in.pre_downsample_method));
            postDownsampler.setMethod(static_cast<pre_processing::PCdownsampler::DownsampleMethod>(config_in.post_downsample_method));
            normalEstimator.setMethod(static_cast<pre_processing::PCnormalEstimator::NormalEstimationMethod>(config_in.normal_estimation_method));
            
            got_initial_config = true;
        }
    
        // ROS related
        ros::Publisher pubPCsOut;   //!< publisher for the processed clouds

        ros::Subscriber subPCin;    //!< subscriber for raw point cloud data
        
        // dynamic reconfigure server
        dynamic_reconfigure::Server<pf_matching_core::preprocessorConfig> dynReconfigServer;
        pf_matching_core::preprocessorConfig config; //!< latest dynamic reconfigure parameters
        bool got_initial_config = false;
        
        // various others
        PCpreprocessorSettings settings;  //!< startup settings storage
        pre_processing::PCdownsampler preDownsampler;
        pre_processing::PCdownsampler postDownsampler;
        pre_processing::PCnormalEstimator normalEstimator; //!< ~
        
        // for statistics
        boost::timer::cpu_timer bTimer; //!< for measuring runtimes

};


} // end namespace
} // end namespace

// make nodelet known
PLUGINLIB_EXPORT_CLASS(pf_matching::ROS::PCpreprocessorNodelet, nodelet::Nodelet)
