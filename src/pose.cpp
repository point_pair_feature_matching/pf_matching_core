
/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke, Markus Franz Ziegler
 */

#include <pose.hpp>

// std
#include <iomanip> // for stream output formating

namespace pf_matching{

/**
 * @brief calculate the weighted average of two unit quaternions that represent orientations
 * 
 * see "Averaging Quaternions" by Ynag Cheng et al. 2007 equation (18) and sourrounding
 * 
 * @param q1 first unit quaternion
 * @param q2 second unit quaternion
 * @param w1 weight of q1
 * @param w2 weight of q2
 * @return resulting unit quaternion of undetermined sign (since q and -q represent the same rotations);
 *         For orthogonal quaternions, the quaternion with the highest weight (and q1 for w1 = w2) 
 *         is returned.
 */
static inline Eigen::Quaternionf unitQuaternionAverage(const Eigen::Quaternionf& q1,
                                                       const Eigen::Quaternionf& q2,
                                                       float w1,
                                                       float w2){
    
    float innerProduct = q1.dot(q2);
    
    // if quaternions are orthogonal, return the one with the higher weight
    // for equal weights, return q1
    if(innerProduct == 0){
        if(w1 >= w2){
            return q1;
        }
        else{
            return q2;
        }
    }
    
    // weighted average
    float z = sqrt( (w1 - w2) * (w1 - w2) + 4*w1*w2*innerProduct*innerProduct);
    float q1Factor = 2 * w1 * innerProduct;
    float q2Factor = w2 - w1 + z;
    
    // averaged quaternion
    Eigen::Quaternionf q;
    q.w() = q1Factor * q1.w() + q2Factor * q2.w();
    q.x() = q1Factor * q1.x() + q2Factor * q2.x();
    q.y() = q1Factor * q1.y() + q2Factor * q2.y();
    q.z() = q1Factor * q1.z() + q2Factor * q2.z();
    
    return q.normalized();
}
    

Pose::Pose(const Eigen::Affine3f& transformation, const float weight_){
    t = transformation.translation();
    q = transformation.rotation();
    weight = weight_;
}


Pose::Pose(){
    t.x() = 0;
    t.y() = 0;
    t.z() = 0;
    
    q = Eigen::Quaternionf::Identity();
    
    weight = 0;
}


Pose::Pose(const Eigen::Affine3f& Tsg, const Eigen::Affine3f& Tmg, float alpha, float weight_, int sceneRefPointIdx_,  int maxIdx_){
    
    // calculate pose transform
    Eigen::AngleAxisf Rx(alpha, Eigen::Vector3f::UnitX());       // aligning rotation around x
    Eigen::Affine3f transform = Tsg.inverse() * Rx * Tmg;
    
    // extract translation and rotation part
    t = transform.translation();
    q = transform.rotation();
    
    // store weight
    weight = weight_;

    // store additional information for multi-threading:
    sceneRefPointIdx = sceneRefPointIdx_;
    maxIdx = maxIdx_;
}

Pose::Pose(const Eigen::Vector3f& translation, Eigen::Quaternionf& rotation, const float weight_){

    t = translation;
    q = rotation;
    weight = weight_;
}


const Eigen::Affine3f Pose::asTransformation() const{
    return Eigen::Translation3f(t) * q;
}


bool Pose::isSimilar(const Pose& otherPose, const float transThresh, const float rotThresh){
    
    // check translation part first, use squared norm for faster calculation
    if(!translationIsSimilar(otherPose, transThresh)){
        return false;
    }
    
    // check rotation parts, use \Phi_6 - function as a metric
    if(2 * acos(fabs(q.dot(otherPose.q))) > rotThresh){
        return false;
    }
    
    return true;
}


bool Pose::translationIsSimilar(const Pose& otherPose, const float transThresh){
    
    // use squared norm for faster calculation
    Eigen::Vector3f diff = t - otherPose.t;
    return diff.squaredNorm() <= transThresh * transThresh;
}

std::ostream& operator<<(std::ostream& stream, const Pose p){
    
    stream << std::fixed << std::setw(8) << std::setprecision(4)
           << "weight: " << p.weight
           << std::fixed << std::setw(11) << std::setprecision(6)
           << ", translation: (" << p.t.transpose() << ")^T"
           << std::fixed << std::setw(8) << std::setprecision(6)
           << ", quaternion rotation (w, x, y, z): ("
           << p.q.w() << ", " << p.q.x() << ", "<< p.q.y() << ", "<< p.q.z() << ")";

    return stream;
}

PoseCluster::PoseCluster(const Pose& p, unsigned int votingBallIdx_):
Pose(p), // standard copy constructor
votingBallIdx(votingBallIdx_)
{
}

PoseCluster::PoseCluster(unsigned int votingBallIdx_):
Pose(),  // standard constructor
votingBallIdx(votingBallIdx_)
{
}

void PoseCluster::addPose(Pose* p){
//    std::cout << "[PoseCluster::addPose] add pose of base class used. Should be used for Choi Voting." << std::endl;
    t = (p->t * p->weight + t * weight) / (p->weight + weight);
    q = unitQuaternionAverage(q, p->q, weight, p->weight);
    weight += p->weight;
}


PoseWithAssociatedModelPoint::PoseWithAssociatedModelPoint():
Pose(),
i_r(0)
{
}

PoseWithAssociatedModelPoint::PoseWithAssociatedModelPoint(const Eigen::Affine3f& transformation, const float weight_, const unsigned int i_r_):
Pose(transformation, weight_),
i_r(i_r_)
{
}


PoseWithAssociatedModelPoint::PoseWithAssociatedModelPoint(const Eigen::Affine3f& Tsg, const Eigen::Affine3f& Tmg, float alpha, float weight_, int sceneRefPointIdx_, int maxIdx_, const unsigned int i_r_):
Pose(Tsg, Tmg, alpha, weight_, sceneRefPointIdx_, maxIdx_),
i_r(i_r_)
{
//	std::cout << "[PoseWithAssociatedModelPoint::PoseWithAssociatedModelPoint] construct pose with weight " << weight << std::endl;
}


PoseClusterWithAssociatedModelPoints::PoseClusterWithAssociatedModelPoints(const PoseWithAssociatedModelPoint& p, unsigned int numberOfModelPoints_, unsigned int votingBallIdx_):
Pose(p),
PoseCluster(p, votingBallIdx_),
PoseWithAssociatedModelPoint(p), // standard copy constructor
numberOfModelPoints(numberOfModelPoints_)
{
	FlagArrayOfModelPoints.resize(numberOfModelPoints, false);
//	if (p.i_r > numberOfModelPoints){
//		FlagArrayOfModelPoints.resize(p.i_r, false);
//	}
	FlagArrayOfModelPoints.set(p.i_r, true);

//	std::cout << "[PoseClusterWithAssociatedModelPoints::PoseClusterWithAssociatedModelPoints] constructed cluster with weight" << weight << std::endl;
}

// TODO: 14.04.2018 to write:
void PoseClusterWithAssociatedModelPoints::addPose(Pose* p){ //WithAssociatedModelPoint& p){

	PoseWithAssociatedModelPoint* p_casted = dynamic_cast<PoseWithAssociatedModelPoint*>(p);
	if (p_casted!= nullptr){
//		std::cout << "[PoseClusterWithAssociatedModelPoints::addPose] add pose of derived class used. Should be used for Hinterstoisser Voting." << std::endl;
		// update pose-cluster centroid
		t = (p_casted->t * p_casted->weight + t * weight) / (p_casted->weight + weight);
		q = unitQuaternionAverage(q, p_casted->q, weight, p_casted->weight);
		weight += p_casted->weight;

		// flag model point as "used"
		FlagArrayOfModelPoints.set(p_casted->i_r, true);
	}
	else {
//		std::cout << "[PoseClusterWithAssociatedModelPoints::addPose] dynamic_cast error! only update the pose-cluster-centroid, can't flag a model point" << std::endl;
		// in the case, that the Pose* p pointer is in fact pointing to an pose-object (base class)
		// we can only update pose-cluster centroid, since their is no associated model refPoint
		// (this functionality is used within hinterstoisserClustering()-member-function, when an
		// equivalent pose is added to a pose cluster)
		t = (p->t * p->weight + t * weight) / (p->weight + weight);
		q = unitQuaternionAverage(q, p->q, weight, p->weight);
		weight += p->weight;
	}
}

} // end namespace
