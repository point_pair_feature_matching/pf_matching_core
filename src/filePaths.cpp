/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#include <filePaths.hpp>

#include <boost/filesystem.hpp>
#include <pcl/console/print.h>

namespace bfs = boost::filesystem;

namespace pf_matching{
namespace tools{


std::string formatPathAndCreate(std::string path){
    
    // nothing to do for empty strings
    if(path.empty()){
        return std::string("");
    }
    
    // remove trailing '/' from non-empty directory string
    if(path.back() == '/'){
        path.pop_back();
    }
    
    // replace leading '~' by user's home directory
    if (path[0] == '~') {
        char const* home = getenv("HOME");
        path.replace(0, 1, home);
    }
    
    // add leading '/' if the user forgot
    
    // create specified directory if not already existing
    bfs::create_directories(path);
            
    // check that directory exists now
    if(bfs::is_directory(path)){
        return path;
    }
    else{
        PCL_ERROR("The directory '%s' could not be found or created.\n", path.c_str());
        return std::string("");
    }
}

} // end namespace
} // end namespace