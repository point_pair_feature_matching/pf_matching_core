/*
 * @copyright Copyright (c) 2017
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/*
 * @author Sebastian Krone
 * @date 14/02/17
 * @file processedCloudsSplitterNodelet.cpp
 * @brief Nodelet, that splits up a PreprocessedClouds-Message to the contained PointCloud2-Messages of edges and surfaces
 */

// project
#include <nodeletWithStatistics.hpp>

// ROS code
#include <ros/ros.h>
#include <pluginlib/class_list_macros.h> // for nodelets

// ROS messages
#include <pf_matching_core/ProcessedClouds.h>
#include <sensor_msgs/PointCloud2.h>

//other
#include <string>

// Linux specific
#ifdef __linux__
#include <X11/Xlib.h>
#endif

namespace pf_matching{
namespace ROS{

class ProcessedCloudsSplitterSettings{
    public:

        //topics
        std::string input_topic_processed_clouds;           //!< ROS-topic with processed clouds
        std::string output_topic_processed_surface_clouds;  //!< ROS-topic for processed surface clouds
        std::string output_topic_processed_edges_clouds;     //!< ROS-topic for processed edge clouds

        /**
         * @brief Read the static node settings from the private node handle and check them for plausability.
         * @param privateNh private node handle
         * @return true if reading was successfull, else false
         */
        bool readSettings(const ros::NodeHandle& privateNh){
            bool readOK = true;

            // get topics
            privateNh.param("input_topic_processed_clouds",             input_topic_processed_clouds, std::string("processed_clouds"));
            privateNh.param("output_topic_processed_surface_clouds",    output_topic_processed_surface_clouds, std::string("processed_surface_clouds"));
            privateNh.param("output_topic_processed_edges_clouds",       output_topic_processed_edges_clouds, std::string("processed_edges_clouds"));

            // check for help command
            bool printHelp;
            privateNh.param("help", printHelp, true);
            if(printHelp){
                printHelpMsg();
            }

            return true;

        }
    private:
        /**
         * @brief Print out a nice help message on how to use the node.
         */
        void printHelpMsg(){
            ROS_INFO_STREAM("\nHelp and parameters for processedCloudsSplitterNodelet\n"
                << "-------------------------------------\n"
                << "(To hide this message, start the node / nodelet with _help:=false)\n\n"
                << "Available static parameters (set via _<pname>:=<value>):\n"
                << "--------------------------------------------------------\n"
                << "input_topic_processed_clouds:           topic with processed clouds \n"
                << "output_topic_processed_surface_clouds:  topic for publishing the surface part of the processed clouds\n"
                << "output_topic_processed_edges_clouds:    topic for publishing the edges part of the processed clouds\n"
                << "\n"
                << "This nodelet takes an processed point cloud and publishes the surface, edges part in independend topics.\n"
                << "It is needed to simply switch the verifier from raw clouds to preprocessed, downlampled clouds"
                );
        }
};

/**
 * @class PreprocessedCloudsSplitterNodelet
 * @brief nodelet for splitting up preprocessed clouds to surface and edge part
 */
class ProcessedCloudsSplitterNodelet : public nodelet::Nodelet{

    private:

        /**
         * @brief Setup of the nodelet.
         */
        virtual void onInit(){

            // general node handle, manager's namespace, usually for topic access
            ros::NodeHandle& nh = getNodeHandle(); //manager namespace, for topics
            ros::NodeHandle& private_nh = getPrivateNodeHandle(); // node's private namespace
                                                                  // for parameters

            // read settings
            bool readOK = settings.readSettings(private_nh);
            if(!readOK){
                exit(EXIT_FAILURE);
            }

            // set up publishers
            pubPSCsOut = nh.advertise<sensor_msgs::PointCloud2>(settings.output_topic_processed_surface_clouds, 10, true);
            pubPECsOut = nh.advertise<sensor_msgs::PointCloud2>(settings.output_topic_processed_edges_clouds, 10, true);

            // set up subscriber to the topic with processed clouds
            subPCsIn = nh.subscribe(settings.input_topic_processed_clouds, 10, &ProcessedCloudsSplitterNodelet::splitClouds, this);

        }

        /**
         * @brief splits processed clouds and publishes it's parts
         * @param msg_in message containing the processed cloud
         */
        void splitClouds(const pf_matching_core::ProcessedClouds::ConstPtr& msg_in){
            sensor_msgs::PointCloud2 msg_surface_out = msg_in->surface;
            sensor_msgs::PointCloud2 msg_edges_out = msg_in->edges;
            pubPSCsOut.publish(msg_surface_out);
            pubPECsOut.publish(msg_edges_out);
        }

        // ROS related
        ros::Publisher pubPSCsOut;  //!< publisher for the processed surface clouds
        ros::Publisher pubPECsOut;  //!< publisher for the processed edges clouds

        ros::Subscriber subPCsIn;   //!< subscriber for the processed clouds

        // various others
        ProcessedCloudsSplitterSettings settings; //!< startup settings storage
};

} // end namespace ROS
} // end namespace pf_matching

// make nodelet known
PLUGINLIB_EXPORT_CLASS(pf_matching::ROS::ProcessedCloudsSplitterNodelet, nodelet::Nodelet)
