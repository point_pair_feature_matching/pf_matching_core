/*
 * @copyright Copyright (c) 2019
TU Berlin, Institut fuer Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Authors: Sebastian Krone, Markus Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

/*
 * @author Sebastian Krone
 * @date 20/01/17
 * @file verifierNode.cpp
 * @brief verifies a set of pose hypotheses using the acceptance function from Papazov et al.
 */

// project
//#include <poseVerifier.hpp>
//#include <pose.hpp>
#include <poseHypothesis.hpp>
#include <pairFeatures.hpp>
#include <nodeletWithStatistics.hpp>
#include <resultVisualization.hpp>

// ROS code
#include <ros/ros.h>
#include <pluginlib/class_list_macros.h> // for nodelets

// ROS messages and services
#include <sensor_msgs/PointCloud2.h>
#include <pf_matching_core/WeightedPoseArray.h>
#include <std_srvs/Empty.h>

// dynamic reconfigure functionality
#include <dynamic_reconfigure/server.h> //
#include <pf_matching_core/verifierConfig.h>

// PCL
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/recognition/hv/occlusion_reasoning.h>
#include <pcl/recognition/hv/hv_go.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/range_image/range_image.h> // MFZ: to visualize the range images
#include <pcl/visualization/range_image_visualizer.h>

// other
#include <string>
#include <utility>
#include <boost/timer/timer.hpp>
#include <boost/thread/thread.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/tuple/tuple.hpp>

// Linux specific
#ifdef __linux__
#include <X11/Xlib.h>
#endif

// debugging flag:
//#define visualize_range_image_of_model_pose

namespace pf_matching{
namespace ROS{

class VerifierSettings{
    public:

        // topics
        std::string input_topic_model; //!< ROS-topic with input clouds of the model
        std::string input_topic_scene; //!< ROS-topic with input clouds of the scene
        std::string input_topic_poses; //!< ROS-topic with matched poses

        std::string output_topic_statistics;          //!< general statistics topic
        std::string output_topic_statistics_arrays;   //!< array statistics topic
        std::string output_topic_verified_poses;      //!< verified poses

        /**
         * @brief Read the static node settings from the private node handle and check them for plausability.
         * @param privateNh private node handle
         * @return true if reading was sucessful, else false
         */
        bool readSettings(const ros::NodeHandle& privateNh){

            bool readOk = true;

            // get topics
            privateNh.param("input_topic_model", input_topic_model, std::string("model_clouds"));
            privateNh.param("input_topic_scene", input_topic_scene, std::string("scene_clouds"));
            privateNh.param("input_topic_poses", input_topic_poses, std::string("poses"));

            privateNh.param("output_topic_statistics",         output_topic_statistics, std::string("pf_statistics"));
            privateNh.param("output_topic_statistics_arrays",  output_topic_statistics_arrays,  std::string("pf_statistics_arrays"));
            privateNh.param("output_topic_verified_poses", output_topic_verified_poses, std::string("verifiedPoses"));

            // check for help command
            bool printHelp;
            privateNh.param("help", printHelp, true);
            if(printHelp){
                printHelpMsg();
            }

            return true;
        }
    private:
        /**
         * @brief Print out a nice help message on how to use the node.
         */
        void printHelpMsg(){
            ROS_INFO_STREAM("\nHelp and parameters for verifierNodelet\n"
                << "-----------------------------------------\n"
                << "(TO hide this message, start the node / nodelet with _help:=false)\n\n"
                << "Available static parameters (set via _<pname>:=<value>):\n"
                << "--------------------------------------------------------\n"
                << "input_topic_model: topic with point clouds of the model\n"
                << "input_topic_scene: topic with point clouds of the scene\n"
                << "input_topic_poses: topic with matched poses\n"
                << "\n"
                << "output_topic_verified_poses:      topic for verified poses\n"
                << "output_topic_statistics:          topic for publishing general statistics about node operation\n"
                << "output_topic_statistics_arrays:   topic for publishing multi-value statistics\n"
                << "\n"
                << "Available services:\n"
                << "-------------------\n"
                << "~/clear_models: erase all stored models\n"
                << "\n"
                << " "
                );
        }

};
class VerifierNodelet : public NodeletWithStatistics{
    public:
        typedef pcl::PointNormal                    internalPointType; //!< point type used in this nodelet
        typedef pcl::PointCloud<internalPointType>  internalCloudType; //!< cloud type used in this nodelet
        typedef boost::adjacency_list<boost::listS, boost::listS, boost::undirectedS, PoseHypothesis::Ptr> Graph;
        //typedef boost::undirected_graph<> Graph;
        typedef boost::graph_traits<Graph> GraphTraits;

        enum VerificationMethod{
            ACCEPTANCE_FUNCTION = 0, //<! use Acceptance function by Papazov et al. for verification
            //GLOBAL_HV, //<! use global hypothesis verification by Aldoma et al. for verification
            };

        enum SensorDirection{
            POSITIVE_X = 0,
            NEGATIVE_X,
            POSITIVE_Y,
            NEGATIVE_Y,
            POSITIVE_Z,
            NEGATIVE_Z,
        };

        ~VerifierNodelet(){

            // close down the visualization window properly
            visualizationThread.interrupt();
            visualizationThread.join();
        }

    private:

        /**
         * @brief Setup of the nodelet.
         */
        virtual void onInit(){
            // initialization required on linux systems
            // during thread creation
            #ifdef __linux__
            XInitThreads();
            #endif

            NODELET_DEBUG_STREAM("Initializing verifier nodelet...");

            // create node handle for topic access
            ros::NodeHandle& nh = getNodeHandle(); // manager namespace, for topics
            ros::NodeHandle& private_nh = getPrivateNodeHandle(); // node's private namespace,
                                                                  //for parameters

            // read settings from parameter server
            bool readOK = settings.readSettings(private_nh);
            if(!readOK){
                exit(EXIT_FAILURE);
            }

            // set up publishers
            pubVerifiedPose = nh.advertise<pf_matching_core::WeightedPoseArray>(settings.output_topic_verified_poses, 10, true);
            initStatisticsPublisher(nh, settings.output_topic_statistics);
            initStatisticsArrayPublisher(nh, settings.output_topic_statistics_arrays);

            //set up subscribers to model and scene input topic
            subModel = nh.subscribe(settings.input_topic_model, 5, &VerifierNodelet::modelCloudsInCallback, this);
            subScene = nh.subscribe(settings.input_topic_scene, 5, &VerifierNodelet::sceneCloudsInCallback, this);
            subPoses = nh.subscribe(settings.input_topic_poses, 20, &VerifierNodelet::posesInCallback, this);

            // set up dynamic reconfigure callback
            dynReconfigServer.setCallback(boost::bind(&VerifierNodelet::dynamicReconfigureCallback, this, _1, _2));

            // set up services
            srvClear = private_nh.advertiseService("clear_models", &VerifierNodelet::srvClearModelsCallback, this);

        }

        /**
         * @brief publish an array of pose hypotheses via the specified publisher
         * @param
         */

        void publishVerifiedPoseHypothesisArray(ros::Publisher pub, std::vector<PoseHypothesis::Ptr> poseHypothesesArray,
                                      	  	  	std::string poseType){

            if(poseHypothesesArray.size() > 0){
                pf_matching_core::WeightedPoseArray::Ptr msgOut(new pf_matching_core::WeightedPoseArray);

                std_msgs::Header sceneHeader;
                std::string modelName;
                bool sceneAndModelSet = false;

                for(PoseHypothesis::Ptr& poseHypothesis : poseHypothesesArray){
                    if(!sceneAndModelSet){
                        sceneHeader = poseHypothesis->getScene();
                        modelName = poseHypothesis->getModel();
                        sceneAndModelSet = true;
                    }

                    if(sceneHeader.frame_id != poseHypothesis->getScene().frame_id || modelName != poseHypothesis->getModel())
                    {
                        NODELET_ERROR_STREAM("Tried to publish PoseArray from different models or different scenes.");
                        return;
                    }

                    pf_matching_core::WeightedPose tmpPose;

                    tmpPose.weight = poseHypothesis->getWeight();

                    tmpPose.position.x = poseHypothesis->getHypotheticalPose().getTranslationPart().x();
                    tmpPose.position.y = poseHypothesis->getHypotheticalPose().getTranslationPart().y();
                    tmpPose.position.z = poseHypothesis->getHypotheticalPose().getTranslationPart().z();

                    tmpPose.orientation.x = poseHypothesis->getHypotheticalPose().getRotationPart().x();
                    tmpPose.orientation.y = poseHypothesis->getHypotheticalPose().getRotationPart().y();
                    tmpPose.orientation.z = poseHypothesis->getHypotheticalPose().getRotationPart().z();
                    tmpPose.orientation.w = poseHypothesis->getHypotheticalPose().getRotationPart().w();

                    tmpPose.votingBall = poseHypothesis->getVotingBallIdx();

                    msgOut->poses.push_back(tmpPose);
                }

                msgOut->poseType = poseType;
                msgOut->sceneHeader = sceneHeader;
                msgOut->model = modelName;

                // publish the message
                pub.publish(msgOut);
            }

            //
        }

        /**
         * @brief store the message content for
         * @param msg_in ROS point cloud message
         */
        void modelCloudsInCallback(const sensor_msgs::PointCloud2ConstPtr& msg_in){
            bTimer.start();
            if(!got_initial_config){
                NODELET_WARN("Did not get an initial set of dynamic reconfigure parameters. Doing nothing.");
                return;
            }

            // extract message content to local storage
            internalCloudType::Ptr cloudInPtr(new internalCloudType);
            pcl::fromROSMsg(*msg_in, *cloudInPtr);
            std::string cloudName = cloudInPtr->header.frame_id;

            NODELET_INFO_STREAM("Got new Model cloud with " << cloudInPtr->points.size() << " points for verification.");

            //computing model diameter and d_dist_abs
            Eigen::Vector4f boundingSphere; // xyz + radius

            if(!tools::welzlBoundingSphere(*cloudInPtr, boundingSphere)){
                NODELET_ERROR_STREAM("Model cloud bounding sphere computation failed.");
                return;
            }
            float modelDiameter = 2 * boundingSphere[3];

            float d_dist_abs;
            if(config.d_dist_is_relative){
                d_dist_abs = config.d_dist * modelDiameter;
                NODELET_DEBUG_STREAM("Calculated absolute value for d_dist: " << d_dist_abs);
            }
            else{
                d_dist_abs = config.d_dist;
            }

            models.push_back(cloudInPtr);
            gotPoseHypothesesForModel.push_back(false);
            d_dists.push_back(d_dist_abs);

            float msElapsed = bTimer.elapsed().wall / 1e6;
            NODELET_INFO_STREAM("Preparation of received model cloud done in " << msElapsed << "ms. Number of models: " << models.size());
            publishStatistics("model_preparation_time_ms", msElapsed, config.publish_statistics, cloudInPtr->header.frame_id);
            publishStatistics("model_diameter", modelDiameter, config.publish_statistics, cloudInPtr->header.frame_id);

        }

        /**
         * @brief store
         */
        void sceneCloudsInCallback(const sensor_msgs::PointCloud2ConstPtr& msg_in){
            NODELET_DEBUG_STREAM("Got new scene cloud.");
            bTimer.start();

            if(!got_initial_config){
                NODELET_WARN("Did not get an initial set of dynamic reconfigure parameters. Doing nothing.");
                return;
            }

            // extract message content to local storage
            internalCloudType::Ptr cloudInPtr(new internalCloudType);
            pcl::fromROSMsg(*msg_in, *cloudInPtr);
            internalCloudType::Ptr sceneInPtr(new internalCloudType(*cloudInPtr));
            std::string cloudName = cloudInPtr->header.frame_id;

            NODELET_INFO_STREAM("Scene cloud size for verification: " << cloudInPtr->points.size());

            // clearing pose hypotheses, visualization data and conflict graph data
            for(int i = 0; i < models.size(); i++)
            {
                gotPoseHypothesesForModel[i] = false;
            }
            posehypotheses.clear();
            visualizationCloudPtrs.clear();
            visualizationNames.clear();
            visualizationPoses.clear();
            conflictGraph.clear();
            vertexHypothesisMap.clear();

            scene = sceneInPtr;

            analyzeCameraOrientation();

            if(config.verification_method == VerificationMethod::ACCEPTANCE_FUNCTION){
                // preparing search tree
                kdtree.setInputCloud(scene);

                // preparing scene-occlusion recognition
                internalCloudType::Ptr scene_for_zbuffer (new internalCloudType(*scene));
                if(cameraPointsTo == SensorDirection::NEGATIVE_Z){
                    /*
                     * the camera is looking to negative z-direction. Because of that reason the scene-points
                     * get reflected in the xy-plane before the ZBuffer gets them.
                     */
                    reflectPointCloud(*scene, *scene_for_zbuffer);
                }

                pcl::PointCloud<pcl::PointXYZ>::Ptr temp_scene_for_zbuffer (new pcl::PointCloud<pcl::PointXYZ>);
                pcl::copyPointCloud(*scene_for_zbuffer, *temp_scene_for_zbuffer);

                pcl::PointCloud<pcl::PointXYZ>::ConstPtr const_scene (new pcl::PointCloud<pcl::PointXYZ>(*temp_scene_for_zbuffer));
                zbuffer_scene = pcl::occlusion_reasoning::ZBuffering<pcl::PointXYZ, pcl::PointXYZ>(config.zbufferingresolution, config.zbufferingresolution, 1.f);
                zbuffer_scene.computeDepthMap(const_scene, true);
                NODELET_DEBUG_STREAM("Flipped Scene for Z-Buffer. " << scene->size() << "; " << scene_for_zbuffer->size() << "; " << temp_scene_for_zbuffer->size() << "; " << const_scene->size());


            }

            if(config.show_results){
                // add scene information
                visualizationCloudPtrs.push_back(scene);
                visualizationNames.push_back(scene->header.frame_id);
                visualizationPoses.push_back(Pose());
            }

            float msElapsed = bTimer.elapsed().wall / 1e6;
            NODELET_INFO_STREAM("Preparation of Scene cloud done in " << msElapsed << "ms.");
            publishStatistics("scene_preparation_time_ms", msElapsed, config.publish_statistics, scene->header.frame_id);

        }

        void posesInCallback(const pf_matching_core::WeightedPoseArrayConstPtr& msg_in){
            NODELET_INFO_STREAM("Got new pose hypotheses.");

            //check if pose is final
            if(msg_in->poseType != "final"){
                return;
            }

            if(msg_in->sceneHeader.frame_id != scene->header.frame_id)
            {
                NODELET_ERROR_STREAM("The received poses do not belong to the current scene! Doing nothing");
                return;
            }
            std_msgs::Header sceneHeader = pcl_conversions::fromPCL(scene->header);

            //TODO sceneHeader und model verarbeiten?

            std::vector<pf_matching_core::WeightedPose> poseArray = msg_in->poses;
            std::vector<PoseHypothesis::Ptr> newPoses;

            for(pf_matching_core::WeightedPose& pose : poseArray){
                Eigen::Vector3f translation;
                Eigen::Quaternionf rotation;

                translation.x() = pose.position.x;
                translation.y() = pose.position.y;
                translation.z() = pose.position.z;

                rotation.x() = pose.orientation.x;
                rotation.y() = pose.orientation.y;
                rotation.z() = pose.orientation.z;
                rotation.w() = pose.orientation.w;

                Pose tmpPose(translation, rotation, pose.weight);
                PoseHypothesis::Ptr tmpPoseHypothesis (new PoseHypothesis);
                tmpPoseHypothesis->setHypotheticalPose(tmpPose);
                tmpPoseHypothesis->setModel(msg_in->model);
                tmpPoseHypothesis->setScene(sceneHeader);
                tmpPoseHypothesis->setVotingBallIdx(pose.votingBall);
                posehypotheses.push_back(tmpPoseHypothesis);
                newPoses.push_back(tmpPoseHypothesis);
            }

#ifdef visualize_range_image_of_model_pose
            // debugging: visualization of the range-image of model-pose-hypothesis in scene
            const pcl::PointCloud<pcl::PointNormal>& visualize_scene = *scene;
            float noise_level = 0.0;
            float min_range = 0.0f;
			int border_size = 1;
			pcl::visualization::RangeImageVisualizer rangeImageVisualizer ("Range Image");
			boost::shared_ptr<pcl::RangeImage> rangeImagePtr (new pcl::RangeImage);
			pcl::RangeImage& rangeImage = *rangeImagePtr;
            rangeImage.createFromPointCloud(visualize_scene, pcl::deg2rad (0.5f), pcl::deg2rad (360.0f), pcl::deg2rad (180.0f),
											 Eigen::Affine3f::Identity (), pcl::RangeImage::CoordinateFrame::CAMERA_FRAME, noise_level, min_range, border_size);
//			rangeImagePtr->createEmpty(pcl::deg2rad (0.5f), Eigen::Affine3f::Identity (), pcl::RangeImage::CoordinateFrame::CAMERA_FRAME, pcl::deg2rad (360.0f), pcl::deg2rad (180.0f));
            int top=0, right=0, bottom=0, left=0;
			pcl::PointCloud<internalPointType>& cloudInput = *(models[0]);
			Eigen::Affine3f pose = (posehypotheses[0]->getHypotheticalPose()).asTransformation();
			pcl::transformPointCloudWithNormals(cloudInput, cloudInput, pose.matrix());
			rangeImage.doZBuffer(cloudInput, 0.0, 0.0, top, right, bottom, left);
			rangeImageVisualizer.showRangeImage(rangeImage,-std::numeric_limits<float>::infinity (), std::numeric_limits<float>::infinity (), false);

			while (!rangeImageVisualizer.wasStopped() )
			{
				rangeImageVisualizer.spinOnce ();
				pcl_sleep(0.01);
			}
#endif

            NODELET_DEBUG_STREAM("Number of Poses for verification: " << newPoses.size() << ";" << msg_in->poses.size() << "; " << posehypotheses.size());

            // perform verification for every received pose if selected verification method is acceptance function
            if(config.verification_method == VerificationMethod::ACCEPTANCE_FUNCTION){
                acceptanceFunction(newPoses);
                if(gotPoseHypothesesForAllModels())
                {
                    conflictAnalysis();
                }
            }

            if(gotPoseHypothesesForAllModels())
            {
                NODELET_INFO_STREAM("Verification done for all pose hypotheses of the scene.");
                publishStatistics("verified_n_posehypotheses", posehypotheses.size(), config.publish_statistics, scene->header.frame_id);
                // visualization in separate thread if requested
                showResults(config.show_results);
            }
        }

        /**
         * @brief service callback to delete all stored models
         * @param request (empty)
         * @param response (empty)
         * @return (always true)
         */
        bool srvClearModelsCallback(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response){
            NODELET_INFO_STREAM("Got service request to clear stored models. Deleting "
                             << models.size() << " models, "
                             << gotPoseHypothesesForModel.size() << " values descibing, if pose hypotheses for model were received, and "
                             << d_dists.size() << " d_dist values.");
            models.clear();
            gotPoseHypothesesForModel.clear();
            d_dists.clear();
            return true;
        }

        void dynamicReconfigureCallback(pf_matching_core::verifierConfig &config_in, uint32_t level){

            NODELET_DEBUG_STREAM("Got new dynamic reconfigure configuration.");


            // copy parameters and remember init
            config = config_in;

            got_initial_config = true;
        }

        /**
         * @brief starts the verification of poses via Acceptance Function by Papazov et al.
         * @param poses list of poses to verify
         */
        void acceptanceFunction(std::vector<PoseHypothesis::Ptr> poses){

            // based on PapazovHV from PLC (http://docs.pointclouds.org/1.7.0/classpcl_1_1_papazov_h_v.html)
            if(models.size() < 1)
            {
                NODELET_ERROR_STREAM("The verifier has no model Data yet. Doing nothing");
                return;
            }
            else if(models.size() != d_dists.size())
            {
                NODELET_ERROR_STREAM("The number of d_dists is not the number of models. The verifier needs this values. Doing nothing");
                return;
            }
            else if(models.size() != gotPoseHypothesesForModel.size())
            {
                NODELET_ERROR_STREAM("The number of vector entries in vector gotPoseHypothesesForModel does not match with the number of models! Doing nothing");
                return;
            }

            if(poses.size() > 0)
            {
                std::vector<int> muInRangePoseHypothesesIdxes;
                std::vector<float> mu_v_vector;
                std::vector<float> mu_p_vector;
                std::vector<PoseHypothesis::Ptr> posesInMuRange;
                std::string model_header = poses[0]->getModel();

                //Prepare vectors for parallel work
                mu_v_vector.resize(poses.size());
                mu_p_vector.resize(poses.size());
                NODELET_DEBUG_STREAM("vectors created and resized to " << poses.size());

                internalCloudType::Ptr model;
                float d_dist_abs;
                bool found_model_for_posehypotheses (false);

                // loop over all known models...
                for(int modelCount = 0; modelCount < models.size(); modelCount++){
                    NODELET_DEBUG_STREAM("current modelCount: " << modelCount);
                    // ...and look for the model whose poses just came in
                    if(!gotPoseHypothesesForModel[modelCount] && models[modelCount]->header.frame_id == model_header){
                        // get some info on the model
                    	model = models[modelCount];
                        d_dist_abs = d_dists[modelCount];
                        found_model_for_posehypotheses = true;
                        NODELET_DEBUG_STREAM("current model: " << model->header);
                        gotPoseHypothesesForModel[modelCount] = true;
                        break;
                    }
                }

                // calculate some absolute values
                float searchradius_abs;
                float zbufferingthreshold_selfOcclusion_abs;
                float zbufferingthreshold_sceneOcclusion_abs;
                if(config.searchradius_is_relative){
                    searchradius_abs = config.searchradius * d_dist_abs;
                    NODELET_DEBUG_STREAM("searchradius absolute: " << searchradius_abs << "; sr is relative");
                }
                else{
                    searchradius_abs = config.searchradius;
                    NODELET_DEBUG_STREAM("searchradius absolute: " << searchradius_abs << "; sr is not relative");
                }

                if(config.zbufferingthreshold_is_relative){
                    zbufferingthreshold_selfOcclusion_abs = config.modelzbufferingthreshold * d_dist_abs;
                    zbufferingthreshold_sceneOcclusion_abs = config.scenezbufferingthreshold * d_dist_abs;
                    NODELET_DEBUG_STREAM("zbufferingthreshold for self occlusion absolute: " << zbufferingthreshold_selfOcclusion_abs << "; zbt is not relative");
                    NODELET_DEBUG_STREAM("zbufferingthreshold for scene occlusion absolute: " << zbufferingthreshold_sceneOcclusion_abs << "; zbt is relative");
                }
                else{
                    zbufferingthreshold_selfOcclusion_abs = config.modelzbufferingthreshold;
                    zbufferingthreshold_sceneOcclusion_abs = config.scenezbufferingthreshold;
                    NODELET_DEBUG_STREAM("zbufferingthreshold for self occlusion absolute: " << zbufferingthreshold_selfOcclusion_abs << "; zbt is not relative");
                    NODELET_DEBUG_STREAM("zbufferingthreshold for scene occlusion absolute: " << zbufferingthreshold_sceneOcclusion_abs << "; zbt is not relative");
                }

                publishStatistics("searchradius_abs", searchradius_abs, config.publish_statistics, model_header);
                publishStatistics("zbufferingthreshold_selfOcclusion_abs", zbufferingthreshold_selfOcclusion_abs, config.publish_statistics, model_header);
                publishStatistics("zbufferingthreshold_sceneOcclusion_abs", zbufferingthreshold_sceneOcclusion_abs, config.publish_statistics, model_header);

                if(!found_model_for_posehypotheses){
                    NODELET_ERROR_STREAM("There is no model named '" << model_header << "'. Can't verify pose hypotheses. Doing Nothing!");
                }
                else{

                    NODELET_INFO_STREAM("Calculating mu values for " << poses.size() << " pose hypotheses for Model of " << model->header.frame_id << "...");
                    bTimer.start();
                    int totalFilteredModelPoints = 0;

                    //always verifies single pose hypothesis
                    //#pragma omp parallel for ordered schedule(dynamic)
                    for(int poseCount = 0; poseCount < poses.size(); poseCount++){
                        PoseHypothesis::Ptr poseHypothesis = poses[poseCount];

                        // transform the model to the hypothetic pose
                        internalCloudType::Ptr rotated_model (new internalCloudType());
                        pcl::transformPointCloudWithNormals(*model, *rotated_model, poseHypothesis->getHypotheticalPose().asTransformation());

                        // calculation of occlusions
                        // self-occlusion // MFZ: i.e filter all model points, that are occluded by other model points
                                          //      of the same model. erase those occluded model points from the model point cloud
                        internalCloudType::Ptr flipped_rotated_model (new internalCloudType(*rotated_model));
                        internalCloudType::Ptr prefiltered_rotated_model (new internalCloudType(*rotated_model));

                        NODELET_DEBUG_STREAM("*size of flipped_rotated_model: " << flipped_rotated_model->points.size());
						NODELET_DEBUG_STREAM("*size of prefiltered_rotated_model: " << prefiltered_rotated_model->points.size());

                        if(config.filter_selfocclusion)
                        {

                            pcl::PointCloud<pcl::PointXYZ>::Ptr flipped_rotated_xyz_model (new pcl::PointCloud<pcl::PointXYZ>);

                            if(cameraPointsTo == SensorDirection::NEGATIVE_Z){
                                reflectPointCloud(*rotated_model, *flipped_rotated_model);
                            }

                            pcl::copyPointCloud(*flipped_rotated_model, *flipped_rotated_xyz_model);

                            pcl::occlusion_reasoning::ZBuffering<pcl::PointXYZ, pcl::PointXYZ> zbuffer_model(config.zbufferingresolution/2, config.zbufferingresolution/2, 1.f);
                            pcl::PointCloud<pcl::PointXYZ>::ConstPtr const_rot_model (new pcl::PointCloud<pcl::PointXYZ>(*flipped_rotated_xyz_model));
                            std::vector<int> idxes_to_keep_selfOcclusion;

                            zbuffer_model.computeDepthMap(const_rot_model, true);
                            zbuffer_model.filter(const_rot_model, idxes_to_keep_selfOcclusion, zbufferingthreshold_selfOcclusion_abs);
                            pcl::copyPointCloud(*rotated_model, idxes_to_keep_selfOcclusion, *prefiltered_rotated_model); // prefiltered_rotated_model should now
                            																							  // consist only of those model points
                            																							  // that are not occluded by other model
                            																							  // points in the current pose
                        }

                        internalCloudType::Ptr flipped_prefiltered_rotated_model (new internalCloudType); // empty point cloud
                        internalCloudType::Ptr filtered_rotated_model (new internalCloudType);            // empty point cloud

                        pcl::PointCloud<pcl::PointXYZ>::Ptr flipped_prefiltered_rotated_xyz_model (new pcl::PointCloud<pcl::PointXYZ>); // empty point cloud

                        if(cameraPointsTo == SensorDirection::NEGATIVE_Z){
                            reflectPointCloud(*prefiltered_rotated_model, *flipped_prefiltered_rotated_model);
                        }
                        // MFZ: else-statement added, otherwise there occur cases in which the flipped_prefiltered_rotated_model stays empty!
                        //      in those cases the whole verifying logic processes empty model clouds and thus will not work correctly.
                        else{
                        	pcl::copyPointCloud(*prefiltered_rotated_model, *flipped_prefiltered_rotated_model);
                        }

                        pcl::copyPointCloud(*flipped_prefiltered_rotated_model, *flipped_prefiltered_rotated_xyz_model); // empty point cloud, if sensor-direction is not NEGATIVE_Z

                        // scene-occlusions // MFZ: i.e filter all model points, that are occluded by scene points.
                        					//      erase those occluded model points from the model point cloud
                        pcl::PointCloud<pcl::PointXYZ>::ConstPtr const_prefiltered_model (new pcl::PointCloud<pcl::PointXYZ>(*flipped_prefiltered_rotated_xyz_model)); // empty point cloud, if sensor-direction is not NEGATIVE_Z
                        std::vector<int> idxes_to_keep_sceneOcclusion;

                        NODELET_DEBUG_STREAM("*size of flipped_prefiltered_rotated_xyz_model: " << flipped_prefiltered_rotated_xyz_model->points.size());
                        NODELET_DEBUG_STREAM("*size of const_prefiltered_model: " << const_prefiltered_model->points.size());
                        NODELET_DEBUG_STREAM("*zbufferingthreshold_sceneOcclusion_abs: " << zbufferingthreshold_sceneOcclusion_abs);

                        zbuffer_scene.filter(const_prefiltered_model, idxes_to_keep_sceneOcclusion, zbufferingthreshold_sceneOcclusion_abs);
                        pcl::copyPointCloud(*prefiltered_rotated_model, idxes_to_keep_sceneOcclusion, *filtered_rotated_model);

                        NODELET_DEBUG_STREAM("*size of vector indxes_to_keep_sceneOcclusion: " << idxes_to_keep_sceneOcclusion.size());

                        totalFilteredModelPoints += filtered_rotated_model->points.size(); // MFZ: TODO: these are not the points that were filtered, but the ones that survived, because they were relevant

                        NODELET_DEBUG_STREAM("rotated_model Size: " << rotated_model->points.size() << " Points");
                        NODELET_DEBUG_STREAM("flipped_rotated_model Size: " << flipped_rotated_model->points.size() << " Points");
                        NODELET_DEBUG_STREAM("prefiltered_rotated_model Size: " << prefiltered_rotated_model->points.size() << " Points");
                        NODELET_DEBUG_STREAM("filtered_rotated_model Size: " << filtered_rotated_model->points.size() << " Points"); // hier liegt das erste mal 0 vor

                        // calculate m, mv and mp
                        float m = rotated_model->points.size();
                        float m_v = 0;
                        float m_p = 0;

                        std::vector<float> pointRadiusSqaredDistance;
                        std::vector<int> pointIdxRadiusSearch;
                        std::vector<int> pointIdxesArroundCurrentPoint;
                        std::vector<int> explainedPoints;
                        std::vector<int> pointsInModelRange;

                        // MFZ: loop over the remaining model points
                        for (int i = 0; i < filtered_rotated_model->points.size(); i++)
                        {
                            internalPointType currentPoint = filtered_rotated_model->points[i];
                            int found_neighbors = kdtree.radiusSearch(currentPoint, searchradius_abs, pointIdxRadiusSearch, pointRadiusSqaredDistance);
                            NODELET_DEBUG_STREAM("found_neighbors: " << found_neighbors);

                            Eigen::Vector3f modelPointVector = currentPoint.getNormalVector3fMap();

                            for(int& idx : pointIdxRadiusSearch)
                            {
                                Eigen::Vector3f scenePointVector = scene->points[idx].getNormalVector3fMap();

                                float angle = vectorAngle(scenePointVector, modelPointVector);
                                float angle_in_pi = angle / M_PI;
                                NODELET_DEBUG_STREAM("angle between model points normal(" << modelPointVector.x() << "," << modelPointVector.y() << "," << modelPointVector.z() << ") and scene points normal(" << scenePointVector.x() << "," << scenePointVector.y() << "," << scenePointVector.z() << "): " << angle << " that is " << angle_in_pi << " times Pi.");

                                pointsInModelRange.push_back(idx);

                                if(!config.use_verification_of_normals || angle_in_pi <= config.angleDiffThresh_in_pi){
                                    explainedPoints.push_back(idx);
                                    pointIdxesArroundCurrentPoint.push_back(idx);
                                }

                            }

                            if(pointIdxesArroundCurrentPoint.size() > 0){
                                m_v++;
                                NODELET_DEBUG_STREAM("counts to m_v");
                            }
                            else
                            {
                                NODELET_DEBUG_STREAM("counts to m_p");
                                m_p++;
                            }

                            std::ostringstream output;
                            output << "Punkt (" << currentPoint.x << ", " << currentPoint.y << ", " << currentPoint.z << " | " << currentPoint.normal_x << ", " << currentPoint.normal_y << ", " << currentPoint.normal_z << " ) im pointIdxRadiusSearch-Vector bei i = " << i << ": ";
                            for(int& idx : pointIdxesArroundCurrentPoint)
                            {
                                output << "(" << scene->points[idx].x << ", " << scene->points[idx].y << ", " << scene->points[idx].z << " | " << scene->points[idx].normal_x << ", " << scene->points[idx].normal_y << ", " << scene->points[idx].normal_z << " ); ";
                            }

                            NODELET_DEBUG_STREAM(output.str());

                            pointIdxesArroundCurrentPoint.clear();
                        } // end loop over remaining model points


                        float mu_v = m_v/m;
                        float mu_p = m_p/m;

                        mu_v_vector[poseCount] = mu_v;
                        mu_p_vector[poseCount] = mu_p;

                        NODELET_DEBUG_STREAM("mu_v: " << mu_v << "; mu_p: " << mu_p);

                        bool poseGotVerified = false;
                        if(mu_v >= config.supportthreshold && mu_p <= config.penaltythreshold){
                            std::sort (explainedPoints.begin(), explainedPoints.end());
                            explainedPoints.erase(std::unique(explainedPoints.begin(), explainedPoints.end()), explainedPoints.end());
                            std::sort (pointsInModelRange.begin(), pointsInModelRange.end());
                            pointsInModelRange.erase(std::unique(pointsInModelRange.begin(), pointsInModelRange.end()), pointsInModelRange.end());
                            NODELET_DEBUG_STREAM("mps per sps: " << m_v/explainedPoints.size());
                            NODELET_DEBUG_STREAM("mps per sps: " << m_v/(explainedPoints.size()*m));
                            poseHypothesis->setUsedSceneIndices(pointsInModelRange);
                            poseHypothesis->setWeight(explainedPoints.size());
                            poseHypothesis->setMpsPerSps(m_v/(explainedPoints.size()*m));

                            muInRangePoseHypothesesIdxes.push_back(poseCount);
                            poseGotVerified = true;
                        }

                        NODELET_DEBUG_STREAM(poseCount << " verifiedPoseHypothesesIdxes: " << muInRangePoseHypothesesIdxes.size());


                        if(poseGotVerified && !config.use_conflict_analysis && config.show_results){
                            // add information for each model
//                            visualizationCloudPtrs.push_back(rotated_model);
//                            visualizationNames.push_back(model_header);
//                            visualizationPoses.push_back(Pose());
                            if(config.show_filtered_models_in_visualization)
                            {
                                if(config.filter_selfocclusion)
                                {
                                    visualizationCloudPtrs.push_back(prefiltered_rotated_model);
                                    visualizationNames.push_back(model_header + " (prefiltered)");
                                    visualizationPoses.push_back(Pose());
                                }
                                visualizationCloudPtrs.push_back(filtered_rotated_model);
                                visualizationNames.push_back(model_header + " (filtered)");
                                visualizationPoses.push_back(Pose());
                            }
                            NODELET_DEBUG_STREAM("Added Model " << rotated_model->header.frame_id << " to Visualization.");
                        }
                    }
                    int averageModelPoints = totalFilteredModelPoints / poses.size();
                    float msElapsed = bTimer.elapsed().wall / 1e6;
                    NODELET_DEBUG_STREAM("The mu values were calculated in " << msElapsed << "ms. " << muInRangePoseHypothesesIdxes.size() << " of them are within the thresholds.");
                    publishStatistics("mu-values_calculated", poses.size(), config.publish_statistics, model->header.frame_id);
                    publishStatistics("averageModelPoints", averageModelPoints, config.publish_statistics, model->header.frame_id);
                    publishStatistics("mu-value_time_ms", msElapsed, config.publish_statistics, model->header.frame_id);

                    for(int i = 0; i < poses.size(); i++){
                        PoseHypothesis::Ptr posePtr = poses[i];  // TODO: MFZ 17.05.2018 why generate the pointer here? --> to store this shared pointer in the vector
                        for(int& muInRangeIdx : muInRangePoseHypothesesIdxes){
                            if(muInRangeIdx == i){
                                NODELET_DEBUG_STREAM("Pose with Index " << muInRangeIdx << " is in the range.");
                                posesInMuRange.push_back(posePtr);
                                GraphTraits::vertex_descriptor v = boost::add_vertex(posePtr, conflictGraph);
                                vertexHypothesisMap[v] = posePtr; //!< add vertex-pose pair to map
                            }
                        }
                    }

                    if(config.publish_mu_values){
                        NODELET_DEBUG_STREAM("Going to publish mu-vectors. mu_v has " << mu_v_vector.size() << " values. mu_p has " << mu_p_vector.size() << " values.");
                        publishStatisticsArray("mu_v", mu_v_vector, config.publish_statistics, model_header);
                        publishStatisticsArray("mu_p", mu_p_vector, config.publish_statistics, model_header);
                    }

                    if(!config.use_conflict_analysis)
                    {
                        publishVerifiedPoseHypothesisArray(pubVerifiedPose, posesInMuRange, std::string("verified"));
                        publishStatistics("found_n_positives", posesInMuRange.size(), config.publish_statistics, model->header.frame_id);
                    }

                }

                NODELET_INFO_STREAM("Calculation of " << poses.size() << " mu values for pose hypotheses done.");
            }
        }

        /**
         * @brief adds Edges to Conflict Graph for all Hypotheses
         */
        void addEdgesToConflictGraph(){
            GraphTraits::vertex_iterator i, iend;
            for(boost::tie(i, iend) = boost::vertices(conflictGraph); i != iend; ++i)
            {
                GraphTraits::vertex_iterator j, jend;
                PoseHypothesis::Ptr hypothesisA = vertexHypothesisMap[*i];
                boost::tie(j, jend) = boost::vertices(conflictGraph);
                for(j = i; j != jend; ++j)
                {
                    if(i != j)
                    {
                        PoseHypothesis::Ptr hypothesisB = vertexHypothesisMap[*j];

                        std::vector<int> usedForHypothesisA = hypothesisA->getUsedSceneIndices();
                        std::vector<int> usedForHypothesisB = hypothesisB->getUsedSceneIndices();
                        std::vector<int> usedForBoth;

                        /**
                         * The scenepoints were sorted before they were given to the hypothesis
                         * std::sort(usedForHypothesisA.begin(), usedForHypothesisA.end());
                         * std::sort(usedForHypothesisB.begin(), usedForHypothesisB.end());
                         */

                        std::set_intersection(usedForHypothesisA.begin(), usedForHypothesisA.end(),
                                              usedForHypothesisB.begin(), usedForHypothesisB.end(),
                                              std::back_inserter(usedForBoth));

                        int conflicts = usedForBoth.size();

                        float rateOfConflictingModelPointsA = hypothesisA->getMpsPerSps()*conflicts;
                        float rateOfConflictingModelPointsB = hypothesisB->getMpsPerSps()*conflicts;
                        if(rateOfConflictingModelPointsA > config.conflictthreshold || rateOfConflictingModelPointsB > config.conflictthreshold)
                        {
                            //boost::remove_edge(*i, *j, conflictGraph);
                            NODELET_DEBUG_STREAM("conflict between " << hypothesisA->toString() << " and " << hypothesisB->toString() << " [" << rateOfConflictingModelPointsA << "; " << rateOfConflictingModelPointsB << "]");
                            boost::add_edge(*i, *j, conflictGraph);
                        }
                    }
                }
            }
        }

        /**
         * @brief remove conflicting hypotheses
         */
        void removeConflictingHypotheses(){
            // while the conflict graph has edges (conflicting hypotheses)
            int maxCount = boost::num_vertices(conflictGraph); // zur Sicherheit gibt es eine Abbruchbedingung
            while(boost::num_edges(conflictGraph) > 0)
            {
                GraphTraits::vertex_iterator vi, vend;
                GraphTraits::vertex_descriptor bestv;
                GraphTraits::adjacency_iterator ai, aend, next;
                bool isFirst = true;

                boost::tie(vi, vend) = boost::vertices(conflictGraph); 	// tie() creates a tuple from two or more values
                														// "vertices() returns two iterators of type vertex_iterator,
                														// which refer to the beginning and ending points.
                														// The iterators are returned in a std::pair."
                														// --> so why the hell use tie() instead of std::pair()?


                // look for vertex representing the hypothesis which describes the most scene points and has edge to other vertex
                // loop over vertices of the conflict graph
                for(; vi != vend; ++vi)
                {
                    NODELET_DEBUG_STREAM("Currently watching " << vertexHypothesisMap[*vi]->toString());
                    // find all adjacent vertices to the current vertex vi
                    boost::tie(ai, aend) = boost::adjacent_vertices(*vi, conflictGraph);
                    if(ai == aend){
                        // no adjacent vertices found
                        continue;
                    }

                    if(isFirst){
                        bestv = *vi;
                        isFirst = false;
                        continue;
                    }
                    // is this vertex better then the best vertex set before? better vertex has pose with more explained points
                    if(vertexHypothesisMap[*vi]->getWeight() > vertexHypothesisMap[bestv]->getWeight()){
                        bestv = *vi;
                    }
                }

                NODELET_DEBUG_STREAM("Best one is: " << vertexHypothesisMap[bestv]->toString());

                // remove all vertices adjacent to the best vertex
                boost::tie(ai, aend) = boost::adjacent_vertices(bestv, conflictGraph);
                while(ai != aend)
                {
                    // remove all adjacent vertices of bestv and their edges
                    boost::clear_vertex(*ai, conflictGraph);
                    boost::remove_vertex(*ai, conflictGraph);
                    NODELET_DEBUG_STREAM(boost::num_vertices(conflictGraph) << " vertices and " << boost::num_edges(conflictGraph) << " edges after removing vertex.");

                    // the indices of the vertices changed; need to refresh them
                    boost::tie(ai, aend) = boost::adjacent_vertices(bestv, conflictGraph);
                }

                NODELET_DEBUG_STREAM("Removed adjacent vertices. Remaining Vertices: " << boost::num_vertices(conflictGraph) << "; Remaining Edges: " << boost::num_edges(conflictGraph) << "; countdown: " << maxCount);
                maxCount--;
                if(maxCount <= 0){ // TODO: maxCount contains the number of vertices, not the number of edges...
                    NODELET_ERROR_STREAM("You needed more while loop runs than there are edges in the graph."); //... this error message makes no sense
                    break;
                }
            }
        }

        std::vector<PoseHypothesis::Ptr> getRemainingHypothesesFromConflictGraph(){
            std::vector<PoseHypothesis::Ptr> ret;
            GraphTraits::vertex_iterator i, iend;
            for (std::tie(i, iend) = boost::vertices(conflictGraph); i != iend; ++i)
            {
                PoseHypothesis::Ptr vph = vertexHypothesisMap[*i];

                NODELET_DEBUG_STREAM(vph->toString());
                ret.push_back(vph);
            }
            return ret;
        }

        /**
         * @brief checks if pose hypotheses were received for all models
         */
        bool gotPoseHypothesesForAllModels(){
            bool ret = true;
            for(int i = 0; i < gotPoseHypothesesForModel.size(); i++)
            {
                ret &= gotPoseHypothesesForModel[i];
            }
            return ret;
        }

        /**
         * @brief function abstracts all calculations that have to be done if pose hypotheses for all models have been received
         */
        void conflictAnalysis(){
            // just do this if posehypotheses for all models have been received
            if(config.use_conflict_analysis)
            {
                NODELET_INFO_STREAM("We are at the end of verification. In the Conflict Graph are " << boost::num_vertices(conflictGraph) << " Vertices. Starting adding Edges...");

                bTimer.start();
                addEdgesToConflictGraph();
                float msElapsed = bTimer.elapsed().wall / 1e6;
                NODELET_INFO_STREAM("Edges added in " << msElapsed << "ms. The conflict graph has " << boost::num_vertices(conflictGraph) << " Vertices and " << boost::num_edges(conflictGraph) << " Edges.");
                publishStatistics("edge_adding_time_ms", msElapsed, config.publish_statistics, scene->header.frame_id);

                bTimer.start();
                removeConflictingHypotheses();
                msElapsed = bTimer.elapsed().wall / 1e6;
                NODELET_INFO_STREAM("Removed Conflicts in " << msElapsed << "ms. The conflict graph has " << boost::num_vertices(conflictGraph) << " Vertices and " << boost::num_edges(conflictGraph) << " Edges.");
                publishStatistics("conflic_removing_time_ms", msElapsed, config.publish_statistics, scene->header.frame_id);

                std::vector<PoseHypothesis::Ptr> poseHypothesesWithoutConflicts = getRemainingHypothesesFromConflictGraph();

                for(internalCloudType::Ptr& model : models)
                {
                	int maxWeight = 0;
                	bool first_update = true;
                    std::vector<PoseHypothesis::Ptr> hypothesesOfModel;
                    for(PoseHypothesis::Ptr& pose : poseHypothesesWithoutConflicts)
                    {
                        if(model->header.frame_id == pose->getModel())
                        {
//                        	visualizationCloudPtrs.push_back(model);
//							visualizationNames.push_back(pose->getModel());
//							visualizationPoses.push_back(pose->getHypotheticalPose());
                        	// Changed MFZ 14.05.2019: Visualize only the best verified pose
                        	int weight = pose->getWeight();
                        	if (weight > maxWeight){
                        		maxWeight = weight;
                        		if (first_update){
									visualizationCloudPtrs.push_back(model);
									visualizationNames.push_back(pose->getModel());
									visualizationPoses.push_back(pose->getHypotheticalPose());
									first_update = false;
                        		}
                        		else{
                        			visualizationCloudPtrs.pop_back();
									visualizationNames.pop_back();
									visualizationPoses.pop_back();
                        			visualizationCloudPtrs.push_back(model);
									visualizationNames.push_back(pose->getModel());
									visualizationPoses.push_back(pose->getHypotheticalPose());
                        		}
                        	}
                            hypothesesOfModel.push_back(pose);
                        }
                    }
                    publishVerifiedPoseHypothesisArray(pubVerifiedPose, hypothesesOfModel, std::string("conflictfree"));
                    publishStatistics("found_n_positives", hypothesesOfModel.size(), config.publish_statistics, model->header.frame_id);
                }
            }

        }

        /**
         * @brief starts the verification of all posehypotheses via Global Hypotheses Verification by Aldoma et al.
         */
        /*
        void verifyPoseHypothesesGlobalHV(){
            NODELET_DEBUG_STREAM("Entered verifyPoseHypothesesGlobalHV Method.");
            std::vector<bool> mask_of_verified_poses;

            pcl::GlobalHypothesesVerification<pcl::PointXYZ, pcl::PointXYZ> ghv;
            NODELET_DEBUG_STREAM("ghv Variable created.");

            ghv.setResolution(config.resolution);
            ghv.setInlierThreshold(config.inlierthreshold);
            ghv.setRadiusClutter(config.radiusclutter);
            ghv.setRegularizer(config.regularizer);
            ghv.setClutterRegularizer(config.clutterregularizer);
            ghv.setDetectClutter(config.detectclutter);
            NODELET_DEBUG_STREAM("ghv parameters set.");

            internalCloudType::Ptr flipped_scene (new internalCloudType(*scene));
            if(!cameraPointsToPositiveZ){
                reflectPointCloud(*scene, *flipped_scene);
            }

            pcl::PointCloud<pcl::PointXYZ>::Ptr flipped_xyz_scene (new pcl::PointCloud<pcl::PointXYZ>);
            pcl::copyPointCloud(*flipped_scene, *flipped_xyz_scene);
            ghv.setSceneCloud(flipped_xyz_scene);
            NODELET_DEBUG_STREAM("sceneCloud set." << flipped_xyz_scene->points.size());

            std::vector<internalCloudType::ConstPtr> ghv_models;
            std::vector<pcl::PointCloud<pcl::PointXYZ>::ConstPtr> ghv_xyz_models;
            std::vector<pcl::PointCloud<pcl::Normal>::ConstPtr> ghv_normal_models;
            ghv_models.resize(posehypotheses.size());
            ghv_xyz_models.resize(posehypotheses.size());
            ghv_normal_models.resize(posehypotheses.size());
            NODELET_DEBUG_STREAM("starting aligning and flipping of all models...");
            for(int i = 0; i < posehypotheses.size(); i++){
                PoseHypothese::Ptr poseHypothese = posehypotheses[i];
                for(internalCloudType::Ptr& model : models){
                    if(model->header.frame_id == poseHypothese->getModel()){
                        internalCloudType::Ptr rotated_model (new internalCloudType(*model));
                        internalCloudType::Ptr flipped_rotated_model (new internalCloudType(*model));
                        pcl::PointCloud<pcl::PointXYZ>::Ptr flipped_rotated_xyz_model (new pcl::PointCloud<pcl::PointXYZ>);
                        pcl::PointCloud<pcl::Normal>::Ptr flipped_rotated_normal_model (new pcl::PointCloud<pcl::Normal>);

                        pcl::transformPointCloudWithNormals(*model, *rotated_model, poseHypothese->getHypotheticalPose().asTransformation());
                        reflectPointCloud(*rotated_model, *flipped_rotated_model);
                        pcl::copyPointCloud(*flipped_rotated_model, *flipped_rotated_xyz_model);
                        pcl::copyPointCloud(*flipped_rotated_model, *flipped_rotated_normal_model);

                        internalCloudType::ConstPtr const_flipped_rotated_model (new internalCloudType(*flipped_rotated_model));
                        pcl::PointCloud<pcl::PointXYZ>::ConstPtr const_flipped_rotated_xyz_model (new pcl::PointCloud<pcl::PointXYZ>(*flipped_rotated_xyz_model));
                        pcl::PointCloud<pcl::Normal>::ConstPtr const_flipped_rotated_normal_model (new pcl::PointCloud<pcl::Normal>(*flipped_rotated_normal_model));

                        ghv_models[i] = const_flipped_rotated_model;
                        ghv_xyz_models[i] = const_flipped_rotated_xyz_model;
                        ghv_normal_models[i] = const_flipped_rotated_normal_model;
                    }
                }
            }
            NODELET_DEBUG_STREAM("alingning and flipping done.");

            //ghv.addNormalsClouds(ghv_normal_models);
            //NODELET_DEBUG_STREAM("normalsclouds set.");
            ghv.addModels(ghv_xyz_models, true);
            NODELET_DEBUG_STREAM("models set." << ghv_xyz_models.size());

            NODELET_INFO_STREAM("Verifying " << ghv_models.size() << " models...");
            ghv.verify();
            NODELET_INFO_STREAM("Verification done.");
        }
        */

        /**
         * @brief analyzes the scene cloud and sets the cameraPointsToPositiveZ Parameter
         */
        void analyzeCameraOrientation(){

            bool foundNegativeZ = false;
            bool foundPositiveZ = false;

            for(int pointCount = 0; pointCount < scene->points.size(); pointCount++)
            {
                foundNegativeZ = foundNegativeZ || scene->points[pointCount].z < 0;
                foundPositiveZ = foundPositiveZ || scene->points[pointCount].z > 0;
            }

            if(!foundNegativeZ && foundPositiveZ){
                cameraPointsTo = SensorDirection::POSITIVE_Z;
                NODELET_INFO_STREAM("All points of the scene cloud " << scene->header << " have a positive Z coordinate ==> Camera points to +Z.");
            }
            else if(foundNegativeZ && !foundPositiveZ){
                cameraPointsTo = SensorDirection::NEGATIVE_Z;
                NODELET_INFO_STREAM("All points of the scene cloud " << scene->header << " have a negative Z coordinate ==> Camera points to -Z.");
            }
            else if(foundNegativeZ == foundPositiveZ){
                NODELET_ERROR_STREAM("The points of the scene cloud " << scene->header << " are on the XY-plane or the camera does not point to Z or -Z.");
            }
        }

        void reflectPointCloud (internalCloudType& input, internalCloudType& output){
            Eigen::Matrix4f flipz;
            flipz << 1, 0, 0, 0,
                     0, 1, 0, 0,
                     0, 0,-1, 0,
                     0, 0, 0, 1;

            pcl::transformPointCloudWithNormals(input, output, flipz);
        }

        void showResults(bool show_results){
            if(show_results){
                // close down old visualization thread
                visualizationThread.interrupt();
                visualizationThread.join();

                // path for png saving (or leave empty to save nothing)
                std::string screenshot_path("");
                if(!config.results_screenshot_dir.empty()){
                    screenshot_path = config.results_screenshot_dir + "/" +
                                      std::to_string(ros::Time::now().toSec()) +
                                      scene->header.frame_id + ".png";
                }

                // start new visualization thread
                std::string title = "matching results of " + ros::this_node::getName() + " @ "
                                    + std::to_string(ros::Time::now().toSec()) + " s (ROS-time)";
                visualizationThread = boost::thread(visualization::visualizeClouds<internalPointType>,
                                                    visualizationCloudPtrs,
                                                    visualizationPoses,
                                                    visualizationNames,
                                                    title,
                                                    screenshot_path);
            }
        }

        ////////////////////////////// MEMBER VARIABLES //////////////////////////////

        // ROS related
        ros::Subscriber subModel;
        ros::Subscriber subScene;
        ros::Subscriber subPoses;

        ros::ServiceServer srvClear;

        ros::Publisher pubVerifiedPose;

        // dynamic reconfigure server
        dynamic_reconfigure::Server<pf_matching_core::verifierConfig> dynReconfigServer;
        pf_matching_core::verifierConfig config; //!< latest set of dynamic reconfigure parameters

        bool got_initial_config = false;

        // settings for verifier
        VerifierSettings settings;

        // worker classes and storage
        //PoseVerifier<internalPointType> verifier;

        std::vector<internalCloudType::Ptr> models;
        std::vector<bool> gotPoseHypothesesForModel;
        std::vector<float> d_dists;
        std::vector<PoseHypothesis::Ptr> posehypotheses;
        internalCloudType::Ptr scene;

        Graph conflictGraph;
        std::map<GraphTraits::vertex_descriptor, PoseHypothesis::Ptr> vertexHypothesisMap;

        int cameraPointsTo;

        pcl::KdTreeFLANN<internalPointType> kdtree;
        pcl::occlusion_reasoning::ZBuffering<pcl::PointXYZ, pcl::PointXYZ> zbuffer_scene;

		// for statistics
        boost::timer::cpu_timer bTimer; //!< for measuring runtimes

        // for visualization
        boost::thread visualizationThread; //!< is in a "not-a-thread"-state after construction
        // storage for information to visualize
        std::vector<internalCloudType::ConstPtr> visualizationCloudPtrs;
        std::vector<std::string> visualizationNames;
        std::vector<Pose> visualizationPoses;



};
} // end namespace
} // end namespace

// make nodelet known
PLUGINLIB_EXPORT_CLASS(pf_matching::ROS::VerifierNodelet, nodelet::Nodelet)
