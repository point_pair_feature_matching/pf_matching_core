/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke, Markus Franz Ziegler
 */

# pragma once

#include "../include/pairFeatures.hpp"

// PCL
#include <pcl/console/print.h>
#include <pcl/octree/octree_search.h> 		// for doing ray-tracing within an octree
#include <pcl/octree/octree_pointcloud.h>  	// for generating an octree of a pointcloud

// Eigen lib for cross product
#include <Eigen/Dense>
#include <Eigen/Core>

// STL
#include <math.h> // for signbit
#include <string>
#include <iostream>
#include <fstream> // writing stuff to files (debugging)
#include <utility> // for std::pair
#include <functional>

// boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

// ROS
#include <ros/console.h>

// project
#include <modelHashTable.hpp>
#include <visibilityContextVisualization.hpp>

// debugging flags:
#define print_overflown_quantized_feature
//#define print_continuous_feature
//#define write_invalid_continuous_VisCon_feature_to_file
//#define write_continuous_VisCon_feature_to_file

// visualizing flag:
#define visualize_VisCon_feature


namespace pf_matching{

void writeToFile_wo_cout(std::string lineContent, std::string filename){

	std::ofstream outputFile;
	std::string directory = "/home/markus/Documents/"+filename;
	outputFile.open (directory, std::ios::out | std::ios::app); // do not delete previous content of the file

	// this function has its name "..._wo_cout", because the following line is commented:
//	std::cout << "[hashTable file: writeToFile] " << filename << ": Wrote '" << lineContent << "' to file." << std::endl;

	if (outputFile.is_open()){
		while (outputFile.good()){
			//write to file
			outputFile << lineContent << "\n";
			// close file
			outputFile.close();
		}
	}
	else {
		PCL_ERROR("[writeToFile_wo_cout] Error opening file %s", directory.c_str());
	}
}

float vectorAngle(const Eigen::Vector3f& v0, const Eigen::Vector3f& v1){
    float angle = atanf(v0.cross(v1).norm() / v0.dot(v1));

    // map range [-pi/2, pi/2] to [0, pi]
    if(angle < 0){
        angle += M_PI;
    }
    
    // special case for mapping, -0 maps to pi, while +0 maps to 0
    if(angle == 0 && std::signbit(angle)){
        return M_PI;
    }
    
    return angle;
}

// TODO 09.04.2018 replaced by inline functions
template <typename PointNT> inline void
EigenVecToPCLPointT ( const Eigen::Vector3f& EigenVector,
			  	      PointNT& PointTRef){

	PointTRef.x = EigenVector(0,0);
	PointTRef.y = EigenVector(1,0);
	PointTRef.z = EigenVector(2,0);
}

template <typename PointNT> inline PointNT
EigenVecToPCLPointT ( const Eigen::Vector3f& EigenVector ){

	PointNT Point;
	Point.x = EigenVector(0,0);
	Point.y = EigenVector(1,0);
	Point.z = EigenVector(2,0);
	return Point;
}


 Eigen::Affine3f aligningTransform(const Eigen::Vector3f& p_r, const Eigen::Vector3f& n_r){

    if(n_r.y() == 0.0f && n_r.z() == 0.0f){
        // normal is already parallel to the x-axis, can save some calculations
        return Eigen::Affine3f(Eigen::Translation3f(-1.0 * p_r));
    }
    else{
        // normal not parallel to x-axis, need to make it so!
        
        // unit axes definitions for better code readability
        const Eigen::Vector3f e_x = Eigen::Vector3f::UnitX();
    
        // get rotation that aligns n_r with the x-axis
        Eigen::Quaternionf q;
        q.setFromTwoVectors(n_r, e_x);
        
        // assemble full transformation
        return q * Eigen::Translation3f(-1.0 * p_r);
    }
}


ModelPFinfo::ModelPFinfo(float& alpha_, unsigned int& refPointIndex_, float weight_):alpha(alpha_),
																					 refPointIndex(refPointIndex_),
																					 weight(weight_){
	// doing nothing. already initialized values via initializer list
}


std::ostream& operator<<(std::ostream& stream, const pf_matching::ModelPFinfo info){
    
    stream << "refrence point " << info.refPointIndex 
           << ", weight " << info.weight 
           << ", alpha = " << info.alpha;

    return stream;
}

template <typename PointNT>
MinimalDiscretePF<PointNT>::MinimalDiscretePF(modelHashTable<PointNT>* hashTablePtr){
	setParameters(hashTablePtr);
}

template <typename PointNT>
MinimalDiscretePF<PointNT>::MinimalDiscretePF(featureEntryT discretePF_[4], float alpha_){

    std::copy(discretePF_, discretePF_ + 4, discretePF);
    alpha = alpha_;
    d_dist_abs = NAN;
    d_angle = NAN;
}

template <typename PointNT>
MinimalDiscretePF<PointNT>::MinimalDiscretePF(featureVectorT intFeatureRepresentation, float alpha_){
    
    // disassemble int representation again
    for (int i = 0; i < 4; i++){
        discretePF[i] = ((featureEntryT*)& intFeatureRepresentation)[i];
    }
    
    alpha = alpha_;
    d_dist_abs = NAN;
	d_angle = NAN;
}

template <typename PointNT> bool
MinimalDiscretePF<PointNT>::setParameters(modelHashTable<PointNT>* hashTablePtr){

    // check validity of input
	// should be already done by former checks in PFmodel::setDiscretizationAndVisConParams()

    // copy values
    d_dist_abs = hashTablePtr->get_d_dist_abs();
    d_angle = hashTablePtr->get_d_angle();

    ROS_DEBUG_STREAM("[MinimalDiscretePF::setParameters] All member variables were successfully set: d_dist_abs = "<< d_dist_abs << ", d_angle = " << d_angle);
    return true;
}


template <typename PointNT> bool
MinimalDiscretePF<PointNT>::calculate(float continuousFeature[4],
                                  	  const float& alpha_){
    alpha = alpha_;
    
    #ifdef CHECK_PF_FOR_NAN
    if(isnanf(continuousFeature[0]) ||
       isnanf(continuousFeature[1]) ||
       isnanf(continuousFeature[2]) ||
       isnanf(continuousFeature[3])){
        PCL_WARN("[MinimalHashedPF] input produced NaN in continuous feature vector.\n");
        }
    #endif
    
    return quantize(continuousFeature);
}


template <typename PointNT> bool
MinimalDiscretePF<PointNT>::calculate(const Eigen::Vector3f& p_r, const Eigen::Vector3f& n_r,
                                  	  const Eigen::Vector3f& p_t, const Eigen::Vector3f& n_t,
									  const Eigen::Affine3f& p_r_aligningTransform){

    // align feature with origin (actually only done to p_t) and calculate angle to turn
    // transformed p_t around the x-axis so it lies in the xy-plane
    Eigen::Vector3f p_t_transformed = p_r_aligningTransform * p_t;
    alpha = -atan2f(p_t_transformed.z(), p_t_transformed.y()); // in range of [-pi , +pi]
                   
    // temporary storage for continuous feature
    float f[4] = {0,0,0,0};

    // calculate continuous point feature
    Eigen::Vector3f d = p_t - p_r;  // connecting vector
    f[0] = d.norm();
    
    // on angle calculation method:
    // vectorAngle() leads to an runtime increase of about 20% compared to vectorAngle(),
    // resulting in roughly 5% runtime increase during complete matching. On the other hand,
    // vectorAngle() is nummerically more stable and can handle unnormalized normal vectors.
    // A bit of performance is therefore sacrificed for stability and broader input data.
    f[1] = vectorAngle(n_r, d);
    f[2] = vectorAngle(n_t, d);
    f[3] = vectorAngle(n_r, n_t);

    #ifdef CHECK_PF_FOR_NAN
    if(isnanf(f[0]) || isnanf(f[1]) || isnanf(f[2]) || isnanf(f[3])){
        PCL_WARN("[MinimalHashedPF] input produced NaN in continous feature vector.\n");
        //std::cout << f[0] << ", " << f[1] << ", " << f[2] << ", " << f[3] << "\n";
        //std::cout << "n_r :\n" << n_r << "\nn_t:\n" << n_t <<"\n";
        //std::cout << "dot product: " << std::setprecision(9) << n_r.dot(n_t) << "\n";
    }
    #endif

    // only save quantized value of feature
    return quantize(f);
}

template <typename PointNT> bool
MinimalDiscretePF<PointNT>::calculateNeighbourFeature(int i, int j, int k, int l){

	// temporary storage:
	float neighbourFeature[4];

	switch (i) // distance d
	{
		case 0:
			neighbourFeature[0] = discretePF[0] -1;
			break;
			// If distance gets <=0 the feature becomes invalid. It gets checked in invalidDistance().
		case 1:
			neighbourFeature[0] = discretePF[0];
			break;
		case 2:
			neighbourFeature[0] = discretePF[0] +1;
			break;
			// If distance gets too big (overflow, feature becomes invalid). It gets checked in invalidDistance().
	}
	switch (j) // angle in [0, pi/d_angle]
	{
		case 0:
			neighbourFeature[1] = discretePF[1] -1;
			break;
			// If angle gets <0 the feature becomes invalid. This system here handles only angles in [0, pi/d_angle]. It gets checked in invalidAngle().
		case 1:
			neighbourFeature[1] = discretePF[1];
			break;
		case 2:
			neighbourFeature[1] = discretePF[1] +1;
			break;
			// If angle gets >pi/d_angle the feature becomes invalid. This system here handles only angles in [0, pi/d_angle]. It gets checked in invalidAngle().
	}
	switch (k) // angle in [0, pi/d_angle]
	{
		case 0:
			neighbourFeature[2] = discretePF[2] -1;
			break;
		case 1:
			neighbourFeature[2] = discretePF[2];
			break;
		case 2:
			neighbourFeature[2] = discretePF[2] +1;
			break;
	}
	switch (l) // angle in [0, pi/d_angle]
	{
		case 0:
			neighbourFeature[3] = discretePF[3] -1;
			break;
		case 1:
			neighbourFeature[3] = discretePF[3];
			break;
		case 2:
			neighbourFeature[3] = discretePF[3] +1;
			break;
	}

	// this mapping is erroneous. do not do it. Range is checked in invalidAngle()
//	// map neighbour angle to [0, pi/d_angle]. because it could be out of that range now
//	neighbourFeature[1] = mapNeighbourAngle(neighbourFeature[1]);
//	neighbourFeature[2] = mapNeighbourAngle(neighbourFeature[2]);
//	neighbourFeature[3] = mapNeighbourAngle(neighbourFeature[3]);

	// check validity:
	bool invalid = false;
	invalid = invalid || invalidDistance(neighbourFeature[0]);
	invalid = invalid || invalidAngle(neighbourFeature[1]);
	invalid = invalid || invalidAngle(neighbourFeature[2]);
	invalid = invalid || invalidAngle(neighbourFeature[3]);

	if(!invalid){ // if PPF is valid
		discretePFneighbour[0] = neighbourFeature[0];
		discretePFneighbour[1] = neighbourFeature[1];
		discretePFneighbour[2] = neighbourFeature[2];
		discretePFneighbour[3] = neighbourFeature[3];

		// return TRUE if PPF is valid
		return (!invalid);
	}
	else{
		// TODO: remove after debugging, because it costs unnecessarily time.
		discretePFneighbour[0] = neighbourFeature[0];
		discretePFneighbour[1] = neighbourFeature[1];
		discretePFneighbour[2] = neighbourFeature[2];
		discretePFneighbour[3] = neighbourFeature[3];
		// Return FALSE if overflow occurred or distance d < 0!
		return (!invalid);
	}
}

template <typename PointNT> void
MinimalDiscretePF<PointNT>::getHumanReadableFeature(float feature[4]){
    
    feature[0] = discretePF[0] * d_dist_abs;
    feature[1] = discretePF[1] * d_angle;
    feature[2] = discretePF[2] * d_angle;
    feature[3] = discretePF[3] * d_angle;
}

/**
 * @brief return the discrete feature vector interpreted as a single integer
 * @return 
 */
template <typename PointNT> typename MinimalDiscretePF<PointNT>::featureHashKeyT
MinimalDiscretePF<PointNT>::getDiscreteFeatureAsInt(){

    featureHashKeyT result;
    memcpy(&result, discretePF, sizeof(result));
    return result;
}

template <typename PointNT> typename MinimalDiscretePF<PointNT>::featureHashKeyT
MinimalDiscretePF<PointNT>::getDiscreteNeighbourFeatureAsInt(){

	featureVectorT result;
	memcpy(&result, discretePFneighbour, sizeof(result));
	return result;
}

template <typename PointNT> bool
MinimalDiscretePF<PointNT>::quantize(float continousFeature[4]){

    // express feature values as int - multiples of quantization steps and write them
    // to storage
    // use + 0.5 in parenthesis to have school rounding
    
    // angles won't overflow if d_angle is not ridiculously small
    discretePF[1] = static_cast<featureEntryT>(continousFeature[1] / d_angle + 0.5);
    discretePF[2] = static_cast<featureEntryT>(continousFeature[2] / d_angle + 0.5);
    discretePF[3] = static_cast<featureEntryT>(continousFeature[3] / d_angle + 0.5);
    
    // prevent d value from overflowing for very large scenes
    float d_continous = continousFeature[0] / d_dist_abs + 0.5;
    if(d_continous > std::numeric_limits<featureEntryT>::max()){
        discretePF[0] = std::numeric_limits<featureEntryT>::max();
        return false;
    }
    else{
        discretePF[0] = static_cast<featureEntryT>(d_continous);
        return true;
    }
}


// this mehtod is not used anymore, because it is wrong.
template <typename PointNT> float
MinimalDiscretePF<PointNT>::mapNeighbourAngle(const float& angle){
	if (angle < 0){ // theoretically only -1 should be possible here
		return angle + (M_PI/d_angle)+1;
	}
	else if (angle > M_PI/d_angle){ // theoretically only PI/d_angle + 1 should occur here.
		return angle - (M_PI/d_angle)-1;
	}
	else{
		return angle;
	}
}


template< typename PointNT> bool
MinimalDiscretePF<PointNT>::invalidDistance(const float& distance){
	if (distance > std::numeric_limits<featureEntryT>::max()){ // if d in PPF has ::max()-value, then the "+1 neighbour" exceeds the limits and is invalid.
															   // reducing the neighbour to ::max() would add an already existing PPF to the Hash-Table
															   // and therefore distort the results during matching (--> creating the same vote twice)
		return true;
	}
	else if ( distance <= 0 ) { // a distance <= 0 would imply a PPF with the same point for p_r and p_t
		return true;
	}
	else {
		return false; // distance is valid
	}
}

// ========================================================================================================
// ======================= extended MinimalDiscretePF class for visibility context ========================
// ========================================================================================================

template <typename PointNT>
VisibilityContextDiscretePF<PointNT>::VisibilityContextDiscretePF(modelHashTable<PointNT>* hashTablePtr ):
MinimalDiscretePF<PointNT>(hashTablePtr) {
	setParameters(hashTablePtr);
}


template <typename PointNT>
VisibilityContextDiscretePF<PointNT>::VisibilityContextDiscretePF(featureEntryT discretePF_[4], featureEntryT discreteVC_[4], float alpha_){

    std::copy(discretePF_, discretePF_ + 4, discretePF);
    std::copy(discreteVC_, discreteVC_ + 4, discreteVC);

    this->alpha = alpha_;
	this->d_dist_abs = NAN;
	this->d_angle = NAN;

	// Visibility Context: feature construction parameters
	modelDiameter = NAN;
	d_VisCon_abs = NAN;
	voxelSize_intersectDetect = NAN;
	ignoreFactor_intersectClassific = NAN;
	gapSizeFactor_allCases_intersectClassific = NAN;
	gapSizeFactor_surfaceCase_intersectClassific = NAN;
	alongSurfaceThreshold_intersectClassific = NAN;
	othorgonalToSurfaceThreshold_intersectClassific = NAN;
	advancedIntersectionClassification = false;
	visualizeVisibilityContextFeature = false;
	octreeSearchPtr = nullptr;
	cloudPtr = nullptr;
}


template <typename PointNT>
VisibilityContextDiscretePF<PointNT>::VisibilityContextDiscretePF(featureHashKeyT intFeatureRepresentation, float alpha_){

    // disassemble int representation again
    for (int i = 0; i < 4; i++){
        discretePF[i] = ((featureEntryT*)& intFeatureRepresentation)[i];
        discreteVC[i] = ((featureEntryT*)& intFeatureRepresentation)[i+4];
    }

    this->alpha = alpha_;
  	this->d_dist_abs = NAN;
  	this->d_angle = NAN;

	// Visibility Context: feature construction parameters
	modelDiameter = NAN;
	d_VisCon_abs = NAN;
	voxelSize_intersectDetect = NAN;
	ignoreFactor_intersectClassific = NAN;
	gapSizeFactor_allCases_intersectClassific = NAN;
	gapSizeFactor_surfaceCase_intersectClassific = NAN;
	alongSurfaceThreshold_intersectClassific = NAN;
	othorgonalToSurfaceThreshold_intersectClassific = NAN;
	advancedIntersectionClassification = false;
	visualizeVisibilityContextFeature = false;
	octreeSearchPtr = nullptr;
	cloudPtr = nullptr;
}


template <typename PointNT> bool
VisibilityContextDiscretePF<PointNT>::setParameters(modelHashTable<PointNT>* hashTablePtr){

    // check validity of input
	// should be already done by former checks in PFmodel::setDiscretizationAndVisConParams() and modelHashTable::setParameters()

	// downcasting to set the parameters hash-table-type specific
	if (hashTablePtr->htType == 2){
		STLvisibilityContextModelHashTable<PointNT>* hashTablePtr_casted = dynamic_cast< STLvisibilityContextModelHashTable<PointNT>* >(hashTablePtr);
		if (hashTablePtr_casted != nullptr){
			// visibility context parameters: copy values
			modelDiameter = hashTablePtr_casted->modelDiameter;
			d_VisCon_abs = hashTablePtr_casted->d_VisCon_abs;
			voxelSize_intersectDetect = hashTablePtr_casted->voxelSize_intersectDetect;
			ignoreFactor_intersectClassific = hashTablePtr_casted->ignoreFactor_intersectClassific;
			gapSizeFactor_allCases_intersectClassific = hashTablePtr_casted->gapSizeFactor_allCases_intersectClassific;
			gapSizeFactor_surfaceCase_intersectClassific = hashTablePtr_casted->gapSizeFactor_surfaceCase_intersectClassific;

			// transform thresholds from [deg] into [rad]:
			alongSurfaceThreshold_intersectClassific = hashTablePtr_casted->alongSurfaceThreshold_intersectClassific / 180 * M_PI; //!< transform threshold from [deg] into [rad]
			othorgonalToSurfaceThreshold_intersectClassific = hashTablePtr_casted->othorgonalToSurfaceThreshold_intersectClassific / 180 * M_PI; //!< transform threshold from [deg] into [rad]
			advancedIntersectionClassification = hashTablePtr_casted->advancedIntersectionClassification;
			visualizeVisibilityContextFeature = hashTablePtr_casted->visualizeVisibilityContextFeature;
			octreeSearchPtr = hashTablePtr_casted->octreeSearchPtr;
			cloudPtr = (octreeSearchPtr->getInputCloud());

			ROS_DEBUG_STREAM("[VisibilityContextDiscretePF::setParameters] All member variables were successfully set.");
			return true;
		}
		else
			return false;
	}
	if (hashTablePtr->htType == 4){
		CMPHvisibilityContextModelHashTable<PointNT>* hashTablePtr_casted = dynamic_cast< CMPHvisibilityContextModelHashTable<PointNT>* >(hashTablePtr);
		if(hashTablePtr_casted != nullptr){
			// visibility context parameters: copy values
			modelDiameter = hashTablePtr_casted->modelDiameter;
			d_VisCon_abs = hashTablePtr_casted->d_VisCon_abs;
			voxelSize_intersectDetect = hashTablePtr_casted->voxelSize_intersectDetect;
			ignoreFactor_intersectClassific = hashTablePtr_casted->ignoreFactor_intersectClassific;
			gapSizeFactor_allCases_intersectClassific = hashTablePtr_casted->gapSizeFactor_allCases_intersectClassific;
			gapSizeFactor_surfaceCase_intersectClassific = hashTablePtr_casted->gapSizeFactor_surfaceCase_intersectClassific;

			// transform thresholds from [deg] into [rad]:
			alongSurfaceThreshold_intersectClassific = hashTablePtr_casted->alongSurfaceThreshold_intersectClassific / 180 * M_PI; //!< transform threshold from [deg] into [rad]
			othorgonalToSurfaceThreshold_intersectClassific = hashTablePtr_casted->othorgonalToSurfaceThreshold_intersectClassific / 180 * M_PI; //!< transform threshold from [deg] into [rad]
			advancedIntersectionClassification = hashTablePtr_casted->advancedIntersectionClassification;
			visualizeVisibilityContextFeature = hashTablePtr_casted->visualizeVisibilityContextFeature;
			octreeSearchPtr = hashTablePtr_casted->octreeSearchPtr;
			cloudPtr = (octreeSearchPtr->getInputCloud());

			ROS_DEBUG_STREAM("[VisibilityContextDiscretePF::setParameters] All member variables were successfully set.");
			return true;
		}
		else
			return false;
	}
	else
		return false;
}


template<typename PointNT>
bool VisibilityContextDiscretePF<PointNT>::calculateForMatching(const Eigen::Vector3f& p_r, const Eigen::Vector3f& n_r,
                                  	  	    		 	 	 	const Eigen::Vector3f& p_t, const Eigen::Vector3f& n_t,
																const Eigen::Affine3f& p_r_aligningTransform,
																const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
																const std::vector<std::pair<int,double>> &countVoutZero){

	// align feature with origin (actually only done to p_t) and calculate angle to turn
    // transformed p_t around the x-axis so it lies in the xy-plane
    Eigen::Vector3f p_t_transformed = p_r_aligningTransform * p_t;
    this->alpha = -atan2f(p_t_transformed.z(), p_t_transformed.y());   // need "this->" pointer to access base class member alpha!

    // temporary storage for continuous feature
    float pf[4] = {0,0,0,0};
    float vc[4] = {0,0,0,0}; //!< MFZ 12/09/17 temporary storage for continuous visibility context feature

    // calculate continuous point feature
    // connecting vector d
    Eigen::Vector3f d = p_t - p_r;  //!< MFZ 13/0917 thick red line in visualizer // v_
    pf[0] = d.norm();

    // X. Kroischke on angle calculation method:
    // vectorAngle() leads to an runtime increase of about 20% compared to vectorAngle(),
    // resulting in roughly 5% runtime increase during complete matching. On the other hand,
    // vectorAngle() is numerically more stable and can handle unnormalized normal vectors.
    // A bit of performance is therefore sacrificed for stability and broader input data.
    pf[1] = vectorAngle(n_r, d);
    pf[2] = vectorAngle(n_t, d);
    pf[3] = vectorAngle(n_r, n_t);

    // TODO: MFZ 13.09.2017 Insert calculation of Visibility Context features.
    // calculate "Direction of V_out" vectors
    Eigen::Vector3f minus_d = -1*d;				//!< MFZ 13/0917 turquoise arrow in visualizer
    Eigen::Vector3f delta = n_r.cross(d);		//!< MFZ 13/0917 red arrow in visualizer
    Eigen::Vector3f minus_delta = -1*delta;		//!< MFZ 13/0917 blue arrow in visualizer

    // point cloud of intersected voxelcenters
    typename pcl::PointCloud<PointNT>::Ptr voxelCentersPtr (new pcl::PointCloud<PointNT>);

#ifdef visualize_VisCon_feature
    if(fancy_viewer == nullptr){visualizeVisibilityContextFeature = false;} // handle for case, that fancy viewer was not provided as an argument
#endif
    // TODO: 11.04.2018 reorganizing order of V_out_x-calculation.
    //					in (at least spacious) scenes, v_out_x tends to get invalid, because it easily gets longer than the modelDiameter.
    //					Calculating the V_out_x and checking its validity, saves a lot of computation, because in the invalid case all further
    //					calculations can be skipped.
    //					Currently the order is determined by the likelihood of V_out_x getting 0 in the model. (thus determined during training)
    //					The more likely the V_out_x is 0 in the model, the more likely the V_out_x-Ray runs through free space, not hitting the model itself.
    //					The more spacious the scene, the more likely this V_out_x-Ray hits a surface after a distance > modelDiameter, thus becoming invalid.
    //					It might be wise to implement some kind of machine-learning, that determines an even better order
    //					 - from the model
    //					 - or from the scene itself
    for (int i = 0; i < 4; i++){ // TODO
    	int order = countVoutZero[i].first;
		switch (order) {
			case 0:
				// calculate Intersection Point for V_out_0
				calculateIntersectedVoxelCenters(p_t, d, voxelCentersPtr, "intersection_of_d");
				// calculate V_out_0
				vc[0] = getDistanceForVout(p_t, d, voxelCentersPtr, fancy_viewer, "intersection_of_d");
#ifdef visualize_VisCon_feature
				// draw Intersection Points
				if(visualizeVisibilityContextFeature){ visualization::drawIntersectionPoints<PointNT>(voxelCentersPtr, fancy_viewer, "intersection_of_d", 255, 255, 0);} // yellow points
#endif
				// check validity of V_out_0 value
				if (vc[0] == -1){ // error occurred, PF invalid
#ifdef write_invalid_continuous_VisCon_feature_to_file
					writeToFile_wo_cout("(" + std::to_string(pf[0]) + "," // TODO:: remove after debugging:
									+ std::to_string(pf[1]) + ","
									+ std::to_string(pf[2]) + ","
									+ std::to_string(pf[3]) + "|"
									+ std::to_string(vc[0]) + ","
									+ std::to_string(vc[1]) + ","
									+ std::to_string(vc[2]) + ","
									+ std::to_string(vc[3]) + ")",
									"calculateForMatching.txt");
#endif
					return false;
				}
				break;

			case 1:
				// calculate Intersection Point for V_out_1
				calculateIntersectedVoxelCenters(p_t, delta, voxelCentersPtr, "intersection_of_delta");
				// calculate V_out_1
				vc[1] = getDistanceForVout(p_t, delta, voxelCentersPtr, fancy_viewer,  "intersection_of_delta");
#ifdef visualize_VisCon_feature
				// draw Intersection Points
				if(visualizeVisibilityContextFeature){ visualization::drawIntersectionPoints<PointNT>(voxelCentersPtr, fancy_viewer, "intersection_of_delta", 255, 0, 0);} // red points
#endif
				// check validity of V_out_1 value
				if (vc[1] == -1) { // error occurred, PF invalid
#ifdef write_invalid_continuous_VisCon_feature_to_file
					writeToFile_wo_cout("(" + std::to_string(pf[0]) + "," // TODO:: remove after debugging:
									+ std::to_string(pf[1]) + ","
									+ std::to_string(pf[2]) + ","
									+ std::to_string(pf[3]) + "|"
									+ std::to_string(vc[0]) + ","
									+ std::to_string(vc[1]) + ","
									+ std::to_string(vc[2]) + ","
									+ std::to_string(vc[3]) + ")",
									"calculateForMatching.txt");
#endif
					return false;
				}
				break;

			case 2:
				// calculate Intersection Point for V_out_2
				calculateIntersectedVoxelCenters(p_t, minus_d, voxelCentersPtr, "intersection_of_minus_d");
				// calculate V_out_2
				vc[2] = getDistanceForVout(p_t, minus_d, voxelCentersPtr, fancy_viewer,  "intersection_of_minus_d");
#ifdef visualize_VisCon_feature
				// draw Intersection Points
				if(visualizeVisibilityContextFeature){ visualization::drawIntersectionPoints<PointNT>(voxelCentersPtr, fancy_viewer, "intersection_of_minus_d", 0, 255, 255);} // turquoise points
#endif
				// check validity of V_out_2 value
				if (vc[2] == -1) { // error occurred, PF invalid
#ifdef write_invalid_continuous_VisCon_feature_to_file
					writeToFile_wo_cout("(" + std::to_string(pf[0]) + "," // TODO:: remove after debugging:
									+ std::to_string(pf[1]) + ","
									+ std::to_string(pf[2]) + ","
									+ std::to_string(pf[3]) + "|"
									+ std::to_string(vc[0]) + ","
									+ std::to_string(vc[1]) + ","
									+ std::to_string(vc[2]) + ","
									+ std::to_string(vc[3]) + ")",
									"calculateForMatching.txt");
#endif
					return false;
				}
				break;

			case 3:
				// calculate Intersection Point for V_out_3
				calculateIntersectedVoxelCenters(p_t, minus_delta, voxelCentersPtr, "intersection_of_minus_delta");
				// calculate V_out_3
				vc[3] = getDistanceForVout(p_t, minus_delta, voxelCentersPtr, fancy_viewer,  "intersection_of_minus_delta");
#ifdef visualize_VisCon_feature
				// draw Intersection Points
				if(visualizeVisibilityContextFeature){ visualization::drawIntersectionPoints<PointNT>(voxelCentersPtr, fancy_viewer, "intersection_of_minus_delta", 0, 0, 255);} // blue points
#endif
				// check validity of V_out_3 value
				if (vc[3] == -1) { // error occurred, PF invalid
#ifdef write_invalid_continuous_VisCon_feature_to_file
					writeToFile_wo_cout("(" + std::to_string(pf[0]) + "," // TODO:: remove after debugging:
											+ std::to_string(pf[1]) + ","
											+ std::to_string(pf[2]) + ","
											+ std::to_string(pf[3]) + "|"
											+ std::to_string(vc[0]) + ","
											+ std::to_string(vc[1]) + ","
											+ std::to_string(vc[2]) + ","
											+ std::to_string(vc[3]) + ")",
											"calculateForMatching.txt");
#endif
					return false;
				}
				break;
		}
    }

#ifdef visualize_VisCon_feature
	if(visualizeVisibilityContextFeature){
		visualization::visualizeVout<PointNT>( fancy_viewer,
											   p_t,
											   d,
											   delta,
											   minus_d,
											   minus_delta,
											   vc );
	}
#endif

    #ifdef CHECK_PF_FOR_NAN
    if(isnanf(pf[0]) || isnanf(pf[1]) || isnanf(pf[2]) || isnanf(pf[3]) || isnanf(vc[0]) || isnanf(vc[1]) || isnanf(vc[2]) || isnanf(vc[3])){
        PCL_WARN("[VisibilityContextDiscretePF::calculateForMatching] input produced NaN in continuous feature vector.\n");
        //std::cout << f[0] << ", " << f[1] << ", " << f[2] << ", " << f[3] << "\n";
        //std::cout << "n_r :\n" << n_r << "\nn_t:\n" << n_t <<"\n";
        //std::cout << "dot product: " << std::setprecision(9) << n_r.dot(n_t) << "\n";
    }
    #endif

    // TODO:: remove after debugging:
#ifdef write_continuous_VisCon_feature_to_file
	writeToFile_wo_cout("(" + std::to_string(pf[0]) + ","
							+ std::to_string(pf[1]) + ","
							+ std::to_string(pf[2]) + ","
							+ std::to_string(pf[3]) + "|"
							+ std::to_string(vc[0]) + ","
							+ std::to_string(vc[1]) + ","
							+ std::to_string(vc[2]) + ","
							+ std::to_string(vc[3]) + ")",
							"calculateForMatching.txt");
#endif

    // only save quantized value of feature
    return quantize(pf, vc); // quantize returns false if overflow in feature occurred, else true
}


template<typename PointNT>
bool VisibilityContextDiscretePF<PointNT>::calculateForTraining(const Eigen::Vector3f& p_r, const Eigen::Vector3f& n_r,
                                  	  	    		 	 	 	const Eigen::Vector3f& p_t, const Eigen::Vector3f& n_t,
																const Eigen::Affine3f& p_r_aligningTransform,
																const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer){

	// align feature with origin (actually only done to p_t) and calculate angle to turn
    // transformed p_t around the x-axis so it lies in the xy-plane
    Eigen::Vector3f p_t_transformed = p_r_aligningTransform * p_t;
    this->alpha = -atan2f(p_t_transformed.z(), p_t_transformed.y());

    // temporary storage for continuous feature
    float pf[4] = {0,0,0,0};
    float vc[4] = {0,0,0,0}; //!< MFZ 12/09/17 temporary storage for continuous visibility context feature

    // calculate continuous point feature
    // connecting vector d
    Eigen::Vector3f d = p_t - p_r;  //!< MFZ 13/0917 thick red line in visualizer
    pf[0] = d.norm();

    // X. Kroischke on angle calculation method:
    // vectorAngle() leads to an runtime increase of about 20% compared to vectorAngle(),
    // resulting in roughly 5% runtime increase during complete matching. On the other hand,
    // vectorAngle() is numerically more stable and can handle unnormalized normal vectors.
    // A bit of performance is therefore sacrificed for stability and broader input data.
    pf[1] = vectorAngle(n_r, d);
    pf[2] = vectorAngle(n_t, d);
    pf[3] = vectorAngle(n_r, n_t);

    // TODO: MFZ 13.09.2017 Insert calculation of Visibility Context features.
    // calculate "Direction of V_out" vectors
    Eigen::Vector3f minus_d = -1*d;				//!< MFZ 13/0917 turquoise arrow in visualizer
    Eigen::Vector3f delta = n_r.cross(d);		//!< MFZ 13/0917 red arrow in visualizer
    Eigen::Vector3f minus_delta = -1*delta;		//!< MFZ 13/0917 blue arrow in visualizer

    // point cloud of intersected voxelcenters
    typename pcl::PointCloud<PointNT>::Ptr voxelCentersPtr (new pcl::PointCloud<PointNT>);

    // calculate Intersection Point for V_out_0
    calculateIntersectedVoxelCenters(p_t, d, voxelCentersPtr, "intersection_of_d");
    // calculate V_out_0
    vc[0] = getDistanceForVout(p_t, d, voxelCentersPtr, fancy_viewer, "intersection_of_d");
    if(fancy_viewer == nullptr){visualizeVisibilityContextFeature = false;} // handle for case, that fancy viewer was not provided as an argument
#ifdef visualize_VisCon_feature
    // draw Intersection Points
    if(visualizeVisibilityContextFeature){ visualization::drawIntersectionPoints<PointNT>(voxelCentersPtr, fancy_viewer, "intersection_of_d", 255, 255, 0);} // yellow points
#endif
	// calculate Intersection Point for V_out_1
	calculateIntersectedVoxelCenters(p_t, delta, voxelCentersPtr, "intersection_of_delta");
	// calculate V_out_1
	vc[1] = getDistanceForVout(p_t, delta, voxelCentersPtr, fancy_viewer,  "intersection_of_delta");
#ifdef visualize_VisCon_feature
	// draw Intersection Points
	if(visualizeVisibilityContextFeature){ visualization::drawIntersectionPoints<PointNT>(voxelCentersPtr, fancy_viewer, "intersection_of_delta", 255, 0, 0);} // red points
#endif
	// calculate Intersection Point for V_out_2
	calculateIntersectedVoxelCenters(p_t, minus_d, voxelCentersPtr, "intersection_of_minus_d");
	// calculate V_out_2
	vc[2] = getDistanceForVout(p_t, minus_d, voxelCentersPtr, fancy_viewer,  "intersection_of_minus_d");
#ifdef visualize_VisCon_feature
	// draw Intersection Points
	if(visualizeVisibilityContextFeature){ visualization::drawIntersectionPoints<PointNT>(voxelCentersPtr, fancy_viewer, "intersection_of_minus_d", 0, 255, 255);} // turquoise points
#endif
	// calculate Intersection Point for V_out_3
	calculateIntersectedVoxelCenters(p_t, minus_delta, voxelCentersPtr, "intersection_of_minus_delta");
	// calculate V_out_3
	vc[3] = getDistanceForVout(p_t, minus_delta, voxelCentersPtr, fancy_viewer,  "intersection_of_minus_delta");
#ifdef visualize_VisCon_feature
	// draw Intersection Points
	if(visualizeVisibilityContextFeature){ visualization::drawIntersectionPoints<PointNT>(voxelCentersPtr, fancy_viewer, "intersection_of_minus_delta", 0, 0, 255);} // blue points

	if(visualizeVisibilityContextFeature){
		visualization::visualizeVout<PointNT>( fancy_viewer,
											   p_t,
											   d,
											   delta,
											   minus_d,
											   minus_delta,
											   vc );
	}
#endif
    #ifdef CHECK_PF_FOR_NAN
    if(isnanf(pf[0]) || isnanf(pf[1]) || isnanf(pf[2]) || isnanf(pf[3]) || isnanf(vc[0]) || isnanf(vc[1]) || isnanf(vc[2]) || isnanf(vc[3])){
        PCL_WARN("[VisibilityContextDiscretePF::calculateForTraining] input produced NaN in continuous feature vector.\n");
        //std::cout << f[0] << ", " << f[1] << ", " << f[2] << ", " << f[3] << "\n";
        //std::cout << "n_r :\n" << n_r << "\nn_t:\n" << n_t <<"\n";
        //std::cout << "dot product: " << std::setprecision(9) << n_r.dot(n_t) << "\n";
    }
    #endif

    // only save quantized value of feature
    // TODO: 23.01.2019 remove after debugging:
    if (vc[0] == -1 || vc[1] == -1 || vc[2] == -1 || vc[3] == -1){
    	ROS_ERROR_STREAM("A V_out was invalid during Model-Training. That is not supposed to happen!");
    	return false;
    }
    return quantize(pf, vc); // quantize returns false if overflow in feature occurred, else true
}


// transform the discreteEntryT arrays into a single integer:
template< typename PointNT> typename MinimalDiscretePF<PointNT>::featureHashKeyT
VisibilityContextDiscretePF<PointNT>::getDiscreteFeatureAsInt(){
	featureVectorT tempPF = 0, tempVC = 0;
	memcpy(&tempPF, discretePF, sizeof(tempPF));
	memcpy(&tempVC, discreteVC, sizeof(tempVC));
	featureVectorT tempCombinedF[2] = {tempPF, tempVC};
	typename MinimalDiscretePF<PointNT>::featureHashKeyT result = 0;
	memcpy(&result, tempCombinedF, sizeof(tempCombinedF));
	return result;
}

template <typename PointNT> typename MinimalDiscretePF<PointNT>::featureHashKeyT
VisibilityContextDiscretePF<PointNT>::getDiscreteNeighbourFeatureAsInt(){
	featureVectorT tempPF = 0, tempVC = 0;
	memcpy(&tempPF, discretePFneighbour, sizeof(tempPF));
	memcpy(&tempVC, discreteVCneighbour, sizeof(tempVC));
	featureVectorT tempCombinedF[2] = {tempPF, tempVC};
	typename MinimalDiscretePF<PointNT>::featureHashKeyT result = 0;
	memcpy(&result, tempCombinedF, sizeof(tempCombinedF));
	return result;
}


// quantize the features:
template< typename PointNT> bool
VisibilityContextDiscretePF<PointNT>::quantize( float continuousPFfeature[4],
												float continuousVCfeature[4] ){
#ifdef print_methods_name
	ROS_INFO_STREAM("[VisibilityContextDiscretePF::quantize] quantize"); // TODO: MFZ DEBUG 21/06/17 hinterher wieder entfernen
#endif
    // express feature values as int - multiples of quantization steps and write them
    // to storage
    // use + 0.5 in parenthesis to have school rounding (i.e. round up for >= x.5 and round down for < x.5)

#ifdef print_continuous_feature
	#pragma omp critical (print_1)
	{
	std::cout << "[VisibilityContextDiscretePF::quantize] continuous PF = ("<< continuousPFfeature[0] << ", "
																			<< continuousPFfeature[1] << ", "
																			<< continuousPFfeature[2] << ", "
																			<< continuousPFfeature[3] << " | "
																			<< continuousVCfeature[0] << ", "
																			<< continuousVCfeature[1] << ", "
																			<< continuousVCfeature[2] << ", "
																			<< continuousVCfeature[3]
																		    << ") with d_dist_abs = " << this->d_dist_abs
																			<< " d_angle = " << this->d_angle
																			<< " d_VisCon_abs = " << this->d_VisCon_abs << std::endl;
	}
#endif
    // angles won't overflow if d_angle is not ridiculously small
    discretePF[1] = static_cast<featureEntryT>(continuousPFfeature[1] / this->d_angle + 0.5);
    discretePF[2] = static_cast<featureEntryT>(continuousPFfeature[2] / this->d_angle + 0.5);
    discretePF[3] = static_cast<featureEntryT>(continuousPFfeature[3] / this->d_angle + 0.5);
    
    float d_continuous = continuousPFfeature[0] / (this->d_dist_abs) + 0.5;
    float Vout_0 = continuousVCfeature[0] / (this->d_VisCon_abs) + 0.5;
    float Vout_1 = continuousVCfeature[1] / (this->d_VisCon_abs) + 0.5;
    float Vout_2 = continuousVCfeature[2] / (this->d_VisCon_abs) + 0.5;
    float Vout_3 = continuousVCfeature[3] / (this->d_VisCon_abs) + 0.5;

    // prevent d and Vout values from overflowing for very large scenes
    // --> Model would then have to be huge and the quantization steps really small.
    bool overflowOccured = false;
    std::pair<featureEntryT, bool> checkResult;

    // TODO: 10.04.2018 this overflow check should be redundant during matching, if
    // 					voting balls of [Hinterstoisser et al. 2016]
    //					are used. then d should never be > modelDiameter
    //					for training this check should be enabled for safety.
    if (!useVotingBalls){
		checkResult = checkOverflow(d_continuous);
		discretePF[0] = checkResult.first;
		overflowOccured = overflowOccured || checkResult.second;
    }
    else discretePF[0] = static_cast<featureEntryT>(d_continuous);

    // TODO: 10.04.2018 this overflow check should be redundant,
    //					as getDistanceForVout() already throws an error if
    //					V_out would be > modelDiameter. Also quantization
    // 					steps are checked: if resolution of features is too
    //					fine for the available bit-resolution (e.g. 8 bit)
/*
    checkResult = checkOverflow(Vout_0);
    discreteVCfeature[0] = checkResult.first;
    overflowOccured = overflowOccured || checkResult.second;

    checkResult = checkOverflow(Vout_1);
	discreteVCfeature[1] = checkResult.first;
	overflowOccured = overflowOccured || checkResult.second;

	checkResult = checkOverflow(Vout_2);
	discreteVCfeature[2] = checkResult.first;
	overflowOccured = overflowOccured || checkResult.second;

	checkResult = checkOverflow(Vout_3);
	discreteVCfeature[3] = checkResult.first;
	overflowOccured = overflowOccured || checkResult.second;
*/  // use the following instead:
    discreteVC[0] = static_cast<featureEntryT>(Vout_0);
    discreteVC[1] = static_cast<featureEntryT>(Vout_1);
    discreteVC[2] = static_cast<featureEntryT>(Vout_2);
    discreteVC[3] = static_cast<featureEntryT>(Vout_3);


#ifdef print_overflown_quantized_feature
	// TODO: remove after debugging:
    #pragma omp critical (print_2)
	{
	if (overflowOccured){

		std::cout << "[VisibilityContextDiscretePF::quantize] overflown quantized PF = ("<< d_continuous << ", "
																				<< unsigned(discretePF[1]) << ", "
																				<< unsigned(discretePF[2]) << ", "
																				<< unsigned(discretePF[3]) << "|"
																				<< Vout_0 << ", "
																				<< Vout_1 << ", "
																				<< Vout_2 << ", "
																				<< Vout_3
																				<< ") with d_dist_abs = " << this->d_dist_abs
																				<< " d_angle = " << this->d_angle
																				<< " d_VisCon_abs = " << this->d_VisCon_abs << std::endl;
		std::cout << "[VisibilityContextDiscretePF::quantize] overflown discrete PPF = " << this ;
		}
	}
#endif

	// Negate the flag: Return FALSE if overflow occurred!
	if (overflowOccured) {return false;}
	else {return true;}

}


template< typename PointNT> bool
VisibilityContextDiscretePF<PointNT>::invalidVout(const float& distance){
	if (distance > std::numeric_limits<featureEntryT>::max()){ // if d in PPF has ::max()-value, then the "+1 neighbour" exceeds the limits and is invalid.
															   // reducing the neighbour to ::max() would add an already existing PPF to the Hash-Table
															   // and therefore distort the results during matching (--> creating the same vote twice)
		return true;
	}
	else if ( distance < 0 ) { // a V_out-distance < 0 is impossible
		return true;
	}
	else {
		return false; //  V_out-distance is valid
	}
}

template< typename PointNT> bool
VisibilityContextDiscretePF<PointNT>::invalidDistance(const float& distance){
	if (distance > std::numeric_limits<featureEntryT>::max()){ // if d in PPF has ::max()-value, then the "+1 neighbour" exceeds the limits and is invalid.
															   // reducing the neighbour to ::max() would add an already existing PPF to the Hash-Table
															   // and therefore distort the results during matching (--> creating the same vote twice)
		return true;
	}
	else if ( distance <= 0 ) { // a distance <= 0 would imply a PPF with the same point for p_r and p_t
		return true;
	}
	else {
		return false; // distance is valid
	}
}

template <typename PointNT> Eigen::Vector3f
VisibilityContextDiscretePF<PointNT>::getCentroidOfVoxelAsEigenVec3f(PointNT& searchPoint){
	// search neighbors within the voxel of the searchPoint
	std::vector<int> pointIdxVec; //!< list of points in the voxel
	Eigen::Vector3f centroid (0,0,0); //!< MFZ 26/09/17 centroid of the voxel

	ROS_DEBUG_STREAM( "[VisibilityContextDiscretePF::getCentroidOfVoxelAsEigenVec3f] centroid initialized with = " << centroid );

	// calculate the centroid as the arithmetic mean
	if (octreeSearchPtr->voxelSearch (searchPoint, pointIdxVec))
	{
		Eigen::Vector3f sumPoints (0,0,0); // initializing with 0 is crucial here!
		for (int i = 0; i < pointIdxVec.size(); i++){
			sumPoints = sumPoints + (cloudPtr->points[pointIdxVec[i]].getVector3fMap());
			ROS_DEBUG_STREAM( "[VisibilityContextDiscretePF::getCentroidOfVoxelAsEigenVec3f] sum is currently = " << sumPoints );
		}
		centroid = sumPoints/(pointIdxVec.size());
	}
	else ROS_ERROR_STREAM("[VisibilityContextDiscretePF::getCentroidOfVoxelAsEigenVec3f] Something went horribly wrong, voxel does not contain any points" );

	ROS_DEBUG_STREAM( "[VisibilityContextDiscretePF::getCentroidOfVoxelAsEigenVec3f] centroid calculated to = " << centroid );
	return centroid;
}


template <typename PointNT> Eigen::Vector3f
VisibilityContextDiscretePF<PointNT>::getNormalOfCentroidAsEigenVec3f ( PointNT& searchPoint ){
	int pointIdxNN = 0;
	float squaredDistance = 0;

	// get the nearest neighbour of the searchPoint
	octreeSearchPtr->approxNearestSearch ( searchPoint, pointIdxNN, squaredDistance);

	// get the normal of this nearest neigbour
	return (cloudPtr->points[pointIdxNN].getNormalVector3fMap());
}


template <typename PointNT> void
VisibilityContextDiscretePF<PointNT>::incrementCounter( float& angle,
														int array[3] ){

	if ( (angle <= (M_PI/2+alongSurfaceThreshold_intersectClassific) ) && ( angle >= (M_PI/2-alongSurfaceThreshold_intersectClassific) ) ){ //~90��
		// the ray is likely to lay in the surface (ray and surface-normal form a 90�� angle)
		array[2] ++;
	}
	else if ( ( angle <= (0+othorgonalToSurfaceThreshold_intersectClassific) ) || ( angle >= (M_PI-othorgonalToSurfaceThreshold_intersectClassific) ) ){ //~0��
		// the ray is likely to intersect with the surface under 90�� _|_ (ray and surface-normal form a 0�� angle =)
		array[0] ++;
	}
	else{
		// all other cases. ray intersects in an intermediate angle
		array[1] ++;
	}
}

template <typename PointNT> void
VisibilityContextDiscretePF<PointNT>::decrementCounter( float& angle,
														int array[3] ){

	if ( (angle <= (M_PI/2+alongSurfaceThreshold_intersectClassific) ) && ( angle >= (M_PI/2-alongSurfaceThreshold_intersectClassific) ) ){ //~90��
		// the ray is likely to lay in the surface (ray and surface-normal form a 90�� angle)
		array[2] --;
	}
	else if ( ( angle <= (0+othorgonalToSurfaceThreshold_intersectClassific) ) || ( angle >= (M_PI-othorgonalToSurfaceThreshold_intersectClassific) ) ){ //~0��
		// the ray is likely to intersect with the surface under 90�� _|_ (ray and surface-normal form a 0�� angle =)
		array[0] --;
	}
	else{
		// all other cases. ray intersects in an intermediate angle
		array[1] --;
	}
}

template <typename PointNT> bool
VisibilityContextDiscretePF<PointNT>::angleEquals90Deg( float angle ){

	if ( (angle <= (M_PI/2+alongSurfaceThreshold_intersectClassific) ) && ( angle >= (M_PI/2-alongSurfaceThreshold_intersectClassific) ) ){ //~90��
		// the ray is likely to lay in the surface (ray and surface-normal form a 90�� angle)
		return true;
	}
	else{
		return false;
	}
}


template <typename PointNT> std::pair<Eigen::Vector3f,float>
VisibilityContextDiscretePF<PointNT>::PointWithMinimalDistance ( const Eigen::Vector3f& referredPoint,
																 const Eigen::Vector3f& VoutDirection,
																 const PointNT& searchPoint ){

	// get all Points of the voxel indicated by the searchPoint
	std::vector<int> pointIdxVec; //!< list of points in the voxel

	// calculate the distances of the points to the Vout-Direction vector
	std::vector<std::pair<Eigen::Vector3f,float>> combined;
	std::vector<std::pair<Eigen::Vector3f,float>>::iterator minimalDistance;

	if (octreeSearchPtr->voxelSearch (searchPoint, pointIdxVec)) // if there are points in the voxel
	{
		float distance = 0;
		Eigen::Vector3f point, vector, projectedVector;

		for (int i = 0; i < pointIdxVec.size(); i++){ // loop over points in voxel

			point = cloudPtr->points[pointIdxVec[i]].getVector3fMap();
			vector = point - referredPoint;
			projectedVector = point.dot(VoutDirection/(VoutDirection.norm())) * VoutDirection/(VoutDirection.norm());

			// distance of current point:
			distance = (vector - projectedVector).norm();
			combined.push_back(std::make_pair(point, distance));
		}
		// as in https://stackoverflow.com/questions/33562059/max-element-on-vector-of-pairs-without-predicate
		minimalDistance = std::min_element(begin(combined), end(combined),
		            		[](const std::pair<Eigen::Vector3f,float>& left, const std::pair<Eigen::Vector3f,float>& right){
							return left.second <  right.second;});

//		minimalDistance = std::min_element(distances.begin(), distances.end());
	}
	else ROS_ERROR_STREAM(" [PointWithMinimalDistance] Something went horribly wrong, voxel does not contain any points");

	return std::make_pair(minimalDistance->first, minimalDistance->second);
}

template <typename PointNT> float
VisibilityContextDiscretePF<PointNT>::getDistanceForVout(const Eigen::Vector3f& referredPoint,
														 const Eigen::Vector3f& VoutDirection,
														 const typename pcl::PointCloud<PointNT>::Ptr& voxelCentersPtr,
														 const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
														 const std::string& label){

	// logic for extracting the intersected voxel which makes sense for v_out (a.k.a. the 'point-of-interest' ):

	float voxelDiameter = octreeSearchPtr->getVoxelSquaredDiameter();
	voxelDiameter = sqrt(voxelDiameter);
	bool *flagArrayPtr = new bool [voxelCentersPtr->size()]();	//!< array to set a flag for points, who are a potential point-of-interest
	int position = -1;			//!< position of the point of interest in the flag-array -- default = -1 to detect if no intersection takes place
	int counter[3] = {0,0,0}; 	//!< Array to count the occurrence of the three classes of angles -- between surface-normal and V_out-direction
	float angle = 0; 			//!< angle between V_out-direction and surface-normal
	std::vector<float> angles; 	//!< vector to store the angles between V_out-direction and surface-normal ( in voxel-centroid )

    // looping over all intersected voxel centers
    for (int point = 0; point < (voxelCentersPtr->size()); point ++)
    {
            Eigen::Vector3f voxelCenter = voxelCentersPtr->points[point].getVector3fMap();
            float distance = (voxelCenter - referredPoint).norm();

            // check distance to referred point (points too close to the referred point will be discarded)
            if ( distance >= (ignoreFactor_intersectClassific*voxelDiameter) ) // not too close
            {
                    ROS_DEBUG_STREAM( "*** voxelCenter " << point << " is NOT too close to the referred point." );

                    // check distance to referred point, as points too far away from referred point will be discarded (only relevant during matching, not modelBuilding)
                    if ( distance > (modelDiameter+(voxelDiameter/2)) )
                    { // +voxelDiameter/2 because voxel centers might have an offset of max voxelDiameter/2 from the real object surface
                            // too far away
                            ROS_DEBUG_STREAM( "*** voxelCenter " << point << " is too far (> model-diameter) away from the referred point." );

                            if (point-1 > -1)
                            { // there is a previous point
                                // check if previous intersected voxel center was a "potential point" of interest
                                if (flagArrayPtr[point-1] == true) //yes
                                {
                                        ROS_DEBUG_STREAM( "    The " << point-1<< "'th voxelCenter (previous voxelCenter) is a potential voxelCenter of interest.");
                                        Eigen::Vector3f previousVoxelCenter = (voxelCentersPtr->points[point-1].getVector3fMap());
                                        distance = (voxelCenter - previousVoxelCenter).norm();

                                        // check if point "voxelCenter" is direct neighbour to this previous potential point
                                        if (distance > (gapSizeFactor_allCases_intersectClassific * voxelDiameter)) // no
                                        {
                                                ROS_DEBUG_STREAM( "    voxelCenter " << point << " is too far from the previous voxelCenter. ");

                                                if (gapSizeFactor_surfaceCase_intersectClassific != 0)
                                                { // if factor == 0, then this advanced angle-check will be disabled. (this way we don't need another flag in the config ;-) )
                                                                                                        // the extra gapSizeFactor_surface != 0 makes it possible, to allow wider gaps between two successive intersected voxel centers,
                                                                                                        // but only if their normals indicate 'ray-on-surface'-intersection case. therefore it should be easier to detect rays following surfaces.

                                                        ROS_DEBUG_STREAM( "    Check if surface normal and V_out-direction form an angle of 90 deg +- orthogonalTreshold.");

                                                        // get the normal vector of the voxelCentroid.
                                                        pcl::PointNormal centroidOfVoxel = getCentroidOfVoxelAsPLCPointT( (voxelCentersPtr->points[point]));
                                                        Eigen::Vector3f normalOfCentroid = getNormalOfCentroidAsEigenVec3f( centroidOfVoxel);

                                                        // calculate the angle between V_out-direction and surface-normal
                                                        angle = vectorAngle(VoutDirection, normalOfCentroid);

                                                        if (  (angleEquals90Deg(angle)) && (distance <= (gapSizeFactor_surfaceCase_intersectClassific*voxelDiameter)) )
                                                        {
                                                                // yes, "voxelCenter" is direct neighbour to previous potential point (using the extra wide gap)
                                                                ROS_DEBUG_STREAM( "    voxelCenter " << point << " likely belongs to a intersection where V_out follows the surface.");
                                                                ROS_DEBUG_STREAM( "    Because V_out is now > model-diameter, it is invalid.");
                                                                delete[]flagArrayPtr;
                                                                return -1; // this V_out is invalid
                                                        }
                                                        else
                                                        {
                                                                ROS_DEBUG_STREAM( "    voxelCenter " << point << " is too far from the previous voxelCenter. The former voxelCenter is the voxelCenter of interest.");
                                                                // previous point is is definitively the intersection-"point of interest"
                                                                position = (point-1);
                                                                break; // break the for-loop, finished. ignore the rest of the intersected voxel centers
                                                        }
                                                }
                                                else
                                                { // no, "voxelCenter" is no direct neighbour to previous potential point
                                                        ROS_DEBUG_STREAM( "    voxelCenter " << point << " is too far from the previous voxelCenter. The former voxelCenter "
                                                                  << point-1 << " is the voxelCenter of interest." );
                                                        // previous point is is definitively the intersection-"point of interest"
                                                        position = (point-1);
                                                        break; // break the for-loop, finished. ignore the rest of the intersected voxel centers
                                                }
                                        }
                                        else
                                        {
                                                // yes, "voxelCenter" is direct neighbour to previous potential point
                                                ROS_DEBUG_STREAM( "    voxelCenter " << point << " is close enough to the previous voxelCenter. Because V_out is now > model-diameter, it is invalid.");
                                                delete[]flagArrayPtr;
                                                return -1; // this V_out is invalid
                                        }
                                }
                                else
                                {
                                        // the former point isn't a potential point
                                        ROS_DEBUG_STREAM( "    The " << point-1<< "'th voxelCenter (previous voxelCenter) is NOT a potential voxelCenter of interest. V_out is zero.");
                                        delete[]flagArrayPtr;
                                        return 0; // V_out is zero
                                }
                            }
                            else
                            { // point-1 is not existing, i.e. previous point isn't a potential point
                                    ROS_DEBUG_STREAM( "    The " << point-1<< "'th voxelCenter (previous voxelCenter) is NOT EXISTING, and therefore not a potential voxelCenter of interest. V_out is zero.");
                                    delete[]flagArrayPtr;
                                    return 0; // V_out is zero
                            }
                    }

                    // not too far away
                    ROS_DEBUG_STREAM( "    voxelCenter " << point << " is NOT too far away from the referred point." );

                    // set flag in array "true" --> potential point
                    flagArrayPtr [point] = true;

                    // check if advanced methods should be used:
                    if (gapSizeFactor_surfaceCase_intersectClassific != 0 || advancedIntersectionClassification){

                            // get the normal vector of the voxelCentroid.
                            PointNT centroidOfVoxel = getCentroidOfVoxelAsPLCPointT( (voxelCentersPtr->points[point]));
                            Eigen::Vector3f normalOfCentroid = getNormalOfCentroidAsEigenVec3f( centroidOfVoxel);

                            // calculate the angle between V_out-direction and surface-normal
                            angle = vectorAngle(VoutDirection, normalOfCentroid);

                            // for advancedIntersectionClassification:
                            if (advancedIntersectionClassification){
                                    angles.push_back(angle);
                                    incrementCounter(angle, counter);
                            }
                    }

                    // check if there are voxels left to check
                    if (point != (voxelCentersPtr->size()-1)) // yes
                    {
                            ROS_DEBUG_STREAM( "    There are voxelCenters left to examine." );

                            if (point-1 > -1)
                            { // there is a previous point

                                    // is the previous point a potential point?
                                    if (flagArrayPtr[point-1] == true) //yes
                                    {
                                            ROS_DEBUG_STREAM( "    The " << point-1<< "'th voxelCenter (previous voxelCenter) is a potential voxelCenter of interest.");
                                            Eigen::Vector3f previousVoxelCenter = (voxelCentersPtr->points[point-1].getVector3fMap());
                                            distance = (voxelCenter - previousVoxelCenter).norm();

                                            // check if point "voxelCenter" is direct neighbour to this previous potential point
                                            if (distance > (gapSizeFactor_allCases_intersectClassific * voxelDiameter)) // no
                                            {
                                                    ROS_DEBUG_STREAM( "    voxelCenter " << point << " is too far from the previous voxelCenter. ");

                                                    if (gapSizeFactor_surfaceCase_intersectClassific != 0){ // if factor == 0, then this advanced angle-check will be disabled. (this way we don't need another flag in the config ;-) )
                                                                                                            // the extra gapSizeFactor_surface != 0 makes it possible, to allow wider gaps between two successive intersected voxel centers,
                                                                                                            // but only if their normals indicate 'ray-on-surface'-intersection case. therefore it should be easier to detect rays following surfaces.

                                                            ROS_DEBUG_STREAM( "    Check if surface normal and V_out-direction form an angle of 90 deg +- orthogonalTreshold.");

                                                            if (  (angleEquals90Deg(angle)) && (distance <= (gapSizeFactor_surfaceCase_intersectClassific*voxelDiameter)) ){
                                                                    // yes, "voxelCenter" is direct neighbour to previous potential point (using the extra wide gap)
                                                                    ROS_DEBUG_STREAM( "    voxelCenter " << point << " likely belongs to a intersection where V_out follows the surface. Checking next point.");
                                                                    continue;
                                                            }
                                                            else{
                                                                    ROS_DEBUG_STREAM( "    voxelCenter " << point << " is too far from the previous voxelCenter. The former voxelCenter "<< point-1<< " is the voxelCenter of interest.");
                                                                    // previous point is is definitively the intersection-"point of interest"
                                                                    position = (point-1);
                                                                    // for advancedIntersectionClassification:
                                                                    if (advancedIntersectionClassification){
                                                                            angles.pop_back(); // erase last angle in vector
                                                                            decrementCounter(angle, counter);
                                                                    }
                                                                    break; // break the for-loop, finished. ignore the rest of the intersected voxel centers
                                                            }
                                                    }
                                                    else{ // don't use extra gapSizeFactor
                                                          // no, "voxelCenter" is no direct neighbour to previous potential point
                                                            ROS_DEBUG_STREAM( "    voxelCenter " << point << " is too far from the previous voxelCenter. The former voxelCenter is the voxelCenter of interest.");
                                                            // previous point is is definitively the intersection-"point of interest"
                                                            position = (point-1);
                                                            // for advancedIntersectionClassification:
                                                            if (advancedIntersectionClassification){
                                                                    angles.pop_back();
                                                                    decrementCounter(angle, counter);
                                                            }
                                                            break; // break the for-loop, finished. ignore the rest of the intersected voxel centers
                                                    }
                                            }
                                            else{
                                                // yes, "voxelCenter" is direct neighbour to previous potential point
                                                ROS_DEBUG_STREAM( "    voxelCenter " << point << " is close enough to the previous voxelCenter. Checking next point.");
                                                // examine next intersected voxel center ...
                                                continue;
                                            }

                                    }
                                    else
                                    {
                                            // the former point isn't a potential point, check next voxelCenter "point"
                                            ROS_DEBUG_STREAM( "    The " << point-1<< "'th voxelCenter (previous voxelCenter) is NOT a potential voxelCenter of interest. Checking next point.");
                                            continue;
                                    }
                            }
                            else
                            { // point-1 is not existing, i.e. same case as if it were existing and but were not a potential point
                              // check next voxelCenter "point"
                                    ROS_DEBUG_STREAM( "    The " << point-1<< "'th voxelCenter (previous voxelCenter) is NOT EXISTING and therefore not a potential voxelCenter of interest. Checking next point.");
                                    continue;
                            }
                    }
                    else // no other voxels left to check
                    {
                            ROS_DEBUG_STREAM( "    There are NO voxelCenters left.");

                            if (point-1 > -1)
                            { // a previous point is existing

                                    // is the previous point a potential point?
                                    if (flagArrayPtr[point-1] == true) //yes
                                    {
                                            ROS_DEBUG_STREAM( "    The " << point-1<< "'th voxelCenter (previous voxelCenter) is a potential voxelCenter of interest.");

                                            Eigen::Vector3f previousVoxelCenter = (voxelCentersPtr->points[point-1].getVector3fMap());
                                            distance = (voxelCenter - previousVoxelCenter).norm();

                                            // check if point "voxelCenter" is direct neighbour to this potential previous point
                                            if (distance > (gapSizeFactor_allCases_intersectClassific * voxelDiameter)) // no
                                            {
                                                    ROS_DEBUG_STREAM( "    voxelCenter " << point << " is too far from the previous voxelCenter. ");

                                                    if (gapSizeFactor_surfaceCase_intersectClassific != 0){ // if factor == 0, then this advanced angle-check will be disabled.

                                                            ROS_DEBUG_STREAM( "    Check if surface normal and V_out-direction form an angle of 90 deg +- orthogonalTreshold.");

                                                            if (  (angleEquals90Deg(angle)) && (distance <= (gapSizeFactor_surfaceCase_intersectClassific*voxelDiameter)) ){
                                                                    ROS_DEBUG_STREAM( "    voxelCenter " << point << " likely belongs to an intersection where V_out follows the surface. No points left, it is the voxelCenter of interest.");
                                                                    position = point;
                                                                    break; // break the for-loop
                                                            }
                                                            else{
                                                                    ROS_DEBUG_STREAM( "    voxelCenter " << point << " is too far from the previous voxelCenter. The former voxelCenter "<<point-1<<" is the voxelCenter of interest.");
                                                                    // previous point is is definitively the intersection-"point of interest"
                                                                    position = (point-1);
                                                                    // for advancedIntersectionClassification:
                                                                    if (advancedIntersectionClassification){
                                                                            angles.pop_back(); // erase last angle in vector
                                                                            decrementCounter(angle, counter);
                                                                    }
                                                                    break; // break the for-loop
                                                            }
                                                    }
                                                    else{ // no, "voxelCenter" is no direct neighbour to previous potential point
                                                            ROS_DEBUG_STREAM( "    The former voxelCenter is the voxelCenter of interest.");
                                                            // previous point is definitively the intersection-"point of interest"
                                                            position = (point-1);
                                                            // for advancedIntersectionClassification:
                                                            if (advancedIntersectionClassification){
                                                                    angles.pop_back();
                                                                    decrementCounter(angle, counter);
                                                            }
                                                            break; // break the for-loop
                                                    }

                                            }
                                            else
                                            {       // yes, direct neighbour to previous potential point
                                                    ROS_DEBUG_STREAM( "    voxelCenter " << point << " is close enough to the previous voxelCenter.");
                                                    // point is definitively the intersection-"point-of-interest"
                                                    ROS_DEBUG_STREAM( " There are NO voxelCenters left, voxelCenter "<<point<<" is definitively the voxelCenter of interest.");
                                                    position = point;
                                                    break;
                                            }
                                    }
                                    else // no, previous point is not a potential point
                                    {
                                            // the former point isn't a potential point, point is definitively the intersection-"point-of-interest"
                                            ROS_DEBUG_STREAM( "    The " << point-1<< "'th voxelCenter (previous voxelCenter) is NOT a potential voxelCenter. voxelCenter "<<point<<" is definitively the voxelCenter of interest.");
                                            position = point;
                                            break;
                                    }
                            }
                            else
                            { // there is no previous point, i.e. same case as if there was one which was not potential
                                    ROS_DEBUG_STREAM( "    The " << point-1<< "'th voxelCenter (previous voxelCenter) is NOT EXSISTING, therefore current voxelCenter "<<point<<" is definitively the voxelCenter of interest.");
                                    position = point;
                                    break;
                            }
                    } // no other voxels left to check
            }
            else // point is too close from the referred point
            {
                    // set flag in array "false" --> discard point
                    ROS_DEBUG_STREAM( "*** voxelCenter " << point << " is too close to the referred point!");
                    flagArrayPtr [point] = false;
            }
    } // end of loop over intersected voxelCenters
	ROS_DEBUG_STREAM ("*** Flag-Array: ");
	for (int i = 0; i < (voxelCentersPtr->size()); i++)
	{
		ROS_DEBUG_STREAM ("    at " <<i << "'th: " << flagArrayPtr[i] );
	}
	ROS_DEBUG_STREAM ( "*** voxelCenter-of-interest in Flag-Array: " << position );
	// exception: no point of interest detected:
	if (position == -1)
	{
		delete[]flagArrayPtr;
		return 0;
	}
	ROS_DEBUG_STREAM ("*** Angle-Counter-Array: \n  0 deg = "<< counter[0] <<" \n 45 deg = "<< counter[1] <<" \n 90 deg = "<< counter[2] );

	// point of interest a.k.a. the voxel-center of interest, determined by the above logic:
	Eigen::Vector3f pointOfInterest = voxelCentersPtr->points[position].getVector3fMap();
	// project point of interest onto the direction vector Vout:
	// http://math.oregonstate.edu/home/programs/undergrad/CalculusQuestStudyGuides/vcalc/dotprod/dotprod.html
	float projectedDistance = (pointOfInterest-referredPoint).dot(VoutDirection/(VoutDirection.norm()));

#ifdef visualize_VisCon_feature
	if(visualizeVisibilityContextFeature){
		// draw vectors from the referred point to the point of interest:
		PointNT POI = EigenVecToPCLPointT<PointNT>(pointOfInterest);
		PointNT referredPoint_ = EigenVecToPCLPointT<PointNT>(referredPoint);
		std::string newlabel = label+"_POI";
		fancy_viewer->addArrow ( POI, referredPoint_,	1, 1, 1, false, newlabel);  // white vector
    }
#endif
	// debugging: what if there are two maxima
//	int arbitraryCounter[3] = {1,3,3};
//	std::copy(std::begin(arbitraryCounter), std::end(arbitraryCounter), std::begin(counter));
//	ROS_DEBUG_STREAM ("*** Angle-Counter-Array: \n  0 deg = "<< counter[0] <<" \n 45 deg = "<< counter[1] <<" \n 90 deg = "<< counter[2] );

	if(advancedIntersectionClassification){
		// get the angle that occurred most
		auto maxCount = std::max_element(counter, counter+3); // number of occurrences
		ROS_DEBUG_STREAM("The angle, that occurred most, occurred " << *maxCount << " times");
		int posMaxCount = std::distance(counter, maxCount); // position of max in counter-array
		ROS_DEBUG_STREAM("The angle, that occurred most is at position " << posMaxCount );

		// get the angle that occurred second-most
		// from https://stackoverflow.com/questions/30488586/stdmax-element-for-second-largest-element
		int counterCopy[3] ;
		std::copy(std::begin(counter), std::end(counter), std::begin(counterCopy));
		std::nth_element(counterCopy, counterCopy+1, counterCopy+3, std::greater<float>());
		int secondMaxCount = counterCopy[1];
		ROS_DEBUG_STREAM("The angle, that occurred second-most, occurred " << secondMaxCount << " times");
		auto findSecondMaxCount = std::find(counter, counter+3, secondMaxCount);
		int posSecondMaxCount = std::distance(counter, findSecondMaxCount); // position of 2.max is the same as position of 1.max, if 2.max = 1.max
		ROS_DEBUG_STREAM("The angle, that occurred second-most is at position " << posSecondMaxCount );

		// get the group size of the neighbouring intersected voxels and store their angles
		int groupSize = 1;
//		for (int i = 0; i<((sizeof(flagArray) / sizeof(bool))); i++){ // TODO: MFZ 24/10/17 ich glaube die Schleife l��uft zu lange. sizeOf(flagArray)/sizeOf(bool) ist zu gro�� ?! vielleicht durch 'position' ersetzen.
																	  //					obwohl das vielleicht auch zu gro�� ist.
																	  // das Problem/ der Fehler ��u��erte sich wieder in einem v��llig unlogischen Punkt, f��r den in dieser Loop ein Centroid und eine Normale
																	  // hatten gefunden werden sollen. Wie dieser Punkt zustande kam, (z.b. durch Zugriff auf flagArray[-1]) ohne einen Segmentation-Fault zu werfen,
																	  // ist mir nach wie vor unklar. Das ver��ndern und das verlagern der Berechnungen aus dieser Loop heraus "l��sten das Problem". Die Ursache ist nach
																	  // wie vor unklar.
		for (int i = 0; i< position; i++){
			if ( flagArrayPtr[position-i-1] == true ){ //!< if the preceding intersected voxel is within reach and therefore adds to the connected group
				groupSize ++;
			}
			else{
				// end of the group of connected voxels.
				break;
			}
		}
		ROS_DEBUG_STREAM("The group-size is: " << groupSize);

		if (groupSize < 4){ // get the point with the minimal distance to V_out from all intersected voxels
			// group is too small to find a 'meaningful' maximum of which angle occured most. the cases below would all result in (more or less) the same point
			std::vector<std::pair<Eigen::Vector3f,float>> distances;

			for (int i = 0; i < groupSize; i++){ // for each intersected voxel of this group
				// caclulate the point with minimal distance to V_out from all Points of the current voxel
				if (flagArrayPtr[position -i] != 0){ // TODO:: MFZ 29.06.2018 check if this if statement makes sense! why would I check if a bool array has value 0
					distances.push_back(PointWithMinimalDistance( referredPoint, VoutDirection ,voxelCentersPtr->points[position-i]));
					ROS_DEBUG_STREAM("Voxelcenter " << position-i << " wurde zum Finden des nearest Neighbours untersucht.");
				}
			}
			// get the point with the minimal distance to V_out from all intersected voxels
			// as in https://stackoverflow.com/questions/33562059/max-element-on-vector-of-pairs-without-predicate
			auto minimalDistance = std::min_element(begin(distances), end(distances),
										[](const std::pair<Eigen::Vector3f,float>& left, const std::pair<Eigen::Vector3f,float>& right){
										return left.second <  right.second;});

			pointOfInterest = minimalDistance->first;
			ROS_DEBUG_STREAM("Es gab eine Durchsto��ung der Oberfl��che mit sehr wenigen Intersections.");
		}
		else {
			// group has a meaningful maximum
			if ( (secondMaxCount / *maxCount ) > 0.5){
				// then the max is not significant --> check all connected intersected voxels.
				// or set a flag and calculate feature for both cases?
				// implement a counter in real system.
				ROS_DEBUG_STREAM("there was no unambiguous maximum angle");
				delete[]flagArrayPtr;
				return 0;
			}
			else{
				// max is significant
				if (posMaxCount == 0){
					// max is at 0��
					// ray is likely spiking the surface like this: _|_
					// take the last three intersected voxel centers and calculate the nearest point.
					std::vector<std::pair<Eigen::Vector3f,float>> distances;

					for (int i = 0; i < 3; i++){ // for the last three intersected voxels of this group
						// caclulate the point with minimal distance to V_out from all Points of the current voxel
						if (flagArrayPtr[position -i] == true){
							distances.push_back(PointWithMinimalDistance( referredPoint, VoutDirection ,voxelCentersPtr->points[position-i]));
							ROS_DEBUG_STREAM("Voxelcenter " << position-i << " wurde zum Finden des nearest Neighbours untersucht.");
						}
					}
					// get the point with the minimal distance to V_out from all intersected voxels
					// as in https://stackoverflow.com/questions/33562059/max-element-on-vector-of-pairs-without-predicate
					auto minimalDistance = std::min_element(begin(distances), end(distances),
												[](const std::pair<Eigen::Vector3f,float>& left, const std::pair<Eigen::Vector3f,float>& right){
												return left.second <  right.second;});

					pointOfInterest = minimalDistance->first;
					ROS_DEBUG_STREAM("Es gab eine orthogonale Durchsto��ung der Oberfl��che.");
				}
				else if (posMaxCount == 2){
					// max is at 90��
					// ray is likely following the surface like this: =
					// take the last three intersected voxel centers and calculate the nearest point.
					std::vector<std::pair<Eigen::Vector3f,float>> distances;

					for (int i = 0; i < 3; i++){ // for the last three intersected voxels of this group
						// caclulate the point with minimal distance to V_out from all Points of the current voxel
						if (flagArrayPtr[position -i] == true){
							distances.push_back(PointWithMinimalDistance( referredPoint, VoutDirection ,voxelCentersPtr->points[position-i]));
							ROS_DEBUG_STREAM("Voxelcenter " << position-i << " wurde zum Finden des nearest Neighbours untersucht.");
						}
					}
					// get the point with the minimal distance to V_out from all intersected voxels
					// as in https://stackoverflow.com/questions/33562059/max-element-on-vector-of-pairs-without-predicate
					auto minimalDistance = std::min_element(begin(distances), end(distances),
												[](const std::pair<Eigen::Vector3f,float>& left, const std::pair<Eigen::Vector3f,float>& right){
												return left.second <  right.second;});

					pointOfInterest = minimalDistance->first;
					ROS_DEBUG_STREAM("Es gab einen Verauf an der Oberfl��che.");
				}
				else{
					// max is at 45��
					// ray is likely to intersect in an intermediate angle like this: <
					// take all points from the central intersected voxel-center and calculate the nearest point.
					if (groupSize%2 == 0){
						// take the two centervoxels
						int groupCenter = groupSize/2;
						int center = position - groupCenter;
						ROS_DEBUG_STREAM( "Es gab eine Druchsto��ung der Oberfl��che im gem����igten Winkel. Der mittlere durchsto��ene Voxel ist an Position " << center << "." );

						std::vector<std::pair<Eigen::Vector3f,float>> distances;
						for (int i = 0; i < 2; i++){ // for the the two centervoxels
							// caclulate the point with minimal distance to V_out from all Points of the current voxel
							if (flagArrayPtr[center+i] == true){
								distances.push_back(PointWithMinimalDistance( referredPoint, VoutDirection ,voxelCentersPtr->points[center+i]));
								ROS_DEBUG_STREAM("Voxelcenter " << center+i << " wurde zum Finden des nearest Neighbours untersucht.");
							}
						}
						// get the point with the minimal distance to V_out from all intersected voxels
						// as in https://stackoverflow.com/questions/33562059/max-element-on-vector-of-pairs-without-predicate
						auto minimalDistance = std::min_element(begin(distances), end(distances),
													[](const std::pair<Eigen::Vector3f,float>& left, const std::pair<Eigen::Vector3f,float>& right){
													return left.second <  right.second;});

						pointOfInterest = minimalDistance->first;
					}
					else {
						// take the centervoxel and its both neighbourvoxels
						int groupCenter = static_cast<int>(groupSize/2);
						int center = position - groupCenter;
						ROS_DEBUG_STREAM("Es gab eine Druchsto��ung der Oberfl��che im gem����igten Winkel. Der mittlere durchsto��ene Voxel ist an Position " << center << "." );

						std::vector<std::pair<Eigen::Vector3f,float>> distances;
						for (int i = 0; i < 3; i++){ // for the centervoxel and its both neighbourvoxels
							// caclulate the point with minimal distance to V_out from all Points of the current voxel
							if (flagArrayPtr[center-1+i] == true){
								distances.push_back(PointWithMinimalDistance( referredPoint, VoutDirection ,voxelCentersPtr->points[center-1+i]));
								ROS_DEBUG_STREAM("Voxelcenter " << center-1+i << " wurde zum Finden des nearest Neighbours untersucht.");
							}
						}
						// get the point with the minimal distance to V_out from all intersected voxels
						// as in https://stackoverflow.com/questions/33562059/max-element-on-vector-of-pairs-without-predicate
						auto minimalDistance = std::min_element(begin(distances), end(distances),
													[](const std::pair<Eigen::Vector3f,float>& left, const std::pair<Eigen::Vector3f,float>& right){
													return left.second <  right.second;});

						pointOfInterest = minimalDistance->first;
					}
				}
			}
		}

		float projectedDistance_2 = (pointOfInterest-referredPoint).dot(VoutDirection/(VoutDirection.norm()));
		ROS_DEBUG_STREAM("Die neue, mit dem Voxel-schwerpunkt projizierte Disztanz ist = " << projectedDistance_2 << ".\nUnd die originale ist= " << projectedDistance );

#ifdef visualize_VisCon_feature
		if(visualizeVisibilityContextFeature){
			// draw vectors from the referred point to the point of interest:
			PointNT POI_2 = EigenVecToPCLPointT<PointNT>(pointOfInterest);
			PointNT referredPoint_ = EigenVecToPCLPointT<PointNT>(referredPoint);
			std::string newlabel_2 = label+"_POI_2";
			fancy_viewer->addArrow ( POI_2, referredPoint_,	1, 0.5, 1, false, newlabel_2);  // purple vector
		}
#endif
		delete[]flagArrayPtr;
		return projectedDistance_2;
	} // advanced intersection classification
	delete[]flagArrayPtr;
	return projectedDistance;
}


template <typename PointNT> bool
VisibilityContextDiscretePF<PointNT>::calculateNeighbourFeature(int i, int j, int k, int l, int m, int n, int o, int p){

	// temporary storage:
	float neighbourPFfeature[4];
	float neighbourVCfeature[4];

	// ================================================= PPF =================================================
	switch (i) // distance d
	{
		case 0:
			neighbourPFfeature[0] = discretePF[0] -1;
			break;
			// If distance gets <=0 the feature becomes invalid. It gets checked in invalidDistance().
		case 1:
			neighbourPFfeature[0] = discretePF[0];
			break;
		case 2:
			neighbourPFfeature[0] = discretePF[0] +1;
			break;
			// If distance gets too big (overflow, feature becomes invalid). It gets checked in invalidDistance().
	}
	switch (j) // angle in [0, pi/d_angle]
	{
		case 0:
			neighbourPFfeature[1] = discretePF[1] -1;
			break;
			// If angle gets <0 the feature becomes invalid. This system here handles only angles in [0, pi/d_angle]. It gets checked in invalidAngle().
		case 1:
			neighbourPFfeature[1] = discretePF[1];
			break;
		case 2:
			neighbourPFfeature[1] = discretePF[1] +1;
			break;
			// If angle gets >pi/d_angle the feature becomes invalid. This system here handles only angles in [0, pi/d_angle]. It gets checked in invalidAngle().
	}
	switch (k) // angle in [0, pi/d_angle]
	{
		case 0:
			neighbourPFfeature[2] = discretePF[2] -1;
			break;
		case 1:
			neighbourPFfeature[2] = discretePF[2];
			break;
		case 2:
			neighbourPFfeature[2] = discretePF[2] +1;
			break;
	}
	switch (l) // angle in [0, pi/d_angle]
	{
		case 0:
			neighbourPFfeature[3] = discretePF[3] -1;
			break;
		case 1:
			neighbourPFfeature[3] = discretePF[3];
			break;
		case 2:
			neighbourPFfeature[3] = discretePF[3] +1;
			break;
	}

	// ================================================= VisCon =================================================
	switch (m) // distance V_out_0
	{
		case 0:
			neighbourVCfeature[0] = discreteVC[0] -1;
			break;
			// If V_out-Distance gets <0, the feature becomes invalid. It gets checked in invalidVout().
		case 1:
			neighbourVCfeature[0] = discreteVC[0];
			break;
		case 2:
			neighbourVCfeature[0] = discreteVC[0] +1;
			break;
			// If V_out-Distance gets too big (overflow, feature becomes invalid). It gets checked in invalidVout().
	}
	switch (n) // distance V_out_1
	{
		case 0:
			neighbourVCfeature[1] = discreteVC[1] -1;
			break;
		case 1:
			neighbourVCfeature[1] = discreteVC[1];
			break;
		case 2:
			neighbourVCfeature[1] = discreteVC[1] +1;
			break;
	}
	switch (o) // distance V_out_2
	{
		case 0:
			neighbourVCfeature[2] = discreteVC[2] -1;
			break;
		case 1:
			neighbourVCfeature[2] = discreteVC[2];
			break;
		case 2:
			neighbourVCfeature[2] = discreteVC[2] +1;
			break;
	}
	switch (p) // distance V_out_3
	{
		case 0:
			neighbourVCfeature[3] = discreteVC[3] -1;
			break;
		case 1:
			neighbourVCfeature[3] = discreteVC[3];
			break;
		case 2:
			neighbourVCfeature[3] = discreteVC[3] +1;
			break;
	}

	// the following mapping is wrong.
//	// map neighbour angle to [0, pi/d_angle]. because it could be out of that range now
//	neighbourPFfeature[1] = mapNeighbourAngle(neighbourPFfeature[1]);
//	neighbourPFfeature[2] = mapNeighbourAngle(neighbourPFfeature[2]);
//	neighbourPFfeature[3] = mapNeighbourAngle(neighbourPFfeature[3]);

	// check validity:
	bool invalid = false;
	invalid = invalid || invalidDistance(neighbourPFfeature[0]);
	invalid = invalid || invalidAngle(neighbourPFfeature[1]);
	invalid = invalid || invalidAngle(neighbourPFfeature[2]);
	invalid = invalid || invalidAngle(neighbourPFfeature[3]);
	invalid = invalid || invalidVout(neighbourVCfeature[0]);
	invalid = invalid || invalidVout(neighbourVCfeature[1]);
	invalid = invalid || invalidVout(neighbourVCfeature[2]);
	invalid = invalid || invalidVout(neighbourVCfeature[3]);

	if(invalid == false){ // if PPF is valid
		discretePFneighbour[0] = neighbourPFfeature[0];
		discretePFneighbour[1] = neighbourPFfeature[1];
		discretePFneighbour[2] = neighbourPFfeature[2];
		discretePFneighbour[3] = neighbourPFfeature[3];
		discreteVCneighbour[0] = neighbourVCfeature[0];
		discreteVCneighbour[1] = neighbourVCfeature[1];
		discreteVCneighbour[2] = neighbourVCfeature[2];
		discreteVCneighbour[3] = neighbourVCfeature[3];

		// return TRUE if PPF is valid
		return (true);
	}
	else{
		// TODO: remove after debugging
		discretePFneighbour[0] = neighbourPFfeature[0];
		discretePFneighbour[1] = neighbourPFfeature[1];
		discretePFneighbour[2] = neighbourPFfeature[2];
		discretePFneighbour[3] = neighbourPFfeature[3];
		discreteVCneighbour[0] = neighbourVCfeature[0];
		discreteVCneighbour[1] = neighbourVCfeature[1];
		discreteVCneighbour[2] = neighbourVCfeature[2];
		discreteVCneighbour[3] = neighbourVCfeature[3];
		// Return FALSE if overflow occurred or distance d < 0!
		return (false);
	}
}


// MFZ 18/09/17 added instantiation of templated classes (runtime error occurred 'undefined symbol'):
// this is only necessary such that pairFeatures.cpp does not have to be converted into pairFeaturesImpl.hpp
// in which one could implement the templated functions. Apparently we need pairFeatures.cpp as lib in cmakelists.
// https://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file
// TODO: add instantiations for other point types of those template classes as needed
template class VisibilityContextDiscretePF<pcl::PointNormal>;
template class MinimalDiscretePF<pcl::PointNormal>;

} // end namespace pf_matching
