/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Ziegler
 * @date 08.04.2019
 * @file matcherNodelet.cpp
 * @brief Nodelet for matching preprocessed point clouds to internally stored models
 */

#define CHECK_PF_FOR_NAN



// project
#include <filePaths.hpp>
#include <pairFeatureMatcher.hpp>
#include <nodeletWithStatistics.hpp>
#include <resultVisualization.hpp>
#include <modelSymmetryAnalyzer.hpp>
#include <weighting.hpp>
#include <pairFeatureModel.hpp>
#include <featureTypes.hpp>
#include <pose.hpp>
#include <modelHashTable.hpp>


// ROS code
#include <ros/ros.h>
#include <pluginlib/class_list_macros.h> // for nodelets

// ROS messages and services
#include <pf_matching_core/ProcessedClouds.h>
#include <pf_matching_core/WeightedPoseArray.h>
#include <std_msgs/Header.h>
#include <std_srvs/Empty.h>

// dynamic reconfigure functionality
#include <dynamic_reconfigure/server.h> // 
#include <pf_matching_core/matcherConfig.h>

// PCL
#include <pcl_conversions/pcl_conversions.h>

// other
#include <string>
#include <boost/timer/timer.hpp>
#include <boost/thread/thread.hpp>
#include <log4cxx/logger.h>  // change logger level from within the node itself
// STL
#include <iostream> // TODO: MFZ writing stuff to files (debugging)
#include <fstream> // TODO: MFZ writing stuff to files (debugging)

// Linux specific
#ifdef __linux__
#include <X11/Xlib.h>
#endif

// debugging flags generating files
//#define write_counters_to_file
//#define write_pose_clusters_to_file
#define write_times_to_file

//#define debug_logger_level

namespace pf_matching{
namespace ROS{

// global debugging variable
int counter = 0;
    
/**
 * @class MatcherSettings
 * @brief Class for reading and storing the node's settings
 */
class MatcherSettings{
    public:

        // topics
        std::string input_topic_model; //!< ROS-topic with surface and edges clouds of the model
        std::string input_topic_scene; //!< ROS-topic with surface and edges clouds of the scene
        
        std::string output_topic_statistics;          //!< general statistics topic
        std::string output_topic_statistics_arrays;   //!< array statistics topic
        std::string output_topic_intermediate_poses;  //!< topic for raw and clustered poses
        
        std::string output_topic_poses;               //!< matched poses
        
        /**
         * @brief Read the node settings from the private node handle.
         * @param privateNh private node handle
         * @return true if reading was successful, else false
         */
        bool readSettings(const ros::NodeHandle& privateNh){
            
            // get topics
            privateNh.param("input_topic_model", input_topic_model, std::string("model_clouds"));
            privateNh.param("input_topic_scene", input_topic_scene, std::string("scene_clouds"));

            privateNh.param("output_topic_statistics",         output_topic_statistics, std::string("pf_statistics"));
            privateNh.param("output_topic_statistics_arrays",  output_topic_statistics_arrays,  std::string("pf_statistics_arrays"));
            privateNh.param("output_topic_intermediate_poses", output_topic_intermediate_poses, std::string("intermediate_poses"));
            
            privateNh.param("output_topic_poses", output_topic_poses, std::string("poses"));
            
            // check for help command
            bool printHelp;
            privateNh.param("help", printHelp, true);
            if(printHelp){
                printHelpMsg();
            }

            return true;
        }
    private:
        /**
         * @brief Print out a nice help message on how to use the node.
         */
        void printHelpMsg(){
            ROS_INFO_STREAM("\nHelp and parameters for matcherNodelet\n"
                << "-------------------------------------\n"
                << "(To hide this message, start the node / nodelet with _help:=false)\n\n"
                << "Available static parameters (set via _<pname>:=<value>):\n"
                << "--------------------------------------------------------\n"
                << "input_topic_model: topic with oriented surface and edges clouds of the model\n"
                << "input_topic_scene: topic with oriented surface and edges clouds of the scene\n"
                << "\n"
                << "output_topic_poses:               topic for final poses\n" 
                << "output_topic_statistics:          topic for publishing general statistics about node operation\n"
                << "output_topic_statistics_arrays:   topic for publishing multi-value statistics\n"
                << "output_topic_intermediate_poses:  topic for raw and clustered intermediate poses\n"
                << "\n"
                << "Available services:\n"
                << "-------------------\n"
                << "~/clear_models: erase all stored models\n"
                << "\n"
                << "This nodelet matches one or more models to a scene via pair feature matching:\n"
                << "Train the models first by publishing them to the model input topic, after "
                << "the models are built, trigger the matching by supplying a scene."
                << "\n"
                << "Algorithm parameters are available through the dynamic_reconfigure interface."
                );
        }
};

/**
 * @class MatcherNodelet
 * @brief The nodelet for creating models, storing them and matching them to input scenes via
 *        the pair feature algorithm.
 */
class MatcherNodelet : public NodeletWithStatistics{
    
    public:
        typedef pcl::PointNormal                   internalPointType; //!< point type used in this nodelet
        typedef pcl::PointCloud<internalPointType> internalCloudType; //!< cloud type used in this nodelet
        
        ~MatcherNodelet(){
            
            // close down the visualization window properly
            visualizationThread.interrupt();
            visualizationThread.join();
        }

    private:
        /**
         * @brief Setup of the nodelet.
         */
        virtual void onInit(){
            
            // initialization required on linux systems to prevent "unknown sequence number" error
            // during thread creation
            #ifdef __linux__
            XInitThreads();
            #endif
            
            // change logger level
          	#ifndef debug_logger_level
            log4cxx::Logger::getLogger(ROSCONSOLE_DEFAULT_NAME)->setLevel(
          		  ros::console::g_level_lookup[ros::console::levels::Info]
            );
            #endif
            #ifdef debug_logger_level
            log4cxx::Logger::getLogger(ROSCONSOLE_DEFAULT_NAME)->setLevel(
          		  ros::console::g_level_lookup[ros::console::levels::Debug]
          	);
            #endif
            ros::console::notifyLoggerLevelsChanged();

            // general node handle, manager's namespace, usually for topic access
            ros::NodeHandle& nh = getNodeHandle(); // manager namespace, for topics
            ros::NodeHandle& private_nh = getPrivateNodeHandle(); // node's private namespace, 
                                                                  // for parameters
            
            // read settings
            bool readOK = settings.readSettings(private_nh);
            if(!readOK){
                exit(EXIT_FAILURE);
            }
            
            // set up publishers
            // note: pose publisher is set up via tf::TransformBroadcaster, don't need an extra 
            // publisher
            pubPoses = nh.advertise<pf_matching_core::WeightedPoseArray>(settings.output_topic_poses, 10, true);
            pubIntermediatePoses = nh.advertise<pf_matching_core::WeightedPoseArray>(settings.output_topic_intermediate_poses, 10, true);
            initStatisticsPublisher(nh, settings.output_topic_statistics);
            initStatisticsArrayPublisher(nh, settings.output_topic_statistics_arrays);
            
            // set up subscribers to model and scene input topics
            subModel = nh.subscribe(settings.input_topic_model, 5, 
                                            &MatcherNodelet::modelCloudsInCallback, this);
            subScene = nh.subscribe(settings.input_topic_scene, 5, 
                                            &MatcherNodelet::sceneCloudsInCallback, this);

            
            // set up dynamic reconfigure callback
            dynReconfigServer.setCallback(boost::bind(&MatcherNodelet::dynamicReconfigureCallback, this, _1, _2));
            
            
            // set up services
            srvClear = private_nh.advertiseService("clear_models", &MatcherNodelet::srvClearModelsCallback, this);
        }


//////////////////////////// WORKER MEMBER FUNCTIONS ////////////////////////////////////////
        
        /**
         * @brief publish an array of poses via the specified publisher
         * @param pub the publisher to use
         * @param poseArray vector with pointers to poses
         * @param modelName name or index of model
         * @param poseType e.g. "clustered", "final"
         * @param sceneHeader header of scene the model was matched in
         */
        void publishPoseArray(ros::Publisher pub, std::vector<Pose*> poseArray,
							  std::string modelName, std::string poseType,
							  std_msgs::Header& sceneHeader){
            
            pf_matching_core::WeightedPoseArray::Ptr msgOut(new pf_matching_core::WeightedPoseArray);
            
            // copy over meta data
            msgOut->poseType = poseType;
            msgOut->sceneHeader = sceneHeader;
            msgOut->model = modelName;
            
            // copy over poses to the message
            for(Pose* pose : poseArray){
                
                pf_matching_core::WeightedPose tmpPose;
                
                tmpPose.weight = pose->getWeight();
                
                tmpPose.position.x = pose->getTranslationPart().x();
                tmpPose.position.y = pose->getTranslationPart().y();
                tmpPose.position.z = pose->getTranslationPart().z();
                
                tmpPose.orientation.x = pose->getRotationPart().x();
                tmpPose.orientation.y = pose->getRotationPart().y();
                tmpPose.orientation.z = pose->getRotationPart().z();
                tmpPose.orientation.w = pose->getRotationPart().w();

                PoseCluster* poseCluster = dynamic_cast<PoseCluster*>(pose);
                if (poseCluster){ // a pose cluster was submitted to be published
                	tmpPose.votingBall = poseCluster->getVotingBallIdx();  // =0 if no VB was used, otherwise =1 or =2
                }
                else{  // a raw pose was submitted to be published
                	tmpPose.votingBall = -1; // raw poses do not carry information on their voting-ball membership
                }
                msgOut->poses.push_back(tmpPose);
            }
            
            // publish the message
            pub.publish(msgOut);
        }
        
        /**
         * @brief overload with vector of poses as argument (no pointers)
         */
        template<typename PoseT>
		void publishPoseArray(ros::Publisher pub, std::vector<PoseT> poseArray,
							  std::string modelName, std::string poseType,
							  std_msgs::Header& sceneHeader){

			pf_matching_core::WeightedPoseArray::Ptr msgOut(new pf_matching_core::WeightedPoseArray);

			// copy over meta data
			msgOut->poseType = poseType;
			msgOut->sceneHeader = sceneHeader;
			msgOut->model = modelName;

			// copy over poses to the message
			for(PoseT& pose : poseArray){

				pf_matching_core::WeightedPose tmpPose;

				tmpPose.weight = pose.getWeight();

				tmpPose.position.x = pose.getTranslationPart().x();
				tmpPose.position.y = pose.getTranslationPart().y();
				tmpPose.position.z = pose.getTranslationPart().z();

				tmpPose.orientation.x = pose.getRotationPart().x();
				tmpPose.orientation.y = pose.getRotationPart().y();
				tmpPose.orientation.z = pose.getRotationPart().z();
				tmpPose.orientation.w = pose.getRotationPart().w();

				PoseCluster* poseCluster = dynamic_cast<PoseCluster*>(&pose);
				if (poseCluster){ // a pose cluster was submitted to be published
					tmpPose.votingBall = poseCluster->getVotingBallIdx(); // =0 if no VB was used, otherwise =1 or =2
				}
				else{ // a raw pose was submitted to be published
					tmpPose.votingBall = -1; // raw poses do not carry information on their voting-ball membership
				}

				msgOut->poses.push_back(tmpPose);
			}

			// publish the message
			pub.publish(msgOut);
		}

        /**
         * @brief take a ROS cloud, convert it and store it
         * @param msg_in received ROS message
         * @param storage location to save cloud data to
         */
        void storeInputCloud(const sensor_msgs::PointCloud2& msg_in, internalCloudType::Ptr& storage){
            
            // extract message content to local storage
            internalCloudType::Ptr cloudInPtr(new internalCloudType);
            pcl::fromROSMsg(msg_in, *cloudInPtr);
            storage = cloudInPtr;
        }
        
        /**
         * @brief check if all requirements for constructing a model are given and if so, make a model
         *        and add it to the internal storage.
         */
        void makeModel(float modelDiameter){
            
            featureType featuresToBuild = featuresToMatch;

            NODELET_INFO_STREAM("Creating a new model");
             
            // if only S2S features should be used for rotational symmetry detection, make sure
            // that S2S feature are also built
            if(config.use_rotational_symmetry && !config.use_all_available_features_for_rs){
                featuresToBuild = featuresToBuild | S2S;
                NODELET_INFO_STREAM("Forcing S2S features to be built for rotational symmetry analysis.");
            }
            
            
            // check that we do not use uninitialized parameters
            if(!got_initial_config){
                NODELET_ERROR("Did not get a set of dynamic reconfigure parameters. Not making model.");
            }
            
            // figure out which clouds to use
            bool useSurfaceCloud = featureRequiresSurfaceCloud(featuresToBuild);
            bool useEdgeCloud    = featureRequiresEdgeCloud(featuresToBuild);
            
            // check whether all requirements to construct a new model are fulfilled
            if(useSurfaceCloud && modelSurfaceCloudIn->points.size() == 0){
                NODELET_ERROR("Set features require a surface cloud, but the received model surface"\
                              " cloud is empty. Not making model.");
                return;
            }
            if(useEdgeCloud && modelEdgeCloudIn->points.size() == 0){
                NODELET_ERROR("Set features require an edge cloud, but the received model edge"\
                              " cloud is empty. Not making model.");
                return;
            }
            
            // test unlikely case that feature type to use is set wrongly
            if(featuresToBuild == NONE){
                NODELET_ERROR("No features set for model creation. Doing nothing.");
            }

            // set up a new model
            bTimer.start();
            NODELET_INFO_STREAM("Building hash table(s)...");
            PFmodel<internalPointType>::Ptr modelPtr(new PFmodel<internalPointType>);

            // check the plausibility of the config-values and set them
			modelPtr->setParameters(config.d_dist,
									config.d_VisCon,
									config.voxelSize_intersectDetect,    	//!< MFZ 12/09/2017 added
									config.ignoreFactor_intersectClassific, //!< MFZ 12/09/2017 added
									config.gapSizeFactor_allCases_intersectClassific,		//!< MFZ 12/04/2018 changed
									config.gapSizeFactor_surfaceCase_intersectClassific, 	//!< MFZ 12/04/2018 added
									config.alongSurfaceThreshold_intersectClassific,		//!< MFZ 11/10/2017 added
									config.othorgonalToSurfaceThreshold_intersectClassific,	//!< MFZ 11/10/2017 added
									config.d_angle_in_pi * M_PI,
									config.d_dist_is_relative,
									config.d_VisCon_is_relative,		 		//!< MFZ 12/09/2017 added
									config.advancedIntersectionClassification,  //!< MFZ 11/10/2017 added
									config.visualizeVisibilityContextFeature,	//!< MFZ 11/10/2017 added
									config.use_neighbour_PPFs_for_training,		//!< MFZ 14/03/2018 added
									static_cast<modelHashTableType>(config.model_hash_table_type), //!< MFZ 23/05/2018 added
									modelDiameter); 							//!< MFZ 02/09/2018 added


            if(useSurfaceCloud){
                modelPtr->setSurfaceCloud(modelSurfaceCloudIn);
            }
            if(useEdgeCloud){
                modelPtr->setEdgeCloud(modelEdgeCloudIn);
            }
            modelPtr->setHTmaxLoadFactor(1);    // note: currently fixed value, does not seem to have any effect

            // construct the model and add it to the stored models
            bool constructOK = modelPtr->makeHashTables(featuresToBuild); 
            if(!constructOK){
                NODELET_ERROR("Hash table construction for model creation failed.");
                return;
            }

            models.push_back(modelPtr);

            std::string modelName;
          	modelName = modelPtr->hashTables[0]->refPointCloudPtr->header.frame_id;

            unsigned int modelIndex = models.size() -1;
            
            // analyze the model for discrete rotational symmetries
            if(config.use_rotational_symmetry){
                analyzeModelRotationalSymmetry(modelPtr, modelName);
            }

            // weight the model
            weightModel(modelPtr);
            float model_creation_time = bTimer.elapsed().wall / 1e6;

#ifdef write_times_to_file
            // 26.09.2018 store creation times to compare STL and CMPH model hash table:
			matcher.writeToFile("model " + modelName + " created in: " + std::to_string(model_creation_time) + " ms, Model_HT-type: " +
								std::to_string(static_cast<modelHashTableType>(config.model_hash_table_type)),
								"model_creation_times.txt");
#endif

            NODELET_INFO_STREAM("New model " << modelIndex << " (" << modelName << ")"
                             << " successfully created in " << model_creation_time << " ms.");

            publishStatistics("create_time_ms", model_creation_time, config.publish_statistics,
                              modelName);
            publishStatistics("model_diameter", modelPtr->modelDiameter, config.publish_statistics,
                              modelName);
            publishStatistics("d_dist_abs", modelPtr->d_dist_abs, config.publish_statistics,
                              modelName);

            // erase used point clouds
            modelSurfaceCloudIn = NULL;
            modelEdgeCloudIn = NULL;
        }
        
        /**
         * @brief check if all requirements for matching models in a scene are given and if so, do it
         *        for all stored models
         */
        void matchInScene(){
            
            // check that we do not use uninitialized parameters
            if(!got_initial_config){
                NODELET_ERROR("Did not get a set of dynamic reconfigure parameters. Not matching.");
                return;
            }
            
            // figure out which clouds to use
            bool useSurfaceCloud = featureRequiresSurfaceCloud(featuresToMatch);
            bool useEdgeCloud    = featureRequiresEdgeCloud(featuresToMatch);
            
            // check whether all requirements to construct a new model are fulfilled
            if(useSurfaceCloud && sceneSurfaceCloudIn->points.size() == 0){
                NODELET_ERROR("Set features require a surface cloud, but the received scene surface"\
                              " cloud is empty. Not matching.");
                return;
            }
            if(useEdgeCloud && sceneEdgeCloudIn->points.size() == 0){
                NODELET_ERROR("Set features require an edge cloud, but the received scene edge"\
                              " cloud is empty. Not matching.");
                return;
            }
            
            // test unlikely case that feature type to use is set wrongly
            if(featuresToMatch == NONE){
                NODELET_ERROR("No features set for model creation. Doing nothing.");
                return;
            }
            
            // print a warning message if the matching will take a long time
            const unsigned int nPointsWarn = 2000;
            if(useSurfaceCloud && sceneSurfaceCloudIn->points.size() > nPointsWarn){
                NODELET_WARN_STREAM("Scene surface cloud contains "
                                    << sceneSurfaceCloudIn->points.size()
                                    << " points. Matching will probably take a long time.");
            }
            if(useEdgeCloud && sceneEdgeCloudIn->points.size() > nPointsWarn){
                NODELET_WARN_STREAM("Scene edge cloud contains "
                                    << sceneEdgeCloudIn->points.size()
                                    << " points. Matching will probably take a long time.");
            }
            
            // find the header to use for scene header information
            std_msgs::Header sceneHeader;
            if(useSurfaceCloud){
                sceneHeader = pcl_conversions::fromPCL(sceneSurfaceCloudIn->header);
            }
            else{
                sceneHeader = pcl_conversions::fromPCL(sceneEdgeCloudIn->header);
            }
            
            // check that there is at least one model for matching
            if(models.empty()){
                NODELET_ERROR("Got a scene but no models for matching. Doing Nothing.");
                return;
            }
            
            matcher.setScene(sceneSurfaceCloudIn, sceneEdgeCloudIn);
            
            NODELET_INFO("Matching models to scene.");
            
            // storage for best poses for visualization
            std::vector<Pose> visualizationPoses;

            visualizationPoses.push_back(Pose()); // TODO MFZ 19.04.2018 (*) no idea why xaver pushed an identity-pose at the front of the visualization vector

            unsigned int poseSum = 0;

            // match each model to the scene in turn
            for(unsigned int i = 0; i < models.size(); i++){
                
                // set up the matcher for this model
            	std::string modelName;
                matcher.setModel(models[i]);
                modelName = models[i]->hashTables[0]->refPointCloudPtr->header.frame_id;
                
                if(!config.HS_save_dir.empty()){
                    // set directory for hough space image saving
                    std::stringstream fnameStream;
                    fnameStream << config.HS_save_dir << "/" << ros::Time::now().toSec() << "/"
                                << "model_" << i << "_" << modelName ;
                    matcher.setMiscParameters(fnameStream.str());
                }
                else{
                    // no saving of Hough space images
                    matcher.setMiscParameters("");
                }
                
                // do the matching
                NODELET_INFO_STREAM("Model " << i << " (" << modelName << ")...");
                bTimer.start();

                matcher.match(featuresToMatch);
                counter ++;

                float msElapsed = bTimer.elapsed().wall / 1e6;
                NODELET_INFO_STREAM("Done. Took " << msElapsed << "ms");
                
                // get the n best poses
                NODELET_DEBUG_STREAM("Publishing the best " << config.publish_n_best_poses << " poses...");
                std::vector<Pose*> poses;
				if (!config.use_hypothesis_verification_with_visibility_context){
	                // get the n best poses, regardless of it's belonging to a certain voting ball
					poses = matcher.getBestPosesOfAllVotingBalls(config.publish_n_best_poses); // returns the n best poses of all voting balls (sorted)
				}
				else{
					// hinterstoisser-extensions-case
	                // get the n best poses of each voting ball (for input to verifier nodelet)
					std::vector<std::vector<Pose*>> temp = matcher.getBestPosesOfEachVotingBall(config.publish_n_best_poses);
					for (int votingBall = 0; votingBall < temp.size(); votingBall++ ){
						poses.insert(poses.end(), temp[votingBall].begin(), temp[votingBall].end());
					}
					std::sort(poses.begin(), poses.end(), [](const Pose* a, const Pose* b){return *a > *b;});
				}
				poseSum += poses.size();

#ifdef write_counters_to_file
                // TODO: remove after debugging:
                std::string numberOfRawPoses;
                std::string numberOfClusters;
                if (matcher.useVotingBalls){
                	numberOfRawPoses = std::to_string(matcher.rawPoses.at(0).size()) + "&" + std::to_string(matcher.rawPoses.at(1).size());
                	numberOfClusters = std::to_string(matcher.poseClusters.at(0).size()) + "&" + std::to_string(matcher.poseClusters.at(1).size());
                }
                else{
                	numberOfRawPoses = std::to_string(matcher.rawPoses.at(0).size());
                	numberOfClusters = std::to_string(matcher.poseClusters.at(0).size());
                }
#endif

                if(poses.empty()){
                    NODELET_ERROR("Got no poses for this model after matching, something is wrong.");
#ifdef write_counters_to_file
                    // 11.06.2018 debugging to find buggy Extension-Combinations
                    unsigned int htType = models[i]->hashTables[0]->getHashTableType() ;
                    bool useNeighbourPPFsForTraining__ = models[i]->hashTables[0]->get_useNeighbourPPFsForTraining();
                    matcher.writeToFile(std::to_string(counter) + " "
										+ std::to_string(htType) + " "
										+ std::to_string(useNeighbourPPFsForTraining__) + " "
										+ std::to_string(matcher.useVotingBalls) + " "
										+ std::to_string(matcher.useHinterstoisserClustering) + " "
										+ std::to_string(matcher.voteForAdjacentRotationAngles) + " "
										+ std::to_string(matcher.faHashTableType)
										+ " False "  // bad result
										+ std::to_string(matcher.validPFcounter) + " "
										+ std::to_string(matcher.invalidPFcounter) + " "
										+ std::to_string(matcher.overFlowCounter) + " "
										+ numberOfRawPoses + " "
										+ numberOfClusters,
										"find_buggy_configs.txt");
#endif
                    continue;
                }

                // publish the n best poses
                publishPoseArray(pubPoses, poses, modelName, std::string("final"), sceneHeader);

#ifdef write_counters_to_file
                // 11.06.2018 debugging to find buggy Extension-Combinations
                unsigned int htType = models[i]->hashTables[0]->getHashTableType() ;
                bool useNeighbourPPFsForTraining__ = models[i]->hashTables[0]->get_useNeighbourPPFsForTraining();
				matcher.writeToFile(std::to_string(counter) + " "
									+ std::to_string(htType) + " "
									+ std::to_string(useNeighbourPPFsForTraining__) + " "
									+ std::to_string(matcher.useVotingBalls) + " "
									+ std::to_string(matcher.useHinterstoisserClustering) + " "
									+ std::to_string(matcher.voteForAdjacentRotationAngles) + " "
									+ std::to_string(matcher.faHashTableType)
									+ " True "   // good result
									+ std::to_string(matcher.validPFcounter) + " "
									+ std::to_string(matcher.invalidPFcounter) + " "
									+ std::to_string(matcher.overFlowCounter) + " "
									+ numberOfRawPoses + " "
									+ numberOfClusters,
									"find_buggy_configs.txt");
				// TODO: remove after debugging:
                matcher.validPFcounter = 0; matcher.invalidPFcounter = 0; matcher.overFlowCounter = 0;
#endif
                // TODO: 24.04.2018 write all best poses to file, for debugging
                //matcher.writePosesToFile(poses, "best_poses.txt");

//                // TODO: remove after debugging:
//                unsigned int htType = models[i]->hashTables[0]->getHashTableType() ;
//				bool useNeighbourPPFsForTraining__ = models[i]->hashTables[0]->get_useNeighbourPPFsForTraining();
//                matcher.writeToFile(std::to_string(counter) + " "
//									+ std::to_string(htType) + " "
//									+ std::to_string(useNeighbourPPFsForTraining__) + " "
//									+ std::to_string(matcher.useVotingBalls) + " "
//									+ std::to_string(matcher.useHinterstoisserClustering) + " "
//									+ std::to_string(matcher.voteForAdjacentRotationAngles) + " "
//									+ std::to_string(matcher.faHashTableType)
//									+ " True "   // good result
//									+ numberOfRawPoses + " "
//									+ numberOfClusters,
//									"find_buggy_configs.txt");

                // save the pose for visualization if enabled
                if(config.show_results){
						visualizationPoses.push_back(*poses[0]);
                }
                
                // publish all clustered poses of each voting ball
				if(config.publish_clustered_poses){
					std::vector<std::vector<Pose*>> allPoses = matcher.getBestPosesOfEachVotingBall(0);
					for (int votingBall = 0; votingBall < allPoses.size(); votingBall ++){
						std::stringstream temp;
						temp << "Clustered_Poses_of_Voting_Ball_" << votingBall << "_";
						std::string poseType = temp.str();
						publishPoseArray(pubIntermediatePoses, allPoses[votingBall], modelName,
										 poseType, sceneHeader);
#ifdef write_pose_clusters_to_file
						std::time_t timestamp = std::time(0);
						std::string filename;
						std::string cloudname = models[i]->hashTables[0]->refPointCloudPtr->header.frame_id + "_" + sceneSurfaceCloudIn->header.frame_id + "_";
						int hashTableType = models[i]->hashTables[0]->getHashTableType();
						if( ( hashTableType == 1) || (hashTableType == 2) ){
							filename = "MY_SYSTEM_STL_HT_pose_clusters_of_" + cloudname;
						}
						if( (hashTableType == 3) || (hashTableType == 4) ){
							filename = "MY_SYSTEM_CMPH_HT_pose_clusters_of_" + cloudname;
						}
						// TODO: 24.04.2018 write all pose clusters to file, for debugging
						matcher.writePosesToFile(allPoses[votingBall], filename + std::to_string(timestamp) + ".txt");
#endif
					}
				}
                
#ifdef write_times_to_file
				// 26.09.2018 store matching times to compare STL and CMPH model hash table:
				matcher.writeToFile("model " + modelName + " matched to scene " + sceneHeader.frame_id + " in: "
									+ std::to_string(msElapsed) + " ms. Whereof Voting time with voxelgrid building = "
									+ std::to_string(matcher.msElapesdWithVoxelGridForVotingBallBuilding) + " ms. Voting Time with flag array building = "
									+ std::to_string(matcher.msElapsedWithFlagArrayBuilding) + " ms. Voting Time without flag array building = "
									+ std::to_string(matcher.msElapsedWithoutFlagArrayBuilding) + " ms. Clustering Time = "
									+ std::to_string(msElapsed - matcher.msElapesdWithVoxelGridForVotingBallBuilding) + " ms. Model_HT-type: " +
									std::to_string(static_cast<modelHashTableType>(config.model_hash_table_type)),
									"model_creation_times.txt");
#endif

                // statistics
                publishStatistics("match_time_ms", msElapsed,
                                  config.publish_statistics, modelName);
                publishStatistics("voting_time_ms_incl_voting_ball_voxel_grid_building",
                				  matcher.msElapesdWithVoxelGridForVotingBallBuilding,
                				  config.publish_statistics, modelName);
                publishStatistics("voting_time_ms_incl_flag_array_building",
								  matcher.msElapsedWithFlagArrayBuilding,
								  config.publish_statistics, modelName);
                publishStatistics("voting_time_ms",
								  matcher.msElapsedWithoutFlagArrayBuilding,
								  config.publish_statistics, modelName);
                publishStatisticsArray("n_raw_poses", matcher.getNumberOfRawPoses(),
                                  config.publish_statistics, modelName);
                publishStatisticsArray("n_pose_clusters", matcher.getNumberOfPoseClusters(),
                                  config.publish_statistics, modelName);
                publishStatisticsArray("raw_poses_median_weight", matcher.getRawPosesMeanWeight(),
                                  config.publish_statistics, modelName);
                publishStatisticsArray("raw_poses_max_weight", matcher.getRawPosesMaxWeight(),
                                  config.publish_statistics, modelName);
                publishStatistics("n_model_voxels", models[i]->numberOfOccupiedVoxels,
                				  config.publish_statistics, modelName);
                publishStatistics("n_model_PFs", models[i]->hashTables[0]->getNumberOfModelPFs(),
                				  config.publish_statistics, modelName);
                publishStatistics("n_unique_model_PFs", models[i]->hashTables[0]->getNumberOfUniqueModelPFs(),
                		 	 	  config.publish_statistics, modelName);
                
                std::vector<std::vector<float>> poseClusterWeights = matcher.getPoseClusterWeights();
                if(matcher.useVotingBalls){
					for (int votingBall = 0; votingBall < poseClusterWeights.size(); votingBall++){
						publishStatisticsArray("pose_cluster_weights_for_voting_ball_" + std::to_string(votingBall+1), poseClusterWeights[votingBall],
											   config.publish_pose_cluster_weights, modelName);
					}
                }
                else{
                	publishStatisticsArray("pose_cluster_weights_for_voting_ball_0", poseClusterWeights[0],
                						   config.publish_pose_cluster_weights, modelName);
                }

                // delete raw poses and pose clusters
                matcher.freeMemory();
            } // end of loop: match each model to the scene in turn
            
            publishStatistics("pose_hypotheses_sum", poseSum, config.publish_statistics);

            publishStatistics("matched_models", models.size(), config.publish_statistics);
            
            NODELET_INFO("Matching done.");
            
            // visualization in separate thread if requested
            if(config.show_results){
                
                // storage for information to visualize
                std::vector<internalCloudType::ConstPtr> visualizationCloudPtrs;
                std::vector<std::string> visualizationNames;
                
                // add scene information
                if(useSurfaceCloud){
                    visualizationCloudPtrs.push_back(sceneSurfaceCloudIn);
                }
                else{
                    visualizationCloudPtrs.push_back(sceneEdgeCloudIn);
                }
                visualizationNames.push_back(sceneHeader.frame_id);
                
                // add information for each model
                for(unsigned int i = 0; i < models.size(); i++){
                    internalCloudType::ConstPtr displayCloud;
                    if(useSurfaceCloud){
                        displayCloud = models[i]->surfaceCloudPtr;
                    }
                    else{
                        displayCloud = models[i]->edgeCloudPtr;
                    }
                    visualizationCloudPtrs.push_back(displayCloud);
                    visualizationNames.push_back(displayCloud->header.frame_id);
                }
                
                // close down old visualization thread
                visualizationThread.interrupt();
                visualizationThread.join();
                
                // path for png saving (or leave empty to save nothing)
                std::string screenshot_path("");
                if(!config.results_screenshot_dir.empty()){
                    screenshot_path = config.results_screenshot_dir + "/" +
                                      std::to_string(ros::Time::now().toSec()) +
                                      sceneHeader.frame_id + ".png";
                }
                
                // start new visualization thread
                std::string title = "matching results of " + ros::this_node::getName() + " @ " 
                                    + std::to_string(ros::Time::now().toSec()) + " s (ROS-time)";
                visualizationThread = boost::thread(visualization::visualizeClouds<internalPointType>,
                                                    visualizationCloudPtrs,
                                                    visualizationPoses,
                                                    visualizationNames,
                                                    title,
                                                    screenshot_path);
            }
        }
        
    ///////////////////////// CALLBACK FUNCTIONS ///////////////////////////////////////////////

        /**
         * @brief callback for changing dynamic reconfigure parameters, copies the parameters in
         * the appropriate place.
         * @param config_in the new configuration
         * @param level bitfield specifying which parameters changed
         */
        void dynamicReconfigureCallback(pf_matching_core::matcherConfig &config_in, uint32_t level){
        
            NODELET_DEBUG_STREAM("Got new dynamic reconfigure configuration.");
            
            // note: setting the matcher parameters here directly should be save as long as no
            // asyncronous spinner is used and all callbacks are processed serially
            matcher.setMatchingParameters(config_in.d_alpha_in_pi * M_PI,
                                          config_in.refPointStep,
                                          config_in.maxThresh,
                                          config_in.clusterTransThreshRelative,
                                          config_in.clusterRotThresh_in_pi * M_PI,
										  config_in.use_neighbour_PPFs_for_matching,
										  config_in.use_voting_balls,
										  config_in.use_hinterstoisser_clustering,
										  config_in.use_hypothesis_verification_with_visibility_context,
										  config_in.vote_for_adjacent_rotation_angles,
										  static_cast<flagArrayHashTableType>(config_in.flag_array_hash_table_type),
										  config_in.flag_array_quantization_steps);
            
            // expand paths and create (will immediately report back to rqt_reconfigure GUI)
            config_in.HS_save_dir            = tools::formatPathAndCreate(config_in.HS_save_dir);
            config_in.results_screenshot_dir = tools::formatPathAndCreate(config_in.results_screenshot_dir);
            
            // assemble features to match
            featuresToMatch = NONE;
            if(config_in.match_S2S){featuresToMatch = featuresToMatch | S2S;}
            if(config_in.match_B2B){featuresToMatch = featuresToMatch | B2B;}
            if(config_in.match_S2B){featuresToMatch = featuresToMatch | S2B;}
            if(config_in.match_B2S){featuresToMatch = featuresToMatch | B2S;}
            if(config_in.match_S2SVisCon){featuresToMatch = featuresToMatch | S2SVisCon;} //!< MFZ 12/09/2017 S2SVisCon added
            
            // copy parameters and remember init
            config = config_in;
            got_initial_config = true;
        }
        
        /**
         * @brief store the message content and try to make a model from it
         * @param msg_in ROS point cloud message
         */
        void modelCloudsInCallback(const pf_matching_core::ProcessedClouds::ConstPtr& msg_in){
            NODELET_DEBUG("got model clouds");
            storeInputCloud(msg_in->surface, modelSurfaceCloudIn);
            storeInputCloud(msg_in->edges,   modelEdgeCloudIn);
            float modelDiameter = msg_in->cloudDiameter;
//            std::cout << "model diameter = " << modelDiameter << std::endl;
            makeModel(modelDiameter);
        }
        
        /**
         * @brief store the message content and try to match models to the received scene
         * @param msg_in ROS point cloud message
         */
        void sceneCloudsInCallback(const pf_matching_core::ProcessedClouds::ConstPtr& msg_in){
            NODELET_DEBUG("got scene edge cloud");
            storeInputCloud(msg_in->surface, sceneSurfaceCloudIn);
            storeInputCloud(msg_in->edges,   sceneEdgeCloudIn);
            matchInScene();
        }
        
        /**
         * @brief service callback to delete all stored models
         * @param request (empty)
         * @param response (empty)
         * @return (always true)
         */
        bool srvClearModelsCallback(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response){
            NODELET_INFO_STREAM("Got service request to clear stored models. Deleting " 
                             << models.size() << " models.");
            models.clear();
            return true;
        }

        /**
         * @brief analyze the created model for discrete rotational symmetries
         * @param modelPtr the model to analyze
         * @param modelName name of the model
         */
        void analyzeModelRotationalSymmetry(PFmodel<internalPointType>::Ptr modelPtr,
                                            std::string modelName){
            
            featureType featuresForRS;
            if(config.use_all_available_features_for_rs){
                featuresForRS = ALL_FEATURES; // does NOT include visibility context features
            }
            else{
                featuresForRS = S2S;
            }
                                                
            // analyze the model
            NODELET_INFO("Analyzing for rotational symmetry...");
            extensions::ModelSymmetryAnalyzer<internalPointType> symAnalyzer;
            symAnalyzer.setModel(modelPtr);
            symAnalyzer.findEquivalentPoses(matcher,
                                            config.rs_pose_weight_thresh,
                                            config.rs_maxThresh,
                                            featuresForRS);
            int nEquivalentPoses =  modelPtr->equivalentPoses.size();
            
            NODELET_INFO_STREAM("Found " << nEquivalentPoses << " equivalent poses.");
                                                                     
            // reduce model
            if(config.collapse_symmetric_models){
                symAnalyzer.collapseRefPoints();
            }
            
            // statistics
            publishStatistics("n_equivalent_poses", nEquivalentPoses, config.publish_statistics,
                              modelName);
            
            // publish equivalent poses if any were found
            if(config.publish_equivalent_poses && modelPtr->equivalentPoses.size() > 0){
                std_msgs::Header header;
                header.frame_id = modelName;
                header.stamp = ros::Time::now();
                publishPoseArray(pubIntermediatePoses, modelPtr->equivalentPoses, modelName,
                                 "equivalent", header);
            }
        }

        /**
         * @brief weight the features in a model's hash table
         * @param modelPtr model to weight
         */
        void weightModel(PFmodel<internalPointType>::Ptr& modelPtr){

            extensions::ModelWeighter<internalPointType> weighter;
            weighter.setModel(modelPtr);
            
            if(config.discard_features_by_frequency){
                weighter.discardByFrequency(config.frequency_discarding_remaining_ratio);
            }
            
            if(config.discard_features_by_distance){
                weighter.discardByDistance(config.distance_discarding_thresh);
            }
            
            if(config.weight_features_by_distance){
                weighter.weightByDistance(config.distance_weighting_power);
            }
            
            if(config.weight_features_by_frequency){
                weighter.weightByFrequency(config.frequency_weighting_power);
            }
            
            extensions::ModelWeighter<internalPointType>::typeWeightVector featureWeights;
            featureWeights.push_back(extensions::ModelWeighter<internalPointType>::typeWeightPair(S2S, config.S2S_weight));
            featureWeights.push_back(extensions::ModelWeighter<internalPointType>::typeWeightPair(B2B, config.B2B_weight));
            featureWeights.push_back(extensions::ModelWeighter<internalPointType>::typeWeightPair(S2B, config.S2B_weight));
            featureWeights.push_back(extensions::ModelWeighter<internalPointType>::typeWeightPair(B2S, config.B2S_weight));
            weighter.weightByFeatureType(featureWeights);
        }
        
    ////////////////////////// MEMBER VARIABLES /////////////////////////////////
        
        // ROS related
        ros::Publisher pubIntermediatePoses;
        ros::Publisher pubPoses;

        ros::Subscriber subModel;
        ros::Subscriber subScene;
        
        ros::ServiceServer srvClear;
        
        // dynamic reconfigure server
        dynamic_reconfigure::Server<pf_matching_core::matcherConfig> dynReconfigServer;
        pf_matching_core::matcherConfig config; //!< latest set of dynamic reconfigure parameters
        bool got_initial_config = false;
        
        // various others
        MatcherSettings settings;
        featureType featuresToMatch;
        
        // worker classes and storage
        PFmatcher<internalPointType> matcher;
        std::vector<PFmodel<internalPointType>::Ptr> models;
        
        internalCloudType::Ptr modelSurfaceCloudIn;
        internalCloudType::Ptr modelEdgeCloudIn;
        
        internalCloudType::Ptr sceneSurfaceCloudIn;
        internalCloudType::Ptr sceneEdgeCloudIn;
    
        // for statistics
        boost::timer::cpu_timer bTimer; //!< for measuring runtimes
        
        // for visualization
        boost::thread visualizationThread; //!< is in a "not-a-thread"-state after construction
        
};


} // end namespace
} // end namespace

// make nodelet known
PLUGINLIB_EXPORT_CLASS(pf_matching::ROS::MatcherNodelet, nodelet::Nodelet)
