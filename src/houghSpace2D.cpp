/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#include <houghSpace2D.hpp>
#include <cstring>   // for memset
#include <algorithm> // for lower bound
#include <limits.h>  // for USHRT_MAX


namespace pf_matching{

    
HoughBucket2D::HoughBucket2D(float accumulatedWeight_, unsigned int xIndex_, unsigned int yIndex_){
    accumulatedWeight =accumulatedWeight_;
    xIndex = xIndex_;
    yIndex = yIndex_;
}



HoughSpace2D::HoughSpace2D(unsigned int xSize_, unsigned int ySize_):
xSize(xSize_), ySize(ySize_)
{
    // create Hough space and initialize
    votingSpace = new float[xSize * ySize];
    zeroSpace();
}

HoughSpace2D::~HoughSpace2D(){
    delete [] votingSpace;
}

void HoughSpace2D::zeroSpace(){
    memset(votingSpace, 0, sizeof(votingSpace[0]) * xSize * ySize);
}

void HoughSpace2D::vote(float weight, unsigned int xIndex, unsigned int yIndex){
    votingSpace[index(xIndex, yIndex)] += weight;
}


std::vector<HoughBucket2D> HoughSpace2D::getMaxBuckets(float maxThresh){
    
    // storage for maxima
    std::vector<HoughBucket2D> maxBuckets; // sorted by accumulated weight from [0] = biggest to
                                       // end = smallest
    maxBuckets.emplace_back(HoughBucket2D(0, -1, -1)); // add one element with dummy values
        
        
    // iterate over Hough space and find maximum points
    for(int x = 0; x < xSize; x++){
        for(int y = 0; y < ySize; y++){
                
            // add point to list of maximum points if it is within the threshold distance
            // to the current global maximum (first entry of maxPoints)
            if(votingSpace[index(x, y)] >= maxThresh * maxBuckets[0].accumulatedWeight){
                    
                // find place in vector to insert element to create a descending order of elements in vector
                // produces reverse iterator, conversion to forward iterator via .base()-method
                auto insertIt = std::lower_bound(maxBuckets.rbegin(),
                                                 maxBuckets.rend(),
                                                 votingSpace[index(x, y)],
                                                 HoughBucket2D::CompareFloatField);
                    
                // insert
                auto insertedIt = maxBuckets.emplace(insertIt.base(), votingSpace[index(x, y)], x, y);
                
                // if point was inserted as first element, we can cut off all values not
                // within the new threshold range at the end of the vector
                if(insertedIt == maxBuckets.begin()){
                        
                    // find first item to delete, produces reverse iterator
                    auto deleteIt = std::lower_bound(maxBuckets.rbegin(),
                                                     maxBuckets.rend(),
                                                     maxThresh * maxBuckets[0].accumulatedWeight,
                                                     HoughBucket2D::CompareFloatField);

                    // delete points within [deletIt.base()  end)
                    maxBuckets.erase(deleteIt.base(), maxBuckets.end());
                }
            }
        } // loop y
    } // loop x
    
    // if the dummy element with weight 0 is still the biggest, there are no maxima,
    // and we return 0 instead of the buckets, else we return the maxima
    if(maxBuckets[0].accumulatedWeight == 0){
        std::vector<HoughBucket2D> empty;
        return empty;
    }
    else{
        return maxBuckets;
    }
}


const float HoughSpace2D::getBucketValue(const float xIndex_, const float yIndex_){
    return votingSpace[index(xIndex_, yIndex_)];
}


std::vector<unsigned short> HoughSpace2D::getAsUShortImg(float thresholdFactor){
    

    // storage for image
    std::vector<unsigned short> img;
    
    // get the global max number of votes
    std::vector<HoughBucket2D> maxBuckets = getMaxBuckets(1.0);
    float maxVotes;
    if(maxBuckets.size() == 0){
        maxVotes = 0;
    }
    else{
        maxVotes = maxBuckets[0].accumulatedWeight;
    }

    if(maxVotes > 0){
        
        // convert Hough space data and save in image
        // data is normalized with the global max
        for(unsigned int i = 0; i < xSize * ySize; i++){
            
            if(votingSpace[i] >= maxVotes * thresholdFactor){
                img.push_back(votingSpace[i] / maxVotes * USHRT_MAX);
            }
            else{
                img.push_back(0);
            }
        }
    }
    else{
        img.resize(xSize * ySize, 0);
    }

    return img;
}


size_t HoughSpace2D::index(int x, int y) const {
    
    return x + xSize * y;
}

} // end namespace
