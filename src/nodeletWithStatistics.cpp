/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#include <nodeletWithStatistics.hpp>

#include <pluginlib/class_list_macros.h> // for nodelets

namespace pf_matching{
namespace ROS{

void NodeletWithStatistics::publishStatistics(const std::string parameterName, const float value,
                                              const bool condition, const std::string specifier){
    if(condition){
        
        // create new message and fill it
        pf_matching_core::Statistics::Ptr msgPtr(new pf_matching_core::Statistics);
        msgPtr->value = value;
        msgPtr->parameter_name = parameterName;
        msgPtr->specifier = specifier;
        
        // publish
        pubStatistics.publish(msgPtr);
    }
}

void NodeletWithStatistics::publishStatisticsArray(const std::string parameterName, const std::vector<float>& values,
                                                   const bool condition, const std::string specifier){
    if(condition){
        
        // create new message and fill it
        pf_matching_core::StatisticsArray::Ptr msgPtr(new pf_matching_core::StatisticsArray);
        msgPtr->parameter_name = parameterName;
        msgPtr->specifier = specifier;
        
        // copy over array values
        msgPtr->values.reserve(values.size());
        for(const float& f: values){
            msgPtr->values.push_back(f);
        }
        
        // publish
        pubStatisticsArray.publish(msgPtr);
    }
}

} // end namespace
} // end namespace
