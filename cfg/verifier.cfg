#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2017
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# import Python 3 functionality
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

# redefine functions for Python 3 like behavior
from future_builtins import *

# other imports
from dynamic_reconfigure.parameter_generator_catkin import *
import numpy as np

PACKAGE = "pf_matching_core"

gen = ParameterGenerator()

# in cpp, all names become lowercase
verifier_lvl = 0

# GENERAL
general = gen.add_group("general", type="collapse") # or none defaults to visible
pose_verification_method_enum = gen.enum([gen.const("ACCEPTANCE_FUNCTION" ,     int_t, 0, "use acceptance function from Papazov et al."),
#                                          gen.const("GLOBAL_HV",                int_t, 1, "use global hypothesis verification from Aldoma et al.")
                                          ],
                                         "possible pose verification methods")

general.add("verification_method",  int_t, verifier_lvl, "method to use for pose verification", 0, 0, 1, edit_method=pose_verification_method_enum)
general.add("d_dist",             double_t, verifier_lvl, "discretization step for feature distances, only affects models created after setting this", 0.05, 0.00)
general.add("d_dist_is_relative", bool_t, verifier_lvl, "determines if d_dist is relative to model diameter or an absolute value, only affects models created after setting this", True)


# Acceptance Function from Papazov et al.
afpg = gen.add_group("acceptance_function", type="collapse") # or none defaults to visible
afpg.add("supportthreshold", double_t, verifier_lvl, "visiblility threshold for pose verification of papazov et al", 0.1, 0, 1)
afpg.add("penaltythreshold", double_t, verifier_lvl, "penalty threshold for pose verification of papazov et al", 0.1, 0, 1)
afpg.add("searchradius", double_t, verifier_lvl, "radius arround point, in which a other point is an inlier", 1.5, 0)
afpg.add("searchradius_is_relative", bool_t, verifier_lvl, "determines if searchradius is relative to d_dist or an absolute value", True)
afpg.add("filter_selfocclusion", bool_t, verifier_lvl, "determines if zbuffering of the model by itself should be performed", True)
afpg.add("modelzbufferingthreshold", double_t, verifier_lvl, "zbuffering will be used to filter self occluded model points. This threshold describes how far a model point at least can be behind a model point to keep.", 1, 0)
afpg.add("scenezbufferingthreshold", double_t, verifier_lvl, "zbuffering threshold that describes, how far a modelpoint at least can be behind a scenepoint to keep.", 1.5, 0)
afpg.add("zbufferingthreshold_is_relative", bool_t, verifier_lvl, "determines if zbufferingthreshold is relative to d_dist or an absolute value", True)
afpg.add("zbufferingresolution", double_t, verifier_lvl, "resolution of the depth map created by zbufferin", 75, 0)

# Extension of acceptance function by verification of normals: a scenepoint is just described by a modelpoint if angle between normals is small enough
nvpg = gen.add_group("normal_verification", type="collapse")
nvpg.add("use_verification_of_normals", bool_t, verifier_lvl, "determinates if normal verification should be used by acceptance function", True)
nvpg.add("angleDiffThresh_in_pi", double_t, verifier_lvl, "maximum angle between normal of scenepoint and normal of modelpoint", 0.1, 0, 1) 

# Conflict Analysis of pose hypotheses passed the acceptance function
capg = gen.add_group("conflic_analysis", type="collapse")
capg.add("use_conflict_analysis", bool_t, verifier_lvl, "determiantes if conflict analysis is used", True)
capg.add("conflictthreshold", double_t, verifier_lvl, "determinates the percentage of modelpoints that need to intersect to classify two models als intersecting", 0.01, 0, 1)

# DEBUGGING and Information collecting
dpg = gen.add_group("debug_parameters", type="collapse") # or none defaults to visible
dpg.add("publish_statistics", bool_t, verifier_lvl, "publish information like processing times or number of points", True)
dpg.add("show_results", bool_t, verifier_lvl, "Show viewer window with matching results. Disable if performance is a concern! It will slow down matching!", False)
dpg.add("results_screenshot_dir", str_t, verifier_lvl, "Save screenshot of 'show_results' to this ABSOLUTE path. If empty, nothing will be saved.", "")
dpg.add("publish_mu_values", bool_t, verifier_lvl, "enables publishing of µ values to the pf_statistics_array-topic", False)
dpg.add("show_filtered_models_in_visualization", bool_t, verifier_lvl, "enables visualization of prefiltered (if selfocclusion filtering is enabled) and filtered model clouds", False)

# package name, name of node (for documentation only), config file name without extension
exit(gen.generate(PACKAGE, "dynamic_tutorials", "verifier"))
