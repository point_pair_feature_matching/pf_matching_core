/**
 * @copyright Copyright (c) 2019
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Authors: Markus Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @author Markus Ziegler
 * @file visibilityContextVisualization.hpp
 * @brief Functions for displaying the visibility context feature
 */

#pragma once

#include <pcl/point_cloud.h>
#include <string>

namespace pf_matching{
namespace visualization{

/**
 * @brief Function to switch Point-Formats.
 * @param EigenVector Point (x,y,z) to transform into pcl::PointT
 * @param PointTRef The transformed point as pcl::PointT
 */
template <typename PointNT>
void EigenVecToPCLPointT ( const Eigen::Vector3f& EigenVector,
					  	   PointNT& PointTRef);

/**
 * @brief Function to switch Point-Formats.
 * @param EigenVector Point (x,y,z) to transform into pcl::PointT
 * @return The transformed point as pcl::PointT
 */
template <typename PointNT>
PointNT EigenVecToPCLPointT ( const Eigen::Vector3f& EigenVector );

/**
 * @brief display a group of points (e.g. centers of intersected voxels) with a custom color
 * @note needs an instantiation of a PCLviewer
 * @param voxelCentersPtr point cloud (e.g. of intersected voxel centers)
 * @param fancy_viewer boost::shared pointer for instance of PCLVisualizer, which displays the point cloud
 * @param label for point
 * @param r red channel for point color
 * @param g green channel for point color
 * @param b blue channel for point color
 */
template <typename PointNT>
void drawIntersectionPoints(typename pcl::PointCloud<PointNT>::Ptr& voxelCentersPtr,
							const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
							const std::string& label,
							const int r,
							const int g,
							const int b);


//void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
//                            void* viewer_void);


template <typename PointNT>
boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizePointCloud(const typename pcl::PointCloud<PointNT>::ConstPtr cloud);


template <typename PointNT>
void visualizeOctreeCenterpoints( const boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer,
								  const typename pcl::PointCloud<PointNT>::ConstPtr& cloud);


/**
 * @brief displays the PPF
 * @note creates an instantiation of a PCLviewer
 * @note can also display the directions of V_out, but they are currently commented
 * @param cloud to display
 * @param p_r reference point of PPF
 * @param p_t referred point of PPF
 * @param n_r surface normal in reference point of PPF
 * @param n_t surface normal in referred point of PPF
 * @return boost::shared_ptr to the viewer
 */
template <typename PointNT>
boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizeVisConFeature(const typename pcl::PointCloud<PointNT>::ConstPtr cloud,
																			const Eigen::Vector3f& p_r,
																			const Eigen::Vector3f& p_t,
																			const Eigen::Vector3f& n_r,
																			const Eigen::Vector3f& n_t);

template <typename PointNT> void
visualizePCA(const boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer,
			 const Eigen::Vector4f& centroid,
			 const Eigen::Vector3f& eigenValues,
			 const Eigen::Matrix3f& eigenVectors,
			 const float &d_max,
			 const float &d_med,
			 const float &d_min);

/**
 * @brief visualize the V_out part of the PPF
 * @param fancy_viewer instantiation of a PCLVisualizer, that displays base cloud
 * @param p_t referred point of PPF
 * @param d direction of visibility context: V_out_0
 * @param delta direction of visibility context: V_out_1
 * @param minus_d direction of visibility context: V_out_2
 * @param minus_delta direction of visibility context: V_out_3
 * @param Vout
 */
void visualizeVout(const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
				   const Eigen::Vector3f& p_t,
				   const Eigen::Vector3f& d,
				   const Eigen::Vector3f& delta,
				   const Eigen::Vector3f& minus_d,
				   const Eigen::Vector3f& minus_delta,
				   const float Vout[4]);

} // end namespace
} // end namespace


#include "./impl/visibilityContextVisualizationImpl.hpp"
