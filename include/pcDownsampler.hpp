/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @authors Xaver Kroischke, Markus Franz Ziegler
 * @date 08.04.2019
 * @file pcDownsampler.hpp
 * @brief Functions and classes related to subsampling a point cloud.
 */
#pragma once

// PCL
#include <pcl/point_cloud.h>


namespace pf_matching{
namespace pre_processing{
    
/**
 * @class PCdownsampler
 * @brief collection of different downsampling algorithms for point clouds
 */
class PCdownsampler{
    public:
        /**
         * possible downsampling methods to set
         */
        enum DownsampleMethod{
            NONE = 0, //!< skip downsampling
            VOXEL_GRID, //!< use a voxel grid with resulting points at the center of mass of each voxel. suited for small point clouds
            UNIFORM_SPACE, //!< use a voxel grid with resulting points being the point closest to the center of each voxel. suited for all point cloud sizes
            OCTREE_VOXEL_GRID, //! use a octree based voxel grid with resulting points at the center of mass of each voxel. version for large point clouds
        };

        /**
         * @brief set which algorithm to use for downsampling
         * @param method_ method code from PCdownsampler::DownsampleMethod
         */
        bool setMethod(DownsampleMethod method_){
            method = method_;
        }
        
        /**
         * @brief set the desired distance between points after downsampling
         * @param pointDistances_ desired distance
         */
        void setPointDistances(float pointDistances_){
            pointDistances = pointDistances_;
        }
        
        /**
         * @brief downsample using the set parameters and save the result in the output
         * 
         * Depending on the method used, point fields other than xyz might be the mean of sourrounding
         * points (VOXEL_GRID) or the value belonging to the sampled points.
         * 
         * @param[in] cloudPtr shared pointer to input cloud
         * @param[out] cloudDownsampled shared pointer to output, must not be the same as cloudPtr
         * @param[in] deepCopyForNONE if true, do a deep copy of the input cloud into the output cloud,
         *            if false (default), set output pointer = input pointer to save time on big
         *            clouds
         */
        template <typename PointT> void
        downsample(const typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                         typename pcl::PointCloud<PointT>::Ptr& cloudDownsampled,
                   bool deepCopyForNONE = false);

        
    private:
        DownsampleMethod method = NONE;  //!< downsampling method to use
        float pointDistances = -1;       //!< approximate point distances
};
} // end namespace
} // end namespace

#include "./impl/pcDownsamplerImpl.hpp"
