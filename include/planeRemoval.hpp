/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 22/01/16
 * @file planeRemoval.hpp
 * @brief Function templates for removing planes from point clouds
 */
 
#pragma once

// PCL
#include <pcl/point_cloud.h>


namespace pf_matching{
namespace pre_processing{
    
/**
 * @brief remove the biggest plane from a point cloud via RANSAC fitting of a plane and
 *        returning the outliers
 * 
 * You can use input = output on this function.
 * 
 * @param cloudInPtr shared poitner to cloud to remove plane from
 * @param cloudOutPtr result, equal to cloudInPtr if no plane could be fitted
 * @param distanceTresh max. distance of point to plane for it to still belong to the plane
 */
template<typename PointT>
void removeBiggestPlane(typename pcl::PointCloud<PointT>::Ptr& cloudInPtr,
                        typename pcl::PointCloud<PointT>::Ptr& cloudOutPtr,
                        float distanceTresh);

} // end namespace
} // end namespace

#include "./impl/planeRemovalImpl.hpp"