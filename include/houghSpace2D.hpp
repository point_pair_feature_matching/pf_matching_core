/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Ziegler
 * @date 08.04.2019
 * @file houghSpace2D.hpp
 * @brief weighted 2D Hough Space and related classes for weighted voting
 */
 
#pragma once

#include <iostream>
#include <vector>
#include <cstdlib>

namespace pf_matching{

/**
 * @class HoughBucket2D
 * @brief a point in a 2D Hough space
 */
struct HoughBucket2D{
    float accumulatedWeight; //!< total weight accumulateed at this point
    unsigned int xIndex;     //!< index of the model reference point
    unsigned int yIndex;  	 //!< index encoding the alignment angle
    
    /**
     * @brief construct by explicitly setting vaules
     * @param accumulatedWeight_ see accumulatedWeight
     * @param xIndex_ see member xIndex
     * @param yIndex_ see member yIndex
     */
    HoughBucket2D(float accumulatedWeight_, unsigned int xIndex_, unsigned int yIndex_);

    /**
     * @brief console output
     */
    friend std::ostream &operator << (std::ostream &output, const HoughBucket2D& hp)
      { 
         output << "weight: " << hp.accumulatedWeight << " @ (x = " << hp.xIndex << ", y = " << hp.yIndex <<")";
         return output;            
      }
    
    /**
     * @class _CompareFloatField
     * @brief functor for comparing accumulatedWeight with float values
     *        See http://stackoverflow.com/a/2510345 for further reading.
     */
    static struct _CompareFloatField{
        bool operator() (const HoughBucket2D& hp, float value){
            return hp.accumulatedWeight < value;
        }
        bool operator() (float hp, const HoughBucket2D& value){
            return hp < value.accumulatedWeight;
        }
   } CompareFloatField; //!< instantiation, see _CompareFlaotField
                        
};

/**
 * @class HoughSpace2D
 * @brief 2 dimensional Hough space for weighted voting
 */
class HoughSpace2D{
    
    public:
    
        std::vector<HoughBucket2D> maxBuckets; //!< points with maximum weights
        const unsigned int xSize; //!< size of Hough space in first direction
        const unsigned int ySize; //!< size of Hough space in second direction
    
        /**
         * @brief constructor, already sets all buckets to zero
         * @param xSize_ number of buckets in first direction
         * @param ySize_ number of buckets in second direction
         */
        HoughSpace2D(unsigned int xSize_, unsigned int ySize_);
        
        /**
         * @brief destructor
         */
        ~HoughSpace2D();
        
        /**
         * @brief reset all buckets in the Hough space to zero
         */
        void zeroSpace();
        
        /**
         * @brief vote in the Hough space
         * 
         * @warning this function does not check wether any index is out of bounds
         * 
         * @param weight weight to be added to the bucket
         * @param xIndex first index of bucket
         * @param yIndex second index of bucket
         */
        void vote(float weight, unsigned int xIndex, unsigned int yIndex);
        
        //TODO: adding all neighboring values to a bucket for very noisy data -> via kernel filter
        // or integral image?
        
        /**
         * @brief get a vector with the buckets that received the highest accumulated votes
         * @param maxThresh threshold value to define maxima: all buckets with accumulated votes
         *        bigger than maxThresh * global maximum are considered maxima, maxThresh should be
         *        within (0, 1]
         * @return vector of buckets with highest accumulated votes, sorted by votes, element 0 is the
         *         biggest element; If the space is empty (all buckets have weight 0), an empty vector
         *         is returned.
         */
        std::vector<HoughBucket2D> getMaxBuckets(float maxThresh = 0.8);
        
        /**
         * @brief get to value of the specified bucket in the Hough space
         * @param xIndex_ first coordinate
         * @param yIndex_ second coordinate
         * @return bucket value
         */
        const float getBucketValue(const float xIndex_, const float yIndex_);
        
        /**
         * @brief get the Hough space as a 1D vector representation of a unsigned short image
         * 
         * The data is scaled so that the global maximum number of votes corresponds to a value
         * of USHRT_MAX.
         * The image is sized [xSize, ySize]
         * 
         * @param thresholdFactor only show the buckets with more than thresholdFactor * global maximum
         *                        in the image, should be between 0 (all buckets) and 1 (biggest maximum only)
         * @return The image data or an image of zeros if no maximum > 0 was present in the space
         */
        std::vector<unsigned short> getAsUShortImg(float thresholdFactor = 0);
        
    
    private:
        float* votingSpace; //!< the actual Hough space, stored as 1D continous block of memory
                            // use votingSpace(index(x, y)) for accessing the bucket at x and y
        
        /**
         * @brief calculate 1D index of internal storage from 2D coordinates of bucket
         * @param x first coordinate
         * @param y second coordinate
         * @return the 1D index
         */
        size_t index(int x, int y) const;
};

} // end namespace
