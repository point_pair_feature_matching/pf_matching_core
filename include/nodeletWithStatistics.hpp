/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 03/02/16
 * @file nodeletWithStatistics.hpp
 * @brief basic nodelet class for use with statistics topic
 */

#pragma once

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <pf_matching_core/Statistics.h>
#include <pf_matching_core/StatisticsArray.h>


namespace pf_matching{
namespace ROS{

/**
 * @class NodeletWithStatistics
 * @brief base class for nodelets publishing statistical information about their internal processing
 */
class NodeletWithStatistics : public nodelet::Nodelet{

    public:
        NodeletWithStatistics(){
            
        }
    
    protected:
        /**
         * @brief initialize the publisher for statistics output, must be done before using
         *        publishStatistics()!
         * @param nh public node handle
         * @param topicName ROS name of the statistics topic
         */
        void initStatisticsPublisher(ros::NodeHandle& nh, const std::string& topicName){
            pubStatistics = nh.advertise<pf_matching_core::Statistics>(topicName, 10, true);
        }
        
        /**
         * @brief initialize the publisher for statistics array output, must be done before using
         *        publishStatisticsArray()!
         * @param nh public node handle
         * @param topicName ROS name of the statistics array topic
         */
        void initStatisticsArrayPublisher(ros::NodeHandle& nh, const std::string& topicName){
            pubStatisticsArray = nh.advertise<pf_matching_core::StatisticsArray>(topicName, 10, true);
        }
        
        /**
         * @brief publish a statistics message with a single value
         * @param parameterName name of the statistical parameter
         * @param value value of the parameter
         * @param condition condition to check before sending, i.e. if condition == false, no
         *        message will be send
         * @param specifier additionally specifies that the parameter belongs to a certain sub-topic
         *                  in the statistics, e.g. the model number during matching; leave empty
         *                  (default) to make it a general statistics for this node
         */
        void publishStatistics(const std::string parameterName, const float value,
                               const bool condition, const std::string specifier = "");
        
        /**
         * @brief publish a statistics message with an array of values
         * @param parameterName name of the statistical parameter
         * @param values vector of values for the parameter
         * @param condition condition to check before sending, i.e. if condition == false, no
         *        message will be send
         * @param specifier additionally specifies that the parameter belongs to a certain sub-topic
         *                  in the statistics, e.g. the model number during matching; leave empty
         *                  (default) to make it a general statistics for this node
         */

        void publishStatisticsArray(const std::string parameterName, const std::vector<float>& values,
                                    const bool condition, const std::string specifier = "");
    
    private:

        ros::Publisher pubStatistics;      //!< publisher for statistics messages
        ros::Publisher pubStatisticsArray; //!< publisher for statistics arrays
        
};

} // end namespace
} // end namespace
