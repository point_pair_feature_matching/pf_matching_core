/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Franz Ziegler
 * @date 08.04.2019
 * @file pairFeatureModel.hpp
 * @brief model representation and related classes / functions
 */

#pragma once


// PCL
#include <pcl/point_cloud.h> //PointCloud template
#include <pcl/octree/octree_search.h> 		// for doing ray-tracing within an octree
#include <pcl/octree/octree_pointcloud.h>  	// for generating an octree of a pointcloud
#include <pcl/common/pca.h>						// for Principal Components Analysis (PCA)

// STL
#include <unordered_map>
#include <set>
#include <utility>

// boost
#include <boost/functional/hash.hpp> // for boost::hash_combine
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

// c minimal perfect hash
#include <cmph.h>

// project
#include <pairFeatures.hpp>
#include <featureTypes.hpp>
#include <pose.hpp>
#include <modelHashTable.hpp>

namespace pf_matching{

/**
 * @class PFmodel
 * @brief creates and stores the model of an object for matching via the pair feature algorithm
 */
template <typename PointNT>
class PFmodel{
    public:
		friend class modelHashTable<PointNT>;
		friend class visibilityContextModelHashTable<PointNT>;
		friend class STLvisibilityContextModelHashTable<PointNT>;
		friend class CMPHvisibilityContextModelHashTable<PointNT>;
    
        // shared pointer aliases
        typedef boost::shared_ptr<PFmodel<PointNT>> Ptr; 			//!< shared pointer definition for convenience
        typedef boost::shared_ptr<const PFmodel<PointNT>> ConstPtr; //!< shared pointer definition for convenience
        
        // core model information
        featureType containedFeatures;      						 //!< bit field of features for which hash tables exist
        std::vector<typename modelHashTable<PointNT>::Ptr> hashTables; 		 //!< each element is hash table for a certain feature type
         	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 //!  vector is a vector of base class pointers, for max flexibility
        float modelDiameter = 0.0;            						 //!< diameter of model point cloud(s)
		float d_max, d_med, d_min;									 //!< the extension of the object in direction of the PCA-Eigenvectors (= bounding box)
		typename pcl::PointCloud<PointNT>::ConstPtr surfaceCloudPtr; //!< cloud with oriented surface points
        typename pcl::PointCloud<PointNT>::ConstPtr edgeCloudPtr;    //!< cloud with oriented edge points
        
        // feature construction parameters
        float d_dist_abs;   //!< absolute value for distance quantization steps
        float d_angle;      //!< quantization steps to use for angles when constructing features

        // visibility context: feature construction parameters
        float d_VisCon_abs; 					//!< MFZ 12/09/17 absolute value for distance quantization steps for visibility context lengths.
        bool visualizeVisibilityContextFeature; //!< MFZ 11/10/17 Toggle PCLVisualizer on/off to show visibility context feature
        float voxelSize_intersectDetect; 		//!< MFZ 12/09/17 voxel size for intersection detection (visibility context lengths)
        unsigned int numberOfOccupiedVoxels = 0;//!< MFZ 09/08/18 number of occupied voxels in the intersection-detection-voxel-grid / -octree. for statistics

        // rotationally symmetric objects
        std::vector<Pose> equivalentPoses; //!< poses that are equivalent to the identity due to model symmetries

    private:

        // visibility context: feature construction parameters
        float ignoreFactor_intersectClassific; 	 				//!< MFZ 12/09/17 factor for intersection detection (visibility context lengths). intersections within (factor * voxelDiameter) will be ignored.
        float gapSizeFactor_allCases_intersectClassific; 		//!< MFZ 12/04/18 Factor for intersection detection: Closing gaps of size (factor * voxelDiameter) between intersected voxel centers regardless of ray-surface-intersection-case.
        float gapSizeFactor_surfaceCase_intersectClassific;  	//!< MFZ 12/04/18 Factor for intersection detection: Closing gaps of size (factor * voxelDiameter) between intersected voxel centers in case 'ray on surface'. This requires extra calculations. Value = 0 deactivates feature.
        float alongSurfaceThreshold_intersectClassific;			//!< MFZ 11/10/17 Threshold for intersection detection in [degrees]. Tolerance for classifying a ray as _|_ to surface-NORMAL.
		float othorgonalToSurfaceThreshold_intersectClassific; 	//!< MFZ 11/10/17 Threshold for intersection detection in [degrees]. Tolerance for classifying a ray as || to surface-NORMAL.
		bool advancedIntersectionClassification; 				//!< MFZ 11/10/17 Toggle advanced classification of the intersected voxels. Might result in more precise V_out features.

		// visibility context: using search-commands of PCL OctreePointCloudSearch for feature construction
        typename pcl::octree::OctreePointCloudSearch<PointNT>::Ptr octreeSearchPtr; //!< MFZ 13/09/17 shared pointer to octree for visibility context ray casting and other octree based searching

        // for implementation of the methods proposed in [Hinterstoisser et al. 2016]
        bool useNeighbourPPFsForTraining; 		//!< MFZ 14/03/2018 Toggle calculating neighbour PPFs and store them in Model. Using them should lead to more robust models, regarding noise.

        // hash tables
        modelHashTableType mHashTableType;  	//!< MFZ 23/05/2018 type of the model hash table
        float maxLoadFactor = 1; 				//!< load factor for STL hash table buckets (just a guideline(!) for the algorithm)

        // feature construction parameters
        //todo:: make private again after debugging:
    public:
        bool d_dist_relative; 	//!< is d_dist relative to model diameter or absolute?
        float d_dist;         	//!< quantization steps to use for distances when constructing features
        bool d_VisCon_relative; //!< MFZ 12/09/17 is d_VisCon relative to model diameter or absolute?
        float d_VisCon;			//!< MFZ 12/09/17 quantization steps to use for distances of visibility feature when constructing features
    private:
    	// for calculating the dimensions of the voting balls proposed in [Hinterstoisser et al. 2016]:
    	Eigen::Vector4f modelCentroid;
    	Eigen::Vector3f pcaEigenValues;
    	Eigen::Matrix3f pcaEigenVectors; 		//!< the eigenvectors of the model help getting d_min and d_med of the model-bounding-box

    public:

        /**
         * @brief default constructor, sets default values
         */
        PFmodel(){
            clearAll();
        }
    
    
        /**
         * @brief pass a pointer to a cloud with oriented surface points to construct features from
         * @param surfaceCloudPtr_ shared pointer to the cloud
         * @return true if pointer is valid, false otherwise
         */
        bool setSurfaceCloud(const typename pcl::PointCloud<PointNT>::ConstPtr& surfaceCloudPtr_);
        
        
        /**
         * @brief pass a pointer to a cloud with oriented edge (boundary) points to construct features from
         * @param edgeCloudPtr_ shared pointer to the cloud
         * @return true if pointer is valid, false otherwise
         */
        bool setEdgeCloud(const typename pcl::PointCloud<PointNT>::ConstPtr& edgeCloudPtr_);
        

        /**
         * @brief pass algorithm parameters
         *
         * @note overloaded version for visibility context extension
         *
         * @note MFZ 12/09/17 consider aliasing effects during quantization with 'discretization steps'.
         * 		 it might be better to choose the steps < 0.5 * subsampling distance (Nyquist criterion).
         *
         * @param d_dist_ discretization steps for distances (ideally equal to subsampling distance
         *                of the point clouds, 0.05 * object diameter is a good starting point)
         * @param d_dist_relative_ determine if d_dist is relative to the diameter of the model's
         *                         point cloud or an absolute value
         * @param d_angle_ discretization steps for angles in [rad] (literature recommends ~ 10 deg)
         * @param d_VisCon_ quantization steps for the distances of the visiblity context features
         * @param d_VisCon_relative_ determine if d_dist is relative to the diameter of the model's
         *                         	 point cloud or an absolute value
         * @param voxelSize_intersectDetect_ voxel size for intersection detection (visibility context lengths).
         * 									 optimal size depends on model diameter, e.g. 'ape'= 0.001 and 'cuboid'=2.9
         * @param ignoreFactor_intersectDetect_ factor for intersection detection (visibility context lengths).
         * 										intersections within (factor * voxelDiameter) will be ignored.
         * 										good value might be ~0.9
         * @return false if values were not in expected range (and therefore not set),
         *         true otherwise
         */
         bool setParameters(const float& d_dist_,
							const float& d_VisCon_,
							const float& voxelSize_intersectDetect_,
							const float& ignoreFactor_intersectClassific_,
							const float& gapSizeFactor_allCases_intersectClassific_,
							const float& gapSizeFactor_surfaceCase_intersectClassific_,
							const float& alongSurfaceThreshold_intersectClassific_,
							const float& othorgonalToSurfaceThreshold_intersectClassific_,
							const float d_angle_ = M_PI / 20,
							bool d_dist_relative_ = true,
							bool d_VisCon_relative_ = true,
							bool advancedIntersectionClassification_ = true,
							bool visualizeVisibilityContextFeature_ = false,
							bool use_neighbour_PPFs_for_training = false,
							modelHashTableType model_hash_table_type = 0,
							float modelDiameter_ = 0.0);

        /**
         * @brief set the maximum load factor for the hash tables created for storing the features
         * @note has to be set BEFORE the hash tables are created
         * @param maxLoadFactor_ the load factor, must be > 0, if unset, defaults to 1
         */
        void setHTmaxLoadFactor(const float maxLoadFactor_){
            maxLoadFactor = maxLoadFactor_;
        }
        
        /**
         * @brief construct a hash table with pair features for each requested feature
         * @param features reqeusted features (i.e. bitfield for featureTypes)
         * @return true if all tables for the requested features were constructed successfully,
         *         false otherwise
         */
        bool makeHashTables(const featureType& features);
        
        
        /**
         * @brief get the index of a hash table containing a certain feature
         * @param fType_ feature type the hash table should contain
         * @return index of the table (so it is hashTables[index]) or -1 if no such hashTable exists
         */
        int getHashTableIndex(const featureType& fType_) const;

        /**
         * @brief get the type of the model hash table
         * @return enum object with type of model hash table
         */
        modelHashTableType getModelHashTableType() const;

        /**
         * @brief erase all features stored in the model
         */
        void clearFeatures();
        
        /**
         * @brief erases all data perviously stored in the model
         */
        void clearAll();

    private:

        /**
         * @brief add a hash table containing features for a certain feature type, if that type was 
         *        requested. If such a hash table already exists, it will be overwritten.
         * 
         * @note For featues relying on only one point cloud, passing a targetCloudPtr == refCloudPtr
         *       is possible.
         * 
         * @param requestedFeatures bitfield of features to create a hash table for
         * @param thisType feature type for which to create a hash table, if requestedFeatures
         *                  contains thisType
         * @param refCloudPtr cloud with refrence points
         * @param targetCloudPtr cloud with target points
         * @return false if something went wrong creating the hash table, else true
         */
        bool addFeatureType(featureType requestedFeatures,
                            featureType thisType,
                            const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtr,
                            const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr);

        /**
         * @brief decide which hash table type has to be used and add a boost-shared pointer of
         *        this hash table type to the hash tables vector of the PFmodel object
         * @param thisType requested featureType
         */
        void addHashTable(featureType thisType);
};

} // end namespace pf_matching

# include "./impl/pairFeatureModelImpl.hpp"
