/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 25/06/16
 * @file unorganizedEdgeDetection.hpp
 * @brief Functions and classes to extract edges from unordered point clouds
 */
 
 
#pragma once

// PCL
#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/features/principal_curvatures.h>


namespace pf_matching{
namespace extensions{

/**
 * @class UnorganizedEdgeDetection
 * @brief class for detecting edges and their direction in unorganized point clouds based on 
 *        calculating principal curvature values
 * 
 * @note assumptions about the input cloud:
 *       * the normals are already calculated
 *       * the normals have unit length
 *       * the point coordinates and normals are valid, i.e. no NaN or inf values
 *       * the points are spaced at approximately equal distances < the search radius
 * 
 * Edges are estimated by calculating and considering the principal curvature (pc1) at each point
 * and applying a strategy similar to the Canny edge detector. This class uses using non-maxima 
 * suppression (edge thinning) and thresholding with hysteresis and edge following.
 * 
 * The returned edge directions correspond to the unit vector n x v, where n is the point's normal 
 * vector and v is the direction of maximal curvature at the point. Note that the sign of the vector
 * is ambigous.
 */
template<typename PointNT>
class UnorganizedEdgeDetection{
    public:
    
        // internal data types
        typedef pcl::PointCloud<PointNT> CloudNT;               //!< type of clouds to process
        typedef pcl::search::KdTree<PointNT> KdtreeT;           //!< kdtree type
        typedef pcl::PrincipalCurvatures CurvaturesT;           //!< type of curvature storage for single point
        typedef pcl::PointCloud<CurvaturesT> CurvaturesCloudT;  //!< type of curvature storage

        /**
         * @brief set method of nearest neighbour search
         * 
         * If an empty pointer is passed or this function is not called, a new tree will be built.
         * @param tree_ kdTree of input cloud (might be known from previous normal estimation)
         */
        void setSearchMethod(typename KdtreeT::Ptr& tree_){
            tree = tree_;
        }
        
        /**
         * @brief set the radius of the neighbourhood to consider around each point
         * @param searchRadius_ the radius
         */
        void setRadiusSearch(float searchRadius_){
            searchRadius = searchRadius_;
        }
        
        /**
         * @brief set the cloud to find edges in
         * @param cloudPtr_ cloud with point locations and precomputed normals, normals must have
         *                  unit length
         */
        void setInputCloud(const typename CloudNT::ConstPtr& cloudPtr_){
            cloudPtr = cloudPtr_;
        }

        /**
         * @brief set the upper and lower curvature thresholds for finding edge points
         * 
         * A point is an edge point if:
         * * upperThresh <= curvature, or
         * * lowerThresh <= curvature < upperThresh AND the point can be "connected" to a point
         *   fulfilling the first criterion by points fulfilling this criterion
         * 
         * @param lowerThresh_ upper threshold
         * @param upperThresh_ lower threshold
         */
        void setCurvatureThresholds(const float lowerThresh_, const float upperThresh_);
        
        /**
         * @brief extract the edge points from the input cloud
         * @param[out] edgesPtr cloud containing edge points, normals point in one of the two 
         *                      edge directions (and have unit length)
         * @param[out] edgesIndices indices of points in inputcloud that are edge points
         */
        void compute(typename CloudNT::Ptr& edgesPtr,
                     std::vector<int>& edgesIndices);
        
        /**
         * @brief extract the edge points from the input cloud
         * @param[out] edgesPtr cloud containing edge points, normals point in one of the two 
         *                      edge directions (and have unit length)
         * @param[out] edgesIndices indices of points in inputcloud that are edge points
         * @param[out] curvaturesPtr cloud containing the principal curvature information for each
         *                           point of the input cloud
         */
        void compute(typename CloudNT::Ptr& edgesPtr,
                     std::vector<int>& edgesIndices,
                     typename CurvaturesCloudT::Ptr& curvaturesPtr);

    protected:
        typename CloudNT::ConstPtr cloudPtr;  //!< point locations and normals
        float searchRadius = -1;    //!< neighbourhood for all operations
        float lowerThresh = 0.05;   //!< hysteresis lower thresh
        float upperThresh = 0.1;    //!< hysteresis upper thresh

        std::vector<bool> isEdgePoint; //!< is the point an edge point (candidate)?
        typename KdtreeT::Ptr tree;    //!< of input cloud, self-build or provided by user
        typename CurvaturesCloudT::Ptr curvatures; //!< storage for principal curvatures
        pcl::PointCloud<pcl::Normal>::Ptr edgeDirections; //!< normalized edge directions of input cloud
        
        /**
         * @brief calculate the principal curvature of all input points
         */
        void calculatePrincipalCurvatures();
        
        /**
         * @brief find the indices of points that have the (absolute) maximal principal curvature
         *        in their neighborhood in the direction perpendicular to the edge direction
         * 
         * Points with no points in their local neighborhood are not classified as maxima.
         * Saves results in isEdgePoint: true: potential edge point (local maximum),
         *                               false: no maximum, definitely no edge point
         * @param maxPerpendicularDistFactor only points that have a distance to the considered
                                             point of less than x * searchRadius parallel
                                             to the edge direction are considered
         */
        void findLocalCurvatureMaxima(float maxPerpendicularDistFactor);
        
        /**
         * @brief find the actual edge points via thresholding with hysteresis and edge following
         * 
         * on start, assumes isEdgePoint[i] == true for all edge canditates
         * on end, isEdgePoint[i] == true only for true edge points
         */
        void findEdges();
        
        /**
         * @brief copy point locations and orienations to the output clouds
         * @param[out] edgesPtr cloud containing edge points, normals point in one of the two 
         *                      edge directions
         * @param[out] edgesIndices vector with indices of edge points in original cloud
         */
        void copyResults(typename CloudNT::Ptr& edgesPtr, std::vector<int>& edgesIndices);
        
        /**
         * @brief show curvatures of points (bight blue -> curvature >= upperThresh,
         *        black -> curvature = 0) and content of isEdgePoint (green -> true) in viewer window
         */
        void showCurvatureAndEdges();
};
    
} // end namespace pf_matching
} // end namespace

#include "./impl/unorganizedEdgeDetectionImpl.hpp"