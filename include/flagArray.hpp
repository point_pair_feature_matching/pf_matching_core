/**
 * @copyright Copyright (c) 2019
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Authors: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @author Markus Ziegler
 * @date 23.03.2018
 * @file flagArray.hpp
 * @brief Definition of flag array
 */

#pragma once

// PCL
#include <pcl/point_types.h>
#include <pcl/point_cloud.h> //PointCloud template

// project
//#include <pairFeatureModel.hpp>
#include <pairFeatures.hpp>
//#include <modelHashTable.hpp> // now forward declared 01/06/2018

// STD
#include <unordered_map>  // for STL hash table
#include <vector>
#include <string.h>
#include <cstring>    // for strcpy
#include <iostream>

// CMPH library (c minimal perfect hash)
#include <cmph.h>


namespace pf_matching{

// forward declaration of templated class
template <typename PointNT> class flagArray;
template <typename PointNT> class modelHashTable;
template <typename PointNT> class visibilityContextModelHashTable;
template <typename PointNT> class STLmodelHashTable;
template <typename PointNT> class STLvisibilityContextModelHashTable;
template <typename PointNT> class CMPHmodelHashTable;
template <typename PointNT> class CMPHvisibilityContextModelHashTable;
/**
 *@struct CMPHflagArray
 *@brief struct to hold data of flag array hash table
 */
template <typename PointNT>
struct CMPHflagArray{
	friend class flagArray<PointNT>;
	public:

		bool checkFlagAtPosition(unsigned int alpha, uint32_t hashValue, int threadID = 0);
		bool resetFlags(int threadID);

		inline uint64_t getIndex( uint32_t hashValue, int threadID = 0){
			return hashValue + ( threadID * this->tableSize );
		}

		CMPHflagArray(){							//!< ctor
			this->flagArrayPtr = NULL;
			this->numberOfUniqueModelPFs = 0;
			this->maxNumberOfThreads = 1;
		}

		~CMPHflagArray(){							//!< dtor
			free(this->flagArrayPtr);
		}

	private:
		void init(modelHashTable<PointNT>* modelHashTablePtr, int maxNumberOfThreads_ = 1);
		typedef typename flagArray<PointNT>::flagArrayResolution flagArrayResolution;
		std::vector<std::vector<uint32_t>> foundModelHashValues;	//!< storage of all PPF-Hash Values of one refPoint (to easily clear flag array)
		uint64_t numberOfUniqueModelPFs; 							//!< number of unique model pair features
		flagArrayResolution *flagArrayPtr;							//!< actual flag array. has as many entries as the model has unique PFs
		int maxNumberOfThreads;										//!< maximum number of threads of the system
		uint64_t tableSize;											//!< hash table size ( = number of entries per thread),  includes padding during multi-threading
		unsigned int tablePadding;									//!< additional padding for aligning the thread-parts of flag-array to cachelines.
																	//!< to prevent performance drops due to "false sharing" during multi-threading

};


/**
 *@struct STLflagArray
 *@brief struct to hold data of flag array hash table
 */
template <typename PointNT>
struct STLflagArray: public std::unordered_map<	typename MinimalDiscretePF<PointNT>::featureHashKeyT, 					// key type
												typename std::vector<typename flagArray<PointNT>::flagArrayResolution>,	// mapped type
												std::hash<typename MinimalDiscretePF<PointNT>::featureHashKeyT>>{	 	// hash function
	friend class flagArray<PointNT>;
	public:

		virtual bool checkFlagAtPosition(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF, int threadID);
		virtual bool resetFlags(int threadID);
		virtual ~STLflagArray(){};
	protected:
		virtual void init(uint64_t scenePoints, modelHashTable<PointNT>* modelHashTablePtr, int maxNumberOfThreads_ = 1);
		// for convenience:
		int maxNumberOfThreads;					//!< maximum number of threads of the system
		uint64_t numberOfUniqueModelPFs;
		typedef typename std::unordered_map<typename MinimalDiscretePF<PointNT>::featureHashKeyT,
											typename std::vector<typename flagArray<PointNT>::flagArrayResolution>,
											typename std::hash<typename MinimalDiscretePF<PointNT>::featureHashKeyT>> hashTableT;
		std::vector<std::vector<typename MinimalDiscretePF<PointNT>::featureHashKeyT>> foundModelHashKeys;	//!< storage of all PPF-Hash Keys of one refPoint (to easily clear flag array)
};


/**
 *@struct STLflagArray
 *@brief struct to hold data of flag array hash table
 */
template <typename PointNT>
struct STLflagArrayPrebuilt: public STLflagArray<PointNT>{

	friend class flagArray<PointNT>;

	private:

		void init(uint64_t scenePoints, modelHashTable<PointNT>* modelHashTablePtr, int maxNumberOfThreads_ = 1);

	protected:

		using STLflagArray<PointNT>::maxNumberOfThreads;
		using STLflagArray<PointNT>::numberOfUniqueModelPFs;
		using typename STLflagArray<PointNT>::hashTableT;
		using STLflagArray<PointNT>::foundModelHashKeys;

	public:

		bool checkFlagAtPosition(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF, int threadID);
		bool resetFlags(int threadID);
};

/**
 * @struct OPHflagArray
 * @brief struct to hold data of flag array with OWN_PERFECT_HASHER
 */
template <typename PointNT>
struct OPHflagArray {
	friend class flagArray<PointNT>;
	private:
		// for convenience
		typedef typename flagArray<PointNT>::flagArrayResolution flagArrayResolution;

		uint64_t tableSize;			//!< hash table size ( = number of entries per thread), includes padding during multi-threading
		int angleBase;    			//!< max value of quantized angle
		int distanceBase; 			//!< max value of quantized distance
		int visconBase;   			//!< max value of quantized viscon distance
		int maxpower;     			//!< needed for base-conversion
		int maxNumberOfThreads;		//!< maximum number of threads of the system
		unsigned int tablePadding;	//!< additional padding for aligning the thread-parts of flag-array to cachelines.
		 	 	 	 	 	 	 	//!< to prevent performance drops due to "false sharing" during multi-threading

		std::vector<std::vector<uint64_t>> foundModelHashValues;	//!< storage of all PPF-Hash Values of one refPoint (to easily clear flag array)
		flagArrayResolution *flagArrayPtr;							//!< actual flag array. has as many entries as there are possible PFs, i.e. a lot!

		OPHflagArray(){
			this->flagArrayPtr = NULL;
		}

		~OPHflagArray(){							//!< dtor
			free(this->flagArrayPtr);
		}

		/**
		* @brief perfect minimal hash-function, calculating the hash value for the input-PPF
		* @note the hash value is the index of the hash table, where the PPF can be found
		* @note this calculation is very complex and takes a lot of time. use it carefully!
		* @param[in] PPF
		* @return hash-value for the quantized PPF
		*/
		uint64_t ownHashFunctionWithoutConversion(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF);

		uint64_t ownHashFunctionWithConversion(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF);

		uint64_t ownHashFunctionWithoutConversion_visCon(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF);

		uint64_t ownHashFunctionWithConversion_visCon(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF);

		uint64_t ownHashFunctionWithConversionWithLoop(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF);

		// memberfunctions-zeiger auf die zu verwendende hash-function
		// welche hash function zu verwenden ist, wird im ctor anhand der user-vorgabe von "algo" ermittelt
		// und dem pointer die entsprechende adresse zur gew��nschten hash function ��bergeben.
		// Definition des Schnittstellenzeigers:
		uint64_t (OPHflagArray<PointNT>::*hashFunction)(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF);

		void init(modelHashTable<PointNT>* modelHashTablePtr, int maxNumberOfThreads_ = 1);

		inline uint64_t getIndex( uint64_t hashValue, int threadID = 0){
			return hashValue + ( threadID * tableSize );
		}

	public:

		bool checkFlagAtPosition(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF, int threadID);
		bool resetFlags(int threadID);
		void setParameters(modelHashTable<PointNT>*  modelHashTablePtr);

};

/**
 *@class flagArray
 *@brief Interface to build a flag array. Different kinds of hash tables are available.
 *@note Currently available hash table types:
 *	 		- CMPH CHD
 *			- STL unordered map
 *			- Own perfect hasher
 */
// TODO: 28/05/2018 add hash table with own perfect hash function to flag array hash table types. it's the fasted when used with S2S
template <typename PointNT>
class flagArray {

	public:
		// for convenience:
		typedef uint32_t flagArrayResolution;  //!< TODO: simply increase the max resolution by replacing variable-type with uint64_t to enable up to 64 bins ( = 5,625��)
		 	 	 	 	 	 	 	 	 	   //! but keep in Mind, that you have to increase the upper bound in matcher-config file as well, currently it is set to max 32 bins.
		typedef typename MinimalDiscretePF<PointNT>::featureHashKeyT featureHashKeyT;

		flagArray(flagArrayHashTableType hashAlgorithm_, uint64_t scenePoints_, modelHashTable<PointNT>* modelHashTablePtr, int maxNumberOfThreads_ = 1); //!< ctor
		~flagArray(){};	//!< dtor

	private:
		flagArrayHashTableType hashAlgorithm; 			//!< hash function algorithm to use for this hash table
		CMPHflagArray<PointNT> cmphData; 				//!< struct containing specific data for cmph_chd hash function
		STLflagArray<PointNT> stlData; 					//!< struct containing table for STL hash table
		STLflagArrayPrebuilt<PointNT> stlDataPrebuilt; 	//!< struct containing table for prebuilt STL hash table
		OPHflagArray<PointNT> ophData;					//!< struct containing table for OWN_PERFECT_HASHER

	public:
		/**
		* @brief set the alpha_scene flag during matching of the current scene-PPF
		* @note as specified in [Hinterstoisser et al. 2016]
		* @param[in] position (alpha_scene) to check the flag
		* @param[in] current scene-PPF
		* @return true if PPF is not flagged at the position (and thus may vote), false if PPF is flagged at the position (and thus may not vote)
		*/
		inline bool checkFlagAtPosition_withCMPH(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, uint32_t hashValue, bool neighbourPF, int threadID = 0){
			return cmphData.checkFlagAtPosition(alpha, hashValue, threadID);
		}
		inline bool checkFlagAtPosition_withSTL(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, uint32_t hashValue, bool neighbourPF, int threadID = 0){
			return stlData.checkFlagAtPosition(alpha, sceneFeaturePtr, neighbourPF, threadID);
		}
		inline bool checkFlagAtPosition_withSTLprebuilt(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, uint32_t hashValue, bool neighbourPF, int threadID = 0){
			return stlDataPrebuilt.checkFlagAtPosition(alpha, sceneFeaturePtr, neighbourPF, threadID);
		}
		inline bool checkFlagAtPosition_withOPH(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, uint32_t hashValue, bool neighbourPF, int threadID = 0){
			return ophData.checkFlagAtPosition(alpha, sceneFeaturePtr, neighbourPF, threadID);
		}


		/**
		* @brief resetFlags
		* @return true if no error occurred
		*/
		inline bool resetFlags_withCMPH(int threadID = 0){
			return cmphData.resetFlags(threadID);
		}
		inline bool resetFlags_withSTL(int threadID = 0){
			return stlData.resetFlags(threadID);
		}
		inline bool resetFlags_withSTLprebuilt(int threadID = 0){
			return stlDataPrebuilt.resetFlags(threadID);
		}
		inline bool resetFlags_withOPH(int threadID){
			return ophData.resetFlags(threadID);
		}


		// memberfunctions-zeiger auf die zu verwendende hash-function
		// welche hash function zu verwenden ist, wird im ctor anhand der user-vorgabe von "hashAlgorithm_" ermittelt
		// und dem pointer die entsprechende adresse zur gewuenschten hash function uebergeben.
		// Definition des Schnittstellenzeigers:
		// resetFlags() ...
		bool (flagArray<PointNT>::*resetFlags)(int threadID);
		// ... und die setFlagAtPosition() Funktionen als Schnittstellenzeiger variabel sein.
		bool (flagArray<PointNT>::*checkFlagAtPosition)(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, uint32_t hashValue, bool neighbourPF, int threadID);

};

} // namespace pf_matching


# include "impl/flagArrayImpl.hpp"

