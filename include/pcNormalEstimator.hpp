/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 09/10/15
 * @file pcNormalEstimator.hpp
 * @brief Functions and classes related to finding the normals of a point cloud.
 */
#pragma once



// other
#include <string>
#include <random>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree.h>


namespace pf_matching{
namespace pre_processing{

/**
 * @class PCnormalEstimator
 * @brief collection of different normal estimation algorithms for point clouds
 */
class PCnormalEstimator{
    public:
    
        /**
        * possible normal estimation methods to set
        * 
        * runtime relation:
        * * non-organized: MLS_POLY_2 > LINEAR > MLS_TANGENT
        * * organized: LINEAR_ORGANIZED > LLS_ORGANIZED
        */
        enum NormalEstimationMethod{
            NONE = 0,         //!< skip normal estimation (e.g. because there already are normals)
            LINEAR_ORGANIZED, //!< linear plane estimation using integral images for organized clouds
            LLS_ORGANIZED,    //!< use first-order Taylor approximation least square fit on organized data
            LINEAR,           //!< simple linear plane estimation using least square plane fits
            MLS_TANGENT,      //!< moving least squares, using tangent based estimation for normals
            MLS_POLY_2        //!< moving least squares, using 2. order polynom based estimation for normals
        };

        /**
         * @brief set wich algorithm to use for normal estimation
         * @param method_ method code from PCnormalEstimator::NormalEstimationMethod
         */
        void setMethod(NormalEstimationMethod method_){
            method = method_;
        }
        
        /**
         * @brief set the search radius for finding neighboring points for estimating the surface
         * @note is only used for methods that assume an unorganized cloud (that don't end on 
         *       "_ORGANIZED")
         * @param searchRadius_ the radius
         */
        void setSearchRadius(float searchRadius_){
            searchRadius = searchRadius_;
        }

        /**
         * @brief estimate the normals of a cloud using the set parameters and save them together 
         *        with the original point data in the output
         * 
         * @warning cloudPtr and cloudNormalsPtr should not point to the same cloud. Certain algorithms
         *          will fail if input = output storage.
         * 
         * @warning if the input cloud is not dense, NaNs might be removed, so the input might be modified
         * 
         * There is no guarantee that the resulting normals will be normalized or the output will
         * be dense in any way. However, organized clouds will remain organized.
         * 
         * @param[in,out] cloudPtr shared pointer to the input cloud, contents might be modified to
         *                remove NaNs
         * @param[out] cloudNormalsPtr shared pointer to the output cloud
         * @param[in] flipNormals determine if the directions of all normals should be flipped after
         *            estimation; By default, estimated normals are oriented towards the cloud's
                      viewpoint in PCL.
         * @param[in] deepCopyForNONE if true, do a deep copy of the input cloud into the output cloud,
         *            if false (default), set output pointer = input pointer to save time on big
         *            clouds
         */
        template <typename PointT, typename PointN> void
        estimateNormals(typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                        typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr,
                        bool flipNormals = false,
                        bool deepCopyForNONE = false);
        
    private:
        NormalEstimationMethod method = NONE; //!< estimation method to use
        float searchRadius = -1;              //!< search radius when using unorganized clouds
        
        /**
         * @brief estimate normals using linear model with OMP paralleization
         * @param[in] cloudPtr shared pointer to the input cloud
         * @param[out] cloudNormalsPtr shared pointer to the output cloud
         */
        template <typename PointT, typename PointN> void 
        normalEstimationLinear(const typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                                     typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr);
        /**
         * @brief estimate normals using linear model with integral image on organized clouds
         * @param[in] cloudPtr shared pointer to the organized input cloud
         * @param[out] cloudNormalsPtr shared pointer to the output cloud
         */
        template <typename PointT, typename PointN> void
        normalEstimationLinearOrganized(const typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                                              typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr);
        
        /**
         * @brief estimate normals using the linear least squares method on organized clouds
         * @param[in] cloudPtr shared pointer to the organized input cloud
         * @param[out] cloudNormalsPtr shared pointer to the output cloud
         */
        template <typename PointT, typename PointN> void
        normalEstimationLLS(const typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                                  typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr);
        
        /**
         * @brief estimate normals using moving least square methods
         * if the input cloud is not dense, NaNs will be removed, so the input might be modified
         * @param[in,out] cloudPtr shared pointer to the organized input cloud
         * @param[out] cloudNormalsPtr shared pointer to the output cloud
         * @param[in] usePolynomial if true, use a polynomial surface model instead of linear
         * @param[in] polynomialOrder order of the surface polynomial model, only used if
         *            usePolynomial == true
         */
        template <typename PointT, typename PointN> void
        normalEstimationMLS(typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                            typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr,
                            bool usePolynomial,
                            unsigned int polynomialOrder);
};
} // end namespace
} // end namespace

#include "./impl/pcNormalEstimatorImpl.hpp"