/* @copyright
Copyright (c) 2019
TU Berlin, Institut für Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Authors: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
 * modelHashTable.hpp
 *
 *  Created on: May 23, 2018
 *  Author: Markus Franz Ziegler
 *  six hash table classes for model creation. two base classes and four child classes
 */

#pragma once

// PCL
#include <pcl/point_cloud.h> //PointCloud template
#include <pcl/octree/octree_search.h> 		// for doing ray-tracing within an octree
#include <pcl/octree/octree_pointcloud.h>  	// for generating an octree of a pointcloud

// STL
#include <unordered_map>
#include <set>
#include <utility>  // e.g. std::pair
#include <vector>

// boost
#include <boost/functional/hash.hpp> // for boost::hash_combine

// c minimal perfect hash
#include <cmph.h>

// project
#include <featureTypes.hpp>
#include <pairFeatures.hpp>
#include <flagArray.hpp>

// debugging flags generating (binary) file output
//#define write_scene_PPfs_hashKeys_to_binary_file  //<-- this also defines the macro in the Impl file!
//#define write_alphas_to_binary_file               //<-- this also defines the macro in the Impl file!
//#define write_scene_PPfs_hashKeys_to_binary_file
//#define write_cmph_hash_keys_out_of_range_to_file
//#define write_counters_to_file                    //<-- this also defines the macro in the Impl file!


namespace pf_matching {

// forward declarations
template <typename PointNT> class PFmodel;
class HoughSpace2D;
template <typename PointNT> class PFmatcher;


// ========================================================================================================
// ======================================== hash table base class =========================================
// ========================================================================================================

template <typename PointNT>
class modelHashTable {

	friend class MinimalDiscretePF<PointNT>;
	friend class VisibilityContextDiscretePF<PointNT>;

	protected:
		featureType fType; 						//!< type of feature stored in this hash table
		unsigned int htType = 0;				//!< simple number indicating the type of the hash table:
		 	 	 	 	 	 	 	 	 	 	//!  0 = base class
		 	 	 	 	 	 	 	 	 	 	//!  1 = STLmodelHashTable
		 	 	 	 	 	 	 	 	 	 	//!  2 = STLvisibilityContextModelHashTable
		 	 	 	 	 	 	 	 	 	 	//!  3 = CMPHmodelHashTable
		 	 	 	 	 	 	 	 	 	 	//!  4 = CMPHvisibilityContextModelHashTable

		PFmodel<PointNT>* modelPtr;				//!< model pointer to the model which contains this hash table

		// for implementation of the anti-noise-methods proposed in [Hinterstoisser et al. 2016]
		//  - training
		bool useNeighbourPPFsForTraining;		//!< toggle calculating neighbour PPFs "on" to store them in model. Using them should lead to more robust models, regarding noise.
		//  - matching
        bool voteForAdjacentRotationAngles;
		flagArrayHashTableType faHashTableType;
		unsigned int flagArrayQuantizationSteps;
		float d_alpha;              			//!< quantization step for alpha
		unsigned int n_alpha;       			//!< number of quantization steps for alpha

		// feature construction parameters
		float d_dist_abs;						//!< absolute value for distance quantization steps
        float d_angle;      					//!< quantization steps to use for angles when constructing features
		float modelDiameter;					//!< only here so that it can be used in OPHflagArray::setParameters()

		// statistics. to be published
		std::size_t numberOfUniqueModelPPFs = 0;
		std::size_t numberOfModelPPFs = 0;

	public:

		// debugging members
#ifdef write_scene_PPfs_hashKeys_to_binary_file
		std::vector<uint64_t> sceneHashKeys;
#endif
#ifdef write_alphas_to_binary_file
		std::vector<float> alphas;
		std::vector<float> alphas_s;
		std::vector<float> alphas_m;
		std::vector<unsigned int> alphaIndices;
#endif
#ifdef write_cmph_hash_keys_out_of_range_to_file
		std::vector<typename MinimalDiscretePF<PointNT>::featureHashKeyT> HashKeysOutOfRange;
		std::vector<uint32_t>HashValuesOutOfRange;
#endif
#ifdef write_counters_to_file
		double numberOfFeaturesFoundInHT = 0;
		double numberOfFeaturesNotFoundInHT = 0;
		double CMPHoutOfRangeCounter = 0;
		double FAdiscardsVoteCounter = 0;
		double FAdiscardsAdjacentVoteCounter = 0;
		double originalVoteCounter = 0;
		double smallerVoteCounter = 0;
		double biggerVoteCounter = 0;
#endif

		// shared pointer alias
		typedef boost::shared_ptr<modelHashTable<PointNT> > Ptr; 	  //!< shared pointer definition for convenience

		typename pcl::PointCloud<PointNT>::ConstPtr refPointCloudPtr; //!< the reference points used to construct the table

		modelHashTable(PFmodel<PointNT>& model);
		virtual ~modelHashTable();

		// virtual member functions, providing services that DO depend on the hash table type
		virtual bool constructFeatures(	const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtrIn,
										const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
										const bool refPointDirectionAmbiguous,
										const bool targetPointDirectionAmbiguous,
										const unsigned int stepSize = 1) = 0;
		virtual bool voteWithFeature(MinimalDiscretePF<PointNT>* sceneFeaturePtr, HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray, flagArray<PointNT>* refPointFlagArrayPtr) = 0;
		virtual bool collapseHashTable(std::vector<int>& equivalencies, bool deleteUniquePoints, unsigned int nPoints) = 0;
		virtual std::vector<unsigned int>  getHistogram() = 0;
		virtual void eraseKeys(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys) = 0;
		virtual void getHighFrequencyPFs(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys, unsigned int minEntries) = 0;
		virtual void weightByDistance(float power) = 0;
		virtual void discardByDistance(float distThresh) = 0 ;
		virtual void weightByFrequency(float power) = 0;
		virtual void weightByValue(float weight) = 0;
		virtual uint64_t getNumberOfModelPFs() = 0;
		virtual uint64_t getNumberOfUniqueModelPFs() = 0;

		// non-virtual member functions, providing services that do NOT depend on the hash table type
		bool setMatchingParameters(PFmatcher<PointNT>* matcher);
		unsigned int incrementAlphaSceneIndex(const float& alpha_s);
		unsigned int decrementAlphaSceneIndex(const float& alpha_s);
		unsigned int getAlphaSceneIndex(float &alpha_s);
//		unsigned int incrementAlphaIndex(unsigned int& alphaIndex); // unused
//		unsigned int decrementAlphaIndex(unsigned int& alphaIndex); // unused
		std::pair<float, unsigned int> incrementAlpha(const float& alpha_m, const float& alpha_s);
		std::pair<float, unsigned int> decrementAlpha(const float& alpha_m, const float& alpha_s);
		featureType getFeatureType();
		unsigned int getHashTableType();
		float get_d_dist_abs();
		float get_d_angle();
		float get_modelDiameter();
		bool get_useNeighbourPPFsForTraining();

	protected:

		virtual bool setParameters(PFmodel<PointNT>& model);

};

// ========================================================================================================
// =============================== visibility context hash table base class ===============================
// ========================================================================================================

template <typename PointNT>
class visibilityContextModelHashTable : virtual public modelHashTable<PointNT> {

	friend class MinimalDiscretePF<PointNT>;
	friend class VisibilityContextDiscretePF<PointNT>;

	public:
		// shared pointer aliases
		typedef boost::shared_ptr<visibilityContextModelHashTable<PointNT> > Ptr; 	  //!< shared pointer definition for convenience

		// visibility context: visualization
		bool visualizeVisibilityContextFeature;  //!< MFZ 11/10/17 Toggle PCLVisualizer on/off to show visibility context feature

	protected:
		// visibility context: feature construction parameters
		float d_VisCon_abs;
		float voxelSize_intersectDetect; 		 				//!< MFZ 12/09/17 voxel size for intersection detection (visibility context lengths)
		float ignoreFactor_intersectClassific; 	 				//!< MFZ 12/09/17 factor for intersection detection (visibility context lengths). intersections within (factor * voxelDiameter) will be ignored.
		float gapSizeFactor_allCases_intersectClassific; 		//!< MFZ 12/04/18 Factor for intersection detection: Closing gaps of size (factor * voxelDiameter) between intersected voxel centers regardless of ray-surface-intersection-case.
		float gapSizeFactor_surfaceCase_intersectClassific;  	//!< MFZ 12/04/18 Factor for intersection detection: Closing gaps of size (factor * voxelDiameter) between intersected voxel centers in case 'ray on surface'. This requires extra calculations. Value = 0 deactivates feature.
		float alongSurfaceThreshold_intersectClassific;			//!< MFZ 11/10/17 Threshold for intersection detection in [degrees]. Tolerance for classifying a ray as _|_ to surface-NORMAL.
		float othorgonalToSurfaceThreshold_intersectClassific; 	//!< MFZ 11/10/17 Threshold for intersection detection in [degrees]. Tolerance for classifying a ray as || to surface-NORMAL.
		bool advancedIntersectionClassification; 				//!< MFZ 11/10/17 Toggle advanced classification of the intersected voxels. Might result in more precise V_out features.

		// visibility context: training
		std::vector<std::pair<int, double>> countVoutZero ={{0,0},{1,0},{2,0},{3,0}}; //!< MFZ 11.04.2018 count the times a model PF has a V_out_x = 0 during model training;

		// visibility context: using search-commands of OctreePointCloudSearch for feature construction
		typename pcl::octree::OctreePointCloudSearch<PointNT>::Ptr octreeSearchPtr; //!< MFZ 13/09/17 shared pointer to octree for visibility context ray casting and other octree based searching

	public:
		visibilityContextModelHashTable(PFmodel<PointNT>& model);
		float get_d_VisCon_abs();
		std::vector<std::pair<int, double>> getVoutZeroCounter();
		void setOctreeSearchPtr(typename pcl::octree::OctreePointCloudSearch<PointNT>::Ptr& newPtr);

	protected:

		bool setParameters(PFmodel<PointNT>& model);

};


// ========================================================================================================
// ============================= STL hash table for original PFmatching class =============================
// ========================================================================================================

/**
 * @class STLmodelHashTable
 * @brief Hash table for storing information about all calculated features of a certain featureType.
 *
 * The table key is the hashed feature value. Each bucket contains information about all features
 * that produced the feature value represented by key.
 */
template <typename PointNT>
class STLmodelHashTable : public std::unordered_multimap<typename MinimalDiscretePF<PointNT>::featureHashKeyT, ModelPFinfo>,
						  virtual public modelHashTable<PointNT>{

	friend class MinimalDiscretePF<PointNT>;
	friend class VisibilityContextDiscretePF<PointNT>;

    public:

		// shared pointer aliases
		typedef boost::shared_ptr<STLmodelHashTable<PointNT> > Ptr; 	  //!< shared pointer definition for convenience
		typedef std::unordered_multimap<typename MinimalDiscretePF<PointNT>::featureHashKeyT, ModelPFinfo> hashTableT;

        /**
         * @brief construct uninitialized storage for features of a certain type
         * @param fType_ the feature type that this hash table holds. Note that this is not checked
         *        for plausibility.
         * @param maxLoadFactor the max. allowed load factor of the hash table
         */
        STLmodelHashTable(featureType fType_, PFmodel<PointNT>& model, float maxLoadFactor = 1.0);

        /**
         * @brief Calculate features using point clouds and insert them in the hash table. Any
         *        features already stored in the table will be deleted beforehand.
         *
         * For calculating features that only rely on a single point cloud, refCloudPtr == targetCloudPtr
         * can be passed.
         *
         * This function assumes valid pointers that are != NULL and that all passed clouds are dense
         * (i.e. contain no NaN or Inf values).
         *
         * @param refCloudPtrIn shared pointer to the oriented point cloud of the reference points
         * @param targetCloudPtr shared pointer to the oriented point cloud of the target points
         * @param d_dist_abs discretization steps for d-value (absolute)
         * @param d_angle discretization steps for alpha in rad
         * @param refPointDirectionAmbiguous true if features should be created for both positive and
         *                                   negativ normal direction (false: only positive direction
         *                                   is considered)
         * @param targetPointDirectionAmbiguous true if features should be created for both positive and
         *                                      negativ normal direction (false: only positive direction
         *                                      is considered)
         * @param stepSize only use every stepSize-th point in the cloud as a reference point
         */
        bool constructFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtrIn,
                               const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
                               const bool refPointDirectionAmbiguous,
                               const bool targetPointDirectionAmbiguous,
                               const unsigned int stepSize = 1);

        /**
         * @brief get a histogram of how many entries exist per discrete features
         * @return the histogram, histogram[i] is the number of discrete features that have exactly
         *         i entries in the hash table
         */
        std::vector<unsigned int> getHistogram();

        bool voteWithFeature(MinimalDiscretePF<PointNT>* sceneFeaturePtr, HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray, flagArray<PointNT>* refPointFlagArrayPtr);

        bool collapseHashTable(std::vector<int>& equivalencies, bool deleteUniquePoints, unsigned int nPoints);

        uint64_t getNumberOfUniqueModelPFs();

        uint64_t getUniqueModelPFs(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& uniquePFs);

        uint64_t getNumberOfModelPFs();

        void eraseKeys(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys);

        void getHighFrequencyPFs(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys, unsigned int minEntries);

        void weightByDistance(float power);

        void discardByDistance(float distThresh);

        void weightByFrequency(float power);

        void weightByValue(float weight);

        using modelHashTable<PointNT>::getFeatureType;

    protected:

        /**
         *
         * @param model
         * @return true if parameters are valid
         */
		bool setParameters(PFmodel<PointNT>& model);

		void voteInHoughSpaces(typename hashTableT::iterator& infoIt, HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray, unsigned int alphaIndex, float alpha);
};

// ========================================================================================================
// ======================== STL hash table class for visibility context matching ==========================
// ========================================================================================================

/**
 * @class VisibilityContextFeatureHashTable
 * @brief Extended version of the original hash table for storing information about calculated S2S features and their Visibility Context -- as proposed in [Kim and Medioni 2011].
 *
 * The hash table key contains the quantized S2S information and its visibility-context. It has 128 bit. This key maps to the feature value, stored as "ModelPFinfo" struct.
 * The hashed Key is a combination (boost::hash_combine) of the 128 bit information and only of size 64 bit. Hence collisions (= equal hashes) are theoretically unavoidable but presumably won't occur very often.
 * std::unstructured_map solves these conflicts with the provided overloaded '== operator'. That way no information gets lost, even if two hashes are equal.
 */
template <typename PointNT>
class STLvisibilityContextModelHashTable : public STLmodelHashTable<PointNT>,
										   public visibilityContextModelHashTable<PointNT> {

	friend class MinimalDiscretePF<PointNT>;
	friend class VisibilityContextDiscretePF<PointNT>;

    public:

		// shared pointer aliases
		typedef boost::shared_ptr<STLvisibilityContextModelHashTable<PointNT> > Ptr; 	  //!< shared pointer definition for convenience

		/**
		 * @brief construct uninitialized storage for features of type S2SVisCon.
		 * @param fType_ The feature type that this hash table holds. Note that this is not checked
		 *        for plausablility. Default to S2SVisCon.
		 * @param maxLoadFactor The suggested max. number of hashes per bucket. Default to 1.0. ('max_load_factor doesn't let you set the maximum load factor yourself, it just lets you give a hint.')
		 *
		 * http://www.boost.org/doc/libs/1_54_0/doc/html/unordered/buckets.html
		 */
		STLvisibilityContextModelHashTable(featureType fType_, PFmodel<PointNT>& model,float maxLoadFactor = 1.0);


		/**
		 * @brief For the Visibilty Context [Kim and Medioni 2011] adapted member function for calculating 'S2SVisCon' features using point clouds and insert them in the hash table.
		 * 		  Any features already stored in the table will be deleted beforehand.
		 *
		 * For calculating features that only rely on a single point cloud, refCloudPtr == targetCloudPtr
		 * can be passed.
		 *
		 * This function assumes valid pointers that are != NULL and that all passed clouds are dense
		 * (i.e. contain no NaN or Inf values).
		 *
		 * @param refCloudPtrIn shared pointer to the oriented point cloud of the reference points
		 * @param targetCloudPtr shared pointer to the oriented point cloud of the target points
		 * @param octreeSearchPtr shared pointer to the octree structure for ray casting (visibility context)
		 * @param refPointDirectionAmbiguous true if features should be created for both positive and
		 *                                   negativ normal direction (false: only positive direction
		 *                                   is considered). Defaults to false.
		 * @param targetPointDirectionAmbiguous true if features should be created for both positive and
		 *                                      negativ normal direction (false: only positive direction
		 *                                      is considered). Defaults to false.
		 * @param stepSize only use every stepSize-th point in the cloud as a reference point.Defaults to 1.0.
		 */
		bool constructFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtrIn,
							   const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
							   const bool refPointDirectionAmbiguous,
							   const bool targetPointDirectionAmbiguous,
							   const unsigned int stepSize = 1);

		void weightByDistance(float power);

		void discardByDistance(float distThresh);

    protected:

		/**
		 *
		 * @param model
		 * @return
		 */
		bool setParameters(PFmodel<PointNT>& model);

};


// ========================================================================================================
// ===================== hash table class with CMPH-CHD minimal perfect hash function =====================
// ========================================================================================================

/**
 * @struct CMPH_CHD_data
 * @brief struct for storing the data of the CMPH CHD minimal perfect hasher
 *
 * @note CMPH library: 		http://cmph.sourceforge.net/
 * @note CHD algorithm: 	http://cmph.sourceforge.net/chd.html
 */
struct CMPH_CHD_data{

public:
  cmph_t *hash;
  FILE* mphf_fd;
  cmph_io_adapter_t *source;
  CMPH_CHD_data(){}  			//!< ctor
  ~CMPH_CHD_data(){} 			//!< dtor
};

/**
 * @class CMPHmodelHashTable
 * @brief In memory-usage minimized hash table with minimal-perfect-hash-function for storing information about calculated S2S features.
 *
 * The hash table key contains the quantized S2S information. It has 64 bit.
 * The hash value is calculated from the hash key with the CHPH-CHD minimal perfect hash algorithm.
 * Therefore the hash value maps from the key directly to the index in the hash table, where the PPF-struct is stored.
 * Compared to the std::unordered_multimap, a bucket-structure is not needed and therefore the hash table should be of minimal size.
 */
template <typename PointNT>
class CMPHmodelHashTable: virtual public modelHashTable<PointNT> {

	friend class MinimalDiscretePF<PointNT>;
	friend class VisibilityContextDiscretePF<PointNT>;
	friend class STLflagArrayPrebuilt<PointNT>; // needs access to minimal hash table for initialization

    protected:

		// for building the minimal hash table:
		std::vector<typename MinimalDiscretePF<PointNT>::featureHashKeyT> hashTableKeys;	//!< temporary storage for all PPFs extracted from the model
//		std::vector<const char*> hashTableKeys;		//!< temporary storage for all PPFs extracted from the model
		CMPH_CHD_data hashFunctionData;														//!< struct containing specific data for cmph_chd hash function
		std::vector<std::pair<typename MinimalDiscretePF<PointNT>::featureHashKeyT, std::vector<ModelPFinfo> > > minimalHashTable; //!< hash table with minimal memory usage

    public:

		// shared pointer alias
		typedef boost::shared_ptr<CMPHmodelHashTable<PointNT> > Ptr; 	  //!< shared pointer definition for convenience

		/**
		 * @brief construct uninitialized storage for features of type S2SVisCon.
		 * @param fType_ The feature type that this hash table holds. Note that this is not checked
		 *        for plausibility. Default to S2SVisCon.
		 * @param maxLoadFactor The suggested max. number of hashes per bucket. Default to 1.0. ('max_load_factor doesn't let you set the maximum load factor yourself, it just lets you give a hint.')
		 *
		 * http://www.boost.org/doc/libs/1_54_0/doc/html/unordered/buckets.html
		 */
		CMPHmodelHashTable(featureType fType_, PFmodel<PointNT>& model);

		~CMPHmodelHashTable(){
			destroyMinimalPerfectHashFunction();
		}

		/**
		 * @brief For calculating pair features using point clouds and insert them in the hash table.
		 * 		  Any features already stored in the table will be deleted beforehand.
		 *
		 * For calculating features that only rely on a single point cloud, refCloudPtr == targetCloudPtr
		 * can be passed.
		 *
		 * This function assumes valid pointers that are != NULL and that all passed clouds are dense
		 * (i.e. contain no NaN or Inf values).
		 *
		 * @param refCloudPtrIn shared pointer to the oriented point cloud of the reference points
		 * @param targetCloudPtr shared pointer to the oriented point cloud of the target points
		 * @param refPointDirectionAmbiguous true if features should be created for both positive and
		 *                                   negativ normal direction (false: only positive direction
		 *                                   is considered). Defaults to false.
		 * @param targetPointDirectionAmbiguous true if features should be created for both positive and
		 *                                      negativ normal direction (false: only positive direction
		 *                                      is considered). Defaults to false.
		 * @param stepSize only use every stepSize-th point in the cloud as a reference point.Defaults to 1.0.
		 */
		bool constructFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtrIn,
							   const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
							   const bool refPointDirectionAmbiguous,
							   const bool targetPointDirectionAmbiguous,
							   const unsigned int stepSize = 1);

		uint32_t getHashValue(typename MinimalDiscretePF<PointNT>::featureHashKeyT hashKey) const;

		bool voteWithFeature(MinimalDiscretePF<PointNT>* sceneFeaturePtr, HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray, flagArray<PointNT>* refPointFlagArrayPtr);

		bool collapseHashTable(std::vector<int>& equivalencies, bool deleteUniquePoints, unsigned int nPoints);


		/**
		 * @brief get a histogram of how many entries exist per discrete features
		 * @return the histogram, histogram[i] is the number of discrete features that have exactly
		 *         i entries in the hash table
		 */
		std::vector<unsigned int> getHistogram(); //TODO: adjust member function to new key-type

		uint64_t getNumberOfUniqueModelPFs();

		uint64_t getNumberOfModelPFs();

		void eraseKeys(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys);

		void getHighFrequencyPFs(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys, unsigned int minEntries);

		void weightByDistance(float power);

		void discardByDistance(float distThresh);

		void weightByFrequency(float power);

		void weightByValue(float weight);

		using modelHashTable<PointNT>::getFeatureType;

    protected:

		void collectAllModelFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
									 MinimalDiscretePF<PointNT> &modelFeature,
				   	   	   	   	     std::vector<float> &targetPointNormalSigns,
									 const unsigned int stepSize = 1);

		/**
		 * @note: if one would want to keep more then one CMPH model in the RAM, one would need to specify
		 * 		  a model-specific name as "hashFunctionFileNameWithoutExtension". Only then it is possible
		 * 		  to load the corresponding hashFunction.
		 * @param hashFunctionFileNameWithoutExtension
		 * @return true if successfully constructed
		 */
		bool constructMinimalPerfectHashFunction(std::string hashFunctionFileNameWithoutExtension);

		void destroyMinimalPerfectHashFunction();

		void storeAllModelFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
				 	 	 	 	   MinimalDiscretePF<PointNT> &modelFeature,
								   std::vector<float> &targetPointNormalSigns,
								   const unsigned int stepSize = 1);

		/**
		 *
		 * @param model
		 * @return
		 */
		bool setParameters(PFmodel<PointNT>& model);

		void voteInHoughSpaces(uint32_t& hashValue, int& feature, HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray, unsigned int alphaIndex, float alpha);

};


// ========================================================================================================
// ===== hash table class with CMPH-CHD minimal perfect hash function for visibility context matching =====
// ========================================================================================================

/**
 * @class CMPHvisibilityContextModelHashTable
 * @brief In memory-usage minimized hash table with minimal-perfect-hash-function for storing information about calculated S2S features and their visibilty context..
 *
 * The hash table key contains the quantized S2S information and its visibility-context. It has 64 bit.
 * The hash value is calculated from the hash key with the CHPH-CHD minimal perfect hash algorithm.
 * Therefore the hash value maps from the key directly to the index in the hash table, where the PPF-struct is stored.
 * Compared to the std::unordered_multimap, a bucket-structure is not needed and therefore the hash table should be of minimal size.
 */
template <typename PointNT>
class CMPHvisibilityContextModelHashTable : public visibilityContextModelHashTable<PointNT>,
											public CMPHmodelHashTable<PointNT> {

	friend class MinimalDiscretePF<PointNT>;
	friend class VisibilityContextDiscretePF<PointNT>;
	friend class STLflagArrayPrebuilt<PointNT>; // needs access to minimal hash table for initialization

    public:

		// shared pointer aliases
		typedef boost::shared_ptr<CMPHvisibilityContextModelHashTable<PointNT> > Ptr; 	  //!< shared pointer definition for convenience

		/**
		 * @brief construct uninitialized storage for features of type S2SVisCon.
		 * @param fType_ The feature type that this hash table holds. Note that this is not checked
		 *        for plausablility. Default to S2SVisCon.
		 * @param maxLoadFactor The suggested max. number of hashes per bucket. Default to 1.0. ('max_load_factor doesn't let you set the maximum load factor yourself, it just lets you give a hint.')
		 *
		 * http://www.boost.org/doc/libs/1_54_0/doc/html/unordered/buckets.html
		 */
		CMPHvisibilityContextModelHashTable(featureType fType_, PFmodel<PointNT>& model);

		~CMPHvisibilityContextModelHashTable(){
			// doing nothing. calls destructor of parent class which in turn destroys hash function
		}

		/**
		 * @brief For the Visibilty Context [Kim and Medioni 2011] adapted member function for calculating 'S2SVisCon' features using point clouds and insert them in the hash table.
		 * 		  Any features already stored in the table will be deleted beforehand.
		 *
		 * For calculating features that only rely on a single point cloud, refCloudPtr == targetCloudPtr
		 * can be passed.
		 *
		 * This function assumes valid pointers that are != NULL and that all passed clouds are dense
		 * (i.e. contain no NaN or Inf values).
		 *
		 * @param refCloudPtrIn shared pointer to the oriented point cloud of the reference points
		 * @param targetCloudPtr shared pointer to the oriented point cloud of the target points
		 * @param octreeSearchPtr shared pointer to the octree structure for ray casting (visibility context)
		 * @param d_dist_abs discretization steps for d-value (absolute)
		 * @param d_angle discretization steps for alpha in rad
		 * @param d_VisCon_abs discretization step for Visibility Context lengths.
		 * @param refPointDirectionAmbiguous true if features should be created for both positive and
		 *                                   negativ normal direction (false: only positive direction
		 *                                   is considered). Defaults to false.
		 * @param targetPointDirectionAmbiguous true if features should be created for both positive and
		 *                                      negativ normal direction (false: only positive direction
		 *                                      is considered). Defaults to false.
		 * @param stepSize only use every stepSize-th point in the cloud as a reference point.Defaults to 1.0.
		 */
		bool constructFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtrIn,
							   const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
							   const bool refPointDirectionAmbiguous,
							   const bool targetPointDirectionAmbiguous,
							   const unsigned int stepSize = 1);

		void weightByDistance(float power);

		void discardByDistance(float distThresh);

		using modelHashTable<PointNT>::getFeatureType;

    protected:

		void collectAllModelFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
									 VisibilityContextDiscretePF<PointNT> &modelFeature,
				   	   	   	   	     std::vector<float> &targetPointNormalSigns,
									 const unsigned int stepSize = 1);

		void storeAllModelFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
				 	 	 	 	   VisibilityContextDiscretePF<PointNT> &modelFeature,
								   std::vector<float> &targetPointNormalSigns,
								   const unsigned int stepSize = 1);

		bool setParameters(PFmodel<PointNT>& model);

};

} /* namespace pf_matching */


#include "./impl/modelHashTableInline.hpp"
#include "./impl/modelHashTableImpl.hpp"
