/**
/* @copyright
Copyright (c) 2019
TU Berlin, Institut f�r Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Authors: Markus Franz Ziegler
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "../visibilityContextVisualization.hpp"

// PCL
#define VTK_EXCLUDE_STRSTREAM_HEADERS
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/common/actor_map.h>
#undef VTK_EXCLUDE_STRSTREAM_HEADERS
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>
// For visualizing the point cloud and the visibility context features
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/parse.h>

// PCL specific includes (PCL also has to be mentioned in package.xml as run and build dependencies)
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// Eigen lib for cross product
#include <Eigen/Dense>
#include <Eigen/Core>

// ROS log statements
#include <ros/console.h>

// boost
#include <boost/thread/mutex.hpp>

// other
//#include <algorithm>

namespace pf_matching{
namespace visualization{


template <typename PointNT>
void EigenVecToPCLPointT ( const Eigen::Vector3f& EigenVector,
					  	   PointNT& PointTRef){

	PointTRef->x = EigenVector(0,0);
	PointTRef->y = EigenVector(1,0);
	PointTRef->z = EigenVector(2,0);
}

template <typename PointNT>
PointNT EigenVecToPCLPointT ( const Eigen::Vector3f& EigenVector ){

	PointNT Point;
	Point.x = EigenVector(0,0);
	Point.y = EigenVector(1,0);
	Point.z = EigenVector(2,0);
	return Point;
}

template <typename PointNT>
void drawIntersectionPoints(typename pcl::PointCloud<PointNT>::Ptr& voxelCentersPtr,   // hier m�sste ein typename hin, sollte man die Funktion zum Template machen wollen.
							const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
							const std::string& label,
							const int r,
							const int g,
							const int b){

	typedef PointNT templatedPoint;
	fancy_viewer->removePointCloud(label);
	typename pcl::visualization::PointCloudColorHandlerCustom<PointNT> single_color(voxelCentersPtr, r, g, b);  // rgb points
	fancy_viewer->addPointCloud<templatedPoint>(voxelCentersPtr, single_color, label);   // nach -> m�sste vermutlich auch ein typename oder ein template hin
	fancy_viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, label); // size of the points
}

// TODO: MFZ 02.05.2018 if I want to use this, I somehow have to deal with the resulting abiguity.
// 						somehow this function already exists. this problem came up, when I included
//						Sebastian Krones Hypothesis-Verification Code. but i think i already encountered this problem sometime before. dont know how i solved it back then
//void keyboardEventOccurred (const pcl::visualization::KeyboardEvent &event,
//                            void* viewer_void)
//{
//  pcl::visualization::PCLVisualizer *viewer = static_cast<pcl::visualization::PCLVisualizer *> (viewer_void);
//  if (event.getKeySym () == "r" && event.keyDown ())
//  {
//    ROS_INFO_STREAM("x was pressed => continuing with next PPF"); //TODO: 13/10/17 MFZ: change to DEBUG_STREAM
//    // do something, that breaks the visualizer-loop
////    viewer->stopped_ = true;
//  }
//}


template <typename PointNT>
boost::shared_ptr<pcl::visualization::PCLVisualizer> visualizePointCloud(const typename pcl::PointCloud<PointNT>::ConstPtr cloud){

	  // =================================Open 3D viewer and add point cloud================================
	  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("Visibility Context Viewer"));
	  viewer->setBackgroundColor (0, 0, 0);
	  viewer->addPointCloud<PointNT>(cloud, "sample_cloud");
	  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample_cloud"); // size of the points
	//  viewer->addCoordinateSystem (1); // coordinate system at (0,0,0) with scale x
	  viewer->initCameraParameters ();
//	  viewer->registerKeyboardCallback (keyboardEventOccurred, (void*)viewer.get ());

	  return (viewer);
}

template <typename PointNT>
void visualizeOctreeCenterpoints( const boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer,
								  const typename pcl::PointCloud<PointNT>::ConstPtr& cloud){

	typename pcl::visualization::PointCloudColorHandlerCustom<PointNT> separate_color (cloud, 0, 150, 70); // greenish coloured? cloud with voxelcenters of octree
	viewer->addPointCloud<PointNT>(cloud, separate_color, "octree_center_points");
	viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "octree_center_points"); // size of the points
}


template <typename PointNT>
void visualizeVisConFeature(const boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer,
							const Eigen::Vector3f& p_r,
							const Eigen::Vector3f& p_t,
							const Eigen::Vector3f& n_r,
							const Eigen::Vector3f& n_t){

  // =====================================Add Arrows at cloud points====================================
  // origin for visualization purposes
  PointNT origin;
  origin.x = 0;
  origin.y = 0;
  origin.z = 0;

  // calculate "Direction of V_out" vectors
//  Eigen::Vector3f d = p_t - p_r;
//  Eigen::Vector3f minus_d = -1*d;
//  Eigen::Vector3f delta = n_r.cross(d);
//  Eigen::Vector3f minus_delta = -1*delta;

  // transform Eigen::vector to PointNT
  PointNT referencePoint = EigenVecToPCLPointT<PointNT>(p_r);
  PointNT referredPoint = EigenVecToPCLPointT<PointNT>(p_t);

//  PointNT referredPoint_d;
//  PointNT* referredPoint_d_Ptr = &referredPoint_d;
//  EigenVecToPCLPointT((p_t+d), referredPoint_d_Ptr);
//
//  PointNT referredPoint_minus_d;
//  PointNT* referredPoint_minus_d_Ptr = &referredPoint_minus_d;
//  EigenVecToPCLPointT((p_t+minus_d), referredPoint_minus_d_Ptr);
//
//  PointNT referredPoint_delta;
//  PointNT* referredPoint_delta_Ptr = &referredPoint_delta;
//  EigenVecToPCLPointT((p_t+delta), referredPoint_delta_Ptr);
//
//  PointNT referredPoint_minus_delta;
//  PointNT* referredPoint_minus_delta_Ptr = &referredPoint_minus_delta;
//  EigenVecToPCLPointT((p_t+minus_delta), referredPoint_minus_delta_Ptr);

  // add line d
  viewer->addLine<PointNT>(referencePoint, referredPoint, 1.0,0,0 ,"line"); // thick red line
  viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 3, "line");

  // add spheres around the reference and referred point
//  viewer->addSphere (referencePoint, 0.05, 0.5, 1.0, 0.0, "sphere_reference_point");
//  viewer->addSphere (referredPoint,  0.05, 1.0, 0.5, 0.0, "sphere_referred_point");
//  viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, "sphere_reference_point");
//  viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, "sphere_referred_point");

  // add vectors of reference and referred point:  (attention: the arrow-head is drawn at the start-point, hence the confusing order)
  viewer->addArrow ( referencePoint, 	origin, 0.5, 1.0, 0.0, false, "arrow_0_to_reference");  // yellow-green
  viewer->addArrow ( referredPoint, 	origin, 1.0, 0.5, 0.0, false, "arrow_0_to_referred"); 	// orange

}

/* TODO: MFZ 09/10/17 Old, delete after successfull integration
template <typename PointNT>
void EigenVecToPCLPointT(Eigen::Vector3f EigenVectorPtr,
						 PointNT* PointTPtr){

	PointTPtr->x = EigenVectorPtr(0,0);
	PointTPtr->y = EigenVectorPtr(1,0);
	PointTPtr->z = EigenVectorPtr(2,0);
}
*/

template <typename PointNT> void
visualizePCA(const boost::shared_ptr<pcl::visualization::PCLVisualizer>& viewer,
			 const Eigen::Vector4f& centroid,
			 const Eigen::Vector3f& eigenValues,
			 const Eigen::Matrix3f& eigenVectors,
			 const float &d_max,
			 const float &d_med,
			 const float &d_min){
	// visualize PCA
	ROS_DEBUG_STREAM("[visualizePCA] the eigenvalues of the covariance matrix are: "<< eigenValues[0] <<", "<< eigenValues[1] <<", "<< eigenValues[2]);
	PointNT centroid_ = EigenVecToPCLPointT<PointNT>(centroid.head<3>());
	PointNT eigenVector_0 = EigenVecToPCLPointT<PointNT>(centroid.head<3>() + eigenVectors.col(0)*d_max/2);
	PointNT eigenVector_1 = EigenVecToPCLPointT<PointNT>(centroid.head<3>() + eigenVectors.col(1)*d_med/2);
	PointNT eigenVector_2 = EigenVecToPCLPointT<PointNT>(centroid.head<3>() + eigenVectors.col(2)*d_min/2);
	viewer->addArrow(eigenVector_0, centroid_, 0,1,0, true, "eigenVector_0"); //green
	viewer->addArrow(eigenVector_1, centroid_, 1,1,0, true, "eigenVector_1"); //yellow
	viewer->addArrow(eigenVector_2, centroid_, 1,0,0, true, "eigenVector_2"); //red
}


template <typename PointNT>
void visualizeVout(const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
				   const Eigen::Vector3f& p_t,
				   const Eigen::Vector3f& d,
				   const Eigen::Vector3f& delta,
				   const Eigen::Vector3f& minus_d,
				   const Eigen::Vector3f& minus_delta,
				   const float Vout[4]){

	// ====================================Visualize Feature Extension====================================
	PointNT referredPoint =	EigenVecToPCLPointT<PointNT>(p_t);

	// display the Vout_0 vector in the point cloud
	PointNT referredPoint_Vout_0 = EigenVecToPCLPointT<PointNT>((p_t+(Vout[0]*(d/(d.norm())))) );
	fancy_viewer->addArrow ( referredPoint_Vout_0, referredPoint,	1, 1, 0, 	false, "arrow_Vout_0"); // yellow arrow

	// display the Vout_1 vector in the point cloud
	PointNT referredPoint_Vout_1 = EigenVecToPCLPointT<PointNT>((p_t+(Vout[1]*(delta/(delta.norm())))) );
	fancy_viewer->addArrow ( referredPoint_Vout_1, referredPoint,	1, 0, 0, 	false, "arrow_Vout_1"); // red arrow

	// display the Vout_2 vector in the point cloud
	PointNT referredPoint_Vout_2 = EigenVecToPCLPointT<PointNT>((p_t+(Vout[2]*(minus_d/(minus_d.norm())))) );
	fancy_viewer->addArrow ( referredPoint_Vout_2, referredPoint,	0, 1, 1, 	false, "arrow_Vout_2"); // turquoise arrow

	// display the Vout_3 vector in the point cloud
	PointNT referredPoint_Vout_3 = EigenVecToPCLPointT<PointNT>((p_t+(Vout[3]*(minus_delta/(minus_delta.norm())))) );
	fancy_viewer->addArrow ( referredPoint_Vout_3, referredPoint,	0, 0, 1, 	false, "arrow_Vout_3"); // blue arrow

	ROS_INFO_STREAM ("Feature-Extension calculated: \nVout_0 (d, yellow) = "<< Vout[0] << "\nVout_1 (delta, red)= "
			<< Vout[1] << "\nVout_2 (minus_d, turquoise)= "<< Vout[2] << "\nVout_3 (minus_delta, blue)= "<< Vout[3] );

}

// MFZ 18/09/17 added instantiation of templated class (runtime error occurred 'undefined symbol'):
//template void EigenVecToPCLPointT<pcl::PointNormal>(Eigen::Vector3f EigenVectorPtr, pcl::PointNormal* PointTPtr);

} // end namespace
} // end namespace
