/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

#include "../pairFeatureModel.hpp"


// PCL
#include <pcl/point_types.h>
#include <pcl/common/io.h>
#include <pcl/common/geometry.h>
#include <pcl/common/norms.h>
#include <pcl/console/print.h>
#include <pcl/common/pca.h>		// for Principal Components Analysis (PCA)
#include <pcl/common/common.h> 	// for getMinMax3D

// STL
//#include <utility>
#include <string>
#include <iostream>

// c minimal perfect hash
#include <cmph.h>

// project
#include <boundingSphere.hpp>
#include <geometry_msgs/Point.h>
#include <modelHashTable.hpp>


using geometry_msgs::Point;

namespace pf_matching{

void pause(std::string functionName, std::string lastTask, std::string nextTask) {
    std::cin.clear();
    std::cout << "[" << functionName << "] Finished " << lastTask << ", continue to " << nextTask
              << ". \nPress any key to continue...";
    std::cin.ignore();
}


/**
 * @class PFmodel
 * @brief creates and stores the model of an object for matching via the pair feature algorithm
 */

template <typename PointNT> bool 
PFmodel<PointNT>::setSurfaceCloud(const typename pcl::PointCloud<PointNT>::ConstPtr& surfaceCloudPtr_){
    
    if(surfaceCloudPtr_){
        surfaceCloudPtr = surfaceCloudPtr_;
            if(!surfaceCloudPtr->is_dense){
                PCL_WARN("[PFmodel::setSurfaceCloud] Surface cloud is not dense. NaN values " \
                          "will lead to wrong calculation of features!\n");
            }
        return true;
    }
    else{
        PCL_WARN("[PFmodel::setSurfaceCloud] Passed pointer points to NULL!\n");
        return false;
    }
}


template <typename PointNT> bool 
PFmodel<PointNT>::setEdgeCloud(const typename pcl::PointCloud<PointNT>::ConstPtr& edgeCloudPtr_){
    
    if(edgeCloudPtr_){
        edgeCloudPtr = edgeCloudPtr_;
            if(!edgeCloudPtr->is_dense){
                PCL_WARN("[PFmodel::setEdgeCloud] Edge cloud is not dense. NaN values " \
                          "will lead to wrong calculation of features!\n");
            }
        return true;
    }
    else{
        PCL_WARN("[PFmodel::setEdgeCloud] Passed pointer points to NULL!\n");
        return false;
    }
}


/**
 * @brief setting the feature parameters. function for visibility context extension
 */
template <typename PointNT> bool
PFmodel<PointNT>::setParameters( const float& d_dist_,
								 const float& d_VisCon_,
								 const float& voxelSize_intersectDetect_,
								 const float& ignoreFactor_intersectClassific_,
								 const float& gapSizeFactor_allCases_intersectClassific_,
								 const float& gapSizeFactor_surfaceCase_intersectClassific_,
								 const float& alongSurfaceThreshold_intersectClassific_,
								 const float& othorgonalToSurfaceThreshold_intersectClassific_,
								 const float d_angle_,
								 bool d_dist_relative_,
								 bool d_VisCon_relative_,
								 bool advancedIntersectionClassification_,
								 bool visualizeVisibilityContextFeature_,
								 bool use_neighbour_PPFs_for_training,
								 modelHashTableType model_hash_table_type,
								 float modelDiameter_){

    // check validity of input
    if( (d_dist_ <= 0) || (d_VisCon_ <= 0) || (voxelSize_intersectDetect_ <= 0) || (ignoreFactor_intersectClassific_ < 0) || (gapSizeFactor_allCases_intersectClassific_ <= 0) || (gapSizeFactor_surfaceCase_intersectClassific_ < 0) ){
        PCL_ERROR("[PFmodel::setParameters] d_dist_ <= 0 or d_VisCon_ <=0 or voxelSize_intersectDetect <=0  or ignoreFactor_intersectClassific_ < 0, or gapSizeFactor_allCases_intersectClassific <= 0, or gapSizeFactor_surfaceCase_intersectClassific < 0, can't use that.\n");
        return false;
    }

    if(d_angle_ > M_PI || d_angle_ <= 0){
        PCL_ERROR("[PFmodel::setParameters] d_angle_ is not in [0, pi).\n");
        return false;
    }

    if((alongSurfaceThreshold_intersectClassific_ < 0) || (alongSurfaceThreshold_intersectClassific_ > 90) || (othorgonalToSurfaceThreshold_intersectClassific_ < 0) || (othorgonalToSurfaceThreshold_intersectClassific_ > 90) ){
    	PCL_ERROR("[PFmodel::setParameters] alongSurfaceThreshold_intersectClassific or alongSurfaceThreshold_intersectClassific are < 0 deg or > 90 deg. Can't use that.\n");
		return false;
    }

    if (modelDiameter_ < 0.0){
    	PCL_ERROR("[PFmodel::setParameters] modelDiameter < 0. Can't use that.\n");
		return false;
    }

//    std::cout << "model diameter = " << modelDiameter_ << ", d_dist_relative = " << d_dist_relative_ << std::endl;
    // TODO: the following warnings are old. Because we now always submit the pcPreprocessors modelDiameter.
//    if((modelDiameter_ == 0.0) && (d_dist_relative_ == true)){
//    	PCL_WARN("[PFmodel::setParameters] It seems as if you want to give 'd_points_abs' and' d_dist_rel' as input. Attention! This will cause the matcher to calculate the models diameter.\n"
//    			 "Keep in mind, that this calculated diameter will be calculated with the downsampled model point cloud. Therefore it will be differing from a diameter calculated by the pcPreprocessor, which uses the not-downsampled cloud!\n");
//    }
//    if((modelDiameter_ != 0.0) && (d_dist_relative_ == false)){
//		PCL_WARN("[PFmodel::setParameters] It seems as if you want to give 'd_points_rel' and' d_dist_abs' as input. Attention! This will cause the pcPreprocessor to calculate the models diameter.\n"
//				 "Keep in mind, that this calculated diameter will be calculated with the not-downsampled model point cloud. Therefore it will be differing from a diameter calculated by the matcher, which uses the downsampled cloud!\n");
//	}

    if(modelDiameter_ == 0.0){
		PCL_WARN("[PFmodel::setParameters] Attention, 'model diameter' is unknown! Therefore the matcher will calculate it. Usually this is done by the pcPreprocessor.\n");
	}

    // copy values
    d_dist = d_dist_;
    d_dist_relative = d_dist_relative_;
    d_angle = d_angle_;
    mHashTableType = model_hash_table_type;
    modelDiameter = modelDiameter_;

    // visibility context parameters: copy values
    d_VisCon = d_VisCon_;
    d_VisCon_relative = d_VisCon_relative_;
    voxelSize_intersectDetect = voxelSize_intersectDetect_;
    ignoreFactor_intersectClassific = ignoreFactor_intersectClassific_;
    gapSizeFactor_allCases_intersectClassific = gapSizeFactor_allCases_intersectClassific_,
	gapSizeFactor_surfaceCase_intersectClassific = gapSizeFactor_surfaceCase_intersectClassific_,
    alongSurfaceThreshold_intersectClassific = alongSurfaceThreshold_intersectClassific_;
    othorgonalToSurfaceThreshold_intersectClassific = othorgonalToSurfaceThreshold_intersectClassific_;
    advancedIntersectionClassification = advancedIntersectionClassification_;
    visualizeVisibilityContextFeature = visualizeVisibilityContextFeature_;

    // anti noise-methods as proposed in [Hinterstoisser et al. 2016]
    useNeighbourPPFsForTraining = use_neighbour_PPFs_for_training;

    ROS_DEBUG_STREAM("[PFmodel::setParameters] All member variables were successfully set.");
    return true;
}

template <typename PointNT> bool 
PFmodel<PointNT>::makeHashTables(const featureType& features){
    
    bool surfaceReqired = featureRequiresSurfaceCloud(features);
    bool edgeRequired =   featureRequiresEdgeCloud(features);
    
    // check that all required data was set
    if(surfaceReqired && !surfaceCloudPtr){
        PCL_ERROR("[PFmodel::makeHashTables]: Surface cloud was not set but is required.\n");
        return false;
    }
    
    if(edgeRequired && !edgeCloudPtr){
        PCL_ERROR("[PFmodel::makeHashTables]: Edge cloud was not set but is required.\n");
        return false;
    }
    
    if(isnan(d_dist) || isnan(d_angle)){
        PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameters were not set.\n");
        return false;
    }
    if(features & S2SVisCon){
    	if (isnan(d_VisCon)){
    		PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_VisCon was not set.\n");
			return false;
    	}
    }
    
    // MFZ 30.08.2018: if clause added. we get modelDiameter from preprocessor now. otherwise modelDiameter == 0.0
    if (modelDiameter == 0.0){
		// calculate model diameter (max of surface and edge cloud diameter)
		float modelDiameterSurface = 0;
		float modelDiameterEdge = 0;
		Eigen::Vector4f boundingSphere; // xyz + radius

		if(surfaceReqired){
			if(!tools::welzlBoundingSphere(*surfaceCloudPtr, boundingSphere)){
				PCL_ERROR("[PFmodel::makeHashTables]: Surface bounding sphere computation failed.\n");
				return false;
			}
			modelDiameterSurface = 2 * boundingSphere[3];
		}
		if(edgeRequired){
			if(!tools::welzlBoundingSphere(*edgeCloudPtr, boundingSphere)){
				PCL_ERROR("[PFmodel::makeHashTables]: Edge bounding sphere computation failed.\n");
				return false;
			}
			modelDiameterEdge = 2 * boundingSphere[3];
		}

		modelDiameter = std::max(modelDiameterSurface, modelDiameterEdge);
    }

    // MFZ 12.04.2018 added: Use PCA to estimate d_min and d_med of an imaginary bounding box.
	pcl::PCA<PointNT> pcaBoudingBoxEstimation;
	pcaBoudingBoxEstimation.setInputCloud(surfaceCloudPtr);
	modelCentroid = pcaBoudingBoxEstimation.getMean();
	pcaEigenValues = pcaBoudingBoxEstimation.getEigenValues();
	pcaEigenVectors = pcaBoudingBoxEstimation.getEigenVectors();
	pcl::PointCloud<PointNT> projectedRefPointCloud;
	pcaBoudingBoxEstimation.project(*surfaceCloudPtr, projectedRefPointCloud);
	Eigen::Vector4f min, max, diff;
	pcl::getMinMax3D(projectedRefPointCloud, min, max);
	diff = max - min;
	d_max = diff(0,0);
	d_med = diff(1,0);
	d_min = diff(2,0);

    if(features & S2SVisCon){

    	// calculate absolute quantization step sizes
		if(d_dist_relative){
			if (d_dist > 1){ // relative step-size > 100%
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_dist is relative but > 1. Would not be able to build a valid model.\n");
				return false;
			}
			if (d_dist < 1/pow(2, sizeof(typename VisibilityContextDiscretePF<PointNT>::featureEntryT)*8-1) ){ // relative step-size < feature-resolution
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_dist is relative but too small. Would not be able to build a valid model.\n");
				return false;
			}
			d_dist_abs = d_dist * modelDiameter;
			PCL_DEBUG("Calculated absolute value for d_dist: %f\n", d_dist_abs);
			}
		else{
			if (d_dist > modelDiameter){ // absolute step-size > model-diameter
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_dist is absolute, but bigger than model-diameter. Would not be able to build a valid model.\n");
				return false;
			}
			if (d_dist < modelDiameter/pow(2, sizeof(typename VisibilityContextDiscretePF<PointNT>::featureEntryT)*8-1) ){ // absolute step-size < feature-resolution
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_dist is absolute, but too small. Would not be able to build a valid model.\n");
				return false;
			}
			d_dist_abs = d_dist;
		}
		if(d_VisCon_relative){
			if (d_VisCon > 1){
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_VisCon is relative but > 1. Would not be able to build a valid model.\n");
				return false;
			}
			if (d_VisCon < 1/pow(2, sizeof(typename VisibilityContextDiscretePF<PointNT>::featureEntryT)*8-1) ){
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_VisCon is relative but too small. Would not be able to build a valid model.\n");
				return false;
			}
			d_VisCon_abs = d_VisCon * modelDiameter;
			PCL_DEBUG("Calculated absolute value for d_VisCon: %f\n", d_VisCon_abs);
		}
		else{
			if (d_VisCon > modelDiameter){
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_VisCon is absolute, but bigger than model-diameter. Would not be able to build a valid model.\n");
				return false;
			}
			if (d_VisCon < modelDiameter/pow(2, sizeof(typename VisibilityContextDiscretePF<PointNT>::featureEntryT)*8-1) ){
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_VisCon is absolute, but too small. Would not be able to build a valid model.\n");
				return false;
			}
			d_VisCon_abs = d_VisCon;
		}

		// construct voxel grid for intersection detection
		octreeSearchPtr.reset (new pcl::octree::OctreePointCloudSearch<PointNT> (voxelSize_intersectDetect * modelDiameter));
		octreeSearchPtr->setInputCloud(surfaceCloudPtr);

		// Add points from input cloud to octree:
		octreeSearchPtr->addPointsFromInputCloud();

		// Get center points of all occupied voxels:
		std::vector<PointNT, Eigen::aligned_allocator<PointNT>> centerPoints;
		numberOfOccupiedVoxels = octreeSearchPtr->getOccupiedVoxelCenters(centerPoints);
		ROS_INFO_STREAM("[PFmodel::makeHashTables] Model point cloud has got "<< surfaceCloudPtr->points.size() <<" points.");
		ROS_INFO("[PFmodel::makeHashTables] Parameters: modelDiameter = %f, d_dist = %f, d_dist_abs = %f.", modelDiameter, d_dist, d_dist_abs);
		ROS_INFO_STREAM("[PFmodel::makeHashTables] Octree with Points from Model-PointCloud filled. There are "<< numberOfOccupiedVoxels <<" occupied voxels with a diameter of "
						<< sqrt(octreeSearchPtr->getVoxelSquaredDiameter()) <<" and a side-length of " << sqrt(octreeSearchPtr->getVoxelSquaredSideLen()));
		ROS_INFO_STREAM("                          --> Ratio of voxels/points = " << (float)numberOfOccupiedVoxels/(float)(surfaceCloudPtr->points.size()));

	/*	// get preprocessor-parameters: distance of the points. needed to calculate good voxel-size for intersection detection
		// /model_preprocessor/d_points
		// /model_preprocessor/d_points_is_relative
		float d_points;
		float d_points_abs;
		bool d_points_is_relative;
		int post_downsample_method;
		bool gotParameters = true;
		gotParameters =& ros::param::get ("/d_points", d_points );
		gotParameters =& ros::param::get ("/d_points_is_relative", d_points_is_relative );
		gotParameters =& ros::param::get ("/post_downsample_method", post_downsample_method);

		if (!gotParameters || isnan(d_points) || isnan(d_points_is_relative) || post_downsample_method ==1) // none = 0
		{
			// TODO: MFZ am 20/09/17 erledigen!
			PCL_ERROR("[PFmodel::makeHashTables] Could NOT get parameter 'd_points' or 'd_points_is_relative' from model_preprocessor-node (either no access or NaN-values).\n"
					"-->Continuing with original voxelSize_intersectDetect from rqt_reconfigure.");  //!< MFZ 19/09/17 This might occur, when no pre-downsampling of the model is performed.
					 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 // MFZ 10/10/17 ...because d_points might be NaN when no downsampling happened.


			// construct voxel grid for intersection detection // TODO: MFZ 13/09/17 Komplett überdenken!
			std::cout << "[PFmodel::makeHashTables] direkt vor dem octree reset"<< std::endl;
			octreeSearchPtr.reset (new pcl::octree::OctreePointCloudSearch<PointNT> (voxelSize_intersectDetect));
			octreeSearchPtr->setInputCloud(surfaceCloudPtr);

			// Add points from input cloud to octree:
			octreeSearchPtr->addPointsFromInputCloud();

			// Get center points of all occupied voxels:
			std::vector<PointNT, Eigen::aligned_allocator<PointNT>> centerPoints;
			int numberOfOccupiedVoxels = octreeSearchPtr->getOccupiedVoxelCenters(centerPoints);
			PCL_INFO("[PFmodel::makeHashTables] Octree with Points from PointCloud filled. There are %i occupied voxels with a diameter of %f and a side-length of %f. "
				"	There are %i Points in the Point Cloud. --> Reduction-Ratio = %f.\n", numberOfOccupiedVoxels,(octreeSearchPtr->getVoxelSquaredDiameter()),
				(octreeSearchPtr->getVoxelSquaredSideLen()),(surfaceCloudPtr->points.size()), (numberOfOccupiedVoxels/(surfaceCloudPtr->points.size())));
		}
		else{
			PCL_INFO("Successfully retrieved parameters 'd_points' and 'd_points_is_relative' from model_preprocessor-node.\n"
					"-->Continuing with optimal ratio for voxelSize_intersectDetect");
			if (d_points_is_relative){
				d_points_abs = modelDiameter * d_points;
				PCL_INFO("Calculated absolute value for d_points: %f\n", d_points_abs);
			}
			else{
				d_points_abs = d_points;
				PCL_INFO("Retrieved absolute value for d_points: %f from parameterserver.\n", d_points_abs);
			}
			// MFZ 19/09/17 it might be better to overwrite the original voxelSize from the parameter server,
			// since it seems, that an octree with ~25% less points than the downsampeled PointCloud, works best. (ratio empirically determined)
			// calculate voxelSize as ~125% of d_points_abs
			voxelSize_intersectDetect = 1.16 * d_points_abs; // factor 1.25 (125%) is typically too large. although the number of occupied voxels
															 // decreases as the voxelSize increases, the correlation is not linear.
															 // number of occupied voxels decreases faster than the size of Voxels increase.


			// construct voxel grid for intersection detection // TODO: MFZ 13/09/17 Komplett überdenken!?
			std::cout << "[PFmodel::makeHashTables] direkt vor dem octree reset"<< std::endl;
			octreeSearchPtr.reset (new pcl::octree::OctreePointCloudSearch<PointNT> (voxelSize_intersectDetect));
			octreeSearchPtr->setInputCloud(surfaceCloudPtr);

			// Add points from input cloud to octree:
			octreeSearchPtr->addPointsFromInputCloud();

			// Get center points of all occupied voxels:
			std::vector<PointNT, Eigen::aligned_allocator<PointNT>> centerPoints;
			int numberOfOccupiedVoxels = octreeSearchPtr->getOccupiedVoxelCenters(centerPoints);
			PCL_INFO("Octree with Points from PointCloud filled. There are %i occupied voxels with a diameter of %f and a side-length of %f. "
				"	There are %i Points in the Point Cloud. --> Reduction-Ratio = %f.\n", numberOfOccupiedVoxels,(octreeSearchPtr->getVoxelSquaredDiameter()),
				(octreeSearchPtr->getVoxelSquaredSideLen()),(surfaceCloudPtr->points.size()), (numberOfOccupiedVoxels/(surfaceCloudPtr->points.size())));

		}
*/
    } // if (features & S2SVisCon)

    else{ // for all other feature-types than S2SVisCon

		// calculate absolute quantization step sizes
		if(d_dist_relative){
			if (d_dist > 1){
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_dist is relative but > 1. Would not be able to build a valid model.\n");
				return false;
			}
			if (d_dist < 1/pow(2, sizeof(typename MinimalDiscretePF<PointNT>::featureEntryT)*8-1) ){
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_dist is relative but too small. Would not be able to build a valid model.\n");
				return false;
			}
			d_dist_abs = d_dist * modelDiameter;
			PCL_DEBUG("Calculated absolute value for d_dist: %f\n", d_dist_abs);
		}
		else{
			if (d_dist > modelDiameter){
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_dist is absolute, but bigger than model-diameter. Would not be able to build a valid model.\n");
				return false;
			}
			if (d_dist < modelDiameter/pow(2, sizeof(typename MinimalDiscretePF<PointNT>::featureEntryT)*8-1) ){
				PCL_ERROR("[PFmodel::makeHashTables]: Quantization parameter d_dist is absolute, but too small. Would not be able to build a valid model.\n");
				return false;
			}
			d_dist_abs = d_dist;
		}

		ROS_INFO_STREAM("[PFmodel::makeHashTables] Model point cloud has got "<< surfaceCloudPtr->points.size() <<" points.");
		ROS_INFO("[PFmodel::makeHashTables] Parameters: modelDiameter = %f, d_dist = %f, d_dist_abs = %f\n", modelDiameter, d_dist, d_dist_abs);
	}

    // construct and store hash tables for all features (that were requested)
    bool featuresOK = true;
    featuresOK &= addFeatureType(features, S2S, 	  surfaceCloudPtr, surfaceCloudPtr);
    featuresOK &= addFeatureType(features, B2B,    		 edgeCloudPtr, edgeCloudPtr);
    featuresOK &= addFeatureType(features, S2B, 	  surfaceCloudPtr, edgeCloudPtr);
    featuresOK &= addFeatureType(features, B2S,    		 edgeCloudPtr, surfaceCloudPtr);
    featuresOK &= addFeatureType(features, S2SVisCon, surfaceCloudPtr, surfaceCloudPtr);

    // delete octree data structure
//  octreeSearchPtr->deleteTree();

    return featuresOK;
}


template <typename PointNT> int 
PFmodel<PointNT>::getHashTableIndex(const featureType& fType_) const{
    
    // search for table with the kind of features:
	for(auto it = hashTables.begin(); it != hashTables.end(); it++){
	//for(auto it: hashTables)
		if( (*it)->getFeatureType() == fType_){
			return it - hashTables.begin();
			// index calculation works only for random access iterator, for other containers,
			// use std::distance(hashTables.begin(), it), but this is O(n) instead of O(1)
		}
	}

    return -1;
}

template <typename PointNT> modelHashTableType
PFmodel<PointNT>::getModelHashTableType() const{
	return mHashTableType;
}

template <typename PointNT> void 
PFmodel<PointNT>::clearFeatures(){

    containedFeatures = NONE;
    hashTables.clear();
}


template <typename PointNT> void 
PFmodel<PointNT>::clearAll(){
    
    clearFeatures();
    ROS_DEBUG_STREAM("[PFmodel::clearAll()] just before setting all member variables to NAN.");
    d_dist = NAN;
    d_angle = NAN;
    d_VisCon = NAN;
    mHashTableType = MH_NONE;
    voxelSize_intersectDetect = NAN;
    ignoreFactor_intersectClassific = NAN;
    gapSizeFactor_allCases_intersectClassific = NAN;
    gapSizeFactor_surfaceCase_intersectClassific = NAN;
    alongSurfaceThreshold_intersectClassific = NAN;
    othorgonalToSurfaceThreshold_intersectClassific = NAN;
    advancedIntersectionClassification = NAN;
    visualizeVisibilityContextFeature = NAN;
    useNeighbourPPFsForTraining = NAN;
    ROS_DEBUG_STREAM("[PFmodel::clearAll()] just after setting member variables to NAN.");
    surfaceCloudPtr = NULL; //or nullptr?
    edgeCloudPtr  = NULL;
    ROS_DEBUG_STREAM("[PFmodel::clearAll()] just after setting all member variables to NAN.");
//    std::cout << "[PFmodel::clearAll] direkt vor dem octree = NULL"<< std::endl; // TODO: MFZ DEBUG 14/09/17 hinterher wieder entfernen
//    octreeSearchPtr = NULL;
//    std::cout << "[PFmodel::clearAll] direkt nach dem octree = NULL"<< std::endl; // TODO: MFZ DEBUG 14/09/17 hinterher wieder entfernen
    modelDiameter = NAN;
    maxLoadFactor = 1;
}


template <typename PointNT> bool 
PFmodel<PointNT>::addFeatureType(featureType requestedFeatures,
                    			 featureType thisType,
								 const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtr,
								 const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr){

    // check that thisType was actually requested
    if(requestedFeatures & thisType){
        
        // check if a hash table already exists for this feature,
        // otherwise create new one and use that
        int htIndex = getHashTableIndex(thisType);
        if(htIndex < 0){
        	addHashTable(thisType);
            htIndex = hashTables.size() - 1;
        }

        // make the features (and delete any previous content)
        bool targetPointDirectionAmbiguous = featureHasAmbiguousTargetPointNormal(thisType);
        bool refPointDirectionAmbigous = featureHasAmbiguousRefPointNormal(thisType);
        bool constructOK = hashTables[htIndex]->constructFeatures(refCloudPtr, targetCloudPtr,
                                                                  refPointDirectionAmbigous,
                                                                  targetPointDirectionAmbiguous);
        
        // if something went wrong, delete the table again
        if(!constructOK){
            hashTables.erase(hashTables.end());
            return false;
        }
        
        // remember that a table for this type exists
        containedFeatures = containedFeatures | thisType;
    }

    return true;
}

template <typename PointNT> void
PFmodel<PointNT>::addHashTable(featureType thisType){

	switch (mHashTableType) {

		case MH_STL: // STL

			if ( thisType & S2SVisCon ){
				typename boost::shared_ptr<STLvisibilityContextModelHashTable<PointNT> > hashTablePtr (new STLvisibilityContextModelHashTable<PointNT>(thisType, *this, 1.0));
				hashTables.push_back(hashTablePtr);
			}
			else {
				typename boost::shared_ptr<STLmodelHashTable<PointNT> > hashTablePtr( new STLmodelHashTable<PointNT>(thisType, *this, 1.0));
				hashTables.push_back(hashTablePtr);
			}
			break;

		case MH_CMPH_CHD: // CMPH

			if (thisType & S2SVisCon){
				typename boost::shared_ptr<CMPHvisibilityContextModelHashTable<PointNT> > hashTablePtr (new CMPHvisibilityContextModelHashTable<PointNT>(thisType, *this));
				hashTables.push_back(hashTablePtr);
			}
			else {
				typename boost::shared_ptr<CMPHmodelHashTable<PointNT> > hashTablePtr ( new CMPHmodelHashTable<PointNT>(thisType, *this));
				hashTables.push_back(hashTablePtr);
			}
			break;
	}
}

} // end namespace pf_matching
