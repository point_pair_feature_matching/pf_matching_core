/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once


#include "../pairFeatureMatcher.hpp"

// PCL
#include <pcl/io/png_io.h>            // saving images to png
#include <pcl/kdtree/kdtree_flann.h>  // kd tree for radius-search
// std
#include <sstream>
#include <stdio.h>
#include <algorithm> // vector sorting
#include <unistd.h>     // for getting
#include <sys/types.h>  // current users
#include <pwd.h>        // home directory

// boost
#include <boost/timer/timer.hpp>
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>

// project
#include <filePaths.hpp>
#include <flagArray.hpp>
#include <modelHashTable.hpp>


// debugging flags generating console output
//#define print_VisCon_feature
//#define print_overflown_VisCon_feature

// debugging flag generating (binary) files
//#define write_scene_PPfs_hashKeys_to_binary_file
//#define write_rawPoses_to_binary_file
//#define write_voting_time_to_file
//#define write_cmph_hash_keys_out_of_range_to_file
//#define write_alphas_to_binary_file    // <-- set this macro only in header file, not here!
//#define write_counters_to_file         // <-- set this macro only in header file, not here!

// switch between octree radius-search and kd-tree flann radius search
// the octree search seems to be a bit faster
#define use_octree_radius_search

// visualizing flag
#define visualize_VisCon_feature

// switch multi-threaded / single-threaded matching
// for multi-threading OPEN MP is necessary
#ifdef _OPENMP
#include <omp.h>
//#define omp_get_max_threads() 4 //!< allows choosing custom number of threads
#else
#define omp_get_max_threads() 1
#define omp_get_thread_num() 0
#endif


namespace pf_matching{


template <typename PointNT> void
PFmatcher<PointNT>::setModel(const typename PFmodel<PointNT>::ConstPtr& modelPtr_){

    if(modelPtr_){
        modelPtr = modelPtr_;
    }
    else{
        PCL_WARN("[PFmatcher::setModel] Passed pointer points to NULL!\n");
    }
}



template <typename PointNT> void
PFmatcher<PointNT>::setScene(const typename pcl::PointCloud<PointNT>::ConstPtr& surfaceCloudPtr_,
                             const typename pcl::PointCloud<PointNT>::ConstPtr& edgeCloudPtr_){
    
    sceneSurfaceCloudPtr = surfaceCloudPtr_;
    sceneEdgeCloudPtr = edgeCloudPtr_;
    
    if(sceneSurfaceCloudPtr &&
       (!sceneSurfaceCloudPtr->is_dense && sceneSurfaceCloudPtr->points.size() > 0)){
        PCL_WARN("[PFmatcher::setScene] Surface cloud is not dense. NaN values " \
                 "will lead to wrong calculation of features!\n");
    }
    
    if(sceneEdgeCloudPtr && 
       (!sceneEdgeCloudPtr->is_dense && sceneEdgeCloudPtr->points.size() > 0)){
        PCL_WARN("[PFmatcher::setScene] Edge cloud is not dense. NaN values " \
                 "will lead to wrong calculation of features!\n");
    }
}

template <typename PointNT> void
PFmatcher<PointNT>::setMatchingParameters(float d_alpha_,
                                          unsigned int refPointStep_,
                                          float maxThresh_,
                                          float clusterTransThreshRelative_,
                                          float clusterRotThresh_,
										  bool use_neighbour_PPFs_for_matching,
										  bool use_voting_balls,
										  bool use_hinterstoisser_clustering,
										  bool use_hypothesis_verification_with_visibility_context,
										  bool vote_for_adjacent_rotation_angles,
										  flagArrayHashTableType flag_array_hash_table_type,
										  unsigned int flag_array_quantization_steps,
                                          bool useEquivalentModelPoses_){
    
    matchingParamsSet = true;
                               
    d_alpha = d_alpha_;
    n_alpha = ceilf(2 * M_PI / d_alpha);
    refPointStep = refPointStep_;
    maxThresh = maxThresh_;
    
    clusterTransThreshRelative = clusterTransThreshRelative_;
    clusterRotThresh   = clusterRotThresh_;

    useEquivalentModelPoses = useEquivalentModelPoses_;

    // EXTENSION methods from [Hinterstoisser et al. 2016]
    useNeighbourPPFsForMatching = use_neighbour_PPFs_for_matching;
    useVotingBalls = use_voting_balls;
	useHinterstoisserClustering = use_hinterstoisser_clustering;
	usePoseVerification = use_hypothesis_verification_with_visibility_context;
	voteForAdjacentRotationAngles = vote_for_adjacent_rotation_angles;
	faHashTableType = flag_array_hash_table_type;
	flagArrayQuantizationSteps = flag_array_quantization_steps;
	if (flagArrayQuantizationSteps != n_alpha ){
		ROS_WARN_STREAM("[PFmatcher::setMatchingParameters] number of flag array bins (" << flagArrayQuantizationSteps <<
						") and number of voting-space alpha bins (" << n_alpha << ") differ! Might not be a wise choice.");
	}
	else{
		ROS_WARN_STREAM("[PFmatcher::setMatchingParameters] number of flag array bins (" << flagArrayQuantizationSteps <<
						") equals number of voting-space alpha bins (" << n_alpha << "). Should be a good choice.");
	}
}


template<typename PointNT> template<typename PointT> void
PFmatcher<PointNT>::copyMatchingParameters(const PFmatcher<PointT>& otherMatcher){
    
    matchingParamsSet = otherMatcher.matchingParamsSet;
                               
    d_alpha = otherMatcher.d_alpha;
    n_alpha = otherMatcher.n_alpha;
    refPointStep = otherMatcher.refPointStep;
    maxThresh = otherMatcher.maxThresh;
    
    clusterTransThreshRelative = otherMatcher.clusterTransThreshRelative;
    clusterRotThresh           = otherMatcher.clusterRotThresh;
    
    useEquivalentModelPoses = otherMatcher.useEquivalentModelPoses;

    // EXTENSION methods from [Hinterstoisser et al. 2016]
    useNeighbourPPFsForMatching = otherMatcher.useNeighbourPPFsForMatching;
	useVotingBalls = otherMatcher.useVotingBalls;
	useHinterstoisserClustering = otherMatcher.useHinterstoisserClustering;
	usePoseVerification = otherMatcher.usePoseVerification;
	voteForAdjacentRotationAngles = otherMatcher.voteForAdjacentRotationAngles;
	faHashTableType = otherMatcher.faHashTableType;
	flagArrayQuantizationSteps = otherMatcher.flagArrayQuantizationSteps;
}

template <typename PointNT> void
PFmatcher<PointNT>::setMiscParameters(std::string HSsaveDir_){
    
    miscParamsSet = true;
    
    saveDir = tools::formatPathAndCreate(HSsaveDir_);
    if(saveDir.empty()){
        saveHoughSpaceToFile = false;
    }
    else{
        saveHoughSpaceToFile = true;
    }
}

template <typename PointNT> void
PFmatcher<PointNT>::switchOffExtensions(){
	// switch off EXTENSION methods from [Hinterstoisser et al. 2016]
	useNeighbourPPFsForMatching = false;
	useVotingBalls = false;
	useHinterstoisserClustering = false;
	usePoseVerification = false;
	voteForAdjacentRotationAngles = false;
	faHashTableType = FA_NONE;
	flagArrayQuantizationSteps = 1;
}

template <typename PointNT> void
PFmatcher<PointNT>::match(featureType featuresToMatch){
    
    // clear any previous results
    rawPoses.clear();
    poseClusters.clear();

    // check that all parameters were set
    if( !(miscParamsSet && matchingParamsSet) ){
        PCL_ERROR("[PFmatcher::match] Some required parameters were not set.\n");
        return;
    }
    
    // check that model was set
    if(!modelPtr){
        PCL_ERROR("[PFmatcher::match] The model for matching was not set or does not exist.\n");
        return;
    }
    
    // note: checking the scene happens in matchFeature()  //TODO: MFZ 17.04.2018 what's up with that line here?

    // do one matching per feature
    matchFeature(featuresToMatch, S2S, sceneSurfaceCloudPtr, sceneSurfaceCloudPtr);
    matchFeature(featuresToMatch, B2B, sceneEdgeCloudPtr,    sceneEdgeCloudPtr);
    matchFeature(featuresToMatch, S2B, sceneSurfaceCloudPtr, sceneEdgeCloudPtr);
    matchFeature(featuresToMatch, B2S, sceneEdgeCloudPtr,    sceneSurfaceCloudPtr);
    matchVisConFeature(featuresToMatch, S2SVisCon, sceneSurfaceCloudPtr, sceneSurfaceCloudPtr); // TODO: MFZ 16/10/17 added this line for visibility context

    clusterRawPoses();
}

template <typename PointNT> std::vector<Pose*>
PFmatcher<PointNT>::getBestPosesOfAllVotingBalls(unsigned int n){
	// MFZ 17.04.2018 changed this function to cope with an arbitrary number of voting balls
	std::vector<Pose*> bestPoses;
	std::vector<Pose*> result;
	for (int votingBall = 0; votingBall < poseClusters.size(); votingBall ++){
    	if(n==0 || n > poseClusters[votingBall].size()){
    		bestPoses.insert(bestPoses.end(), poseClusters[votingBall].begin(), poseClusters[votingBall].end());
		}
    	else{
    		bestPoses.insert(bestPoses.end(), poseClusters[votingBall].begin(), poseClusters[votingBall].begin() + n);
		}
    }
	std::sort(bestPoses.begin(), bestPoses.end(), [](const Pose* a, const Pose* b){return *a > *b;});
	if(n==0 || n > bestPoses.size()){
		return bestPoses;
	}
	else{
		result.insert(result.end(), bestPoses.begin(), bestPoses.begin()+n);
		return result;
	}
}

template <typename PointNT> std::vector<std::vector<Pose*>>
PFmatcher<PointNT>::getBestPosesOfEachVotingBall(unsigned int n){
	// MFZ 17.04.2018 changed this function to cope with an arbitrary number of voting balls
	std::vector<std::vector<Pose*>> result;
	result.resize(poseClusters.size());
	for (int votingBall = 0; votingBall < poseClusters.size(); votingBall ++){
    	if(n==0 || n > poseClusters[votingBall].size()){
			result[votingBall].insert(result[votingBall].end(), poseClusters[votingBall].begin(), poseClusters[votingBall].end());
		}
    	else{
			result[votingBall].insert(result[votingBall].end(), poseClusters[votingBall].begin(), poseClusters[votingBall].begin() + n);
		}
    }
    return result;
}

template <typename PointNT> std::vector<Pose*>
PFmatcher<PointNT>::getBestPosesOfVotingBall(unsigned int n, unsigned int votingBall){

	std::vector<Pose*> result;
	if(n==0 || n > poseClusters[votingBall].size()){
		result.insert(result.end(), poseClusters[votingBall].begin(), poseClusters[votingBall].end());
	}
	else{
		result.insert(result.end(), poseClusters[votingBall].begin(), poseClusters[votingBall].begin() + n);
	}
    return result;
}

template <typename PointNT> std::vector<float>
PFmatcher<PointNT>::getNumberofValidPosesSimple(float thresh){
    
	std::vector<float> result;
	// loop over an arbitrary number of voting balls
	for ( int votingBall = 0; votingBall < poseClusters.size(); votingBall ++){
		// check precondition
		if(poseClusters[votingBall].empty()){
			PCL_ERROR("[PFmatcher::getNumberofValidPoses] no pose clusters for this voting ball. Did you match?\n");
			result.push_back(0);
		}
		unsigned int n = 0;
		float firstClusterWeight = poseClusters[votingBall][0]->getWeight();

		// check pose clusters, in descending weight order
		for(auto& p: poseClusters[votingBall]){
			if(p->getWeight() / firstClusterWeight >= thresh){
				n++;
			}
			else{
				// Pose clusters are ordered by weight. As soon as one drops below the threshold,
				// all following will too.
				break;
			}
		}
		result.push_back(n);
	}
    return result;
}

template <typename PointNT> std::vector<float>
PFmatcher<PointNT>::getNumberOfPoseClusters(){
	std::vector<float> result;
	for (int votingBall = 0; votingBall < poseClusters.size(); votingBall ++){
		result.push_back(poseClusters[votingBall].size());
	}
	return result;
}

template <typename PointNT> std::vector<std::vector<float>>
PFmatcher<PointNT>::getPoseClusterWeights(unsigned int n){
    std::vector<std::vector<float>> result;
	for ( int votingBall = 0; votingBall < poseClusters.size(); votingBall++){
		result.push_back( getWeights(n, poseClusters[votingBall]) );
	}
	return result;
}

template <typename PointNT> std::vector<float>
PFmatcher<PointNT>::getPoseClusterMedianWeight(){
    std::vector<float> result;
	for (int votingBall = 0; votingBall < poseClusters.size(); votingBall ++){
    	// pose clusters are sorted by weight after pose clustering
		result.push_back( getMedianWeight(poseClusters[votingBall]) );
    }
	return result;
}

template <typename PointNT> std::vector<float>
PFmatcher<PointNT>::getNumberOfRawPoses(){
	std::vector<float> result;
	for (int votingBall = 0; votingBall < rawPoses.size(); votingBall ++ ){
		result.push_back(rawPoses[votingBall].size());
	}
	return result;
}

template <typename PointNT> std::vector<std::vector<float>>
PFmatcher<PointNT>::getRawPosesWeights(){
	// be aware, that the sorting of the raw poses depends on the clustering strategy!
	std::vector<std::vector<float>> result;
	for (int votingBall = 0; votingBall < rawPoses.size(); votingBall ++){
		result.push_back(getWeights(0, rawPoses[votingBall]));
	}
	return result;
}


template <typename PointNT> std::vector<float>
PFmatcher<PointNT>::getRawPosesMeanWeight(){
	std::vector<float> result;
	std::vector<std::vector<float>> weights = getRawPosesWeights();
	for (int votingBall = 0; votingBall < rawPoses.size(); votingBall ++) {
		result.push_back( std::accumulate(weights[votingBall].begin(), weights[votingBall].end(), 0.0)/ weights[votingBall].size() );
	}
	return result;
}

template <typename PointNT>std::vector<float>
PFmatcher<PointNT>::getRawPosesMedianWeight(){
	std::vector<float> result;
	for ( int votingBall = 0; votingBall < rawPoses.size(); votingBall ++){
		result.push_back(getMedianWeight(rawPoses[votingBall]));
	}
    // after pose clustering (part of matching), the raw poses are sorted by weight
    return result;
}

template <typename PointNT> std::vector<float>
PFmatcher<PointNT>::getRawPosesMaxWeight(){
	std::vector<float> result;
	for (int votingBall = 0; votingBall < rawPoses.size(); votingBall ++){
		// either the first or the last element of the rawPoses vector should contain the max-element.
		// this depends on the clustering-method and or the time you want to get the max
		if(rawPoses[votingBall].front()->getWeight() > rawPoses[votingBall].back()->getWeight() ){
			result.push_back( rawPoses[votingBall].front()->getWeight() );
		}
		else {
			result.push_back( rawPoses[votingBall].back()->getWeight() );
		}
	}
	return result;
}

template <typename PointNT> void
PFmatcher<PointNT>::freeMemory(){
	// delete raw poses
	for (int votingBall = 0; votingBall < rawPoses.size(); votingBall ++){
		for (int pose = 0; pose < rawPoses[votingBall].size(); pose++){
			delete rawPoses[votingBall][pose];
		}
	}
	// delete pose clusters
	for (int votingBall = 0; votingBall < poseClusters.size(); votingBall ++){
		for (int poseCluster = 0; poseCluster < poseClusters[votingBall].size(); poseCluster++){
			delete poseClusters[votingBall][poseCluster];
		}
	}
	rawPoses.clear();
	poseClusters.clear();
}

template <typename PointNT> unsigned int
PFmatcher<PointNT>::getNumberOfVotingBalls(){
	return poseClusters.size();
}


template <typename PointNT> void
PFmatcher<PointNT>::writePosesToFile(std::vector<Pose*> poses, std::string filename){

	// get home directory of current user
	const char *homedirPtr = getenv("HOME");
	if ( homedirPtr == NULL ) {
		homedirPtr = getpwuid(getuid())->pw_dir;
	}
	std::string homedir(homedirPtr);

	// write poses to file
	std::ofstream outputFile;
	std::string directory = homedir + "/Documents/"+filename;
	outputFile.open (directory, std::ios::out | std::ios::app); // do not delete previous content of the file

	PCL_INFO("[PFmatcher::writePosesToFile] %s there are %u poses in this voting ball.\n", filename.c_str(), poses.size());

	if (outputFile.is_open()){
		//write to file
		for ( int pose = 0; pose < poses.size(); pose ++){
			outputFile << *poses[pose] << "\n";
			if(!outputFile.good()){
				PCL_ERROR("[PFmatcher::writePosesToFile] Error writing Poses to file %s.\n", filename.c_str());
				break;
			}
		}
		// close file
		outputFile.close();
	}
	else {
		PCL_ERROR("Error opening file %s.\n", directory.c_str());
	}
}


template <typename PointNT> void
PFmatcher<PointNT>::writeToFile(std::string lineContent, std::string filename){

	// get home directory of current user
	const char *homedirPtr = getenv("HOME");
	if ( homedirPtr == NULL ) {
		homedirPtr = getpwuid(getuid())->pw_dir;
	}
	std::string homedir(homedirPtr);

	// write linecontent to file
	std::ofstream outputFile;
	std::string directory = homedir + "/Documents/"+filename;
	outputFile.open (directory, std::ios::out | std::ios::app); // do not delete previous content of the file

	PCL_INFO("[PFmatcher::writeToFile] %s: Wrote '%s' to file.\n", filename.c_str(), lineContent.c_str());

	if (outputFile.is_open()){
		//write to file
		outputFile << lineContent << "\n";
		if(!outputFile.good()){
			PCL_ERROR("[PFmatcher::writeToFile] Error writing '%s' to %s file.\n", lineContent.c_str(), filename.c_str());
		}
		// close file
		outputFile.close();
	}
	else {
		PCL_ERROR("[PFmatcher::writeToFile] Error opening file %s.\n", directory.c_str());
	}
}


template <typename PointNT> bool
PFmatcher<PointNT>::matchFeature(const featureType typesForMatching,
                                 const featureType thisType,
                                 const typename pcl::PointCloud<PointNT>::ConstPtr& sceneRefCloudPtr,
                                 const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr){

	// check that this feature should be matched
    if(!(typesForMatching & thisType)){
        return true;
    }
        
    // check that this feature type is present in the model
    if(!(thisType & modelPtr->containedFeatures)){
        PCL_WARN("[PFmatcher::matchFeature] Model does not contain the feature type %s." \
                 "Skipping matching for this feature.\n",
                 featureToStr(thisType).c_str());
        return false;
    }

    // check that all required scene clouds were set
    if(!(sceneRefCloudPtr && sceneTargetCloudPtr)){
        PCL_WARN("[PFmatcher::matchFeature] Not all required scene point clouds are set for " \
                 "matching feature type %s. Skipping matching for this feature.\n",
                 featureToStr(thisType).c_str());
        return false;
    }
    
    // check if CMPH_CHD flag array is only used in combination with an CMPH_CHD hash table.
	// because CMPH_CHD flag array is not a stand-alone implementation, it needs the
	// CMPH_CHD minimal perfect hash function that is constructed during model training.
	if ( (faHashTableType == FA_CMPH_CHD) && (modelPtr->getModelHashTableType() == MH_STL) ){
		ROS_ERROR_STREAM("[PFmatcher::matchFeature] Can't combine CMPH_CHD flag array and STL model hash table. "\
				 	 	 "CMPH_CHD flag array needs minimal perfect hash function that is constructed during model " \
						 "training with CMPH_CHD model hash table. Skipping matching with this feature.\n");
		return false;
	}

    // get index of Hash table containing this feature type and make reference for easier code
    int htIndex = modelPtr->getHashTableIndex(thisType);
    const typename modelHashTable<PointNT>::Ptr htPtr = modelPtr->hashTables[htIndex];

    // start the clock to get voting performance (compare flag array versions)
    boost::timer::cpu_timer timerWithVoxelGridForVotingBallBuilding;
	boost::timer::cpu_timer timerWithFlagArrayBuilding;
	boost::timer::cpu_timer timerWithoutFlagArrayBuilding;
	timerWithVoxelGridForVotingBallBuilding.start();

	if(useVotingBalls){

		// TODO: 23.04.2018 change this size, if number of voting balls gets dynamic
		rawPoses.resize(2);

		// MFZ 17/11/2017 calculate size of voxel-grid via bounding box:
		//
		// 		d min is the smallest dimension of the object's bounding box and
		// 		d med is the median of its three dimensions,
		//		d_obj is the diameter of the enclosing sphere of the target object
		//
		// 		R_max = d_obj
		// 		R_min = sqrt((d_min)² + (d_med)²)
		//
		// construct two voxel-grids with these voxel-edge-lengths

		// use the dimensions (d_min,d_med,d_max) of the model, obtained with PCA
		// TODO: 12.04.2018 decide with the help of the radii's ratio, if one should use one or more voting balls.
		float r_min = sqrt(pow(modelPtr->d_min,2) + pow(modelPtr->d_med,2));
		float r_max = modelPtr->modelDiameter;
		ROS_INFO_STREAM("[PFmatcher::matchFeature] Using voting balls for matching. The voting ball dimensions are: r_min = " << r_min << " and r_max = " << r_max);

		// info message
		if (voteForAdjacentRotationAngles){ ROS_INFO_STREAM("[PFmatcher::matchFeature] Voting for adjacent scene-rotation-angles too.");}

#ifdef use_octree_radius_search
		// instantiate the octrees for the voting balls
		pcl::octree::OctreePointCloudSearch<PointNT> octreeSmallVotingBall(r_min);
		octreeSmallVotingBall.setInputCloud(sceneRefCloudPtr);
		octreeSmallVotingBall.addPointsFromInputCloud();
		pcl::octree::OctreePointCloudSearch<PointNT> octreeLargeVotingBall(r_max);
		octreeLargeVotingBall.setInputCloud(sceneRefCloudPtr);
		octreeLargeVotingBall.addPointsFromInputCloud();
#else
		// instantiate kd-trees for the voting balls
		// TODO the low performance in my tests could be a result that the NVIDIA graphics card was not installed
		//      using octree-search instead
		pcl::KdTreeFLANN<PointNT> kdRadiusSearchTree;
		kdRadiusSearchTree.setInputCloud(sceneRefCloudPtr);
#endif

		// Instantiate sceneFeature, set parameters
		// 29/05/2018 moved the instantiation up here (instead of instantiating it in 'voteWithTargetPoints'), to make sceneFeature a 'private' copy for each thread.
		MinimalDiscretePF<PointNT> sceneFeature(htPtr.get());

		// copy some parameters from matcher into hashtable to use Hinterstoisser's extensions during matching
		htPtr->setMatchingParameters(this);

		// get max number of threads
		int maxNumberOfThreads = omp_get_max_threads();
		// define custom chunk-size
		int chunkSize = sceneRefCloudPtr->size() / maxNumberOfThreads;

		ROS_WARN_STREAM("[PFmatcher::matchFeature] Maximum number of threads = " << maxNumberOfThreads << ". All of them will be used for matching.");

		// start the clock to compare flag array versions
		timerWithFlagArrayBuilding.start();

		// build flag array
		flagArray<PointNT> *refPointFlagArrayPtr = nullptr;
		if (faHashTableType != FA_NONE){
			// 'sharing' the same flag array between the threads, i.e. not making them 'private' to each tread. Because
			//   1. flag arrays may get too big to be placed on the stack. but for each thread to use its own flag array it has to fit into the stack
			//   2. using the pointer, we do not need to call the flag arrays constructor, unless we really want to. i.e. saving time.
			refPointFlagArrayPtr = new flagArray<PointNT>(faHashTableType, sceneRefCloudPtr->size(), htPtr.get(), maxNumberOfThreads);
		}

		// start the clock to compare flag array versions
		timerWithoutFlagArrayBuilding.start();

		// alternative:
		#pragma omp parallel firstprivate(sceneFeature /*, octreeSmallVotingBall, octreeLargeVotingBall*/) num_threads(maxNumberOfThreads)
		{

		// thread owned, temporary storage for rawPoses:
		std::vector<std::vector<Pose*> > local_rawPoses;
		local_rawPoses.resize(2); // TODO: change this, if more or less then two voting balls shall be used
		for (int votingBall = 0; votingBall < local_rawPoses.size(); votingBall ++){
			local_rawPoses[votingBall].reserve(chunkSize);
		}

		// loop over scene reference points
		// #pragma omp parallel for //TODO: old, because if ordered part below is omitted, the resulting clusters
		// would be random, i.e. different for each run, because clustering becomes a random process
		// #pragma omp parallel for /*ordered*/ schedule(static) firstprivate(sceneFeature, octreeSmallVotingBall, octreeLargeVotingBall) num_threads(maxNumberOfThreads)
		#pragma omp for /*ordered*/ schedule(dynamic)
		for(int i_r = 0; i_r < sceneRefCloudPtr->size(); i_r = i_r + refPointStep){

			// get Eigen-style point information of reference point
			Eigen::Vector3f s_r = sceneRefCloudPtr->points[i_r].getVector3fMap();
			Eigen::Vector3f n_r = sceneRefCloudPtr->points[i_r].getNormalVector3fMap();

			// pre-calculate transform Ts->g (align s_r and n_r to origin / x-axis)
			Eigen::Affine3f Tsg = aligningTransform(s_r, n_r);

			// make Hough Space and space for tracking alpha float values, size depends on model only
			// x -> reference point index
			// y -> alpha index
			HoughSpace2D accumulatorArray(htPtr->refPointCloudPtr->size(), n_alpha); // actual voting space
			HoughSpace2D alphaArray(htPtr->refPointCloudPtr->size(), n_alpha);       // float values for alpha
			std::vector<std::vector<HoughBucket2D>> maxBuckets;

			// use small voting ball first
			// find targetPoints within small radius
			std::vector<int> targetPointIndicesWithinSmallRadius;
			std::vector<float> targetPointsWithinSmallRadiusSQRDdistances;
#ifdef use_octree_radius_search
			int numberOfTargetPoints = octreeSmallVotingBall.radiusSearch(i_r, r_min, targetPointIndicesWithinSmallRadius, targetPointsWithinSmallRadiusSQRDdistances);
#else
			int numberOfTargetPoints = kdRadiusSearchTree.radiusSearch(sceneRefCloudPtr->points[i_r], r_min, targetPointIndicesWithinSmallRadius, targetPointsWithinSmallRadiusSQRDdistances);
#endif
			// vote with small voting ball
			if (numberOfTargetPoints > 0){

				voteWithVotingBall(targetPointIndicesWithinSmallRadius, s_r, n_r, Tsg, accumulatorArray, alphaArray, sceneTargetCloudPtr, htPtr, sceneFeature, refPointFlagArrayPtr);
			}
			else{
				ROS_WARN_STREAM("[PFmatcher::matchFeature] Small voting ball contains no points! Something went wrong!");
			}
			// "We will say that a point pair is accepted by a voting ball if the first point is at the
			//  center of the ball and its distance to the second point is smaller than the radius of the ball" (--> R_min ).
			// [Hinterstoisser et al. 2016]

			// [Hinterstoisser et al. 2016] handle the hypotheses of both voting balls separately
			// search for maxima in accumulator array
			maxBuckets.push_back(accumulatorArray.getMaxBuckets(maxThresh));

			// use the large voting ball
			// find targetPoints within large radius
			std::vector<int> targetPointIndicesWithinLargeRadius;
			std::vector<float> targetPointsWithinLargeRadiusSQRDdistances;
#ifdef use_octree_radius_search
			numberOfTargetPoints = octreeLargeVotingBall.radiusSearch(i_r, r_max, targetPointIndicesWithinLargeRadius, targetPointsWithinLargeRadiusSQRDdistances);
#else
			numberOfTargetPoints = kdRadiusSearchTree.radiusSearch(sceneRefCloudPtr->points[i_r], r_max, targetPointIndicesWithinLargeRadius, targetPointsWithinLargeRadiusSQRDdistances);
#endif

			if( numberOfTargetPoints > 0){
				// erase point indices of targetPoiunts that were already examined with small voting ball
				// the indices returned by radiusSearch() are not sorted
				std::sort(targetPointIndicesWithinLargeRadius.begin(), targetPointIndicesWithinLargeRadius.end());
				std::sort(targetPointIndicesWithinSmallRadius.begin(), targetPointIndicesWithinSmallRadius.end());
				std::vector<int> targetPointIndicesDifference;
				std::set_difference (targetPointIndicesWithinLargeRadius.begin(), targetPointIndicesWithinLargeRadius.end(),
									 targetPointIndicesWithinSmallRadius.begin(), targetPointIndicesWithinSmallRadius.end(),
									 std::inserter(targetPointIndicesDifference, targetPointIndicesDifference.begin()));

				// vote with large voting ball
				voteWithVotingBall(targetPointIndicesDifference, s_r, n_r, Tsg, accumulatorArray, alphaArray, sceneTargetCloudPtr, htPtr, sceneFeature, refPointFlagArrayPtr);

			}
			else{
				ROS_WARN_STREAM("[PFmatcher::matchFeature] Large voting ball contains no points! Something went wrong!");
			}

			// [Hinterstoisser et al. 2016] handle the hypotheses of both voting balls separately
			// search for maxima in accumulator array
			maxBuckets.push_back(accumulatorArray.getMaxBuckets(maxThresh));

			// reset the flags in the flag array
			if(faHashTableType != FA_NONE){
				(refPointFlagArrayPtr->*refPointFlagArrayPtr->resetFlags)(omp_get_thread_num());
			}

			// convert the best buckets back to poses and store them
//			#pragma omp ordered //!< Why did Xaver make this 'ordered'?
//								//! IDEA: 	the reason for this ordered clause is, that this is a simple way, to ensure a deterministic
//								//! 		behaviour of the algorithm. because, if the raw poses were stored in an ARBITRARY order, that
//								//!			differs each run due to the unpredictability of omp parallel for schedule(dynamic), the out-
//								//! 		come of the *clustering* is different each run (non determinism). simply because the cluster-
//								//! 		ing will be a little bit different each time, because the raw poses are ordered differently.
			for (int votingBall = 0; votingBall < maxBuckets.size(); votingBall ++){
				// maximum counter
				int maximumIndex = 0;
				for (HoughBucket2D& bucket : maxBuckets[votingBall]){
					// calculate aligning transform for model reference point
					PointNT m_r = htPtr->refPointCloudPtr->points[bucket.xIndex];
					Eigen::Affine3f Tmg = aligningTransform(m_r.getVector3fMap(),
															m_r.getNormalVector3fMap());

					// calculate alpha as weighted mean of alpha space instead of
					// just using the other accumulator array index
					float alpha = alphaArray.getBucketValue(bucket.xIndex, bucket.yIndex)
								  / bucket.accumulatedWeight;

#ifdef  write_alphas_to_binary_file
					// TODO: 30.07.2018 remove after debugging
					accumulatorArrayAlphas.push_back(alpha);
#endif

					// add raw pose, constructs full aligning transform
					if(useHinterstoisserClustering){
//						#pragma omp critical (add_raw_pose)
//						{
						local_rawPoses[votingBall].emplace_back(new PoseWithAssociatedModelPoint(Tsg, Tmg, alpha, bucket.accumulatedWeight, i_r, maximumIndex, bucket.xIndex));
//						}
					}
					else{
//						#pragma omp critical (add_raw_pose)
//						{
						local_rawPoses[votingBall].emplace_back(new Pose(Tsg, Tmg, alpha, bucket.accumulatedWeight, i_r, maximumIndex));
//						}
					}
					// increment maximum counter
					maximumIndex ++;
				}
			}


			// save image of the Hough space to disk for later analysis
			// layout of the space:
			// o--> reference poins
			// |
			// V alpha steps
			if(saveHoughSpaceToFile){

				// full image (all buckets)
				std::vector<unsigned short> img = accumulatorArray.getAsUShortImg();
				char fname[400];
				snprintf(fname, sizeof(fname),"%s/feature_%d__refP_%d__all.png", saveDir.c_str(), thisType, i_r);

				pcl::io::saveShortPNGFile(fname, &img[0], accumulatorArray.xSize, accumulatorArray.ySize, 1);

				// only the buckets that lead to a pose estimation
				img = accumulatorArray.getAsUShortImg(maxThresh);
				snprintf(fname, sizeof(fname), "%s/feature_%d__refP_%d__top_%.2f.png", saveDir.c_str(), thisType, i_r, maxThresh);

				pcl::io::saveShortPNGFile(fname, &img[0], accumulatorArray.xSize, accumulatorArray.ySize, 1);
			}
		} // loop over refPoints

		// store the local_rawPoses in rawPoses
		#pragma omp critical (merge_local_rawPoses)
		{
		for (int votingBall = 0; votingBall < rawPoses.size(); votingBall ++){
			rawPoses[votingBall].insert(rawPoses[votingBall].begin(),
									    local_rawPoses[votingBall].begin(),
									    local_rawPoses[votingBall].end());
		}
		}

		} // end omp parallel

		if (faHashTableType != FA_NONE){ // if a flag array is used
			delete refPointFlagArrayPtr;
		}
	} // if useVotingBalls

	else { // do not use votingBalls
		rawPoses.resize(1);
		// Instantiate sceneFeature, set parameters
		// 29/05/2018 moved the instantiation up here (instead of instantiating it in 'voteWithTargetPoints'), to make sceneFeature a 'private' copy for each thread.
		MinimalDiscretePF<PointNT> sceneFeature(htPtr.get());

		// copy some parameters from matcher into hash table to use Hinterstoisser's extensions during matching
		htPtr->setMatchingParameters(this);

		// info message
		if (voteForAdjacentRotationAngles){ ROS_INFO_STREAM("[PFmatcher::matchFeature] Voting for adjacent scene-rotation-angles too.");}

		// get max number of threads
		int maxNumberOfThreads = omp_get_max_threads();
		// define custom chunkSize
		int chunkSize = sceneRefCloudPtr->size() / maxNumberOfThreads;

		ROS_WARN_STREAM("[PFmatcher::matchFeature] Maximum number of threads = " << maxNumberOfThreads << ". All of them will be used for matching.");

		// start the clock to compare flag array versions
		timerWithFlagArrayBuilding.start();

		// build flag array
		flagArray<PointNT> *refPointFlagArrayPtr = nullptr;
		if (faHashTableType != FA_NONE){
			// 'sharing' the same flag array between the threads, i.e. not making them 'private' to each tread. Because
			//   1. flag arrays may get too big to be placed on the stack. but for each thread to use its own flag array it has to fit into the stack
			//   2. using the pointer, we do not need to call the flag arrays constructor, unless we really want to. i.e. saving time.
			refPointFlagArrayPtr = new flagArray<PointNT>(faHashTableType, sceneRefCloudPtr->size(), htPtr.get(), maxNumberOfThreads);
		}

		// start the clock to compare flag array versions
		timerWithoutFlagArrayBuilding.start();

		// alternative:
		#pragma omp parallel firstprivate(sceneFeature) num_threads(maxNumberOfThreads)
		{

		// thread owned, temporary storage for rawPoses:
		std::vector<std::vector<Pose*> > local_rawPoses;
		local_rawPoses.resize(1); // TODO: change this, if more or less then two voting balls shall be used
		for (int votingBall = 0; votingBall < local_rawPoses.size(); votingBall ++){
			local_rawPoses[votingBall].reserve(chunkSize);
		}

		// loop over scene reference points
		// #pragma omp parallel for //TODO: old, because if ordered part below is omitted,
		// the resulting clusters will be random, i.e. different for each run, because clustering becomes a random process
		// #pragma omp parallel for /*ordered*/ schedule(static) firstprivate(sceneFeature) num_threads(maxNumberOfThreads)
		#pragma omp for /*ordered*/ schedule(dynamic)
		for(int i_r = 0; i_r < sceneRefCloudPtr->size(); i_r = i_r + refPointStep){

			// get Eigen-style point information of reference point
			Eigen::Vector3f s_r = sceneRefCloudPtr->points[i_r].getVector3fMap();
			Eigen::Vector3f n_r = sceneRefCloudPtr->points[i_r].getNormalVector3fMap();

			// pre-calculate transform Ts->g (align s_r and n_r to origin / x-axis)
			Eigen::Affine3f Tsg = aligningTransform(s_r, n_r);

			// make Hough Space and space for tracking alpha float values, size depends on model only
			// x -> reference point index
			// y -> alpha index
			HoughSpace2D accumulatorArray(htPtr->refPointCloudPtr->size(), n_alpha); // actual voting space
			HoughSpace2D alphaArray(htPtr->refPointCloudPtr->size(), n_alpha);       // float values for alpha
			std::vector<HoughBucket2D> maxBuckets;

			// voting with all target points
			voteWithTargetPoints(s_r, n_r, Tsg, accumulatorArray, alphaArray, sceneTargetCloudPtr, htPtr, sceneFeature, refPointFlagArrayPtr);

			// search for maxima in accumulator array
			maxBuckets = accumulatorArray.getMaxBuckets(maxThresh);

			// reset the flags in the flag array
			if(faHashTableType != FA_NONE){
				(refPointFlagArrayPtr->*refPointFlagArrayPtr->resetFlags)(omp_get_thread_num());
			}

			// convert the best buckets back to poses and store them
//			#pragma omp ordered //!< Why did Xaver make this 'ordered'?
//								//! IDEA: 	the reason for this ordered clause is, that this is a simple way, to ensure a deterministic
//								//! 		behaviour of the algorithm. because, if the raw poses were stored in an ARBITRARY order, that
//								//!			differs each run due to the unpredictability of omp parallel for schedule(dynamic), the out-
//								//! 		come of the *clustering* is different each run (non determinism). simply because the cluster-
//								//! 		ing will be a little bit different each time, because the raw poses are ordered differently.
			// maximum counter
			int maximumIndex = 0;
			for (HoughBucket2D& bucket : maxBuckets){
				// calculate aligning transform for model reference point
				PointNT m_r = htPtr->refPointCloudPtr->points[bucket.xIndex];
				Eigen::Affine3f Tmg = aligningTransform(m_r.getVector3fMap(),
														m_r.getNormalVector3fMap());

				// calculate alpha as weighted mean of alpha space instead of
				// just using the other accumulator array index
				float alpha = alphaArray.getBucketValue(bucket.xIndex, bucket.yIndex)
							  / bucket.accumulatedWeight;

#ifdef write_alphas_to_binary_file
				// TODO: 30.07.2018 remove after debugging
				accumulatorArrayAlphas.push_back(alpha);
#endif

				// add raw pose, constructs full aligning transform
				if(useHinterstoisserClustering){
//					#pragma omp critical (add_raw_pose)
//					{
					local_rawPoses[0].emplace_back(new PoseWithAssociatedModelPoint(Tsg, Tmg, alpha, bucket.accumulatedWeight, i_r, maximumIndex, bucket.xIndex));
//					}
				}
				else{
//					#pragma omp critical (add_raw_pose)
//					{
					local_rawPoses[0].emplace_back(new Pose(Tsg, Tmg, alpha, bucket.accumulatedWeight, i_r, maximumIndex));
//					}
				}
				// increment maximum counter
				maximumIndex ++;
			}

			// save image of the Hough space to disk for later analysis
			// layout of the space:
			// o--> reference poins
			// |
			// V alpha steps
			if(saveHoughSpaceToFile){
				std::string sceneName = sceneRefCloudPtr->header.frame_id;
				std::string modelName = htPtr->refPointCloudPtr->header.frame_id;
				// full image (all buckets)
				std::vector<unsigned short> img = accumulatorArray.getAsUShortImg();
				char fname[400];
				snprintf(fname, sizeof(fname),"%s/%s_%s_feature_%d__refP_%d__all.png", saveDir.c_str(), modelName.c_str(), sceneName.c_str(), thisType, i_r);

				pcl::io::saveShortPNGFile(fname, &img[0], accumulatorArray.xSize, accumulatorArray.ySize, 1);

				// only the buckets that lead to a pose estimation
				img = accumulatorArray.getAsUShortImg(maxThresh);
				snprintf(fname, sizeof(fname), "%s/%s_%s_feature_%d__refP_%d__top_%.2f.png", saveDir.c_str(), modelName.c_str(), sceneName.c_str(), thisType, i_r, maxThresh);

				pcl::io::saveShortPNGFile(fname, &img[0], accumulatorArray.xSize, accumulatorArray.ySize, 1);
			}
		} // loop over reference points

		// store the local_rawPoses in rawPoses
		#pragma omp critical (merge_local_rawPoses)
		{
		for (int votingBall = 0; votingBall < rawPoses.size(); votingBall ++){
			rawPoses[votingBall].insert(rawPoses[votingBall].begin(),
										   local_rawPoses[votingBall].begin(),
										   local_rawPoses[votingBall].end());
		}
		}

		} // end of omp parallel

		if (faHashTableType != FA_NONE){ // if a flag array is used
		    delete refPointFlagArrayPtr;
		}
	} // do not use voting balls

	// time needed for the voting
	msElapsedWithFlagArrayBuilding = timerWithFlagArrayBuilding.elapsed().wall / 1e6;
	msElapsedWithoutFlagArrayBuilding = timerWithoutFlagArrayBuilding.elapsed().wall / 1e6;
	msElapesdWithVoxelGridForVotingBallBuilding = timerWithVoxelGridForVotingBallBuilding.elapsed().wall / 1e6;
	ROS_INFO_STREAM("[PFmatcher::matchFeature] Voting done. Took " << msElapsedWithFlagArrayBuilding <<
					" ms with and " << msElapsedWithoutFlagArrayBuilding << " ms without flagArrayBuildingTime.");
	ROS_INFO_STREAM("[PFmatcher::matchFeature]              With Building time for the Voting Ball Voxel Grids it took " << msElapesdWithVoxelGridForVotingBallBuilding << " ms.");

	save_matching_debug_data_to_files(htPtr, sceneRefCloudPtr);

	return true;
}

template <typename PointNT> void
PFmatcher<PointNT>::save_matching_debug_data_to_files(const typename modelHashTable<PointNT>::Ptr& htPtr,
													  const typename pcl::PointCloud<PointNT>::ConstPtr& sceneRefCloudPtr){

	// get home directory of current user
	const char *homedirPtr = getenv("HOME");
	if ( homedirPtr == NULL ) {
		homedirPtr = getpwuid(getuid())->pw_dir;
	}
	std::string homedir(homedirPtr);

	// TODO: remove after debugging:
#ifdef write_voting_time_to_file
	// save time to file
	std::ofstream outputFile;
	outputFile.open ( homedir + "/Documents/voting_times.txt", std::ios::out | std::ios::app); // do not delete previous content of the file

	if (outputFile.is_open()){
		//write to file
		outputFile << msElapesdWithVoxelGridForVotingBallBuilding << " " <<
					  msElapsedWithFlagArrayBuilding << " " <<
					  msElapsedWithoutFlagArrayBuilding << " " <<
					  faHashTableType << " " <<
					  modelPtr->surfaceCloudPtr->points.size() << " " <<
					  sceneRefCloudPtr->points.size() << "\n";
		// check flags
		if(!outputFile.good()){
			PCL_ERROR("[PFmatcher::matchFeature] Error writing to voting_times.txt file.");
		}
		// close file
		outputFile.close();
	}
	else {
		PCL_ERROR("[PFmatcher::matchFeature] Error opening voting_times.txt file");
	}
#endif

	// TODO: remove after debugging:
#ifdef write_scene_PPfs_hashKeys_to_binary_file
	// TODO: 04.07.2018 in order to find the source of the matchings non-deterministic behaviour,
	//       that leads to different poses for the same experiment. remove this later.
	// write to a binary file within the specified directory:
	std::time_t timestamp = std::time(0);
	std::ofstream outputFile_2;
	std::string filename;
	std::string cloudname = sceneRefCloudPtr->header.frame_id + "_";
	if( (htPtr->getHashTableType() == 1) || (htPtr->getHashTableType() == 2) ){
		filename = homedir + "/Documents/MY_SYSTEM_STL_HT_ppfs_of_" + cloudname;
	}
	if( (htPtr->getHashTableType() == 3) || (htPtr->getHashTableType() == 4) ){
		filename = homedir + "/Documents/MY_SYSTEM_CMPH_HT_ppfs_of_" + cloudname;
	}
	outputFile_2.open ( filename + std::to_string(timestamp) + ".bin", std::ios::out | std::ios::trunc | std::ios::binary); // delete previous content of the file
	if (outputFile_2.is_open()){
		//write to file
		for (int line =0 ; line < htPtr->sceneHashKeys.size() ; line ++){
			outputFile_2.write((char*)&(htPtr->sceneHashKeys[line]), sizeof(uint64_t));
			//check flag
			if(!outputFile_2.good()){
				PCL_ERROR("[PFmatcher::matchFeature] Error writing scene PPFs to %s file.", filename);
				break;
			}
		}
		// close file
		outputFile_2.close();
	}
	else {
		std::cout << "Error opening file for storing the scene PPFs" << std::endl;
	}

	// clear storage vector
	htPtr->sceneHashKeys.clear();
#endif

	// TODO: remove after debugging:
#ifdef write_alphas_to_binary_file
	// TODO: 04.07.2018 in order to find the source of the matchings non-deterministic behaviour,
	//       that leads to different poses for the same experiment. remove this later.
	// write to a binary file within the specified directory:
	std::time_t timestamp = std::time(0);
	std::ofstream outputFile_3;
	std::string filename;
	std::string cloudname = sceneRefCloudPtr->header.frame_id + "_";
	// write alphas
	if( (htPtr->getHashTableType() == 1) || (htPtr->getHashTableType() == 2) ){
		filename = homedir + "/Documents/MY_SYSTEM_STL_HT_alphas_of_" + cloudname;
	}
	if( (htPtr->getHashTableType() == 3) || (htPtr->getHashTableType() == 4) ){
		filename = homedir + "/Documents/MY_SYSTEM_CMPH_HT_alphas_of_" + cloudname;
	}
	outputFile_3.open ( filename + std::to_string(timestamp) + ".bin", std::ios::out | std::ios::trunc | std::ios::binary); // delete previous content of the file
	if (outputFile_3.is_open()){
		//write to file
		for (int line =0 ; line < htPtr->alphas.size() ; line ++){
			outputFile_3.write((char*)&(htPtr->alphas[line]), sizeof(float));
			if(!outputFile_3.good()){
				PCL_ERROR("[PFmatcher::matchFeature] Error writing alphas to %s file.", filename.c_str());
				break;
			}
		}
		// close file
		outputFile_3.close();
	}
	else {
		std::cout << "Error opening file for storing the alphas." << std::endl;
	}

	// write alphas_s
	std::ofstream outputFile_4;
	if( (htPtr->getHashTableType() == 1) || (htPtr->getHashTableType() == 2) ){
		filename = homedir + "/Documents/MY_SYSTEM_STL_HT_alphas_s_of_" + cloudname;
	}
	if( (htPtr->getHashTableType() == 3) || (htPtr->getHashTableType() == 4) ){
		filename = homedir + "/Documents/MY_SYSTEM_CMPH_HT_alphas_s_of_" + cloudname;
	}
	outputFile_4.open ( filename + std::to_string(timestamp) + ".bin", std::ios::out | std::ios::trunc | std::ios::binary); // delete previous content of the file
	if (outputFile_4.is_open()){
		//write to file
		for (int line =0 ; line < htPtr->alphas_s.size() ; line ++){
			outputFile_4.write((char*)&(htPtr->alphas_s[line]), sizeof(float));
			if(!outputFile_4.good()){
				PCL_ERROR("[PFmatcher::matchFeature] Error writing alphas_s to %s file.", filename.c_str());
				break;
			}
		}
		// close file
		outputFile_4.close();
	}
	else {
		std::cout << "Error opening file for storing the alphas_s." << std::endl;
	}

	// write alphas_m
	std::ofstream outputFile_5;
	if( (htPtr->getHashTableType() == 1) || (htPtr->getHashTableType() == 2) ){
		filename = homedir + "/Documents/MY_SYSTEM_STL_HT_alphas_m_of_" + cloudname;
	}
	if( (htPtr->getHashTableType() == 3) || (htPtr->getHashTableType() == 4) ){
		filename = homedir + "/Documents/MY_SYSTEM_CMPH_HT_alphas_m_of_" + cloudname;
	}
	outputFile_5.open ( filename + std::to_string(timestamp) + ".bin", std::ios::out | std::ios::trunc | std::ios::binary); // delete previous content of the file
	if (outputFile_5.is_open()){
		//write to file
		for (int line =0 ; line < htPtr->alphas_m.size() ; line ++){
			outputFile_5.write((char*)&(htPtr->alphas_m[line]), sizeof(float));
			if(!outputFile_5.good()){
				PCL_ERROR("[PFmatcher::matchFeature] Error writing alphas_m to %s file.", filename.c_str());
				break;
			}
		}
		// close file
		outputFile_5.close();
	}
	else {
		std::cout << "Error opening file for storing the alphas_m." << std::endl;
	}

	// write alphaIndices
	std::ofstream outputFile_6;
	cloudname = htPtr->refPointCloudPtr->header.frame_id + "_";
	if( (htPtr->getHashTableType() == 1) || (htPtr->getHashTableType() == 2) ){
		filename = homedir + "/Documents/MY_SYSTEM_STL_HT_alphaIndices_of_" + cloudname;
	}
	if( (htPtr->getHashTableType() == 3) || (htPtr->getHashTableType() == 4) ){
		filename = homedir + "/Documents/MY_SYSTEM_CMPH_HT_alphaIndices_of_" + cloudname;
	}
	outputFile_6.open ( filename + std::to_string(timestamp) + ".bin", std::ios::out | std::ios::trunc | std::ios::binary); // delete previous content of the file
	if (outputFile_6.is_open()){
		//write to file
		for (int line =0 ; line < htPtr->alphaIndices.size() ; line ++){
			outputFile_6.write((char*)&(htPtr->alphaIndices[line]), sizeof(unsigned int));
			if(!outputFile_6.good()){
				PCL_ERROR("[PFmatcher::matchFeature] Error writing alphaInidces to %s file.", filename.c_str());
				break;
			}
		}
		// close file
		outputFile_6.close();
	}
	else {
		std::cout << "Error opening file for storing the alphaIndices." << std::endl;
	}

	// write accumulator array alphas
	std::ofstream outputFile_7;
	cloudname = htPtr->refPointCloudPtr->header.frame_id + "_model_in_" + sceneRefCloudPtr->header.frame_id + "_";
	if( (htPtr->getHashTableType() == 1) || (htPtr->getHashTableType() == 2) ){
		filename = homedir + "/Documents/MY_SYSTEM_STL_HT_accumulatorArrayAlphas_of_" + cloudname;
	}
	if( (htPtr->getHashTableType() == 3) || (htPtr->getHashTableType() == 4) ){
		filename = homedir + "/Documents/MY_SYSTEM_CMPH_HT_accumulatorArrayAlphas_of_" + cloudname;
	}
	outputFile_7.open ( filename + std::to_string(timestamp) + ".bin", std::ios::out | std::ios::trunc | std::ios::binary); // delete previous content of the file
	if (outputFile_7.is_open()){
		//write to file
		for (int line =0 ; line < accumulatorArrayAlphas.size() ; line ++){
			outputFile_7.write((char*)&(accumulatorArrayAlphas[line]), sizeof(unsigned int));
			if(!outputFile_7.good()){
				PCL_ERROR("[PFmatcher::matchFeature] Error writing accumulatorArrayAlphas to %s file.", filename.c_str());
				break;
			}
		}
		// close file
		outputFile_7.close();
	}
	else {
		std::cout << "Error opening file for storing the accumulatorArrayAlphas." << std::endl;
	}

	// clear storage vectors
	htPtr->alphas.clear();
	htPtr->alphas_s.clear();
	htPtr->alphas_m.clear();
	htPtr->alphaIndices.clear();
	accumulatorArrayAlphas.clear();
#endif

	// TODO: remove after debugging:
#ifdef write_rawPoses_to_binary_file
	// TODO: 14.07.2018 in order to find the source of the matchings non-deterministic behaviour,
	//       that leads to different number of clusters for the same experiment. STL returns other number then CMPH. remove this later.
	// write to a binary file within the specified directory:
	std::time_t timestamp_ = std::time(0);
	std::string filename_;
	std::string cloudname_ = sceneRefCloudPtr->header.frame_id + "_";
	if( (htPtr->getHashTableType() == 1) || (htPtr->getHashTableType() == 2) ){
		filename_ = "MY_SYSTEM_STL_HT_rawPoses_of_" + cloudname_ + std::to_string(timestamp_) + ".txt";
	}
	if( (htPtr->getHashTableType() == 3) || (htPtr->getHashTableType() == 4) ){
		filename_ = "MY_SYSTEM_CMPH_HT_rawPoses_of_" + cloudname_ + std::to_string(timestamp_) + ".txt";
	}
	for (int votingBall = 0; votingBall < rawPoses.size(); votingBall ++){
		writePosesToFile(rawPoses[votingBall], filename_);
	}
#endif

#ifdef write_counters_to_file
	double sumIS = htPtr->numberOfFeaturesFoundInHT + htPtr->numberOfFeaturesNotFoundInHT + htPtr->FAdiscardsVoteCounter + htPtr->CMPHoutOfRangeCounter;
	double sumSHOULD = sceneRefCloudPtr->size() * (sceneRefCloudPtr->size()-1); // ignoring stepsize and voting balls
	this->writeToFile(std::to_string(htPtr->numberOfFeaturesFoundInHT) + " "
					+ std::to_string(htPtr->numberOfFeaturesNotFoundInHT) + " "
					+ std::to_string(htPtr->FAdiscardsVoteCounter) + " "
					+ std::to_string(htPtr->CMPHoutOfRangeCounter) + " "
					+ std::to_string(sumIS) + " "
					+ std::to_string(sumSHOULD) + " "
					+ std::to_string(htPtr->originalVoteCounter) + " "
					+ std::to_string(htPtr->biggerVoteCounter) + " "
					+ std::to_string(htPtr->smallerVoteCounter)+ " "
					+ std::to_string(htPtr->get_d_dist_abs()) + " "
					+ std::to_string(htPtr->get_modelDiameter()) +  " "
					+ std::to_string(htPtr->get_d_angle()) + " "
					+ std::to_string(modelPtr->d_dist) + " "
					+ std::to_string(sceneRefCloudPtr->size()),
					"voting_counters.txt" );

	// reset all counters
	htPtr->numberOfFeaturesFoundInHT = 0;
	htPtr->numberOfFeaturesNotFoundInHT = 0;
	htPtr->FAdiscardsVoteCounter = 0;
	htPtr->CMPHoutOfRangeCounter = 0;
	htPtr->originalVoteCounter = 0;
	htPtr->biggerVoteCounter = 0;
	htPtr->smallerVoteCounter = 0;
#endif

#ifdef write_cmph_hash_keys_out_of_range_to_file
	// TODO: remove this after debugging: ( THIS IS ONLY FOR USE WITH CMPH HASH TABLE and only S2S without VisCon, use with STL will crash )
	typename CMPHmodelHashTable<PointNT>::Ptr htPtr_casted = boost::dynamic_pointer_cast<CMPHmodelHashTable<PointNT> >(htPtr);
	if (htPtr_casted != nullptr){
		double upperBound = htPtr_casted->minimalHashTable.size()-1;
		for (int entry = 0; entry < htPtr->HashKeysOutOfRange.size(); entry ++){
			// write 64 bit hash key to file
			std::bitset<64> binaryValue (htPtr->HashKeysOutOfRange[entry]);
			// disassemble int representation again
			uint16_t discretePF[4];
			for (int i = 0; i < 4; i++){
				discretePF[i] = ((uint16_t*)& htPtr->HashKeysOutOfRange[entry])[i];
			}
			this->writeToFile(std::to_string(htPtr->HashKeysOutOfRange[entry]) + " " + binaryValue.to_string() + " (" +  std::to_string(discretePF[0]) + "," +
																														 std::to_string(discretePF[1]) + "," +
																														 std::to_string(discretePF[2]) + "," +
																														 std::to_string(discretePF[3]) + ") "
							 + std::to_string(htPtr->HashValuesOutOfRange[entry]) + " but HashTable is [0," + std::to_string(upperBound) +"]",
							 "hash_keys_out_of_CMPH_ht_range.txt");
		}
	}

	// clear storage vectors
	htPtr->HashValuesOutOfRange.clear();
	htPtr->HashKeysOutOfRange.clear();
#endif

}

template <typename PointNT> void
PFmatcher<PointNT>::voteWithTargetPoints(const Eigen::Vector3f& s_r, const Eigen::Vector3f& n_r, 
                                         const Eigen::Affine3f& Tsg,
                                         HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
                                         const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr,
                                         const typename modelHashTable<PointNT>::Ptr& htPtr,
										 MinimalDiscretePF<PointNT>& sceneFeature,
										 flagArray<PointNT>* refPointFlagArrayPtr){
    
    // voting with all target points
    for(int i_t = 0; i_t < sceneTargetCloudPtr->size(); i_t++){
        
        // get Eigen-syle point information of target point
        Eigen::Vector3f s_t = sceneTargetCloudPtr->points[i_t].getVector3fMap();
        Eigen::Vector3f n_t = sceneTargetCloudPtr->points[i_t].getNormalVector3fMap();

        // no feature of points at same location (will lead to d = 0 and therefore an 
        // undefined feature vector)
        if(s_r != s_t){
            
            // calculate the feature
            bool noOverflow = sceneFeature.calculate(s_r, n_r,
            										 s_t, n_t,
                                                     Tsg);
            
            // Do not use this feature if an overflow occurred during quantization
            // of feature vector. 1. It's no valid data, 2. Overflows with correctly
            // set d_dist will only occur if the distance between points is way bigger
            // than the model diameter. And for that, there are no features stored in
            // the model hash table anyway.
            if(!noOverflow){
                PCL_DEBUG("[PFmatcher::voteWithTargetPoints] Overflow in d during feature " \
                          "quantization detected. d_dist too small or scene is very "\
                          "big compared to the model.\n");
#ifdef write_counters_to_file
				#pragma omp atomic update
                this->overFlowCounter ++;
#endif
                continue;
            }

            MinimalDiscretePF<PointNT>* sceneFeaturePtr = &sceneFeature; // create base class pointer...
            if (!(htPtr->voteWithFeature(sceneFeaturePtr, accumulatorArray, alphaArray, refPointFlagArrayPtr))){ // ...to enable use of virtual function voteWithFeature()
#ifdef write_counters_to_file
            	#pragma omp atomic update
            	this->invalidPFcounter ++;
#endif
			}
#ifdef write_counters_to_file
            else {
				#pragma omp atomic update
				this->validPFcounter ++;
			}
#endif
        } // s_r != s_t
    } // loop over target points
}


template <typename PointNT> void
PFmatcher<PointNT>::voteWithVotingBall(	const std::vector<int> &targetPointIndices,
										const Eigen::Vector3f& s_r, const Eigen::Vector3f& n_r,
										const Eigen::Affine3f& Tsg,
										HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
										const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr,
										const typename modelHashTable<PointNT>::Ptr& htPtr,
										MinimalDiscretePF<PointNT>& sceneFeature,
										flagArray<PointNT>* refPointFlagArrayPtr){

	// loop over the input set of target Points
	for ( int i_t : targetPointIndices){

		// get Eigen-syle point information of target point
		Eigen::Vector3f s_t = sceneTargetCloudPtr->points[i_t].getVector3fMap();
		Eigen::Vector3f n_t = sceneTargetCloudPtr->points[i_t].getNormalVector3fMap();

		// TODO: 28/03/2018 this if statement (s_r = s_t) is obsolete, isnt it? the vector would
		//		 			not contain the refPoint itself

		// calculate the feature
		bool noOverflow = sceneFeature.calculate(s_r, n_r,
												 s_t, n_t,
												 Tsg);

		// Do not use this feature if an overflow occurred during quantization
		// of feature vector. 1. It's no valid data, 2. Overflows with correctly
		// set d_dist will only occur if the distance between points is way bigger
		// than the model diameter. And for that, there are no features stored in
		// the model hash table anyway.
		if(!noOverflow){
			PCL_DEBUG("[PFmatcher::voteWithVotingBall] Overflow in d during feature " \
					  "quantization detected. d_dist too small or scene is very "\
					  "big compared to the model.\n");
#ifdef write_counters_to_file
			#pragma omp atomic update
			this->overFlowCounter ++;
#endif
			continue;
		}

		MinimalDiscretePF<PointNT>* sceneFeaturePtr = &sceneFeature; // create base class pointer...
		if (!(htPtr->voteWithFeature(sceneFeaturePtr, accumulatorArray, alphaArray, refPointFlagArrayPtr))){ // ...to enable use of virtual function voteWithFeature()
#ifdef write_counters_to_file
			#pragma omp atomic update
			this->invalidPFcounter ++;
#endif
		}
#ifdef write_counters_to_file
		else {
			#pragma omp atomic update
			this->validPFcounter ++;
		}
#endif
	} // loop over target points
}


/**
 * @brief version of matchFeature() for visibility context
 */
template <typename PointNT> bool
PFmatcher<PointNT>::matchVisConFeature(const featureType typesForMatching,
                                 	   const featureType thisType,
									   const typename pcl::PointCloud<PointNT>::ConstPtr& sceneRefCloudPtr,
									   const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr){

	ROS_INFO_STREAM("[PFmatcher::matchVisConFeature] Scene point cloud has got " << sceneRefCloudPtr->points.size() << " points." ); // TODO: MFZ 08/03/2018 remove after debugging!

	// check that this feature should be matched
    if(!(typesForMatching & thisType)){
    	ROS_INFO_STREAM("[PFmatcher::matchVisConFeature] S2SVisCon Feature not required, skip matching with this feature."); // TODO: MFZ 08/03/2018 remove after debugging!
        return true;
    }

    // check that this feature type is present in the model
    if(!(thisType & modelPtr->containedFeatures)){
        PCL_WARN("[PFmatcher::matchFeature] Model does not contain the feature type %s." \
                 "Skipping matching for this feature.\n",
                 featureToStr(thisType).c_str());
        return false;
    }

    // check that all required scene clouds were set
    if(!(sceneRefCloudPtr && sceneTargetCloudPtr)){
        PCL_WARN("[PFmatcher::matchFeature] Not all required scene point clouds are set for " \
                 "matching feature type %s. Skipping matching for this feature.\n",
                 featureToStr(thisType).c_str());
        return false;
    }

    // check if CMPH_CHD flag array is only used in combination with an CMPH_CHD hash table.
	// because CMPH_CHD flag array is not a stand-alone implementation, it needs the
	// CMPH_CHD minimal perfect hash function that is constructed during model training.
	if ( (faHashTableType == FA_CMPH_CHD) && (modelPtr->getModelHashTableType() == MH_STL) ){
		ROS_WARN_STREAM("[PFmatcher::matchFeature] Can't combine CMPH_CHD flag array and STL model hash table. "\
						"CMPH_CHD flag array needs minimal perfect hash function that is constructed during model " \
						"training with CMPH_CHD model hash table. Skipping matching with this feature.\n");
		return false;
	}

    // get index of Hash table containing this feature type and make reference for easier code
    int htIndex = modelPtr->getHashTableIndex(thisType);
    const typename modelHashTable<PointNT>::Ptr htPtr = modelPtr->hashTables[htIndex];

    // construct voxel grid for intersection detection in visibility context calculation // TODO: MFZ 11/10/17 methodenauswahl begründen !
	ROS_DEBUG_STREAM("[PFmatcher::matchVisConFeature] direkt vor dem octree reset");
	octreeSearchPtr.reset (new pcl::octree::OctreePointCloudSearch<PointNT> (modelPtr->voxelSize_intersectDetect * modelPtr->modelDiameter)); //!< construct voxel grid with absolute voxel edge-length
	octreeSearchPtr->setInputCloud(sceneRefCloudPtr);

	// Add points from input cloud to octree:
	octreeSearchPtr->addPointsFromInputCloud();
	// Get center points of all occupied voxels:
	std::vector<PointNT, Eigen::aligned_allocator<PointNT>> centerPoints;
	unsigned int numberOfOccupiedVoxels = octreeSearchPtr->getOccupiedVoxelCenters(centerPoints);
	ROS_INFO_STREAM("[PFmatcher::matchVisConFeature] Octree with Points from Scene-PointCloud filled. There are "<< numberOfOccupiedVoxels <<" occupied voxels with a diameter of "
							<< sqrt(octreeSearchPtr->getVoxelSquaredDiameter()) <<" and a side-length of " << sqrt(octreeSearchPtr->getVoxelSquaredSideLen()));

	// create PCLVisualizer
    boost::shared_ptr<pcl::visualization::PCLVisualizer> fancy_viewer;
#ifdef visualize_VisCon_feature
   	if (modelPtr->visualizeVisibilityContextFeature){
   		// show pointcloud in visualizer
   		fancy_viewer = visualization::visualizePointCloud<PointNT>(sceneRefCloudPtr);

   		// show center points of all occupied voxels:
		std::vector<PointNT, typename Eigen::aligned_allocator<PointNT> > voxelCenters;
		octreeSearchPtr->getOccupiedVoxelCenters (voxelCenters);
		typename pcl::PointCloud<PointNT>::Ptr voxelCentersCloudPtr (new typename pcl::PointCloud<PointNT>);
		voxelCentersCloudPtr->points = voxelCenters;
		visualization::visualizeOctreeCenterpoints<PointNT>(fancy_viewer, voxelCentersCloudPtr);
   	}
#else
   	fancy_viewer = nullptr;
#endif

	// start the clock to get voting performance (compare flag array versions)
	boost::timer::cpu_timer timerWithVoxelGridForVotingBallBuilding;
	boost::timer::cpu_timer timerWithFlagArrayBuilding;
	boost::timer::cpu_timer timerWithoutFlagArrayBuilding;
	timerWithVoxelGridForVotingBallBuilding.start();

	if(useVotingBalls){

		// TODO: 23.04.2018 change this size, if number of voting balls gets dynamic
		rawPoses.resize(2);

		// MFZ 17/11/2017 calculate size of voxel-grid via bounding box:
		//
		// 		d min is the smallest dimension of the object's bounding box and
		// 		d med is the median of its three dimensions,
		//		d_obj is the diameter of the enclosing sphere of the target object
		//
		// 		R_max = d_obj
		// 		R_min = sqrt((d_min)² + (d_med)²)
		//
		// construct two voxel-grids with these voxel-edge-lengths

		// use the dimensions (d_min,d_med,d_max) of the model, obtained with PCA
		// TODO: 12.04.2018 decide with the help of the radii's ratio, if one should use one or more (is three (which radius???) really an option?) voting balls.
		float r_min = sqrt(pow(modelPtr->d_min,2) + pow(modelPtr->d_med,2));
		float r_max = modelPtr->modelDiameter;
		ROS_INFO_STREAM("[PFmatcher::matchVisConFeature] Using voting balls for matching. The voting ball dimensions are: r_min = " << r_min << " and r_max = " << r_max);

		// info message
		if (voteForAdjacentRotationAngles){ ROS_INFO_STREAM("[PFmatcher::matchVisConFeature] Voting for adjacent scene-rotation-angles too.");}

#ifdef use_octree_radius_search
		// instantiate the octrees for the voting balls
		pcl::octree::OctreePointCloudSearch<PointNT> octreeSmallVotingBall(r_min);
		octreeSmallVotingBall.setInputCloud(sceneRefCloudPtr);
		octreeSmallVotingBall.addPointsFromInputCloud();
		pcl::octree::OctreePointCloudSearch<PointNT> octreeLargeVotingBall(r_max);
		octreeLargeVotingBall.setInputCloud(sceneRefCloudPtr);
		octreeLargeVotingBall.addPointsFromInputCloud();
#else
		// instantiate kd-trees for the voting balls
		// TODO the low performance in my tests could be a result that the NVIDIA graphics card was not installed
		//      using octree-search instead
		pcl::KdTreeFLANN<PointNT> kdRadiusSearchTree;
		kdRadiusSearchTree.setInputCloud(sceneRefCloudPtr);
#endif

		// Get count of all occupied voxels:
		double numberOfSceneRefPoints = sceneRefCloudPtr->size();

		// downcasting of hash table pointer (to enable use countVoutZero() )
		typename visibilityContextModelHashTable<PointNT>::Ptr htPtr_casted = boost::dynamic_pointer_cast<visibilityContextModelHashTable<PointNT> >(htPtr);

		// set scene-point-cloud octree. because htPtr and modelPtr still have octree of model-point-cloud
		// this automatically (via constructor) sets cloudPtr of sceneFeature-object to scene-point-cloud too
		htPtr_casted->setOctreeSearchPtr(octreeSearchPtr);

		// instantiation of sceneFeature
		VisibilityContextDiscretePF<PointNT> sceneFeature(htPtr.get());

		// adjust some parameters, that were set incorrectly in sceneFeature constructor
		sceneFeature.visualizeVisibilityContextFeature = modelPtr->visualizeVisibilityContextFeature; // because the flag in hash table is changed during model training
		// copy some parameters from matcher into hash table to use Hinterstoisser's extensions during matching
		htPtr->setMatchingParameters(this);

		// get max number of threads
		int maxNumberOfThreads = omp_get_max_threads();
		// define custom chunk-size
		int chunkSize = sceneRefCloudPtr->size() / maxNumberOfThreads;

		ROS_WARN_STREAM("[PFmatcher::matchVisConFeature] Maximum number of threads = " << maxNumberOfThreads << ". All of them will be used for matching.");

		// start the clock to get voting performance (compare flag array versions)
		timerWithFlagArrayBuilding.start();

		// build flag array
		flagArray<PointNT> *refPointFlagArrayPtr = nullptr;
		if (faHashTableType != FA_NONE){
			// 'sharing' the same flag array between the threads, i.e. not making them 'private' to each tread. Because
			//   1. flag arrays may get too big to be placed on the stack. but for each thread to use its own flag array it has to fit into the stack
			//   2. using the pointer, we do not need to call the flag arrays constructor, unless we really want to. i.e. saving time.
			refPointFlagArrayPtr = new flagArray<PointNT>(faHashTableType, numberOfSceneRefPoints, htPtr.get(), maxNumberOfThreads);
		}

		// start the clock to get voting performance (compare flag array versions)
		timerWithoutFlagArrayBuilding.start();

		// loop over scene reference points
		#pragma omp parallel for /*ordered*/ schedule(dynamic/*, chunkSize*/) firstprivate(sceneFeature /*, octreeSmallVotingBall, octreeLargeVotingBall*/) if(!(modelPtr->visualizeVisibilityContextFeature) ) num_threads(maxNumberOfThreads)
		for(int i_r = 0; i_r < sceneRefCloudPtr->size(); i_r = i_r + refPointStep){

			// get Eigen-style point information of reference point
			Eigen::Vector3f s_r = sceneRefCloudPtr->points[i_r].getVector3fMap();
			Eigen::Vector3f n_r = sceneRefCloudPtr->points[i_r].getNormalVector3fMap();

			// pre-calculate transform Ts->g (align s_r and n_r to origin / x-axis)
			Eigen::Affine3f Tsg = aligningTransform(s_r, n_r);

			// make Hough Space and space for tracking alpha float values, size depends on model only
			// x -> reference point index
			// y -> alpha index
			HoughSpace2D accumulatorArray(htPtr->refPointCloudPtr->size(), n_alpha); // actual voting space
			HoughSpace2D alphaArray(htPtr->refPointCloudPtr->size(), n_alpha);       // float values for alpha
			std::vector<std::vector<HoughBucket2D>> maxBuckets;

			sceneFeature.useVotingBalls = true; // for skipping the "d"-overflow check in quantize().
												// because of voting balls d should never get > modelDiameter.

			// use small voting ball first
			// find targetPoints within small radius
			std::vector<int> targetPointIndicesWithinSmallRadius;
			std::vector<float> targetPointsWithinSmallRadiusSQRDdistances;
#ifdef use_octree_radius_search
			int numberOfTargetPoints = octreeSmallVotingBall.radiusSearch(i_r, r_min, targetPointIndicesWithinSmallRadius, targetPointsWithinSmallRadiusSQRDdistances);
#else
			int numberOfTargetPoints = kdRadiusSearchTree.radiusSearch(sceneRefCloudPtr->points[i_r], r_min, targetPointIndicesWithinSmallRadius, targetPointsWithinSmallRadiusSQRDdistances);
#endif

			// vote with small voting ball
			if (numberOfTargetPoints > 0){

#ifdef visualize_VisCon_feature
				if (sceneFeature.visualizeVisibilityContextFeature){
					// Visualize voting ball (runs in a separate thread)
					fancy_viewer->removeShape("small_voting_ball");
					fancy_viewer->addSphere<PointNT>(sceneRefCloudPtr->points[i_r], r_min, 255,0,0, "small_voting_ball");
					fancy_viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, "small_voting_ball");
					typename pcl::PointCloud<PointNT>::Ptr smallVotingBallPointsPtr (new typename pcl::PointCloud<PointNT>);
					for (int index : targetPointIndicesWithinSmallRadius){
						smallVotingBallPointsPtr->points.push_back(sceneRefCloudPtr->points[index]);
					}
					visualization::drawIntersectionPoints<PointNT>(smallVotingBallPointsPtr, fancy_viewer, "small_voting_ball_points", 255,0,0);  // red points
				}
#endif

				voteWithVotingBall(targetPointIndicesWithinSmallRadius, s_r, n_r, Tsg, accumulatorArray, alphaArray, sceneTargetCloudPtr, htPtr_casted, sceneFeature, fancy_viewer, refPointFlagArrayPtr);
			}
			else{
				ROS_WARN_STREAM("[PFmatcher::matchVisConFeature] Small voting ball contains no points! Something went wrong!");
			}
			// "We will say that a point pair is accepted by a voting ball if the first point is at the
			//  center of the ball and its distance to the second point is smaller than the radius of the ball" (--> R_min ).
			// [Hinterstoisser et al. 2016]

			// [Hinterstoisser et al. 2016] handle the hypotheses of both voting balls separately
			// search for maxima in accumulator array
			maxBuckets.push_back(accumulatorArray.getMaxBuckets(maxThresh));

			// use the large voting ball
			// find targetPoints within large radius
			std::vector<int> targetPointIndicesWithinLargeRadius;
			std::vector<float> targetPointsWithinLargeRadiusSQRDdistances;
#ifdef use_octree_radius_search
			numberOfTargetPoints = octreeLargeVotingBall.radiusSearch(i_r, r_max, targetPointIndicesWithinLargeRadius, targetPointsWithinLargeRadiusSQRDdistances);
#else
			numberOfTargetPoints = kdRadiusSearchTree.radiusSearch(sceneRefCloudPtr->points[i_r], r_max, targetPointIndicesWithinLargeRadius, targetPointsWithinLargeRadiusSQRDdistances);
#endif

			if( numberOfTargetPoints > 0){
				// erase point indices of targetPoiunts that were already examined with small voting ball
				// the indices given by radiusSearch() are NOT sorted!
				std::sort(targetPointIndicesWithinLargeRadius.begin(), targetPointIndicesWithinLargeRadius.end());
				std::sort(targetPointIndicesWithinSmallRadius.begin(), targetPointIndicesWithinSmallRadius.end());
				std::vector<int> targetPointIndicesDifference;
				std::set_difference (targetPointIndicesWithinLargeRadius.begin(), targetPointIndicesWithinLargeRadius.end(),
									 targetPointIndicesWithinSmallRadius.begin(), targetPointIndicesWithinSmallRadius.end(),
									 std::inserter(targetPointIndicesDifference, targetPointIndicesDifference.begin()));

				// vote with the large voting ball
#ifdef visualize_VisCon_feature
				if (sceneFeature.visualizeVisibilityContextFeature){
					// Visualize voting ball (runs in a separate thread)
					fancy_viewer->removeShape("large_voting_ball");
					fancy_viewer->addSphere<PointNT>(sceneRefCloudPtr->points[i_r], r_max, 255,255,0, "large_voting_ball");
					fancy_viewer->setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_OPACITY, 0.5, "large_voting_ball");
					typename pcl::PointCloud<PointNT>::Ptr largeVotingBallPointsPtr (new typename pcl::PointCloud<PointNT>);
					for (int index : targetPointIndicesDifference){
						largeVotingBallPointsPtr->points.push_back(sceneRefCloudPtr->points[index]);
					}
					visualization::drawIntersectionPoints<PointNT>(largeVotingBallPointsPtr, fancy_viewer, "large_voting_ball_points", 255,255,0); // yellow points
				}
#endif
				voteWithVotingBall(targetPointIndicesDifference, s_r, n_r, Tsg, accumulatorArray, alphaArray, sceneTargetCloudPtr, htPtr_casted, sceneFeature, fancy_viewer, refPointFlagArrayPtr);

			}
			else{
				ROS_WARN_STREAM("[PFmatcher::matchVisConFeature] Large voting ball contains no points! Something went wrong!");
			}

			// [Hinterstoisser et al. 2016] handle the hypotheses of both voting balls separately
			// search for maxima in accumulator array
			maxBuckets.push_back(accumulatorArray.getMaxBuckets(maxThresh));

			// reset the flags in the flag array
			if(faHashTableType != FA_NONE){
				(refPointFlagArrayPtr->*refPointFlagArrayPtr->resetFlags)(omp_get_thread_num());
			}

			// convert the best buckets back to poses and store them
//			#pragma omp ordered //!< Why did Xaver make this 'ordered'?
//								//! IDEA: 	the reason for this ordered clause is, that this is a simple way, to ensure a deterministic
//								//! 		behaviour of the algorithm. because, if the raw poses were stored in an ARBITRARY order, that
//								//!			differed each run due to the unpredictability of omp parallel for schedule(dynamic), the out-
//								//! 		come of the *clustering* would be different each run (non determinism). simply because the cluster-
//								//! 		ing would be a little bit different each time, because the raw poses were in a different order.
			for (int votingBall = 0; votingBall < maxBuckets.size(); votingBall ++){
				int maximumIndex = 0;
				for (HoughBucket2D& bucket : maxBuckets[votingBall]){
					// calculate aligning transform for model reference point
					PointNT m_r = htPtr->refPointCloudPtr->points[bucket.xIndex];
					Eigen::Affine3f Tmg = aligningTransform(m_r.getVector3fMap(),
															m_r.getNormalVector3fMap());

					// calculate alpha as weighted mean of alpha space instead of
					// just using the other accumulator array index
					float alpha = alphaArray.getBucketValue(bucket.xIndex, bucket.yIndex)
								  / bucket.accumulatedWeight;

#ifdef  write_alphas_to_binary_file
					// TODO: 30.07.2018 remove after debugging
					accumulatorArrayAlphas.push_back(alpha);
#endif

					// add raw pose, constructs full aligning transform
					if(useHinterstoisserClustering){
						#pragma omp critical (add_raw_pose)
						{
						rawPoses[votingBall].emplace_back(new PoseWithAssociatedModelPoint(Tsg, Tmg, alpha, bucket.accumulatedWeight, i_r, maximumIndex, bucket.xIndex));
						}
					}
					else{
						#pragma omp critical (add_raw_pose)
						{
						rawPoses[votingBall].emplace_back(new Pose(Tsg, Tmg, alpha, bucket.accumulatedWeight, i_r, maximumIndex ));
						}
					}
					// increment maximum counter
					maximumIndex ++;
				}
			}


			// save image of the Hough space to disk for later analysis
			// layout of the space:
			// o--> reference poins
			// |
			// V alpha steps
			if(saveHoughSpaceToFile){

				// full image (all buckets)
				std::vector<unsigned short> img = accumulatorArray.getAsUShortImg();
				char fname[400];
				snprintf(fname, sizeof(fname),"%s/feature_%d__refP_%d__all.png", saveDir.c_str(), thisType, i_r);

				pcl::io::saveShortPNGFile(fname, &img[0], accumulatorArray.xSize, accumulatorArray.ySize, 1);

				// only the buckets that lead to a pose estimation
				img = accumulatorArray.getAsUShortImg(maxThresh);
				snprintf(fname, sizeof(fname), "%s/feature_%d__refP_%d__top_%.2f.png", saveDir.c_str(), thisType, i_r, maxThresh);

				pcl::io::saveShortPNGFile(fname, &img[0], accumulatorArray.xSize, accumulatorArray.ySize, 1);
			}

		} // loop over reference points

		if (faHashTableType != FA_NONE){ // if a flag array is used
			delete refPointFlagArrayPtr;
		}
	} // if useVotingBalls

	else { // do not use votingBalls
		rawPoses.resize(1);

		// downcasting of hash table pointer (to enable use countVoutZero() )
		typename visibilityContextModelHashTable<PointNT>::Ptr htPtr_casted = boost::dynamic_pointer_cast<visibilityContextModelHashTable<PointNT> >(htPtr);

		// set scene-point-cloud octree. because htPtr and modelPtr still have octree of model-point-cloud
		// this automatically sets cloudPtr of VisConFeature-Object to scene-point-cloud too (via constructor)
		htPtr_casted->setOctreeSearchPtr(octreeSearchPtr);

		// TODO: MFZ 16/10/17 place instantiation of sceneFeature here (instead of instantiating it in 'voteWithTargetPoints')
		VisibilityContextDiscretePF<PointNT> sceneFeature(htPtr.get());

		// adjust some parameters, that were set incorrectly in sceneFeature constructor
		sceneFeature.visualizeVisibilityContextFeature = modelPtr->visualizeVisibilityContextFeature; // because the flag in hash table is changed during runtime by the user

		// copy some parameters from matcher into hash table to use [Hinterstoisser et al. 2016] extensions during matching
		htPtr->setMatchingParameters(this);

		// info message
		if (voteForAdjacentRotationAngles){ ROS_INFO_STREAM("[PFmatcher::matchVisConFeature] Voting for adjacent scene-rotation-angles too.");}

		// get max number of threads
		int maxNumberOfThreads = omp_get_max_threads();
		// define custom chunk-size
		int chunkSize = sceneRefCloudPtr->size() / maxNumberOfThreads;

		ROS_WARN_STREAM("[PFmatcher::matchVisConFeature] Maximum number of threads = " << maxNumberOfThreads << ". All of them will be used for matching.");

		// start the clock to get voting performance (compare flag array versions)
		timerWithFlagArrayBuilding.start();

		// build flag array
		flagArray<PointNT> *refPointFlagArrayPtr = nullptr;
		if (faHashTableType != FA_NONE){
			// 'sharing' the same flag array between the threads, i.e. not making them 'private' to each tread. Because
			//   1. flag arrays may get too big to be placed on the stack. but for each thread to use its own flag array it has to fit into the stack
			//   2. using the pointer, we do not need to call the flag arrays constructor, unless we really want to. i.e. saving time.
			refPointFlagArrayPtr = new flagArray<PointNT>(faHashTableType, sceneRefCloudPtr->size(), htPtr.get(), maxNumberOfThreads);
		}

		// start the clock to get voting performance (compare flag array versions)
		timerWithoutFlagArrayBuilding.start();

		// loop over scene reference points
		#pragma omp parallel for /*ordered*/ schedule(dynamic /*, chunkSize*/) firstprivate(sceneFeature) if(!(modelPtr->visualizeVisibilityContextFeature) ) num_threads(maxNumberOfThreads)
		for(int i_r = 0; i_r < sceneRefCloudPtr->size(); i_r = i_r + refPointStep){

			// get Eigen-style point information of reference point
			Eigen::Vector3f s_r = sceneRefCloudPtr->points[i_r].getVector3fMap();
			Eigen::Vector3f n_r = sceneRefCloudPtr->points[i_r].getNormalVector3fMap();

			// pre-calculate transform Ts->g (align s_r and n_r to origin / x-axis)
			Eigen::Affine3f Tsg = aligningTransform(s_r, n_r);

			// make Hough Space and space for tracking alpha float values, size depends on model only
			// x -> reference point index
			// y -> alpha index
			HoughSpace2D accumulatorArray(htPtr->refPointCloudPtr->size(), n_alpha); // actual voting space
			HoughSpace2D alphaArray(htPtr->refPointCloudPtr->size(), n_alpha);       // float values for alpha
			std::vector<HoughBucket2D> maxBuckets;

			// voting with all target points
			voteWithTargetPoints(s_r, n_r, Tsg, accumulatorArray, alphaArray, sceneTargetCloudPtr, htPtr_casted, sceneFeature, fancy_viewer, refPointFlagArrayPtr);

			// search for maxima in accumulator array
			maxBuckets = accumulatorArray.getMaxBuckets(maxThresh);

			// reset the flags in the flag array
			if(faHashTableType != FA_NONE){
				(refPointFlagArrayPtr->*refPointFlagArrayPtr->resetFlags)(omp_get_thread_num());
			}

			// convert the best buckets back to poses and store them
//			#pragma omp ordered //!< Why did Xaver make this 'ordered'?
//								//! IDEA: 	the reason for this ordered clause is, that this is a simple way, to ensure a deterministic
//								//! 		behaviour of the algorithm. because, if the raw poses were stored in an ARBITRARY order, that
//								//!			differs each run due to the unpredictability of omp parallel for schedule(dynamic), the out-
//								//! 		come of the *clustering* is different each run (non determinism). simply because the cluster-
//								//! 		ing will be a little bit different each time, because the raw poses are ordered differently.
			int maximumIndex = 0;
			for (HoughBucket2D& bucket : maxBuckets){
				// calculate aligning transform for model reference point
				PointNT m_r = htPtr->refPointCloudPtr->points[bucket.xIndex];
				Eigen::Affine3f Tmg = aligningTransform(m_r.getVector3fMap(),
														m_r.getNormalVector3fMap());

				// calculate alpha as weighted mean of alpha space instead of
				// just using the other accumulator array index
				float alpha = alphaArray.getBucketValue(bucket.xIndex, bucket.yIndex)
							  / bucket.accumulatedWeight;

#ifdef write_alphas_to_binary_file
				// TODO: 30.07.2018 remove after debugging
				accumulatorArrayAlphas.push_back(alpha);
#endif

				if(useHinterstoisserClustering){
					#pragma omp critical (add_raw_pose)
					{
					rawPoses[0].emplace_back(new PoseWithAssociatedModelPoint(Tsg, Tmg, alpha, bucket.accumulatedWeight, i_r, maximumIndex, bucket.xIndex));
					}
				}
				else{
					#pragma omp critical (add_raw_pose)
					{
					rawPoses[0].emplace_back(new Pose(Tsg, Tmg, alpha, bucket.accumulatedWeight, i_r, maximumIndex ));
					}
				}
				// increment maximum counter
				maximumIndex ++;
			}

			// save image of the Hough space to disk for later analysis
			// layout of the space:
			// o--> reference poins
			// |
			// V alpha steps
			if(saveHoughSpaceToFile){

				// full image (all buckets)
				std::vector<unsigned short> img = accumulatorArray.getAsUShortImg();
				char fname[400];
				snprintf(fname, sizeof(fname),"%s/feature_%d__refP_%d__all.png", saveDir.c_str(), thisType, i_r);

				pcl::io::saveShortPNGFile(fname, &img[0], accumulatorArray.xSize, accumulatorArray.ySize, 1);

				// only the buckets that lead to a pose estimation
				img = accumulatorArray.getAsUShortImg(maxThresh);
				snprintf(fname, sizeof(fname), "%s/feature_%d__refP_%d__top_%.2f.png", saveDir.c_str(), thisType, i_r, maxThresh);

				pcl::io::saveShortPNGFile(fname, &img[0], accumulatorArray.xSize, accumulatorArray.ySize, 1);
			}
		} // loop over reference points

		if (faHashTableType != FA_NONE){ // if a flag array is used
		    delete refPointFlagArrayPtr;
		}
	} // do not use votingBalls

    // time needed for the voting
	float msElapsedWithFlagArrayBuilding = timerWithFlagArrayBuilding.elapsed().wall / 1e6;
	float msElapsedWithoutFlagArrayBuilding = timerWithoutFlagArrayBuilding.elapsed().wall / 1e6;
	float msElapesdWithVoxelGridForVotingBallBuilding = timerWithVoxelGridForVotingBallBuilding.elapsed().wall / 1e6;
	ROS_INFO_STREAM("[PFmatcher::matchVisConFeature] Voting done. Took " << msElapsedWithFlagArrayBuilding <<
					" ms with and " << msElapsedWithoutFlagArrayBuilding << " ms without flagArrayBuildingTime.");
	ROS_INFO_STREAM("[PFmatcher::matchVisConFeature]              With Building time for the Voting Ball Voxel Grids it took " << msElapesdWithVoxelGridForVotingBallBuilding << " ms.");

	save_matching_debug_data_to_files(htPtr, sceneRefCloudPtr);

    return true;
}


/**
 * @brief version for S2S + visibility context feature
 */
template <typename PointNT> void
PFmatcher<PointNT>::voteWithTargetPoints(const Eigen::Vector3f& s_r, const Eigen::Vector3f& n_r,
                                         const Eigen::Affine3f& Tsg,
                                         HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
                                         const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr,
										 const typename visibilityContextModelHashTable<PointNT>::Ptr& htPtr,
										 VisibilityContextDiscretePF<PointNT>& sceneFeature,
										 const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
										 flagArray<PointNT>* refPointFlagArrayPtr){

	// voting with all target points
    for(int i_t = 0; i_t < sceneTargetCloudPtr->size(); i_t++){

        // get Eigen-syle point information of target point
        Eigen::Vector3f s_t = sceneTargetCloudPtr->points[i_t].getVector3fMap();
        Eigen::Vector3f n_t = sceneTargetCloudPtr->points[i_t].getNormalVector3fMap();

        // no feature of points at same location (will lead to d = 0 and therefore an
        // undefined feature vector)
        if(s_r != s_t){

#ifdef visualize_VisCon_feature
        	if (sceneFeature.visualizeVisibilityContextFeature && (fancy_viewer != nullptr)){
				// show PPF (+ VisCon) in visualizer
				visualization::visualizeVisConFeature<PointNT>(fancy_viewer, s_r, s_t, n_r, n_t);
			}
#endif

        	// calculate the feature
            bool noOverflow = sceneFeature.calculateForMatching(s_r, n_r, s_t, n_t,
                                                     	 	 	Tsg, fancy_viewer,
																htPtr->getVoutZeroCounter());
#ifdef print_VisCon_feature
			#pragma omp critical (printFeature)
            {
            ROS_INFO_STRAM("[PFmatcher::voteWithTargetPoints] PPF = " << sceneFeature );
        	}
#endif

#ifdef visualize_VisCon_feature
            if (sceneFeature.visualizeVisibilityContextFeature && (fancy_viewer != nullptr)){
            	ROS_INFO_STREAM("[PFmatcher::voteWithTargetPoints] visualizing Visibility Context Feature");
            	// Visualizer loop (runs in a separate thread)
				while ((!fancy_viewer->wasStopped ())){ // needs to be stopped by user 'closing' the window, clicking x.
					fancy_viewer->spinOnce (100);
					boost::this_thread::sleep (boost::posix_time::microseconds (100000));
				}

				char input;
				std::cout << "\nWould you like to visualize the next visibility feature too? [y/n]\nNote: Choosing 'n' is permanent for this scene." << std::endl;
				std::cin >> input;

				while ((input != 'y') && (input !='n'))
				{
					std::cout << "Input unknown. Would you like to visualize the next visibility feature too? [y/n]" << std::endl;
					std::cin >> input;
				}
				if (input == 'y'){
					fancy_viewer->resetStoppedFlag();
					fancy_viewer->removeAllShapes();
				}
				else { // input has to be 'n'
					sceneFeature.visualizeVisibilityContextFeature = false;
//					modelPtr->visualizeVisibilityContextFeature = false;  // TODO: MFZ 26/10/17 compiler error: trying to assign a read only object ??? probably because of ConstPtr.
					ROS_INFO_STREAM("[PFmatcher::voteWithTargetPoints] Set visualizeVisibilityContextFeature to FALSE");
					fancy_viewer->removeAllShapes();
					fancy_viewer->removeAllPointClouds();
					fancy_viewer->close(); //VTK bug prevents window from being closed.
				}
			}
#endif

            // Do not use this feature if an overflow occurred during quantization
            // of feature vector. 1. It's no valid data, 2. Overflows with correctly
            // set d_dist will only occur if the distance between points is way bigger
            // than the model diameter. And for that, there are no features stored in
            // the model hash table anyway.
            // MFZ: the "overflow" flag is also triggered, if a V_out Feature is
            //      invalid. for invalid V_outs getDistanceForVout() returns a length of -1
            if(!noOverflow){
                ROS_DEBUG_STREAM("[PFmatcher::voteWithTargetPoints] Overflow in d during feature " \
                          	  	"quantization detected. d_dist too small or scene is very "\
								"big compared to the model.\n");
#ifdef write_counters_to_file
				#pragma omp atomic update
                this->overFlowCounter ++;
#endif

#ifdef print_overflown_VisCon_feature
			#pragma omp critical (print)
			{
			ROS_INFO_STREAM("[PFmatcher::voteWithTargetPoints] " << this->overFlowCounter << ". overflown PPF = "<< sceneFeature );
			}
#endif
                continue;
            }

            MinimalDiscretePF<PointNT>* sceneFeaturePtr = &sceneFeature; // create base class pointer...
            if (!(htPtr->voteWithFeature(sceneFeaturePtr, accumulatorArray, alphaArray, refPointFlagArrayPtr))){ // ...to enable use of virtual function voteWithFeature()
#ifdef write_counters_to_file
				#pragma omp atomic update
            	this->invalidPFcounter ++;
#endif
            }
#ifdef write_counters_to_file
            else {
				#pragma omp atomic update
            	this->validPFcounter ++;
            }
#endif
        } // s_r != s_t
    } // loop over target points
}


template <typename PointNT> void
PFmatcher<PointNT>::voteWithVotingBall(	const std::vector<int> &targetPointIndices,
										const Eigen::Vector3f& s_r, const Eigen::Vector3f& n_r,
										const Eigen::Affine3f& Tsg,
										HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
										const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr,
										const typename visibilityContextModelHashTable<PointNT>::Ptr& htPtr,
										VisibilityContextDiscretePF<PointNT>& sceneFeature,
										const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
										flagArray<PointNT>* refPointFlagArrayPtr){

	// loop over the input set of target Points
	for ( int i_t : targetPointIndices){

		// get Eigen-style point information of target point
		Eigen::Vector3f s_t = sceneTargetCloudPtr->points[i_t].getVector3fMap();
		Eigen::Vector3f n_t = sceneTargetCloudPtr->points[i_t].getNormalVector3fMap();

		// if statement (s_r = s_t) is obsolete, isn't it? the NN-vector would not contain the refPoint itself

#ifdef visualize_VisCon_feature
		if (sceneFeature.visualizeVisibilityContextFeature){
			// show PPF (+ VisCon) in visualizer
			visualization::visualizeVisConFeature<PointNT>(fancy_viewer, s_r, s_t, n_r, n_t);
		}
#endif
		// calculate the feature
		bool noOverflow = sceneFeature.calculateForMatching(s_r, n_r, s_t, n_t,
												 	 	 	Tsg, fancy_viewer,
															htPtr->getVoutZeroCounter());
#ifdef print_VisCon_feature
		#pragma omp critical (printFeature)
		{
		ROS_INFO_STRAM("[PFmatcher::voteWithVotingBall] PPF = " << sceneFeature );
		}
#endif


#ifdef visualize_VisCon_feature
		if (sceneFeature.visualizeVisibilityContextFeature){
			// Visualizer loop (runs in a separate thread)
			while ((!fancy_viewer->wasStopped ())){ // needs to be stopped by user 'closing' the window, clicking x.
				fancy_viewer->spinOnce (100);
				boost::this_thread::sleep (boost::posix_time::microseconds (100000));
			}

			char input;
			std::cout << "\nWould you like to visualize the next visibility feature too? [y/n]\nNote: Choosing 'n' is permanent for this scene." << std::endl;
			std::cin >> input;

			while ((input != 'y') && (input !='n'))
			{
				std::cout << "Input unknown. Would you like to visualize the next visibility feature too? [y/n]" << std::endl;
				std::cin >> input;
			}
			if (input == 'y'){
				fancy_viewer->resetStoppedFlag();
				fancy_viewer->removeAllShapes();
			}
			else { // input has to be 'n'
				sceneFeature.visualizeVisibilityContextFeature = false;
//				modelPtr->visualizeVisibilityContextFeature = false;  // TODO: MFZ 26/10/17 compiler error: trying to assign a read only object ??? probably because of ConstPtr.
				ROS_INFO_STREAM("[PFmatcher::voteWithVotingBall] Set visualizeVisibilityContextFeature to FALSE");
				fancy_viewer->removeAllShapes();
				fancy_viewer->removeAllPointClouds();
				fancy_viewer->close(); //VTK bug prevents window from being closed.
			}
		}
#endif

		// Do not use this feature if an overflow occurred during quantization
		// of feature vector. 1. It's no valid data, 2. Overflows with correctly
		// set d_dist will only occur if the distance between points is way bigger
		// than the model diameter. And for that, there are no features stored in
		// the model hash table anyway.
		if(!noOverflow){
			ROS_DEBUG_STREAM("[PFmatcher::voteWithVotingBall] Overflow in d during feature " \
					  	  	 "quantization detected. d_dist too small or scene is very "\
							 "big compared to the model.\n");
#ifdef write_counters_to_file
			#pragma omp atomic update
			this->overFlowCounter ++;
#endif

#ifdef print_overflown_VisCon_feature
			#pragma omp critical (print)
			{
			ROS_INFO_STREAM("[PFmatcher::voteWithVotingBall] " << this->overFlowCounter << ". overflown PPF = "<< sceneFeature );
			}
#endif

			continue;
		}

		MinimalDiscretePF<PointNT>* sceneFeaturePtr = &sceneFeature; // create base class pointer...
		if (!(htPtr->voteWithFeature(sceneFeaturePtr, accumulatorArray, alphaArray, refPointFlagArrayPtr))){ // ...to enable use of virtual function voteWithFeature()
#ifdef write_counters_to_file
			#pragma omp atomic update
			this->invalidPFcounter ++;
#endif
		}
#ifdef write_counters_to_file
		else {
			#pragma omp atomic update
			this->validPFcounter ++;
		}
#endif
	} // loop over target points
}

template <typename PointNT> void
PFmatcher<PointNT>::clusterRawPoses(){

	if (useHinterstoisserClustering){
		// clustering as in [Hinterstoisser et al. 2016] with optional symmetry-handling
		hinterstoisserClustering();
	}
	else {
		// clustering as in [Choi et al. 2012] with optional symmetry-handling by [Kroischke 2016]
		choiClustering();
	}
}


template <typename PointNT> void
PFmatcher<PointNT>::choiClustering(){
    
	ROS_INFO_STREAM("[PFmatcher::choiClustering] Performing clustering as described in [Choi et al. 2012].");

	// get max number of threads
	int maxNumberOfThreads = omp_get_max_threads();

	int numberOfVotingBalls = rawPoses.size();
	poseClusters.resize(numberOfVotingBalls);

	ROS_WARN_STREAM("[PFmatcher::choiClustering] Maximum number of threads = " << maxNumberOfThreads << ". "\
			        "Number of voting balls = " << numberOfVotingBalls << ". "\
				 	"It's recommended to have at least one thread per voting ball for clustering.");

	#pragma omp parallel for schedule(static) num_threads(maxNumberOfThreads)
	for (int votingBall = 0; votingBall < numberOfVotingBalls; votingBall ++){
		ROS_DEBUG_STREAM("[PFmatcher::choiClustering] before the clustering there are raw poses: " << rawPoses[votingBall].size() );

		// sorting the poses first by refPoint-index and second by the index x, where x is the x'th maximum extracted from the accumulator space
		// this should result in the same order that would be created by the originally used "ordered"-clause in the for-loop over the refPoints in matchFeature()
		// but this way we can successfully speed up the machtFeature()-for-loop using multi-threading. the ordered clause prevents a speedup despite multi-threaded for-loop.
		std::sort(rawPoses[votingBall].begin(), rawPoses[votingBall].end(),
				  [](const Pose* a, const Pose* b){
					  if (a->sceneRefPointIdx != b->sceneRefPointIdx) return ( a->sceneRefPointIdx < b->sceneRefPointIdx);
					  return ( a->maxIdx < b->maxIdx);
					 });

		// sort the vector containing the raw poses in descending order (first = highest weight)
		// note: sorting the vector here after all insertions has complexity O(n logn) while
		// keeping it sorted during the insertion process would have been be O(n������������������)
		std::sort(rawPoses[votingBall].begin(),
				  rawPoses[votingBall].end(),
				  [](const Pose* a, const Pose* b){return *a > *b;});

		// clear and resize pose cluster vector
		poseClusters[votingBall].clear();
		poseClusters[votingBall].reserve(rawPoses[votingBall].size());

		// add raw poses to clusters or create new clusters
		// poses with highest weight are added first
		for (Pose* posePtr : rawPoses[votingBall]){

			// check if current pose is similar to any existing pose cluster
			// if so, add it and stop searching
			bool foundCluster = false;
			for(PoseCluster* clusterPtr : poseClusters[votingBall]){

				// try identity pose
				if(clusterPtr->isSimilar(*posePtr, clusterTransThreshRelative * modelPtr->d_dist_abs, clusterRotThresh)){
//					#pragma opm critical (addPose)
//					{
					clusterPtr->addPose(posePtr);
//					}
					foundCluster = true;
					break;
				}

				// try equivalent poses
				// Assuming the model center is within the model, we can do a quick plausibility
				// check before the costly transforming and in-depth checking of the equivalent
				// poses:
				// The pose translation has to be within one model diameter of the pose cluster,
				// otherwise, no pose will fit.
				// TODO: MFZ 16.04.2018 --> imho all symmetry-axes go through the centroid of the model,
				// 							consequently, for all equivalent poses the translation should not only lie
				//							within one model diameter, but even within the cluster Translation Threshold
				if(useEquivalentModelPoses &&
				   clusterPtr->translationIsSimilar(*posePtr, modelPtr->modelDiameter)){
				//if(useEquivalentModelPoses){
					bool foundEquivalent = false;
					for(const Pose& equivalentPose: modelPtr->equivalentPoses){

						Pose transformedPose = *posePtr * equivalentPose;
						if(clusterPtr->isSimilar(transformedPose, clusterTransThreshRelative * modelPtr->d_dist_abs, clusterRotThresh)){
//							#pragma omp critical (addPose)
//							{
							clusterPtr->addPose(&transformedPose);
//							}
							foundCluster = true;
							foundEquivalent = true;
							break;
						}
					}

					if(foundEquivalent){
						break;
					}
				}
			}

			// if no similar cluster was found, create a new one from this pose
			if(!foundCluster){
				if (useVotingBalls){
					poseClusters[votingBall].push_back(new PoseCluster(*posePtr, votingBall + 1)); // store voting ball ID in cluster (for later evaluation)
				}
				else{
					poseClusters[votingBall].push_back(new PoseCluster(*posePtr, 0)); // store info that no voting ball was used in cluster (for later evaluation)
				}
			}
		}

		// sort pose clusters by weight in descending order
		std::sort(poseClusters[votingBall].begin(),
				  poseClusters[votingBall].end(),
				  [](const Pose* a, const Pose* b){return *a > *b;});

		ROS_DEBUG_STREAM("[PFmatcher::choiClustering] after the clustering there are pose clusters: " << poseClusters[votingBall].size());
	}

}

template <typename PointNT> void
PFmatcher<PointNT>::hinterstoisserClustering(){

	ROS_INFO_STREAM("[PFmatcher::hinterstoisserClustering] Performing clustering as described in [Hinterstoisser et al. 2016].");

	// check that the number of equivalent poses is not too big to handle:
	if(useEquivalentModelPoses && (modelPtr->equivalentPoses.size() > 32)){
		ROS_ERROR_STREAM("[PFmatcher::hinterstoisserClustering] Number of equivalent poses exceeds 32. This can not be handled. Aborting clustering.");
		// --> you would have to change the EquivalentPoseIndicesOfAlreadyMergedEquivalentClusters from uint32_t to uint64_t
		return;
	}

	// 0. do clustering for each voting ball separately

	// get max number of threads
	int maxNumberOfThreads = omp_get_max_threads();

	int numberOfVotingBalls = rawPoses.size();
	poseClusters.resize(numberOfVotingBalls);

	ROS_WARN_STREAM("[PFmatcher::hinterstoisserClustering] Maximum number of threads = " << maxNumberOfThreads << ". "\
			        "Number of voting balls = " << numberOfVotingBalls << ". "\
				 	"It's recommended to have at least one thread per voting ball for clustering.");

	#pragma omp parallel for schedule(static) num_threads(maxNumberOfThreads)
	for ( int votingBall = 0; votingBall < numberOfVotingBalls; votingBall ++){
		ROS_DEBUG_STREAM("[PFmatcher::hinterstoisserClustering] Before the clustering there are raw poses: " << rawPoses[votingBall].size());

		// sorting the poses first by refPoint-index and second by the index x, where x is the x'th maximum extracted from the accumulator space
		// this should result in the same order that would be created by the originally used "ordered"-clause in the for-loop over the refPoints in matchFeature()
		// but this way we can successfully speed up the machtFeature()-for-loop using multi-threading. the ordered clause prevents a speedup despite multi-threaded for-loop.
		std::sort(rawPoses[votingBall].begin(), rawPoses[votingBall].end(),
				  [](const Pose* a, const Pose* b){
					  if (a->sceneRefPointIdx != b->sceneRefPointIdx) return ( a->sceneRefPointIdx < b->sceneRefPointIdx);
					  return ( a->maxIdx < b->maxIdx);
					 });

		// 1. sort raw poses "bottom up", i.e. process poses with smallest weight first
		std::sort(rawPoses[votingBall].begin(),
				  rawPoses[votingBall].end(),
				  [](const Pose* a, const Pose* b){return *a < *b;});

		// clear and resize pose cluster vector
		poseClusters[votingBall].clear();
		poseClusters[votingBall].reserve(rawPoses[votingBall].size());

//	}
//	#pragma omp parallel for collapse(2) schedule(dynamic) num_threads(maxNumberOfThreads)
//	for ( int votingBall = 0; votingBall < numberOfVotingBalls; votingBall ++){
		// make those cluster methods run in parallel will cause non-deterministic behaviour.
		// every run of the program will lead to different clusters because the order in which
		// the raw poses are clustered, is unspecified.

		// 2. process poses with smallest weight first
		for (Pose* posePtr : rawPoses[votingBall]){

			PoseWithAssociatedModelPoint* posePtr_casted = dynamic_cast<PoseWithAssociatedModelPoint*>(posePtr);

			// check if current pose is similar to any existing pose cluster
			// if so, add it, but DON'T stop searching, search further
			bool foundCluster = false;
			for(PoseCluster* clusterPtr : poseClusters[votingBall]){

				// try identity pose
				if(clusterPtr->isSimilar(*posePtr, clusterTransThreshRelative * modelPtr->d_dist_abs, clusterRotThresh)){

					PoseClusterWithAssociatedModelPoints* clusterPtr_casted = dynamic_cast<PoseClusterWithAssociatedModelPoints*>(clusterPtr);

					if (!clusterPtr_casted->isModelPointAlreadyPresent(*posePtr_casted)){ // here the casted pointers need to be used, otherwise the
																						  // isModelPointAlreadyPresent() member function could not get called
//						#pragma omp critical
//						{
						clusterPtr->addPose(posePtr);   // addPose() is virtual!
//						}
						foundCluster = true;
					}
//					else{
//						std::cout << "[PFmatcher::hinterstoisserClustering] modelPoint is already present." << std::endl;
//					}
				}
			}
			if (!foundCluster){
				if(useVotingBalls){
					poseClusters[votingBall].push_back(new PoseClusterWithAssociatedModelPoints(*posePtr_casted, modelPtr->surfaceCloudPtr->points.size(), votingBall + 1)); // store voting ball ID in cluster (for later evaluation)
				}
				else {
					poseClusters[votingBall].push_back(new PoseClusterWithAssociatedModelPoints(*posePtr_casted, modelPtr->surfaceCloudPtr->points.size(), 0 )); // store info that no voting ball was used in cluster (for later evaluation)
				}
			}
		}
		// 3. detecting rotational symmetries of pose clusters
		// try equivalent poses
		// Assuming the model center is within the model, we can do a quick plausibility
		// check before the costly transforming and in-depth checking of the equivalent
		// poses:
		// The pose translation has to be within one model diameter of the pose cluster,
		// otherwise, no pose will fit.
		// MFZ 16.04.2018 --> imho all symmetry-axes go through the centroid of the model,
		// 					  therefore for all equivalent poses the translation should not only lie
		//					  within one model diameter, but even within the cluster Translation Threshold
		// MFZ 16.04.2018 to achieve compatibility with Hinterstoisser's Clustering,
		//				  this symmetry check needs to be placed *AFTER* the clustering (not *during*, as in Choi Clustering)
		if(useEquivalentModelPoses){

			// 3 a) intermediate step: sort the poses in descending order according to their cluster weight ("top down")
			std::sort(poseClusters[votingBall].begin(),
							  poseClusters[votingBall].end(),
							  [](const PoseCluster* a, const PoseCluster* b){return *a > *b;});

			for (int poseClusterA = 0; poseClusterA < poseClusters[votingBall].size(); poseClusterA ++){
				if (poseClusters[votingBall][poseClusterA] == nullptr){
					continue;
				}
				// binary number that gets a 1 at the position of each equivalent-pose-index that was already used to add a equivalent clusterB to clusterA:
				uint32_t alreadyUsedEquivalentPoses = 0b00000000000000000000000000000000;

				for (int poseClusterB = poseClusterA+1 ; poseClusterB < poseClusters[votingBall].size(); poseClusterB ++){
					if (poseClusters[votingBall][poseClusterB] == nullptr){
						continue;
					}
					// 4. check if translational part of clusterA and clusterB is similar
					if (poseClusters[votingBall][poseClusterA]->translationIsSimilar(*poseClusters[votingBall][poseClusterB], modelPtr->modelDiameter)){

						// 5. if so, loop through all detected equivalent poses
						int equivalentPoseIndex= -1;
						for(const Pose& equivalentPose: modelPtr->equivalentPoses){
							equivalentPoseIndex ++;

							// 6. transform clusterB with equivalent pose T: clusterB'= T*clusterB
							Pose transformedPose = *(poseClusters[votingBall][poseClusterB]) * equivalentPose;

							// 7. check if the equivalent pose aligns clusterB' and clusterA
							if(poseClusters[votingBall][poseClusterA]->isSimilar(transformedPose, clusterTransThreshRelative * modelPtr->d_dist_abs, clusterRotThresh)){

								// 8. move 1 to position of equivalent pose index
								uint32_t flag = 0b00000000000000000000000000000001;
								flag = flag << equivalentPoseIndex;

								// 9. check if the equivalent pose has not been used to merge clusterA with clusterB yet
								if( !(flag & alreadyUsedEquivalentPoses)){

									// 10. add equivalent poseClusterB' to poseClusterA
									poseClusters[votingBall][poseClusterA]->addPose(&transformedPose);
									// 11. add the index of the equivalent pose to the binary number that stores the indices
									alreadyUsedEquivalentPoses = alreadyUsedEquivalentPoses | flag;
									// 12. delete the pose clusterB because its equivalent clusterB* was added to clusterA
									delete (poseClusters[votingBall][poseClusterB]);
									poseClusters[votingBall][poseClusterB] = nullptr;
									break; // no need to check the remaining equivalent poses
								}
								else{ // equivalent pose was already used to merge clusterA with clusterB, check next equivalentPose
									continue;
								}
							}
						}
					}
				} // loop over poseClusterB
			} // loop over poseClusterA

			// 13. sort out all nullptr's
			ROS_DEBUG_STREAM("[PFmatcher::hinterstoisserClustering] before the cleansing there are pose clusters: " << poseClusters[votingBall].size());
			std::vector<PoseCluster*>::iterator bound = std::partition(poseClusters[votingBall].begin(), poseClusters[votingBall].end(), [](const PoseCluster* a){return a != nullptr;});
			poseClusters[votingBall].erase(bound, poseClusters[votingBall].end());
			ROS_DEBUG_STREAM("[PFmatcher::hinterstoisserClustering] after the cleansing there are pose clusters: " << poseClusters[votingBall].size());

		} // useEquivalentModelPoses

		// 14. sort poses "top down"
		std::sort(poseClusters[votingBall].begin(),
				  poseClusters[votingBall].end(),
				  [](const PoseCluster* a, const PoseCluster* b){return *a > *b;});

	} // loop over voting balls
}



template<typename PointNT> template <typename PType> std::vector<float>
PFmatcher<PointNT>::getWeights(unsigned int n, std::vector<PType*>& poses){ // MFZ 18.04.2018 changed to PType*

    if(n == 0 || n > poses.size()){
        n = poses.size();
    }
    
    std::vector<float> weights;
    weights.reserve(n);
    
    for(unsigned int i = 0; i < n; i++){
        weights.push_back(poses[i]->getWeight());  // MFZ 18.04.2018 changed . to ->
    }
    
    return weights;
}


template<typename PointNT> template <typename PType> float
PFmatcher<PointNT>::getMedianWeight(std::vector<PType*>& sortedPoses){ // MFZ 18.04.2018 changed to PType*
            
            float median;
            size_t size = sortedPoses.size();
            
            if(size  % 2 == 0){
                median = (sortedPoses[size / 2 - 1]->getWeight() + 			// MFZ 18.04.2018 changed . to ->
                          sortedPoses[size / 2]->getWeight()       ) / 2;	// MFZ 18.04.2018 changed . to ->
            }
            else{
                median = sortedPoses[size / 2]->getWeight(); // integer truncation in division // MFZ 18.04.2018 changed . to ->
            }
            
            return median;
        }
    
} // end namespace
