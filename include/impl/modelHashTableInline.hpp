/*
 * modelHashTableInline.hpp
 *
 *  Created on: Jun 4, 2018
 *      Author: Makus Franz Ziegler
 */

#pragma once

//#include "../modelHashTable.hpp"

namespace pf_matching{

template <typename PointNT> inline featureType
modelHashTable<PointNT>::getFeatureType(){
	return this->fType;
}

template <typename PointNT> inline unsigned int
modelHashTable<PointNT>::getHashTableType(){
	return this->htType;
}

template <typename PointNT> inline float
modelHashTable<PointNT>::get_d_dist_abs(){
	return this->d_dist_abs;
}

template <typename PointNT> inline float
modelHashTable<PointNT>::get_d_angle(){
	return this->d_angle;
}

template <typename PointNT> inline float
modelHashTable<PointNT>::get_modelDiameter(){
	return this->modelDiameter;
}

template <typename PointNT> inline bool
modelHashTable<PointNT>::get_useNeighbourPPFsForTraining(){
	return this->useNeighbourPPFsForTraining;
}

template <typename PointNT> inline std::vector<std::pair<int, double>>
visibilityContextModelHashTable<PointNT>::getVoutZeroCounter(){
	return this->countVoutZero;
}

template <typename PointNT> inline void
visibilityContextModelHashTable<PointNT>::setOctreeSearchPtr(typename pcl::octree::OctreePointCloudSearch<PointNT>::Ptr& newPtr){
	this->octreeSearchPtr = newPtr;
}

template <typename PointNT> inline float
visibilityContextModelHashTable<PointNT>::get_d_VisCon_abs(){
	return d_VisCon_abs;
}

template <typename PointNT> inline unsigned int
modelHashTable<PointNT>::incrementAlphaSceneIndex(const float& alpha_s){

	// increment alpha scene by amount of hough-voting-space bin-size
	float alpha_s_neighbour = alpha_s + this->d_alpha;

	// ensure that alpha scene is in [-pi, pi) after addition
	// (will be off by max. one period)
	if(alpha_s_neighbour < -M_PI){
		alpha_s_neighbour += 2 * M_PI;
	}
	else if(alpha_s_neighbour >= M_PI){
		alpha_s_neighbour -= 2 * M_PI;
	}

	// quantize to get alpha scene index in flag array
	unsigned int alpha_s_NeighbourIndex = ( alpha_s_neighbour + M_PI) * (this->flagArrayQuantizationSteps)/(2*M_PI); // floor rounding

	return alpha_s_NeighbourIndex;
}

template <typename PointNT> inline unsigned int
modelHashTable<PointNT>::decrementAlphaSceneIndex(const float& alpha_s){

	// increment alpha scene by amount of hough-voting-space bin-size
	float alpha_s_neighbour = alpha_s - this->d_alpha;

	// ensure that alpha scene is in [-pi, pi) after addition
	// (will be off by max. one period)
	if(alpha_s_neighbour < -M_PI){
		alpha_s_neighbour += 2 * M_PI;
	}
	else if(alpha_s_neighbour >= M_PI){
		alpha_s_neighbour -= 2 * M_PI;
	}

	// quantize to get alpha scene index in flag array
	unsigned int alpha_s_NeighbourIndex = ( alpha_s_neighbour + M_PI) * (this->flagArrayQuantizationSteps)/(2*M_PI); // floor rounding

	return alpha_s_NeighbourIndex;
}

template <typename PointNT> inline unsigned int
modelHashTable<PointNT>::getAlphaSceneIndex(float &alpha_s){
	if (alpha_s >= M_PI){ // special case that could occur, since atan2f() in minimalDiscretePF::calculate() returns
		                  // values in the interval [-pi ,+pi], but for the flagging array we need them in [-pi, +pi)
		return 0;
	}
	else if (alpha_s < -M_PI){
		alpha_s = -3.141592; // in case that, due to rounding errors, the input is < -pi, we set it to slightly > -pi
		return 0;
	}
	else{
		unsigned int alpha_s_index = (alpha_s + M_PI) * (this->flagArrayQuantizationSteps)/(2*M_PI); // floor rounding
		return alpha_s_index;
	}
}


/// these methods are commented, because they behave slightly different than 'decrementAlpha' and 'incrementAlpha'
/// i.e. under special circumstances they produce other indices which are then differing by +-1
/*
template <typename PointNT> inline unsigned int
modelHashTable<PointNT>::incrementAlphaIndex(unsigned int& alphaIndex){
	// alphaNeighbourIndex needs to be within [0,29] for default n_alpha = 30
	int alphaNeighbourIndex = alphaIndex + 1;
	if (alphaNeighbourIndex >= this->n_alpha){ // check upper boundary -- by default n_alpha = 30
		alphaNeighbourIndex = alphaNeighbourIndex - this->n_alpha;
	}
	return static_cast<unsigned int>(alphaNeighbourIndex);
}

template <typename PointNT> inline unsigned int
modelHashTable<PointNT>::decrementAlphaIndex(unsigned int& alphaIndex){
	// alphaNeighbourIndex needs to be within [0,29] for default n_alpha = 30
	int alphaNeighbourIndex = alphaIndex - 1;
	if (alphaNeighbourIndex < 0){ // check lower boundary
		alphaNeighbourIndex = alphaNeighbourIndex + this->n_alpha;
	}
	return static_cast<unsigned int>(alphaNeighbourIndex);
}
*/

template <typename PointNT> inline std::pair<float, unsigned int>
modelHashTable<PointNT>::incrementAlpha(const float& alpha_m, const float& alpha_s){

	// increment alpha by amount of hough-voting-space bin-size
	float alpha_neighbour = alpha_m - (alpha_s + this->d_alpha);

	// ensure that alpha scene is in [-pi, pi) after addition
	// (will be off by max. one period)
	if(alpha_neighbour < -M_PI){
		alpha_neighbour += 2 * M_PI;
	}
	else if(alpha_neighbour >= M_PI){
		alpha_neighbour -= 2 * M_PI;
	}

	// quantize to get alpha index in hough voting space
	unsigned int alphaNeighbourIndex = ( alpha_neighbour + M_PI) / this->d_alpha; // floor rounding

	return std::make_pair(alpha_neighbour, alphaNeighbourIndex);
}

template <typename PointNT> inline std::pair<float, unsigned int>
modelHashTable<PointNT>::decrementAlpha(const float& alpha_m, const float& alpha_s){

	// increment alpha by amount of hough-voting-space bin-size
	float alpha_neighbour = alpha_m - (alpha_s - this->d_alpha);

	// ensure that alpha scene is in [-pi, pi) after addition
	// (will be off by max. one period)
	if(alpha_neighbour < -M_PI){
		alpha_neighbour += 2 * M_PI;
	}
	else if(alpha_neighbour >= M_PI){
		alpha_neighbour -= 2 * M_PI;
	}

	// quantize to get alpha index in hough voting space
	unsigned int alphaNeighbourIndex = ( alpha_neighbour + M_PI) / this->d_alpha; // floor rounding; // floor rounding

	return std::make_pair(alpha_neighbour, alphaNeighbourIndex);
}

template <typename PointNT> inline void
STLmodelHashTable<PointNT>::eraseKeys(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys){
	for (auto const & key: keys){
		this->erase(key);
	}
}

} // end namespace pf_matching

