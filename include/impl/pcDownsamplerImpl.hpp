/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

#include "../pcDownsampler.hpp"

// other
#include <string>
#include <random>

// project
#include "octreeVoxelGridFilter.hpp"

// PCL
#include <pcl/common/io.h>
#include <pcl/filters/voxel_grid.h>
#define PCL_NO_PRECOMPILE // so that templated algorithm can be used
#include <pcl/keypoints/uniform_sampling.h>
#undef PCL_NO_PRECOMPILE // not required for other algorithms



namespace pf_matching{
namespace pre_processing{


template <typename PointT> void
PCdownsampler::downsample(const typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                          typename pcl::PointCloud<PointT>::Ptr& cloudDownsampled,
                          bool deepCopyForNONE){
    
    // check that in- and output are different
    if(cloudPtr == cloudDownsampled){
        PCL_ERROR("[PCdownsampler::downsample] input and output pointer point to the same storage!\n");
        return;
    }
    
    // skip if cloud is empty
    if(cloudPtr->points.empty()){
        cloudDownsampled->points.clear();
        PCL_DEBUG("[PCdownsampler::downsample] Input is empty, returning empty cloud.\n");
        return;
    }

    switch(method){
        case NONE:

            // copy over contents, either deep or just pointer
            if(deepCopyForNONE){
                pcl::copyPointCloud(*cloudPtr, *cloudDownsampled);
            }
            else{
                cloudDownsampled = cloudPtr;
            }

            break;
            
        case VOXEL_GRID: // note: NaN inputs are ok
        	// MFZ 21.11.2018: this approach actually generates new points. the new points
        	//                 are the centroids of each voxel.
            /**
             * MFZ 21.11.2018: This approach does not work with large point clouds in combination with small down-sampling distances.
        	 * https://github.com/PointCloudLibrary/pcl/issues/585
        	 **/
        	{
            pcl::VoxelGrid<PointT> vg;
            vg.setInputCloud(cloudPtr);
            vg.setLeafSize(pointDistances, pointDistances, pointDistances);
            vg.setDownsampleAllData(true);
            vg.filter(*cloudDownsampled);
            }
        	break;

        case OCTREE_VOXEL_GRID: // note: NaN inputs are ?
        	/**
        	 * MFZ 21.11.2018: Using a custom voxel grid filter. Based on an octree, it can process even large point clouds.
        	 */
        	{
        	OctreeVoxelGrid<PointT> vg(pointDistances);
			vg.setInputCloud(cloudPtr);
			vg.filter(*cloudDownsampled);
        	}
            break;
            
        case UNIFORM_SPACE: // note: NaN inputs are ok, empty inputs are ok
        	// MFZ 21.11.2018: It seems as if this approach does not generate new points from the voxel centroids as VOXEL_GRID does.
        	//                 Instead it just filters the points and returns only the point nearest to each voxel center.
        	//                 NOTE: This approach does not seem to have problems with the size of the point cloud
            {
            pcl::PointCloud<int> indices;
            pcl::UniformSampling<PointT> us;
            us.setInputCloud(cloudPtr);
            us.setRadiusSearch(pointDistances);
            us.compute(indices);  //--> calls detectKeypoints() member function of UniformSampling() class
            
            cloudDownsampled->clear();
            cloudDownsampled->is_dense = cloudPtr->is_dense;
            
            for(int& i : indices.points){
                cloudDownsampled->push_back(cloudPtr->points[i]);
            }
            }
            break;
            
        default:
            PCL_ERROR("[PCdownsampler::downsample] set downsampling method  %d is unknown.\n", method);
            
            // copy over contents, either deep or just pointer
            if(cloudPtr != cloudDownsampled){
                if(deepCopyForNONE){
                    pcl::copyPointCloud(*cloudPtr, *cloudDownsampled);
                }
                else{
                    cloudDownsampled = cloudPtr;
                }
            }
    }

}

} // end namespace
} // end namespace


