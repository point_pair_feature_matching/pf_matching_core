/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke, Markus Franz Ziegler
 */

#pragma once

#include "../pcDownsampler.hpp"  // why? why not weighting.hpp

// standard libs
#include <math.h>
#include <set>

// project
#include <pairFeatureModel.hpp>
#include <modelHashTable.hpp>
#include <pairFeatures.hpp>

//PCL
#include <pcl/console/print.h>

namespace pf_matching{
namespace extensions{


template <typename PointNT> void
ModelWeighter<PointNT>::weightByDistance(float power){
    
    // weight each hash table separately
    for(auto& hashTablePtr: modelPtr->hashTables){
        
    	hashTablePtr->weightByDistance(power);
    }
}


template <typename PointNT> void
ModelWeighter<PointNT>::discardByDistance(float thresh){
    
    // weight each hash table separately
    for(auto& hashTablePtr: modelPtr->hashTables){
        
        float distThresh = thresh * modelPtr->modelDiameter;
    
        hashTablePtr->discardByDistance(distThresh);
    }
}


template <typename PointNT> void
ModelWeighter<PointNT>::weightByFrequency(float power){
    
    // weight each hash table separately
    for(auto& hashTablePtr: modelPtr->hashTables){

        hashTablePtr->weightByFrequency(power);
    }
}


template <typename PointNT> void
ModelWeighter<PointNT>::discardByFrequency(float minRemaining){
    
    // weight each hash table separately
    for(auto& hashTablePtr: modelPtr->hashTables){
    
        // find out how many entries a feature has to have at least so that we remove it
        unsigned int n = 0;
        std::vector<unsigned int> histogram = hashTablePtr->getHistogram();
        unsigned int minEntries = 0;
        unsigned int sum = 0;
        while(sum < minRemaining * n){
            sum += minEntries * histogram[minEntries];
            minEntries++;
        }
        
        // find all the features (keys) who's elements need to be removed
        std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT> keys;
        hashTablePtr->getHighFrequencyPFs(keys, minEntries);
        
        // remove all entries for the collected keys
        hashTablePtr->eraseKeys(keys);
    }
}


template <typename PointNT> void
ModelWeighter<PointNT>::weightByFeatureType(typeWeightVector& typeWeightPairs){
    
    for(auto& hashTablePtr: modelPtr->hashTables){
    
        // find the weight to apply
        float weight = -1;
        for(auto& typeWeightPair: typeWeightPairs){
            if(hashTablePtr->getFeatureType() == typeWeightPair.first){
                weight = typeWeightPair.second;
            }
        }
        
        // weight with the found weight
        if(weight < 0){
            PCL_WARN("[ModelWeighter::weightByFeatureType] No weight supplied for feature "\
                     "type %d in model, not changing weights for these features.\n", 
                     hashTablePtr->getFeatureType());
        }
        else{
            if(weight != 1){
                hashTablePtr->weightByValue(weight);
            }
        }
    }
}

} // end namespace
} // end namespace
