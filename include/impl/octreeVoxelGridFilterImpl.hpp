/**
 * @author Markus Ziegler
 * @date 11/22/18
 * @file octreeVoxelGridFilter.hpp
 * @brief Definition of methods related to down-sampling a point cloud with an octree based voxel grid filter.
 * @note Code based on code from Julius Kammerl and Robert Huitl:
 *       http://www.pcl-users.org/VoxelGridFilter-Leaf-size-is-too-small-td4025570.html
 */

#pragma once

#include "../octreeVoxelGridFilter.hpp"


namespace pf_matching{
namespace pre_processing{



/////////////////// OCTREE CONTAINER CLASS /////////////////////////////////////////

template<typename PointT>
OctreeCentroidContainer<PointT>::OctreeCentroidContainer()
	: point_counter_(0)
{
	this->reset();
}


template<typename PointT>
OctreeCentroidContainer<PointT> *
OctreeCentroidContainer<PointT>::deepCopy() const
{
	return (new OctreeCentroidContainer(*this));
}


template<typename PointT> void
OctreeCentroidContainer<PointT>::addPoint(const PointT& new_point)
{
	++point_counter_;

	pt_sum_.x += new_point.x;
	pt_sum_.y += new_point.y;
	pt_sum_.z += new_point.z;
}


template<typename PointT> void
OctreeCentroidContainer<PointT>::getCentroid(PointT& centroid_arg) const
{
	if(point_counter_) {
		float fc = static_cast<float>(point_counter_);
		centroid_arg.x = pt_sum_.x / fc;
		centroid_arg.y = pt_sum_.y / fc;
		centroid_arg.z = pt_sum_.z / fc;
	}
}


template<typename PointT> void
OctreeCentroidContainer<PointT>::reset()
{
	point_counter_ = 0;
	pt_sum_.x = pt_sum_.y = pt_sum_.z = 0;
}



/////////////////// TEMPLATE SPECIALIZATIONS /////////////////////////////////////////

// Specializations for pcl::PointNormal where the normal is averaged, too
// use the "inline" keyword to enable template specifications
template<>
inline void OctreeCentroidContainer<pcl::PointNormal>::addPoint(const pcl::PointNormal& new_point)
{
	++point_counter_;

	pt_sum_.x += new_point.x;
	pt_sum_.y += new_point.y;
	pt_sum_.z += new_point.z;

	pt_sum_.normal_x += new_point.normal_x;
	pt_sum_.normal_y += new_point.normal_y;
	pt_sum_.normal_z += new_point.normal_z;
}


template<>
inline void OctreeCentroidContainer<pcl::PointNormal>::getCentroid(pcl::PointNormal& centroid_arg) const
{
	if(point_counter_) {
		float fc = static_cast<float>(point_counter_);
		centroid_arg.x = pt_sum_.x / fc;
		centroid_arg.y = pt_sum_.y / fc;
		centroid_arg.z = pt_sum_.z / fc;

		centroid_arg.normal_x = pt_sum_.normal_x / fc;
		centroid_arg.normal_y = pt_sum_.normal_y / fc;
		centroid_arg.normal_z = pt_sum_.normal_z / fc;
	}
}


template<>
inline void OctreeCentroidContainer<pcl::PointNormal>::reset()
{
	point_counter_ = 0;
	pt_sum_.x = pt_sum_.y = pt_sum_.z = 0;
	pt_sum_.normal_x = pt_sum_.normal_y = pt_sum_.normal_z = 0;
}



/////////////////// OCTREE VOXEL GRID CLASS /////////////////////////////////////////

template<typename PointT>
OctreeVoxelGrid<PointT>::OctreeVoxelGrid(double edgeLength)
	: pcl::octree::OctreePointCloudVoxelCentroid< PointT, OctreeCentroidContainer<PointT> >(edgeLength)
{

}


template<typename PointT> void
OctreeVoxelGrid<PointT>::filter(pcl::PointCloud<PointT>& outputCloud)
{
	this->defineBoundingBox();
	this->addPointsFromInputCloud();

	size_t count = this->getVoxelCentroids(outputCloud.points);
	outputCloud.width = count;
	outputCloud.height = 1;
	outputCloud.is_dense = false;
}


} // end namespace
} // end namespace
