/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke, Markus Ziegler
 */

#pragma once

#include "../resultVisualization.hpp"

// PCL
#define VTK_EXCLUDE_STRSTREAM_HEADERS
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/common/actor_map.h>
#undef VTK_EXCLUDE_STRSTREAM_HEADERS
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>
// For visualizing the point cloud and the new visibility context features
#include <boost/thread/thread.hpp>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/pcd_io.h>
//#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
// PCL specific includes (PCL also has to be mentioned in package.xml as run and build dependencies)
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

// Eigen lib for cross product
//#include <Eigen/Dense>
//#include <Eigen/Core>

// ROS log statements
#include <ros/console.h>

// project
#include <boundingSphere.hpp>
#include <pose.hpp>

// boost
#include <boost/thread/mutex.hpp>

// other
#include <algorithm>

namespace pv = pcl::visualization;

namespace pf_matching{
namespace visualization{


inline void createRGBcolor(float& r, float& g, float& b, const unsigned int i, const unsigned int N){
    
    if(N == 1){
        r = 1;
        g = 0;
        b = 0;
    }
    else{
        // map i to range [0, 2] with 0: only red, 1:only green, 2: only blue
        float position = (2.0 * i) / (N - 1);
        
        // convert the position to color values
        if(position <= 1){
            // [0...1] r and g
            r = (1- position); // relative values of r and g
            g = position;
            b = 0;
        }
        else{
            // (1...2] g and b
            position -= 1;
            
            r = 0;
            g = 1 - position;
            b = position;
        }
        
        float maxValue = std::max(r, std::max(g, b));
            r /= maxValue;
            g /= maxValue;
            b /= maxValue;
    }
    
    r *= 255;
    g *= 255;
    b *= 255;
}


template<typename PointT>
inline void visualizeClouds(const std::vector< typename pcl::PointCloud<PointT>::ConstPtr> cloudPtrs,
                     const std::vector<Pose> cloudPoses,
                     const std::vector<std::string> names,
                     const std::string windowName,
                     const std::string pngSavePath = ""){

    // threading related
    static boost::mutex visualizerMutex;
    
    // display constants
    const int fontsize = 15;
    const int pointSize = 3;
                         
    // create and set up the visualizer
    pv::PCLVisualizer visualizer;
    visualizer.setBackgroundColor (0.7, 0.7, 0.7);
    visualizer.setWindowName(windowName);
    
    // transform and add the point clouds to the display
    for(unsigned int i = 0; i < cloudPtrs.size(); i++){
        
        // get a color, first color is always black
        float r, g, b;
        if(i == 0){
            r = b = g = 0;
        }
        else{
            createRGBcolor(r, g, b, i -1, cloudPtrs.size() - 1);
        }
        
        
        // transform cloud according to pose
        typename pcl::PointCloud<PointT>::Ptr transformedPtr(new typename pcl::PointCloud<PointT>);
        pcl::transformPointCloud(*cloudPtrs[i], *transformedPtr, cloudPoses[i].asTransformation());
        
        // delete sensor information (might otherwise lead to "weird" display
        transformedPtr->sensor_origin_ = Eigen::Vector4f(0, 0, 0, 0);
        transformedPtr->sensor_orientation_ = Eigen::Quaternionf::Identity();
        
        // add to display
        // internal naming for clouds is just 0, 1, 2, ...
        // internal naming for text is 0_text, 1_text, ...
        std::string cloudName = std::to_string(i);
        std::string textName = cloudName + "_text";
        
        pv::PointCloudColorHandlerCustom<PointT> cloudColor(transformedPtr, r, g, b);
        visualizer.addPointCloud(transformedPtr, cloudColor, cloudName);
        visualizer.setPointCloudRenderingProperties(pv::PCL_VISUALIZER_POINT_SIZE,
                                                    pointSize, cloudName);
        
        visualizer.addText(names[i], 0, floor(fontsize * 1.2) * i + 10, fontsize,
                           r/255.0, g/255.0, b/255.0, textName);
        
        // add origins
        /*
        Eigen::Vector4f boundingSphere;
        pf_matching::tools::epos6BoundingSphere(*transformedPtr, boundingSphere);
        float scale = 0.1 * 2 * boundingSphere[3];
        Eigen::Vector3f origin = cloudPoses[i].asTransformation() * Eigen::Vector3f(0, 0, 0);
        visualizer.addCoordinateSystem(scale, cloudPoses[i].asTransformation());
        visualizer.addText3D(names[i], pcl::PointXYZ(origin[0], origin[1], origin[2]), scale / 10, 0, 0, 0);
        */
    }
    
    // save screenshot if requested
    if(!pngSavePath.empty()){
        auto interactorPtr = visualizer.getInteractorStyle();
        interactorPtr->saveScreenshot(pngSavePath);
    }

    while (!visualizer.wasStopped ()) {
        {
            // note: PCLVisuaizer is not thread safe, so locking is required before executing the
            // spinning
            boost::mutex::scoped_lock lock(visualizerMutex);
            visualizer.spinOnce();
        }
        boost::this_thread::sleep_for(boost::chrono::milliseconds(50)); // interruption point
    }
    
    // TODO / Note: A VKT-bug prevents the rendered window to actually be closed. A fix was not yet
    //              available (08/02/2016, see https://github.com/PointCloudLibrary/pcl/issues/172)
    visualizer.close();
}



} // end namespace
} // end namespace
