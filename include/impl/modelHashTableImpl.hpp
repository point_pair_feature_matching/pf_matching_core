/*
 * modelHashTableImpl.hpp
 *
 *  Created on: May 23, 2018
 *      Author: Markus Franz Ziegler
 */

#pragma once

#include "../modelHashTable.hpp"

// project
#include <pairFeatures.hpp>
#include <pairFeatureModel.hpp>
#include <houghSpace2D.hpp>
#include <pairFeatureMatcher.hpp>
#include <flagArray.hpp>
#include <visibilityContextVisualization.hpp>
#include <Eigen/Core>

using Eigen::ScaledProduct;

// debugging flags generating console output
//#define print_feature               // prints the quantized PPF in the console and stores it in the ROS-log file
//#define debug_show_alpha_indices    // for checking the functionality of the flag array and the voting for neighboured alphas

// debugging flags generating binary file output
//#define write_scene_PPfs_hashKeys_to_binary_file  <-- this has to be switched on and off in the header file! dont use this here!
//#define write_alphas_to_binary_file               <-- this has to be switched on and off in the header file! dont use this here!

// visualizer flag
#define visualize_VisCon_feature


namespace pf_matching {

void pause(std::string functionName, std::string lastTask, std::string nextTask);

// ========================================================================================================
// ======================================== hash table base class =========================================
// ========================================================================================================

template <typename PointNT>
modelHashTable<PointNT>::modelHashTable(PFmodel<PointNT>& model){
	setParameters(model);
	ROS_DEBUG_STREAM("[modelHashTable::modelHashTable] modelHashTable object (base class) constructed"); // TODO: removove after debugging
}

template <typename PointNT>
modelHashTable<PointNT>::~modelHashTable()
{
	this->modelPtr = nullptr;
	ROS_DEBUG_STREAM("modelHashTable object (base class) deleted");
}

template <typename PointNT> bool
modelHashTable<PointNT>::setParameters(PFmodel<PointNT>& model){
	// check validity of input
	if( model.d_dist_abs <= 0 ){

		PCL_ERROR("[modelHashTable::setParameters] d_dist_abs <= 0, can't use that.\n");
		return false;
	}

	this->modelPtr = &model;

	// copy values
	this->d_dist_abs = model.d_dist_abs;
	this->d_angle = model.d_angle;
	this->modelDiameter = model.modelDiameter;

	// anti noise-methods as proposed in [Hinterstoisser et al. 2016]
	this->useNeighbourPPFsForTraining = model.useNeighbourPPFsForTraining;

	ROS_DEBUG_STREAM("[modelHashTable::setParameters] done setting parameters"); // TODO: removove after debugging
	return true;
}

template <typename PointNT> bool
modelHashTable<PointNT>::setMatchingParameters(PFmatcher<PointNT>* matcher){

	// copy matching parameters from matcher
	this->voteForAdjacentRotationAngles = matcher->voteForAdjacentRotationAngles;
	this->faHashTableType = matcher->faHashTableType;
	this->flagArrayQuantizationSteps = matcher->flagArrayQuantizationSteps;
	this->d_alpha = matcher->d_alpha;
	this->n_alpha = matcher->n_alpha;
	return true;
}


// ========================================================================================================
// =============================== visibility context hash table base class ===============================
// ========================================================================================================

template <typename PointNT>
visibilityContextModelHashTable<PointNT>::visibilityContextModelHashTable(PFmodel<PointNT>& model) :
modelHashTable<PointNT>(model){
	setParameters(model);
	ROS_DEBUG_STREAM("[visibilityContextModelHashTable::visibilityContextModelHashTable] visibilityContextModelHashTable object (extended base class) constructed"); // TODO: remove after debugging
}

template <typename PointNT> bool
visibilityContextModelHashTable<PointNT>::setParameters( PFmodel<PointNT>& model){

    // check validity of input
    if( model.d_VisCon_abs <= 0 ){

        PCL_ERROR("[visibilityContextModelHashTable::setParameters] d_VisCon_abs_ <=0, can't use that.\n");
        return false;
    }

    // visibility context parameters: copy values
    d_VisCon_abs = model.d_VisCon_abs;
    voxelSize_intersectDetect = model.voxelSize_intersectDetect;
    ignoreFactor_intersectClassific = model.ignoreFactor_intersectClassific;
    gapSizeFactor_allCases_intersectClassific = model.gapSizeFactor_allCases_intersectClassific;
	gapSizeFactor_surfaceCase_intersectClassific = model.gapSizeFactor_surfaceCase_intersectClassific,
    alongSurfaceThreshold_intersectClassific = model.alongSurfaceThreshold_intersectClassific;
    othorgonalToSurfaceThreshold_intersectClassific = model.othorgonalToSurfaceThreshold_intersectClassific;
    advancedIntersectionClassification = model.advancedIntersectionClassification;
    visualizeVisibilityContextFeature = model.visualizeVisibilityContextFeature;
    octreeSearchPtr = model.octreeSearchPtr;

    ROS_DEBUG_STREAM("[visibilityContextModelHashTable::setParameters] All member variables were successfully set.");

    return true;
}

// ========================================================================================================
// ============================= STL hash table for original PFmatching class =============================
// ========================================================================================================

/**
 * @class FeatureHashTable
 * @brief Hash table for storing information about all calculated features of a certain featureType.
 *
 * The table key is the hashed feature value. Each bucket contains information about all features
 * that produced the feature value represented by key.
 */
template <typename PointNT>
STLmodelHashTable<PointNT>::STLmodelHashTable(featureType fType_,
											  PFmodel<PointNT>& model,
											  float maxLoadFactor):
modelHashTable<PointNT>(model){
    this->fType = fType_;
    this->htType = 1;
    this->max_load_factor(maxLoadFactor);
    setParameters(model);
    ROS_INFO_STREAM("[STLmodelHashTable::STLmodelHashTable] STLmodelHashTable object constructed. htType = "<< this->htType);
}

template <typename PointNT> bool
STLmodelHashTable<PointNT>::constructFeatures( const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtrIn,
											   const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
											   const bool refPointDirectionAmbiguous,
											   const bool targetPointDirectionAmbiguous,
											   const unsigned int stepSize){

	// check that clouds are not empty
    if(refCloudPtrIn->points.size() == 0 || targetCloudPtr->points.size() == 0){
        PCL_WARN("[FeatureHashTable::constructFeatures] at least one of the clouds is empty.\n");
        return false;
    }

    // save reference cloud or make new one
    if(refPointDirectionAmbiguous){

        // make new reference point cloud with positive and negative reference point
        // orientation
        typename pcl::PointCloud<PointNT>::Ptr tmpCloudPtr(new pcl::PointCloud<PointNT>);
        pcl::copyPointCloud(*refCloudPtrIn, *tmpCloudPtr);
        for(auto& p: refCloudPtrIn->points){
            tmpCloudPtr->push_back(p);
            tmpCloudPtr->back().getNormalVector3fMap() *= -1.0;
        }
        this->refPointCloudPtr = tmpCloudPtr;
    }
    else{
        this->refPointCloudPtr = refCloudPtrIn;
    }

    // erase all previous content
    this->clear();

    //reserve enough space for all features to be calculated
    this->reserve((this->refPointCloudPtr->points.size() / stepSize) * targetCloudPtr->points.size());

    // figure out normal directions to consider
    std::vector<float> targetPointNormalSigns = {1.0};
    if(targetPointDirectionAmbiguous){
        targetPointNormalSigns.push_back(-1.0);
    }

#ifdef write_scene_PPfs_hashKeys_to_binary_file
    // TODO:: remove this after debugging:
    std::vector<uint64_t> hashKeys;
#endif

	if (this->useNeighbourPPFsForTraining){ // just an info message
		ROS_INFO_STREAM("[STLmodelHashTable::collectAllModelFeatures] Generating neighbour PPFs for model training too." );
	}

    // add all features to hash table
    // i_r -> index of reference point
    // i_t -> index of target point
    MinimalDiscretePF<PointNT> modelFeature(this);
    for(unsigned int i_r = 0; i_r < this->refPointCloudPtr->points.size(); i_r += stepSize){

        // get Eigen-style point information of reference point
        Eigen::Vector3f m_r = this->refPointCloudPtr->points[i_r].getVector3fMap();
        Eigen::Vector3f n_r = this->refPointCloudPtr->points[i_r].getNormalVector3fMap();

        // pre-calculate transform Tm->g (align m_r and n_r to origin / x-axis)
        Eigen::Affine3f Tmg = aligningTransform(m_r, n_r);

        for(unsigned int i_t = 0; i_t < targetCloudPtr->points.size(); i_t++){

            // get Eigen-style point information of target point
            Eigen::Vector3f m_t = targetCloudPtr->points[i_t].getVector3fMap();
            Eigen::Vector3f n_t = targetCloudPtr->points[i_t].getNormalVector3fMap();

            // no feature of points at same location (will lead to d = 0 and therefore an
            // undefined feature vector)
            if(m_r != m_t){

                // calculate feature for all normal directions to consider and add
                for(float& targetPointNormalSign: targetPointNormalSigns){

                    if(!modelFeature.calculate( m_r, n_r,
                    							m_t, targetPointNormalSign * n_t,
												Tmg )){
                    	ROS_ERROR_STREAM("[STLmodelHashTable::constructFeatures] calculated Model-PF (BELOW if printed) is invalid... this should not happen.");
#ifdef print_feature
						ROS_INFO_STREAM("[STLmodelHashTable::constructFeatures] " << modelFeature);
#endif
                    	continue; // overflow during quantization occurred, skip invalid model-PF
                    }
#ifdef print_feature
                    ROS_INFO_STREAM("[STLmodelHashTable::constructFeatures] " << modelFeature);
#endif
                    if (!this->useNeighbourPPFsForTraining){ //!< MFZ 13.03.2018 include IF-statement, to check if neighbours should be calculated or not, because calculateNeighbourFeature() also calculates the original feature!
                    	this->emplace(std::piecewise_construct,
                    				  std::forward_as_tuple(modelFeature.getDiscreteFeatureAsInt()), // hash key
									  std::forward_as_tuple(modelFeature.alpha, i_r));               // constructor arguments for ModelPFinfo

#ifdef write_scene_PPfs_hashKeys_to_binary_file
                    	// TODO:: remove this after debugging:
                    	hashKeys.push_back(modelFeature.getDiscreteFeatureAsInt());
#endif
                    }

                    else{ // use neighbour PFs
						bool featureIsValid = false;
						for (int i = 0; i < 3; i++){
							for (int j = 0; j < 3; j++){
								for (int k = 0; k < 3; k++){
									for (int l = 0; l < 3; l++){
										featureIsValid = modelFeature.calculateNeighbourFeature(i,j,k,l);
#ifdef print_feature
										std::cout << "Quantized neighbour feature: ("<< unsigned(modelFeature.discretePFneighbour[0]) << ", "
																					 << unsigned(modelFeature.discretePFneighbour[1]) << ", "
																					 << unsigned(modelFeature.discretePFneighbour[2]) << ", "
																					 << unsigned(modelFeature.discretePFneighbour[3]) << ")"<< std::endl;
#endif
										if (featureIsValid){ // distance d > 0 and no overflow occurred
											this->emplace(std::piecewise_construct,
														  std::forward_as_tuple(modelFeature.getDiscreteNeighbourFeatureAsInt()),// key
														  std::forward_as_tuple(modelFeature.alpha, i_r));               	// constructor arguments for ModelPFinfo
										}
#ifdef print_feature
										else{
											std::cout << "[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] neighbour (ABOVE if printed) is INVALID!" << std::endl;
										}
#endif
									} // l
								} // k
							} // j
						} // i
					} // use neighbour PFs
				} // targetPointsNormalSigns
			} // if (m_r != m_t)
		} // target points
	} // reference points

#ifdef write_scene_PPfs_hashKeys_to_binary_file
	// TODO: 04.07.2018 in order to find the source of the matchings non-deterministic behaviour,
	//       that leads to different poses for the same experiment. remove this later.
	// write to a binary file within the "~/.ros" directory:
	std::ofstream outputFile;
	std::time_t timestamp = std::time(0);
	std::string filename = this->refPointCloudPtr->header.frame_id + "_";
	outputFile.open ("/home/markus/Documents/MY_SYSTEM_STL_HT_ppfs_of_model_" + filename + std::to_string(timestamp) + ".bin", std::ios::out | std::ios::trunc | std::ios::binary); // delete previous content of the file

	if (outputFile.is_open()){
		while (outputFile.good()){
			//write to file
			for (int line =0 ; line < hashKeys.size() ; line ++){
				outputFile.write((char*)&hashKeys[line], sizeof(uint64_t));
			}
			// close file
			outputFile.close();
		}
	}
	else {
		std::cout << "Error opening file" << std::endl;
	}
#endif

	// store number of all found model PPFs for statistics
	this->numberOfModelPPFs = this->size();
    ROS_INFO_STREAM("[STLmodelHashTable::constructFeatures] The number of found model PPFs were: "<< this->numberOfModelPPFs );

    // store number of all unique model PFFs (not only for statistics)
	std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT> uniqueModelPFs;
	this->getUniqueModelPFs(uniqueModelPFs);
	ROS_INFO_STREAM("[STLmodelHashTable::constructFeatures] The number of found model PPFs were (without duplicates): "<< this->numberOfUniqueModelPPFs );

    return true;
}


template <typename PointNT> std::vector<unsigned int>
STLmodelHashTable<PointNT>::getHistogram(){

    // get all existing keys (unique features)
    std::set<typename MinimalDiscretePF<PointNT>::featureVectorT> keys;
    for(auto it = this->begin(); it != this->end(); it++){
        keys.insert(it->first);
    }

    // get the maximum number of identical features
    int max = 0;
    for(auto& key: keys){
        unsigned int cnt = this->count(key);
        if(max < cnt){
            max = cnt;
        }
    }

    // make space for the whole histogram
    std::vector<unsigned int> histogram;
    histogram.resize(max + 1, 0); // initialize all with 0

    // count features with i elements
    for(auto& key: keys){
        histogram[this->count(key)]++;
    }

    return histogram;
}

template <typename PointNT> bool
STLmodelHashTable<PointNT>::setParameters(PFmodel<PointNT>& model){

	// all parameters were already set in modelHashTable::setParameters()
	return true;
}

template <typename PointNT> bool
STLmodelHashTable<PointNT>::voteWithFeature(MinimalDiscretePF<PointNT>* sceneFeaturePtr,
											HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
											flagArray<PointNT>* refPointFlagArrayPtr){

	// get all similar features
	typename MinimalDiscretePF<PointNT>::featureHashKeyT keyOfScenePF = sceneFeaturePtr->getDiscreteFeatureAsInt();
	auto featureRange = this->equal_range(keyOfScenePF);

#ifdef write_scene_PPfs_hashKeys_to_binary_file
	//TODO: remove this after debugging:
	#pragma omp critical (storeScenePF)
	{
	this->sceneHashKeys.push_back(keyOfScenePF);
	}
#endif

	// continue with next feature if there are no features in the bucket
	if(featureRange.first == this->end()){
#ifdef write_counters_to_file
		// TODO: remove after debugging
		#pragma omp atomic update
		this->numberOfFeaturesNotFoundInHT ++;
#endif
		return false;
	}

#ifdef write_counters_to_file
	// TODO: remove after debugging
	#pragma omp atomic update
	this->numberOfFeaturesFoundInHT ++;
#endif

	bool voteForAlphaSAllowed = true;
	bool voteForAlphaSbiggerNeighbourAllowed = true;
	bool voteForAlphaSsmallerNeighbourAllowed = true;

	// get alpha_scene
	float alpha_s = sceneFeaturePtr->alpha;

	// if flag array shall be used:
	if (this->faHashTableType != FA_NONE){
		// check flagArray if this PF with this pose may vote or not
		// TODO: checkFlagAtPosition currently takes sceneFeature AND hashvalue as input.
		//       this allows switching smoothly between OPH, CMPH and STL flag array version:
		// - OPH needs feature,
		// - STL needs feature(--> for accessing HashKey),
		// - CMPH needs hashValue.

		// quantize alpha_scene for flag array
		unsigned int alpha_s_index = this->getAlphaSceneIndex(alpha_s);

		#ifdef debug_show_alpha_indices
		// TODO: remove after debugging
		std::cout << "alpha_s = " << alpha_s << " alpha_s_index = " << alpha_s_index << std::endl;
		#endif

		#ifdef _OPENMP  // if openMP is available
		if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_index, sceneFeaturePtr, 0, false, omp_get_thread_num()) ){
		#else
		if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_index, sceneFeaturePtr, 0, false, 0) ){
		#endif
			// PF may not vote! There has already been a vote for this PF with this pose alpha_s.
			// std::cout << "[PFmatcher::voteWithTargetPoints] vote not allowed." << std::endl;
			voteForAlphaSAllowed = false;
#ifdef write_counters_to_file
			// TODO: remove after debugging
			#pragma omp atomic update
			this->FAdiscardsVoteCounter ++;
#endif
			if (!this->voteForAdjacentRotationAngles){
				return false;
			}
		}
		else{
			// PF may vote
			voteForAlphaSAllowed = true;
		}

		if (this->voteForAdjacentRotationAngles){

			// checkFlagAtPosition() for both to alpha_s adjacent rotation angles.
			// bigger neighbour
			unsigned int alpha_s_NeighbourIndex = this->incrementAlphaSceneIndex(alpha_s);

#ifdef debug_show_alpha_indices
			// TODO: remove after debugging
			std::cout << "alpha_s = " << alpha_s << " alpha_s_index = " << alpha_s_index << " alpha_s_NeighbourIndex = " << alpha_s_NeighbourIndex << std::endl;
#endif

			#ifdef _OPENMP  // if openMP is available
			if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_NeighbourIndex, sceneFeaturePtr, 0, false, omp_get_thread_num()) ){
			#else
			if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_NeighbourIndex, sceneFeaturePtr, 0, false, 0) ){
			#endif
				// PF may not vote! There has already been a vote for this PF with this pose alpha_s.
				voteForAlphaSbiggerNeighbourAllowed = false;
#ifdef write_counters_to_file
				// TODO: remove after debugging
				#pragma omp atomic update
				this->FAdiscardsAdjacentVoteCounter ++;
#endif
			}
			else{
				// PF may vote
				voteForAlphaSbiggerNeighbourAllowed = true;
			}

			// smaller neighbour
			alpha_s_NeighbourIndex = this->decrementAlphaSceneIndex(alpha_s);

#ifdef debug_show_alpha_indices
			// TODO: remove after debugging
			std::cout << "alpha_s = " << alpha_s << " alpha_s_index = " << alpha_s_index << " alpha_s_NeighbourIndex = " << alpha_s_NeighbourIndex << std::endl;
#endif

			#ifdef _OPENMP  // if openMP is available
			if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_NeighbourIndex, sceneFeaturePtr, 0, false, omp_get_thread_num()) ){
			#else
			if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_NeighbourIndex, sceneFeaturePtr, 0, false, 0) ){
			#endif
				// PF may not vote! There has already been a vote for this PF with this pose alpha_s.
				voteForAlphaSsmallerNeighbourAllowed = false;
#ifdef write_counters_to_file
				// TODO: remove after debugging
				#pragma omp atomic update
				this->FAdiscardsAdjacentVoteCounter ++;
#endif
			}
			else{
				// PF may vote
				voteForAlphaSsmallerNeighbourAllowed = true;
			}
		} // voteForAdjacentRotationAngles
	} // use Flag Array

	// go through all similar features in the hash table and vote
	for(auto infoIt = featureRange.first; infoIt != featureRange.second; infoIt++){

		// get alpha_model from model hash table
		float alpha_m = infoIt->second.alpha;

		// vote, if allowed by flag array. or vote if flag array is disabled
		if(voteForAlphaSAllowed){

			// calculate total alignment angle as alpha = alpha_model - alpha_scene
			float alpha =  alpha_m - alpha_s;

#ifdef write_alphas_to_binary_file
			//TODO: remove this after debugging:
			#pragma omp critical (storeAlphas)
			{
			this->alphas.push_back(alpha);
			this->alphas_s.push_back(alpha_s);
			this->alphas_m.push_back(alpha_m);
			}
#endif

			// ensure that alpha is in [-pi, pi) after subtraction
			// (will be off by max. one period)
			if(alpha < -M_PI){
				alpha += 2 * M_PI;
			}
			else if(alpha >= M_PI){
				alpha -= 2 * M_PI;
			}

			// calculate alpha index in hough-voting space
			unsigned int alphaIndex = (alpha + M_PI) / this->d_alpha; // floor rounding
#ifdef debug_show_alpha_indices
			// TODO: remove after debugging
			std::cout << "alpha = " << alpha << " alphaIndex = " << alphaIndex << std::endl;
#endif

#ifdef write_alphas_to_binary_file
			//TODO: remove this after debugging:
			#pragma omp critical (storeAlphas)
			{
			this->alphaIndices.push_back(alphaIndex);
			}
#endif

			// vote
			this->voteInHoughSpaces(infoIt, accumulatorArray, alphaArray, alphaIndex, alpha);
#ifdef write_counters_to_file
			#pragma omp atomic update
			this->originalVoteCounter ++;
#endif
		}

		// vote for neighboured alphas!
		if (this->voteForAdjacentRotationAngles){
			if(voteForAlphaSbiggerNeighbourAllowed){

				// bigger neighbour
				std::pair<float, unsigned int> alphaNeighbour = this->incrementAlpha(alpha_m, alpha_s);
				this->voteInHoughSpaces(infoIt, accumulatorArray, alphaArray, alphaNeighbour.second, alphaNeighbour.first);
#ifdef write_counters_to_file
				#pragma omp atomic update
				this->biggerVoteCounter ++;
#endif

#ifdef debug_show_alpha_indices
				// TODO: remove after debugging
				std::cout << " alphaNeighbour_Index (bigger) = " << alphaNeighbour.second << " alphaNeighbour (bigger)= " << alphaNeighbour.first << std::endl;
#endif
			}
			if(voteForAlphaSsmallerNeighbourAllowed){
				// smaller neighbour
				std::pair<float, unsigned int> alphaNeighbour = this->decrementAlpha(alpha_m, alpha_s);
				this->voteInHoughSpaces(infoIt, accumulatorArray, alphaArray, alphaNeighbour.second, alphaNeighbour.first);
#ifdef write_counters_to_file
				#pragma omp atomic update
				this->smallerVoteCounter ++;
#endif

#ifdef debug_show_alpha_indices
				// TODO: remove after debugging
				std::cout << " alphaNeighbour_Index (bigger) = " << alphaNeighbour.second << " alphaNeighbour (bigger)= " << alphaNeighbour.first << std::endl;
#endif
			}
		} // voteForAdjacentRotationAngles
	} // loop through all similar features

	return true;
}


template <typename PointNT> bool
STLmodelHashTable<PointNT>::collapseHashTable(std::vector<int>& equivalencies, bool deleteUniquePoints, unsigned int nPoints){

	// remove unused features from hash table and weight remaining ones
	auto entryIt = this->begin();
	while(entryIt != this->end()){

		int nEquivalences = equivalencies[entryIt->second.refPointIndex];
		bool eraseEntry;
		if(deleteUniquePoints){
			eraseEntry = (nEquivalences <= 0);
		}
		else{
			eraseEntry = (nEquivalences < 0);
		}
		if(eraseEntry){
			// ref point was replaced by another equivalent one, feature can be deleted
			entryIt = this->erase(entryIt);
		}
		else{
			// ref point is unique or has equivalences, feature gets higher weight
			entryIt->second.weight *= nEquivalences + 1; // +1 since original point
														 // also counts
			entryIt++;
		}
	}

	// find correspondence between old and new ref point indices:
	// indexCorrespondence[i] holds the new index for ref point with old index i
	// entry of -1 means the point does not exist anymore
	std::vector<int> newIndices;
	newIndices.resize(nPoints, -1);
	int currentIndex = 0;
	for(unsigned int i = 0; i < equivalencies.size(); i++){
		bool pointStillExists;
		if(deleteUniquePoints){
			pointStillExists = (equivalencies[i] > 0);
		}
		else{
			pointStillExists = (equivalencies[i] >= 0);
		}
		if(pointStillExists){
			newIndices[i] = currentIndex;
			currentIndex++;
		}
	}

	// change ref point indices in hash table
	for(auto entry= this->begin(); entry != this->end(); entry++){
		entry->second.refPointIndex = newIndices[entry->second.refPointIndex];
	}
	return true;
}


template <typename PointNT> uint64_t
STLmodelHashTable<PointNT>::getNumberOfUniqueModelPFs(){
	return this->numberOfUniqueModelPPFs;
}


template <typename PointNT> uint64_t
STLmodelHashTable<PointNT>::getUniqueModelPFs(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& uniquePFs){

	for (auto it = this->begin(); it != this->end(); it++){
		uniquePFs.insert(it->first);
	}
	this->numberOfUniqueModelPPFs = uniquePFs.size();
	return this->numberOfUniqueModelPPFs;
}


template <typename PointNT> uint64_t
STLmodelHashTable<PointNT>::getNumberOfModelPFs(){
	return this->numberOfModelPPFs;
}


template <typename PointNT> void
STLmodelHashTable<PointNT>::getHighFrequencyPFs(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys, unsigned int minEntries){

	typename MinimalDiscretePF<PointNT>::featureHashKeyT key;
	for(auto it = this->begin(); it != this->end(); it++){
		key = it->first;
		if(this->count(key) >= minEntries){
			keys.insert(key);
		}
	}
}


template <typename PointNT> void
STLmodelHashTable<PointNT>::weightByDistance(float power){

	for(auto featureInfo = this->begin(); featureInfo != this->end(); featureInfo++){

		// get distance of feature
		MinimalDiscretePF<PointNT> tmpFeature(featureInfo->first, 0); // alpha = 0 is arbitrary
		float d = tmpFeature.discretePF[0] * this->d_dist_abs;

		// weight according to distance of pointspowf(n, -1 * power);
		featureInfo->second.weight *= powf(d / this->modelDiameter, power);
	}
}


template <typename PointNT> void
STLmodelHashTable<PointNT>::discardByDistance(float distThresh){
	auto featureIt = this->begin();
	while(featureIt != this->end()){

		// get distance of feature
		MinimalDiscretePF<PointNT> tmpFeature(featureIt->first, 0); // alpha = 0 is arbitrary
		float d = tmpFeature.discretePF[0] * this->d_dist_abs;

		// erase if distance is small
		// oldIt becomes invalid if it is erased, but other iterators remain valid, so
		// we increment the iterator before erasing.
		auto oldIt = featureIt;
		featureIt++;
		if(d <= distThresh){
			this->erase(oldIt);
		}
	}
}


template <typename PointNT> void
STLmodelHashTable<PointNT>::weightByFrequency(float power){
	// weight each entry separately
	float n = 0;
	for(auto it = this->begin(); it != this->end(); it++){
		n = this->count(it->first);
		it->second.weight *= powf(n, -1 * power);
	}
}

template <typename PointNT> void
STLmodelHashTable<PointNT>::weightByValue(float weight){
	for(auto featureInfo = this->begin(); featureInfo != this->end(); featureInfo++){
		featureInfo->second.weight *= weight;
	}
}


template <typename PointNT> void
STLmodelHashTable<PointNT>::voteInHoughSpaces(typename hashTableT::iterator& infoIt, HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray, unsigned int alphaIndex, float alpha){

	accumulatorArray.vote(infoIt->second.weight,        // weight of feature
						  infoIt->second.refPointIndex,
						  alphaIndex);
	alphaArray.vote(alpha * infoIt->second.weight, 		// weighted alpha
					infoIt->second.refPointIndex,
					alphaIndex);
}


// ========================================================================================================
// ======================== STL hash table class for visibility context matching ==========================
// ========================================================================================================

/**
 * @class VisibilityContextFeatureHashTable
 * @brief Extended version of the original hash table for storing information about calculated S2S features and their Visibility Context -- as proposed in [Kim and Medioni 2011].
 *
 * The hash table key contains the quantized S2S information and its visibility-context. It has 128 bit. This key maps to the feature value, stored as "ModelPFinfo" struct.
 * The hashed Key is a combination (boost::hash_combine) of the 128 bit information and only of size 64 bit. Hence collisions (= equal hashes) are theoretically unavoidable but presumably won't occur very often.
 * std::unstructured_map solves these conflicts with the provided overloaded '== operator'. That way no information gets lost, even if two hashes are equal.
 */
template <typename PointNT>
STLvisibilityContextModelHashTable<PointNT>::STLvisibilityContextModelHashTable(featureType fType_,
																				PFmodel<PointNT>& model,
																				float maxLoadFactor):
modelHashTable<PointNT>(model),
STLmodelHashTable<PointNT>(fType_, model, maxLoadFactor),
visibilityContextModelHashTable<PointNT>(model){
	this->fType = fType_;
	this->htType = 2;
	this->max_load_factor(maxLoadFactor);
	setParameters(model);
	ROS_INFO_STREAM("[STLvisibilityContextModelHashTable::STLvisibilityContextModelHashTable] STLvisibilityContextModelHashTable object constructed. htType = "<< this->htType); // TODO: MFZ remove after debugging
}


template <typename PointNT> bool
STLvisibilityContextModelHashTable<PointNT>::constructFeatures( const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtrIn,
															   	const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
																const bool refPointDirectionAmbiguous,
																const bool targetPointDirectionAmbiguous,
																const unsigned int stepSize){

	// check that clouds are not empty
    if(refCloudPtrIn->points.size() == 0 || targetCloudPtr->points.size() == 0){
        PCL_WARN("[STLvisibilityContextModelHashTable::constructFeatures] at least one of the clouds is empty.\n");
        return false;
    }

    // save reference cloud or make new one
    if(refPointDirectionAmbiguous){

        // make new reference point cloud with positive and negative reference point
        // orientation
        typename pcl::PointCloud<PointNT>::Ptr tmpCloudPtr(new pcl::PointCloud<PointNT>);
        pcl::copyPointCloud(*refCloudPtrIn, *tmpCloudPtr);
        for(auto& p: refCloudPtrIn->points){
            tmpCloudPtr->push_back(p);
            tmpCloudPtr->back().getNormalVector3fMap() *= -1.0;
        }
        this->refPointCloudPtr = tmpCloudPtr;
    }
    else{
        this->refPointCloudPtr = refCloudPtrIn;
    }

    // erase all previous content
    this->clear();

    //reserve enough space for all features to be calculated
    // --> commented this, because reserve() only reserves memory for the buckets (which contain only links to the hashed PPFs) not the hashed PPFs.
    //     there is no way to know the bucket-count after finishing the hash-table build. (because one does not know how many equal PPFs will be detected)
    //     reserving memory for buckets only prevents rehashing during the building process of the hash table. but loosing time due to rehashing in training-mode is not critical.
//    reserve((this->refPointCloudPtr->points.size() / stepSize) * targetCloudPtr->points.size() * 6561); // MFZ 16/11/17 not needed
    //                                                                                     ^ number of PPF+VisCon neighbours

    // figure out normal directions to consider
    std::vector<float> targetPointNormalSigns = {1.0};
    if(targetPointDirectionAmbiguous){
        targetPointNormalSigns.push_back(-1.0);
    }

    // set parameters for modelFeature:
    VisibilityContextDiscretePF<PointNT> modelFeature(this);

    // create PCLVisualizer
    boost::shared_ptr<pcl::visualization::PCLVisualizer> fancy_viewer;
#ifdef visualize_VisCon_feature
	if (this->visualizeVisibilityContextFeature){
		// show pointcloud in visualizer
		fancy_viewer = visualization::visualizePointCloud<PointNT>(this->refPointCloudPtr);//, m_r, m_t, n_r, n_t);

		// show center points of all occupied voxels:
		std::vector<PointNT, typename Eigen::aligned_allocator<PointNT> > voxelCenters;
		this->octreeSearchPtr->getOccupiedVoxelCenters (voxelCenters);
		typename pcl::PointCloud<PointNT>::Ptr voxelCentersCloudPtr (new typename pcl::PointCloud<PointNT>);
		voxelCentersCloudPtr->points = voxelCenters;
		visualization::visualizeOctreeCenterpoints<PointNT>(fancy_viewer, voxelCentersCloudPtr);
	}
	else {
		fancy_viewer = nullptr;
	}
#else
	fancy_viewer = nullptr;
#endif

	if (this->useNeighbourPPFsForTraining){ // just an info message
		ROS_INFO_STREAM("[STLvisibilityContextModelHashTable::collectAllModelFeatures] Generating neighbour PPFs for model training too." );
	}

    // add all features to hash table
    // i_r -> index of reference point
    // i_t -> index of target point
    // loop over all reference points
	for(unsigned int i_r = 0; i_r < this->refPointCloudPtr->points.size(); i_r += stepSize){

        // get Eigen-style point information of reference point
        Eigen::Vector3f m_r = this->refPointCloudPtr->points[i_r].getVector3fMap();
        Eigen::Vector3f n_r = this->refPointCloudPtr->points[i_r].getNormalVector3fMap();

        // pre-calculate transform Tm->g (align m_r and n_r to origin / x-axis)
        Eigen::Affine3f Tmg = aligningTransform(m_r, n_r);

        for(unsigned int i_t = 0; i_t < targetCloudPtr->points.size(); i_t++){

            // get Eigen-style point information of target point
            Eigen::Vector3f m_t = targetCloudPtr->points[i_t].getVector3fMap();
            Eigen::Vector3f n_t = targetCloudPtr->points[i_t].getNormalVector3fMap();

            // no feature of points at same location (will lead to d = 0 and therefore an undefined feature vector)
            if(m_r != m_t){

#ifdef visualize_VisCon_feature
            	if (this->visualizeVisibilityContextFeature){

            		// show PPF (+ VisCon) in visualizer
            		visualization::visualizeVisConFeature<PointNT>(fancy_viewer, m_r, m_t, n_r, n_t);
            		// show PCA-results in visualizer
					visualization::visualizePCA<PointNT>(fancy_viewer, this->modelPtr->modelCentroid, this->modelPtr->pcaEigenValues, this->modelPtr->pcaEigenVectors, this->modelPtr->d_max, this->modelPtr->d_med, this->modelPtr->d_min);
            	}
#endif

                // calculate feature for all normal directions to consider and add
                for(float& targetPointNormalSign: targetPointNormalSigns){

                	if(!modelFeature.calculateForTraining(m_r, n_r,
														  m_t, targetPointNormalSign * n_t,
														  Tmg,
														  fancy_viewer)){

						ROS_ERROR_STREAM("[STLvisibilityContextModelHashTable::constructFeatures] calculated Model-PF (BELOW if printed) is invalid... this should not happen.");

#ifdef print_feature
						ROS_INFO_STREAM ("[STLvisibilityContextModelHashTable::constructFeatures] " << modelFeature);
#endif
						continue; // overflow during quantization occurred, skip invalid model-PF
                	}
#ifdef print_feature
                	ROS_INFO_STREAM ("[STLvisibilityContextModelHashTable::constructFeatures] " << modelFeature);
#endif

					// counter to find out which of the V_out-directions result in V_out = 0 the most
					for (int i = 0; i < 3; i++){
						if (modelFeature.discreteVC[i] == 0){
							this->countVoutZero[i].second ++;
						}
					}

                	if (!this->useNeighbourPPFsForTraining){ //!< MFZ 13.03.2018 include IF-statement, to check if neighbours should be calculated or not, because calculateNeighbourFeature() also calculates the original feature!
                		this->emplace(std::piecewise_construct,
									  std::forward_as_tuple(modelFeature.getDiscreteFeatureAsInt()), 	// hash key, a uint64_t
									  std::forward_as_tuple(modelFeature.alpha, i_r));    			// constructor arguments for ModelPFinfo
                	}
                	else{
                		bool featureIsValid = false;
						for (int i = 0; i < 3; i++){
							for (int j = 0; j < 3; j++){
								for (int k = 0; k < 3; k++){
									for (int l = 0; l < 3; l++){
										for (int m = 0; m < 3; m++){
											for (int n = 0; n < 3; n++){
												for (int o = 0; o < 3; o++){
													for (int p = 0; p < 3; p++){
														featureIsValid = modelFeature.calculateNeighbourFeature(i,j,k,l,m,n,o,p);
#ifdef print_feature
															std::cout << "[STLvisibilityContextModelHashTable::constructFeatures] Quantized neighbour feature: ("<< unsigned(modelFeature.discretePFneighbour[0]) << ", "
																																								<< unsigned(modelFeature.discretePFneighbour[1]) << ", "
																																								<< unsigned(modelFeature.discretePFneighbour[2]) << ", "
																																								<< unsigned(modelFeature.discretePFneighbour[3]) << " | "
																																								<< unsigned(modelFeature.discreteVCneighbour[0]) << ", "
																																								<< unsigned(modelFeature.discreteVCneighbour[1]) << ", "
																																								<< unsigned(modelFeature.discreteVCneighbour[2]) << ", "
																																								<< unsigned(modelFeature.discreteVCneighbour[3]) << ")" << std::endl;
#endif
														if (featureIsValid){ // distance d > 0 and no overflow occurred
															this->emplace(std::piecewise_construct,
																	      std::forward_as_tuple(modelFeature.getDiscreteNeighbourFeatureAsInt()),
																		  std::forward_as_tuple(modelFeature.alpha, i_r));

															// counter to find out which of the V_out-directions result in V_out = 0 the most
															for (int i = 0; i < 3; i++){
																if (modelFeature.discreteVC[i] == 0){
																	this->countVoutZero[i].second ++;
																}
															}
														}
#ifdef print_feature
														else{ // feature is invalid
															std::cout << "[STLvisibilityContextModelHashTable::constructFeatures] neighbour (ABOVE if printed) is INVALID!" << std::endl;
														}
#endif
													} // p
												} // o
											} // n
										} // m
									} // l
								} // k
							} // j
						} // i
                	} // else use neighbour PFs
                } // targetPointsNormalSigns

#ifdef visualize_VisCon_feature
                if (this->visualizeVisibilityContextFeature){
					// Visualizer loop (runs in a separate thread)
					while ((!fancy_viewer->wasStopped ())){ // needs to be stopped by user 'closing' the window, clicking x.
						fancy_viewer->spinOnce (100);
						boost::this_thread::sleep (boost::posix_time::microseconds (100000));
					}

					char input;
					std::cout << "\nWould you like to visualize the next visibility feature too? [y/n]\nNote: Choosing 'n' is permanent for this Hash-Table-type." << std::endl;
					std::cin >> input;

					while ((input != 'y') && (input !='n'))
					{
						std::cout << "Input unknown. Would you like to visualize the next visibility feature too? [y/n]" << std::endl;
						std::cin >> input;
					}
					if (input == 'y'){
						fancy_viewer->resetStoppedFlag();
						fancy_viewer->removeAllShapes();
					}
					else { // input has to be 'n'
						modelFeature.visualizeVisibilityContextFeature = false;
						this->visualizeVisibilityContextFeature = false;  //member of visibility context feature hash table
						ROS_INFO_STREAM("[STLvisibilityContextModelHashTable::constructFeatures] Set visualizeVisibilityContextFeature to FALSE");
						fancy_viewer->removeAllShapes();
						fancy_viewer->removeAllPointClouds();
						fancy_viewer->close(); //VTK bug prevents window from being closed.
					}
                } // visualize visibility context feature
#endif
            } // m_r != m_t
        } // loop over target points
    } // loop over reference points

	// store number of all found model PPFs for statistics
	this->numberOfModelPPFs = this->size();
    ROS_INFO_STREAM("[STLvisibilityContextModelHashTable::constructFeatures] The number of found model PPFs were: "<< this->numberOfModelPPFs );

    // store number of all unique model PFFs (not only for statistics)
	std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT> uniqueModelPFs;
	this->getUniqueModelPFs(uniqueModelPFs);
	ROS_INFO_STREAM("[STLvisibilityContextModelHashTable::constructFeatures] The number of found model PPFs were (without duplicates): "<< this->numberOfUniqueModelPPFs );

	// TODO: 11.04.2018 added counter to find out which of the V_out-directions result in V_out = 0 the most
	ROS_INFO_STREAM("[STLvisibilityContextModelHashTable::constructFeatures] counting V_out_x = 0 for this model: V_out_0 = "<< this->countVoutZero[0].second <<
																											   ", V_out_1 = "<< this->countVoutZero[1].second <<
																											   ", V_out_2 = "<< this->countVoutZero[2].second <<
																											   ", V_out_3 = "<< this->countVoutZero[3].second);
	std::sort(this->countVoutZero.begin(), this->countVoutZero.end(), [](const std::pair<int,double> &left, const std::pair<int,double> &right) {return left.second > right.second;});
	ROS_INFO_STREAM("[STLvisibilityContextModelHashTable::constructFeatures] new order of calculating the V_out: "<< this->countVoutZero[0].first << ", "
																												  << this->countVoutZero[1].first << ", "
																												  << this->countVoutZero[2].first << ", "
																												  << this->countVoutZero[3].first );
	return true;
}


template <typename PointNT> bool
STLvisibilityContextModelHashTable<PointNT>::setParameters(PFmodel<PointNT>& model){

    // all parameters were already set in modelHashTable::setParameters() and visibilityContextModelHashTable::setParameters()
    return true;
}


template <typename PointNT> void
STLvisibilityContextModelHashTable<PointNT>::weightByDistance(float power){

	for(auto featureInfo = this->begin(); featureInfo != this->end(); featureInfo++){

		// get distance of feature
		VisibilityContextDiscretePF<PointNT> tmpFeature(featureInfo->first, 0); // alpha = 0 is arbitrary
		float d = tmpFeature.discretePF[0] * this->d_dist_abs;

		// weight according to distance of points powf(n, -1 * power);
		featureInfo->second.weight *= powf(d / this->modelDiameter, power);
	}
}


template <typename PointNT> void
STLvisibilityContextModelHashTable<PointNT>::discardByDistance(float distThresh){
	auto featureIt = this->begin();
	while(featureIt != this->end()){

		// get distance of feature
		VisibilityContextDiscretePF<PointNT> tmpFeature(featureIt->first, 0); // alpha = 0 is arbitrary
		float d = tmpFeature.discretePF[0] * this->d_dist_abs;

		// erase if distance is small
		// oldIt becomes invalid if it is erased, but other iterators remain valid, so
		// we increment the iterator before erasing.
		auto oldIt = featureIt;
		featureIt++;
		if(d <= distThresh){
			this->erase(oldIt);
		}
	}
}


// ========================================================================================================
// ===================== hash table class with CMPH-CHD minimal perfect hash function =====================
// ========================================================================================================

/**
 * @class CMPHmodelHashTable
 * @brief In memory-usage minimized hash table with minimal-perfect-hash-function for storing information about calculated pair features.
 *
 * The hash table key contains the quantized PF information. It has 64 bit.
 * The hash value is calculated from the hash key with the CHPH-CHD minimal perfect hash algorithm.
 * Therefore the hash value maps from the key directly to the index in the hash table, where the PPF-struct is stored.
 * Compared to the std::unordered_multimap, a bucket-structure is not needed and therefore the hash table should be of minimal size.
 */
template <typename PointNT>
CMPHmodelHashTable<PointNT>::CMPHmodelHashTable(featureType fType_ ,
												PFmodel<PointNT>& model):
modelHashTable<PointNT>(model){
	this->fType = fType_;
	this->htType = 3;
	setParameters(model);
	ROS_INFO_STREAM("[CMPHmodelHashTable::CMPHmodelHashTable] CMPHmodelHashTable object constructed. htType = "<< this->htType);
}


template <typename PointNT> bool
CMPHmodelHashTable<PointNT>::constructFeatures( const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtrIn,
											 	const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
												const bool refPointDirectionAmbiguous,
												const bool targetPointDirectionAmbiguous,
												const unsigned int stepSize){

	// check that clouds are not empty
    if(refCloudPtrIn->points.size() == 0 || targetCloudPtr->points.size() == 0){
        PCL_WARN("[CMPHmodelHashTable::constructFeatures] at least one of the clouds is empty.\n");
        return false;
    }

    // save reference cloud or make new one
    if(refPointDirectionAmbiguous){

        // make new reference point cloud with positive and negative reference point
        // orientation
        typename pcl::PointCloud<PointNT>::Ptr tmpCloudPtr(new pcl::PointCloud<PointNT>);
        pcl::copyPointCloud(*refCloudPtrIn, *tmpCloudPtr);
        for(auto& p: refCloudPtrIn->points){
            tmpCloudPtr->push_back(p);
            tmpCloudPtr->back().getNormalVector3fMap() *= -1.0;
        }
        this->refPointCloudPtr = tmpCloudPtr;
    }
    else{
        this->refPointCloudPtr = refCloudPtrIn;
    }

    // erase all previous content
    hashTableKeys.clear();
    minimalHashTable.clear();

	// reserve memory
	hashTableKeys.reserve( refCloudPtrIn->points.size() * targetCloudPtr->points.size() );

    // figure out normal directions to consider
    std::vector<float> targetPointNormalSigns = {1.0};
    if(targetPointDirectionAmbiguous){
        targetPointNormalSigns.push_back(-1.0);
    }

    // add all features to hash table
    // i_r -> index of reference point
    // i_t -> index of target point
    MinimalDiscretePF<PointNT> modelFeature(this); // create object and set parameters for modelFeature.calculate()

    // collect all PPFs that occur in the current model
    collectAllModelFeatures(targetCloudPtr, modelFeature, targetPointNormalSigns, stepSize);

	// shrink to fit actual size
	hashTableKeys.shrink_to_fit();

	// store number of all found model PPFs for statistics
	this->numberOfModelPPFs = hashTableKeys.size();
    ROS_INFO_STREAM("[CMPHmodelHashTable::constructFeatures] The number of found model PPFs were: "<< this->numberOfModelPPFs );

    // remove all PPF duplicates that where found in the model
	sort( hashTableKeys.begin(), hashTableKeys.end() );
	hashTableKeys.erase( unique( hashTableKeys.begin(), hashTableKeys.end() ), hashTableKeys.end() );
	hashTableKeys.shrink_to_fit();

	// store number of all unique model PFFs (not only for statistics)
	this->numberOfUniqueModelPPFs = hashTableKeys.size();
	ROS_INFO_STREAM("[CMPHmodelHashTable::constructFeatures] The number of found model PPFs were (without duplicates): "<< this->numberOfUniqueModelPPFs );

	this->minimalHashTable.resize(this->numberOfUniqueModelPPFs);
	// create CMPH-CHD hash function from the now known model PFs
	if (!constructMinimalPerfectHashFunction("hashFunction_of_" + this->refPointCloudPtr->header.frame_id)){
		return false;
	}

	// calculate all model PFs again and store them in the hash table
	storeAllModelFeatures(targetCloudPtr, modelFeature, targetPointNormalSigns, stepSize);

	ROS_INFO_STREAM("[CMPHmodelHashTable::constructFeatures] the CMPH-model hash table contains unique PPFs: "<< minimalHashTable.size() );

    return true;
}


template <typename PointNT> void
CMPHmodelHashTable<PointNT>::collectAllModelFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
												     MinimalDiscretePF<PointNT> &modelFeature,
													 std::vector<float> &targetPointNormalSigns,
													 const unsigned int stepSize){

	if (this->useNeighbourPPFsForTraining){ // just an info message
		ROS_INFO_STREAM("[CMPHmodelHashTable::collectAllModelFeatures] Generating neighbour PPFs for model training too." );
	}

	// loop over all reference points
	for(unsigned int i_r = 0; i_r < this->refPointCloudPtr->points.size(); i_r += stepSize){

		// get Eigen-style point information of reference point
		Eigen::Vector3f m_r = this->refPointCloudPtr->points[i_r].getVector3fMap();
		Eigen::Vector3f n_r = this->refPointCloudPtr->points[i_r].getNormalVector3fMap();

		// pre-calculate transform Tm->g (align m_r and n_r to origin / x-axis)
		Eigen::Affine3f Tmg = aligningTransform(m_r, n_r);

		for(unsigned int i_t = 0; i_t < targetCloudPtr->points.size(); i_t++){

			// get Eigen-style point information of target point
			Eigen::Vector3f m_t = targetCloudPtr->points[i_t].getVector3fMap();
			Eigen::Vector3f n_t = targetCloudPtr->points[i_t].getNormalVector3fMap();

			// no feature of points at same location (will lead to d = 0 and therefore an
			// undefined feature vector)
			if(m_r != m_t){

				// calculate feature for all normal directions to consider and add
				for(float& targetPointNormalSign: targetPointNormalSigns){

					if (!modelFeature.calculate(m_r, n_r,
											  	m_t, targetPointNormalSign * n_t,
												Tmg)){

						ROS_ERROR_STREAM("[CMPHmodelHashTable::collectAllModelFeatures] calculated Model-PF (BELOW if printed) is invalid... this should not happen.");
#ifdef print_feature
						ROS_INFO_STREAM("[CMPHmodelHashTable::collectAllModelFeatures] " << modelFeature);
#endif
						continue; // overflow during quantization occurred, skip invalid model-PF
					}
#ifdef print_feature
					ROS_INFO_STREAM("[CMPHmodelHashTable::collectAllModelFeatures] " << modelFeature);
#endif

					if (!this->useNeighbourPPFsForTraining){ //!< MFZ 13.03.2018 include IF-statement, to check if neighbours should be calculated or not, because calculateNeighbourFeature() also calculates the original feature!
//						hashTableKeys.push_back((std::to_string(modelFeature.getDiscreteFeatureAsInt())).c_str());
						hashTableKeys.push_back(modelFeature.getDiscreteFeatureAsInt());
					}
					else{
						bool featureIsValid = false;
						for (int i = 0; i < 3; i++){
							for (int j = 0; j < 3; j++){
								for (int k = 0; k < 3; k++){
									for (int l = 0; l < 3; l++){
										featureIsValid = modelFeature.calculateNeighbourFeature(i,j,k,l);
#ifdef print_feature
										std::cout << "[CMPHmodelHashTable::collectAllModelFeatures] Quantized neighbour feature: ("<< unsigned(modelFeature.discretePFneighbour[0]) << ", "
																																<< unsigned(modelFeature.discretePFneighbour[1]) << ", "
																																<< unsigned(modelFeature.discretePFneighbour[2]) << ", "
																																<< unsigned(modelFeature.discretePFneighbour[3]) << ")" << std::endl;
#endif
										if (featureIsValid){ // distance d > 0 and no overflow occurred
//											hashTableKeys.push_back((std::to_string(modelFeature.getDiscreteNeighbourFeatureAsInt())).c_str());
											hashTableKeys.push_back(modelFeature.getDiscreteNeighbourFeatureAsInt());
										}
#ifdef print_feature
										else{
											std::cout << "[CMPHmodelHashTable::collectAllModelFeatures] neighbour (ABOVE if printed) is INVALID!" << std::endl;
										}
#endif
									} // l
								} // k
							} // j
						} // i
					} // else use neighbour ppfs
				} // targetPointsNormalSigns
			} // if (m_t != m_r)
		} // target points
	} // reference points

#ifdef write_scene_PPfs_hashKeys_to_binary_file
	// TODO: 04.07.2018 in order to find the source of the matchings non-deterministic behaviour,
	//       that leads to different poses for the same experiment. remove this later.
	// write to a binary file within the "~/.ros" directory:
	std::time_t timestamp = std::time(0);
	std::ofstream outputFile;
	std::string filename = this->refPointCloudPtr->header.frame_id + "_";
	outputFile.open ("/home/markus/Documents/MY_SYSTEM_CMPH_HT_ppfs_of_model_" + filename + std::to_string(timestamp) + ".bin", std::ios::out | std::ios::trunc | std::ios::binary); // delete previous content of the file

	if (outputFile.is_open()){
		while (outputFile.good()){
			//write to file
			for (std::size_t line =0 ; line < hashTableKeys.size() ; line ++){
				outputFile.write((char*)&hashTableKeys[line], sizeof(uint64_t));
			}
			// close file
			outputFile.close();
		}
	}
	else {
		std::cout << "Error opening file" << std::endl;
	}
#endif

}


template <typename PointNT> bool
CMPHmodelHashTable<PointNT>::constructMinimalPerfectHashFunction(std::string hashFunctionFileNameWithoutExtension){

	// get number of keys
	std::size_t numberOfKeys = hashTableKeys.size();
	// copy all the keys to char** construct
	char** vector = (char**)calloc( numberOfKeys, sizeof(char*));
	if (vector!=nullptr){ // if calloc() succeeded
		for(std::size_t key = 0; key < numberOfKeys; key++){
			std::string keyString = std::to_string(hashTableKeys[key]);
			vector[key] = (char *)calloc( (keyString.length()+1), sizeof(char));
			if (vector[key]!=nullptr){ // if calloc() succeeded
				// copy the c-string into the vector
				std::strcpy(vector[key], keyString.c_str());
			}
			else{
				std::cout<<"[CMPHmodelHashTable::constructMinimalPerfectHashFunction] Error: Memory allocation for array of hash keys failed!" << std::endl;
			    // free all memory that was allocated up to this point
				for(std::size_t allocatedKey=0; allocatedKey < key; allocatedKey++){
			        free(vector[allocatedKey]);
			    }
				free(vector);
				return false;
			}
		}
	}
	else{
		std::cout<<"[CMPHmodelHashTable::constructMinimalPerfectHashFunction] Error: Memory allocation for array of hash keys failed!" << std::endl;
		free(vector);
		return false;
	}

	// construct the hash function
	hashFunctionData.mphf_fd = fopen( (hashFunctionFileNameWithoutExtension + ".mph").c_str(), "w");
    hashFunctionData.source = cmph_io_vector_adapter(vector, numberOfKeys); /** Adapter pattern API **/ // save vector of keys in pointer to struct cmph_io_adapter_t.
	//Create minimal perfect hash function using the CHD algorithm.
	cmph_config_t *config = cmph_config_new(hashFunctionData.source);/** Hash configuration API **/ // setting the key-source in a new config struct /* see cmph_new() */
	cmph_config_set_verbosity(config, 2);
	cmph_config_set_algo(config, CMPH_CHD);          				 /** Hash configuration API **/ // setting the algorithm
	cmph_config_set_keys_per_bin(config, 1); 	// set keys per bin
	cmph_config_set_b(config, 1);				// set keys per bucket
	cmph_config_set_mphf_fd(config, hashFunctionData.mphf_fd);       /** Hash configuration API **/ // setting the file (?) to serialize the hash function into
	hashFunctionData.hash = cmph_new(config);                        /** Hash API **/               // pointer to the struct containing Hash querying algorithm data, a.k.a. the "hashfunction struct"
																									/*
																									* cmph_new() returns a pointer to a function f which in turn returns a thing called "cmph_t"
																									* so hash is a pointer which contains the address of the function f
																									*/
	cmph_config_destroy(config);									/** Hash configuration API **/  // deleting pointer to config struct (and probably deleting config-struct instance)
	cmph_dump(hashFunctionData.hash, hashFunctionData.mphf_fd);		/** Hash serialization */       // dumping the hash function (struct) into a file to use it later

	// deallocate the memory-blocks
    for(std::size_t key=0; key < numberOfKeys; key++){
        free(vector[key]);
    }
	free(vector);
	hashTableKeys.clear();
	hashTableKeys.shrink_to_fit();

	ROS_INFO_STREAM("[CMPHmodelHashTable::constructMinimalPerfectHashFunction] Initializing the CMPH struct with "<< numberOfKeys << " keys was successful.");

	return true;
}

template<typename PointNT> void
CMPHmodelHashTable<PointNT>::destroyMinimalPerfectHashFunction(){
	// destroying the CMPH-CHD hash function
	cmph_destroy(hashFunctionData.hash);                              /** Hash API **/               // delete the pointer to the struct containing the hash querying algorithm data, (and probably deleting the struct instance)
	cmph_io_vector_adapter_destroy(hashFunctionData.source);          /** Adapter pattern API **/
	fclose(hashFunctionData.mphf_fd);
	ROS_INFO_STREAM("[CMPHmodelHashTable::destroyMinimalPerfectHashFunction] Destroying the CMPH struct");
}


template <typename PointNT> uint32_t
CMPHmodelHashTable<PointNT>::getHashValue(typename MinimalDiscretePF<PointNT>::featureHashKeyT hashKey) const{

	std::string key = std::to_string(hashKey);
	char *keyPtr = new char[key.length() + 1]; // +1 because an array of char always terminates with an additional "\0"
	std::strcpy(keyPtr, key.c_str());          // use the c_str() function, which gives you an array of char
	uint32_t hashValue = cmph_search(hashFunctionData.hash, keyPtr, (cmph_uint32)strlen(keyPtr)); /** Hash API **/ // calculating the hash-value from the key;

	ROS_DEBUG_STREAM("[CMPHmodelHashTable::getHashValue] the quantized feature looks like this (hash key): " << hashKey );
	ROS_DEBUG_STREAM("[CMPHmodelHashTable::getHashValue] the associated hash value is " << hashValue );

	delete[]keyPtr; // valgrind says that delete(keyPtr) is erroneous / 'mismatched'
	return hashValue;
}


template <typename PointNT> void
CMPHmodelHashTable<PointNT>::storeAllModelFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
												   MinimalDiscretePF<PointNT> &modelFeature,
												   std::vector<float> &targetPointNormalSigns,
												   const unsigned int stepSize){

	// loop over all reference points
	for(unsigned int i_r = 0; i_r < this->refPointCloudPtr->points.size(); i_r += stepSize){

		// get Eigen-style point information of reference point
		Eigen::Vector3f m_r = this->refPointCloudPtr->points[i_r].getVector3fMap();
		Eigen::Vector3f n_r = this->refPointCloudPtr->points[i_r].getNormalVector3fMap();

		// pre-calculate transform Tm->g (align m_r and n_r to origin / x-axis)
		Eigen::Affine3f Tmg = aligningTransform(m_r, n_r);

		for(unsigned int i_t = 0; i_t < targetCloudPtr->points.size(); i_t++){

			// get Eigen-style point information of target point
			Eigen::Vector3f m_t = targetCloudPtr->points[i_t].getVector3fMap();
			Eigen::Vector3f n_t = targetCloudPtr->points[i_t].getNormalVector3fMap();

			// no feature of points at same location (will lead to d = 0 and therefore an
			// undefined feature vector)
			if(m_r != m_t){

				// calculate feature for all normal directions to consider and add
				for(float& targetPointNormalSign: targetPointNormalSigns){

					if(!modelFeature.calculate(m_r, n_r,
											   m_t, targetPointNormalSign * n_t,
											   Tmg)){
						continue; // overflow during quantization occurred, skip invalid model-PF
					}

					if (!this->useNeighbourPPFsForTraining){ //!< MFZ 13.03.2018 include IF-statement, to check if neighbours should be calculated or not, because calculateNeighbourFeature() also calculates the original feature!
						typename MinimalDiscretePF<PointNT>::featureHashKeyT hashKey = modelFeature.getDiscreteFeatureAsInt();
						uint32_t bin = getHashValue(hashKey);
 						try {
							// TODO: here I need to place the PPF-key at .first and the ModelPFinfo struct at .second
							// for that I need to check if a pair already exists and if not make one.
							// but what about and how expanding the vector in .second?
							if (minimalHashTable[bin].second.empty()){
								minimalHashTable[bin].first = hashKey;
								minimalHashTable[bin].second.push_back(ModelPFinfo(modelFeature.alpha,i_r));
							}
							else{
								minimalHashTable[bin].second.push_back(ModelPFinfo(modelFeature.alpha,i_r));
							}
						}
						catch (const std::out_of_range& oor){
						    ROS_ERROR_STREAM("[CMPHmodelHashTable::storeAllModelFeatures] Requested Key leads to faulty hashvalue / bin! Error: " << oor.what() );
						}
					}
					else{
						bool featureIsValid = false;
						for (int i = 0; i < 3; i++){
							for (int j = 0; j < 3; j++){
								for (int k = 0; k < 3; k++){
									for (int l = 0; l < 3; l++){
										featureIsValid = modelFeature.calculateNeighbourFeature(i,j,k,l);
										if (featureIsValid){ // distance d > 0 and no overflow occurred
											typename MinimalDiscretePF<PointNT>::featureHashKeyT hashKey = modelFeature.getDiscreteNeighbourFeatureAsInt();
											uint32_t bin = getHashValue(hashKey);

											try {
												// TODO: here I need to place the PPF-key at .first and the ModelPFinfo struct at .second
												// for that I need to check if a pair already exists and if not make one.
												if (minimalHashTable[bin].second.empty()){
													minimalHashTable[bin].first = hashKey;
													minimalHashTable[bin].second.push_back(ModelPFinfo(modelFeature.alpha,i_r));
												}
												else{
													if(minimalHashTable[bin].first != hashKey){
														ROS_ERROR_STREAM("[minimalVisibilityContextFeatureHashTable::storeAllModelFeatures] key in hashtable: " << minimalHashTable[bin].first << " and key of current feature " << hashKey );
														ROS_ERROR_STREAM("[minimalVisibilityContextFeatureHashTable::storeAllModelFeatures] these keys should match but they don't. i.e. two keys got the same hash value. i.e. the hash function is not perfect!");
																		}
													else{ // keys match. OK.
														minimalHashTable[bin].second.push_back(ModelPFinfo(modelFeature.alpha,i_r));
													}
												}
											}
											catch (const std::out_of_range& oor){
												ROS_ERROR_STREAM("[minimalVisibilityContextFeatureHashTable::storeAllModelFeatures] Requested Key not part of Model! Error: " << oor.what() );
											}
										}
									} // l
								} // k
							} // j
						} // i
					} // else use neighbour ppfs
				} // targetPointsNormalSigns
			} // if (m_r != m_t)
		} // target points
	} // reference points

	// now loop over the whole table again, trying to shrink each table-row to its actual size
	for (uint64_t row = 0 ; row < minimalHashTable.size(); row ++){
		for (uint64_t column = 0; column < minimalHashTable[row].second.size(); column ++){
			minimalHashTable[row].second.shrink_to_fit();
		}
	}
	ROS_INFO_STREAM("[CMPHmodelHashTable::storeAllModelFeatures] minimal hash table shrunk to its true size.");
}


template <typename PointNT> std::vector<unsigned int>
CMPHmodelHashTable<PointNT>::getHistogram(){

    // get all existing keys (unique features)
    // minimalHashTable already has only as many rows as unique features.

    // get the maximum number of identical features
    int max = 0;
    for(auto& key: minimalHashTable){ // each row equals a unique feature
        unsigned int cnt = key.second.size();
        if(max < cnt){
            max = cnt;
        }
    }

    // make space for the whole histogram
    std::vector<unsigned int> histogram;
    histogram.resize(max + 1, 0); // initialize all with 0

    // count features with i elements
    for(auto& key: minimalHashTable){
        histogram[key.second.size()]++;
    }

    return histogram;
}


template <typename PointNT> bool
CMPHmodelHashTable<PointNT>::setParameters(PFmodel<PointNT>& model){

    // all parameters were already set in modelHashTable::setParameters()
    return true;
}


template <typename PointNT> bool
CMPHmodelHashTable<PointNT>::voteWithFeature(MinimalDiscretePF<PointNT>* sceneFeaturePtr,
											 HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
											 flagArray<PointNT>* refPointFlagArrayPtr){

	// look-up PF in minimalHashtTable
	typename VisibilityContextDiscretePF<PointNT>::featureHashKeyT keyOfScenePF = sceneFeaturePtr->getDiscreteFeatureAsInt();

#ifdef write_scene_PPfs_hashKeys_to_binary_file
	//TODO: remove this after debugging:
	#pragma omp critical (storeScenePF)
	{
	this->sceneHashKeys.push_back(keyOfScenePF);
	}
#endif

	uint32_t hashValue = getHashValue(keyOfScenePF);
	int numberOfSimilarFeatures = 0;
	try {
		// The CMPH function also calculates valid hashValues ( = bin of HashTable ) for PPFs that are not model PPFs.
		// therefore the model-PPF-hashKey is stored in the hash table as well, to enable a comparison here:
		typename MinimalDiscretePF<PointNT>::featureHashKeyT keyOfModelPFatBin;
		keyOfModelPFatBin = minimalHashTable[hashValue].first;

		if (keyOfModelPFatBin == keyOfScenePF){
			// PF is part of model, get number of all similar features
			numberOfSimilarFeatures = minimalHashTable[hashValue].second.size();
			//ROS_ERROR_STREAM("[PFmatcher::voteWithTargetPoints] Requested Key is part of Model! bin: " << bin << " and it is found " << numberOfSimilarFeatures << " times" );

			if (numberOfSimilarFeatures == 0){
				// error
				// collapsed CMPH hash tables may not contain empty rows.
				ROS_ERROR_STREAM("[CMPHmodelHashTable::voteWithFeature] Requested Key found in model-hash-table BUT second is empty! bin: " << hashValue << " and it is found " << numberOfSimilarFeatures << " times" );
#ifdef write_counters_to_file
				// TODO: remove after debugging
				#pragma omp atomic update
				this->numberOfFeaturesNotFoundInHT ++;
#endif
				return false;
			}
		}
		else{
			// PF is not part of model
			//ROS_WARN_STREAM("[PFmatcher::voteWithTargetPoints] Found scene PF is not part of Model." );
#ifdef write_counters_to_file
			// TODO: remove after debugging
			#pragma omp atomic update
			this->numberOfFeaturesNotFoundInHT ++;
#endif
			return false;
		}
	}
	catch (const std::out_of_range& oor){
		// error
//		ROS_ERROR_STREAM("[CMPHmodelHashTable::voteWithFeature] Calculated Hash Value for requested Hash Key points to a slot outside the Hash Table! This should not happen! Error: " << oor.what() );
		// TODO: remove after debugging
#ifdef write_counters_to_file
		#pragma omp atomic update
		this->CMPHoutOfRangeCounter ++;
#endif
#ifdef write_cmph_hash_keys_out_of_range_to_file
		#pragma omp critical (storeOutOfRangeKey)
		{
		this->HashKeysOutOfRange.push_back(keyOfScenePF);
		this->HashValuesOutOfRange.push_back(hashValue);
		}
#endif
		return false;
	}

#ifdef write_counters_to_file
	// TODO: remove after debugging
	#pragma omp atomic update
	this->numberOfFeaturesFoundInHT ++;
#endif

	bool voteForAlphaSAllowed = true;
	bool voteForAlphaSbiggerNeighbourAllowed = true;
	bool voteForAlphaSsmallerNeighbourAllowed = true;

	// get alpha_scene
	float alpha_s = sceneFeaturePtr->alpha;

	// if flag array shall be used:
	if (this->faHashTableType != FA_NONE){
		// check flagArray if this PF with this pose may vote or not
		// TODO: checkFlagAtPosition currently takes sceneFeature AND hashvalue as input.
		//       this allows switching smoothly between OPH, CMPH and STL flag array version:
		// - OPH needs feature,
		// - STL needs feature(--> for accessing HashKey),
		// - CMPH needs hashValue.

		// quantize alpha_scene for flag array
		unsigned int alpha_s_index = this->getAlphaSceneIndex(alpha_s);

#ifdef debug_show_alpha_indices
	// TODO: remove after debugging
	std::cout << "alpha_s = " << alpha_s << " alpha_s_index = " << alpha_s_index << std::endl;
#endif

		#ifdef _OPENMP  // if openMP is available
		if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_index, sceneFeaturePtr, hashValue, false, omp_get_thread_num()) ){
		#else
		if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_index, sceneFeaturePtr, hashValue, false, 0) ){
		#endif
			// PF may not vote! There has already been a vote for this PF with this pose alpha_s.
			// std::cout << "[PFmatcher::voteWithTargetPoints] vote not allowed." << std::endl;
			voteForAlphaSAllowed = false;
#ifdef write_counters_to_file
			// TODO: remove after debugging
			#pragma omp atomic update
			this->FAdiscardsVoteCounter ++;
#endif
			if (!this->voteForAdjacentRotationAngles){
				return false;
			}
		}
		else{
			// PF may vote
			voteForAlphaSAllowed = true;
		}

		if(this->voteForAdjacentRotationAngles) {

			// checkFlagAtPosition() for both to alpha_s adjacent rotation angles.
			// bigger neighbour
			unsigned int alpha_s_NeighbourIndex = this->incrementAlphaSceneIndex(alpha_s);

#ifdef debug_show_alpha_indices
			// TODO: remove after debugging
			std::cout << "alpha_s = " << alpha_s << " alpha_s_index = " << alpha_s_index << " alpha_s_NeighbourIndex = " << alpha_s_NeighbourIndex << std::endl;
#endif

			#ifdef _OPENMP  // if openMP is available
			if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_NeighbourIndex, sceneFeaturePtr, hashValue, false, omp_get_thread_num()) ){
			#else
			if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_NeighbourIndex, sceneFeaturePtr, hashValue, false, 0) ){ //TODO
			#endif
				// PF may not vote! There has already been a vote for this PF with this pose alpha_s.
				voteForAlphaSbiggerNeighbourAllowed = false;
#ifdef write_counters_to_file
				// TODO: remove after debugging
				#pragma omp atomic update
				this->FAdiscardsAdjacentVoteCounter ++;
#endif
			}
			else {
				// PF may vote
				voteForAlphaSbiggerNeighbourAllowed = true;
			}

			// smaller neighbour
			alpha_s_NeighbourIndex = this->decrementAlphaSceneIndex(alpha_s);

#ifdef debug_show_alpha_indices
			// TODO: remove after debugging
			std::cout << "alpha_s = " << alpha_s << " alpha_s_index = " << alpha_s_index << " alpha_s_NeighbourIndex = " << alpha_s_NeighbourIndex << std::endl;
#endif

			#ifdef _OPENMP  // if openMP is available
			if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_NeighbourIndex, sceneFeaturePtr, hashValue, false, omp_get_thread_num()) ){
			#else
			if ( !(refPointFlagArrayPtr->*refPointFlagArrayPtr->checkFlagAtPosition)(alpha_s_NeighbourIndex, sceneFeaturePtr, hashValue, false, 0) ){ //TODO
			#endif
				// PF may not vote! There has already been a vote for this PF with this pose alpha_s.
				voteForAlphaSsmallerNeighbourAllowed = false;
#ifdef write_counters_to_file
				// TODO: remove after debugging
				#pragma omp atomic update
				this->FAdiscardsAdjacentVoteCounter ++;
#endif
			}
			else {
				// PF may vote
				voteForAlphaSsmallerNeighbourAllowed = true;
			}
		} // voteForAdjacentRotationAngles
	} // use Flag Array

	// go through all similar features in the hash table and vote
	for (int feature = 0; feature < numberOfSimilarFeatures; feature ++){

		// get alpha_model from model hash table
		float alpha_m =  minimalHashTable[hashValue].second[feature].alpha;

		// vote, if allowed by flag array. or vote if flag array is disabled
		if(voteForAlphaSAllowed){

			// calculate total alignment angle as alpha = alpha_model - alpha_scene
			float alpha = alpha_m - alpha_s;

#ifdef write_alphas_to_binary_file
			//TODO: remove this after debugging:
			#pragma omp critical (storeAlphas)
			{
			this->alphas.push_back(alpha);
			this->alphas_s.push_back(alpha_s);
			this->alphas_m.push_back(alpha_m);
			}
#endif

			// ensure that alpha is in [-pi, pi) after subtraction
			// (will be of by max. one period)
			if(alpha < -M_PI){
				alpha += 2 * M_PI;
			}
			else if(alpha >= M_PI){
				alpha -= 2 * M_PI;
			}

			// calculate alpha index in hough-voting space
			unsigned int alphaIndex = (alpha + M_PI) / this->d_alpha; // floor rounding
#ifdef debug_show_alpha_indices
			// TODO: remove after debugging
			std::cout << "alpha = " << alpha << " alphaIndex = " << alphaIndex << std::endl;
#endif

#ifdef write_alphas_to_binary_file
			//TODO: remove this after debugging:
			#pragma omp critical (storeAlphas)
			{
			this->alphaIndices.push_back(alphaIndex);
			}
#endif

			// vote
			this->voteInHoughSpaces(hashValue, feature, accumulatorArray, alphaArray, alphaIndex, alpha);
#ifdef write_counters_to_file
			#pragma omp atomic update
			this->originalVoteCounter ++;
#endif
		}

		// MFZ 27/03/2018 vote for neighboured alphas!
		if (this->voteForAdjacentRotationAngles){
			if(voteForAlphaSbiggerNeighbourAllowed){

				// bigger alpha_s neighbour (which in turn is the smaller neighbour of alpha, because alpha = alpha_m - (alpha_s + d_alpha) )
				std::pair<float, unsigned int> alphaNeighbour = this->incrementAlpha(alpha_m, alpha_s);
				this->voteInHoughSpaces(hashValue, feature, accumulatorArray, alphaArray, alphaNeighbour.second, alphaNeighbour.first);
#ifdef write_counters_to_file
				#pragma omp atomic update
				this->biggerVoteCounter ++;
#endif

#ifdef debug_show_alpha_indices
				// TODO: remove after debugging
				std::cout << " alphaNeighbour_Index (bigger) = " << alphaNeighbour.second << " alphaNeighbour (bigger)= " << alphaNeighbour.first << std::endl;
#endif
			}
			if(voteForAlphaSsmallerNeighbourAllowed){
				// smaller alpha_s neighbour (which in turn is the bigger neighbour of alpha, because alpha = alpha_m - (alpha_s - d_alpha) )
				std::pair<float, unsigned int> alphaNeighbour = this->decrementAlpha(alpha_m, alpha_s);
				this->voteInHoughSpaces(hashValue, feature, accumulatorArray, alphaArray, alphaNeighbour.second, alphaNeighbour.first);
#ifdef write_counters_to_file
				#pragma omp atomic update
				this->smallerVoteCounter ++;
#endif

#ifdef debug_show_alpha_indices
				// TODO: remove after debugging
				std::cout << " alphaNeighbour_Index (bigger) = " << alphaNeighbour.second << " alphaNeighbour (bigger)= " << alphaNeighbour.first << std::endl;
#endif
			}
		} // voteForAdjacentRotationAngles
	} // loop through all similar features

	return true;
}


template <typename PointNT> bool
CMPHmodelHashTable<PointNT>::collapseHashTable(std::vector<int>& equivalencies, bool deleteUniquePoints, unsigned int nPoints){


	// remove unused features from hash table and weight remaining ones
	auto rowIt = minimalHashTable.begin();
	while(rowIt != minimalHashTable.end()){

		auto columnIt = rowIt->second.begin();
		while (columnIt != rowIt->second.end()){
			int nEquivalences = equivalencies[columnIt->refPointIndex];
			bool eraseEntry;
			if(deleteUniquePoints){
				eraseEntry = (nEquivalences <= 0);
			}
			else{
				eraseEntry = (nEquivalences < 0);
			}
			if(eraseEntry){
				// ref point was replaced by another equivalent one, feature can be deleted
				rowIt->second.erase(columnIt);
			}
			else{
				// ref point is unique or has equivalences, feature gets higher weight
				columnIt->weight *= nEquivalences + 1; // +1 since original point
													   // also counts
				columnIt++;
			}
		}
		// if all entries of the PF were erased
		if (rowIt->second.empty()){
			// delete PF
//			this->minimalHashTable.erase(rowIt); // don't do this (explanation see below)
			rowIt++; // carry on instead
		}
		else{
			rowIt++;
		}
	}

	// TODO: 05.06.2018
	// if one deleted the rows that become empty, one would have to learn the CMHP_CHD hash function once again.
	// but then the whole hashtable becomes useless and needs to be build from scratch again.
	// all too complicated. therefore I choose the simple way and leave the empty rows, which allows us to
	// continue using the existing minimal hash table (which is now strictly speaking not minimal anymore)
	// and the existing minimal perfect hash function (also not minimal anymore)

	// find correspondence between old and new ref point indices:
	// indexCorrespondence[i] holds the new index for ref point with old index i
	// entry of -1 means the point does not exist anymore
	std::vector<int> newIndices;
	newIndices.resize(nPoints, -1);
	int currentIndex = 0;
	for(unsigned int i = 0; i < equivalencies.size(); i++){
		bool pointStillExists;
		if(deleteUniquePoints){
			pointStillExists = (equivalencies[i] > 0);
		}
		else{
			pointStillExists = (equivalencies[i] >= 0);
		}
		if(pointStillExists){
			newIndices[i] = currentIndex;
			currentIndex++;
		}
	}

	// change ref point indices in hash table
	for (auto rowIt = minimalHashTable.begin(); rowIt != minimalHashTable.end(); rowIt++){
		for (auto columnIt = rowIt->second.begin(); columnIt != rowIt->second.end(); columnIt++){
			columnIt->refPointIndex = newIndices[columnIt->refPointIndex];
		}
	}


	// attempt to build new reduced hash table:
	ROS_WARN_STREAM("[CMPHmodelHashTable::collapseHashTable] Attempting to create new minimal perfect hash function for collapsed model.");
	// 0. get all remaining PPFs
	if (!(hashTableKeys.empty())){
		ROS_ERROR_STREAM("[CMPHmodelHashTable::collapseHashTable] Error: Vector of hash table keys (hashTableKeys) is not empty. Aborting, keeping old hash function.");
		return false;
	}

	for (auto rowIt = minimalHashTable.begin(); rowIt != minimalHashTable.end(); rowIt ++){
		if (!(rowIt->second.empty())){
			this->hashTableKeys.push_back(rowIt->first);
//			this->hashTableKeys.push_back((std::to_string(rowIt->first)).c_str());
		}
	}
	std::size_t numberOfPPFs = hashTableKeys.size();

	// 1. delete old hashFunction
	this->destroyMinimalPerfectHashFunction();

	// 2. create new hashFunction --> using hashTableKeys above, we can call the construct function again
	if (!(this->constructMinimalPerfectHashFunction("hashFunction_of_collapsed_" + this->refPointCloudPtr->header.frame_id))){
		ROS_ERROR_STREAM("[CMPHmodelHashTable::collapseHashTable] Error: Could not generate new reduced minimal perfect hash function. Now, no hash function is existing at all.");
		return false;
	}

	// 3. temporary storage for new hash Table.
	std::vector<std::pair<typename MinimalDiscretePF<PointNT>::featureHashKeyT, std::vector<ModelPFinfo> > > collapsedMinimalHashTable; //!< hash table with minimal memory usage

	// 4. loop through old hash table and store the old, non-empty rows in the new rows indicated by hashValue of new hashFunction
	collapsedMinimalHashTable.resize(numberOfPPFs);
	for (auto rowIt = minimalHashTable.rbegin(); rowIt != minimalHashTable.rend(); rowIt ++){
		if ( !(rowIt->second.empty()) ){
			uint32_t newRowIdx = this->getHashValue(rowIt->first);
			if ( !(collapsedMinimalHashTable[newRowIdx].second.empty()) ){
				ROS_ERROR_STREAM("[CMPHmodelHashTable::collapseHashTable] Error: trying to overwrite an already filled row: " << newRowIdx);
				pause("collapseHashTable", "index not unique", "wait for enter to continue");
				continue;
			}
			collapsedMinimalHashTable[newRowIdx] = std::make_pair(rowIt->first, rowIt->second);//*rowIt;
			if (newRowIdx >= numberOfPPFs){
				ROS_ERROR_STREAM("[CMPHmodelHashTable::collapseHashTable] Error: Index is too big: "<< newRowIdx);
				pause("collapseHashTable", "index too big", "wait for enter to continue");
			}
		}
		this->minimalHashTable.erase((rowIt+1).base()); // reverse iterating through vector allows us to delete the last element, which should be faster then deleting the first.
														// erasing with reverse iterators see: https://stackoverflow.com/q/1853358
	}

	// 5. overwrite old hashTable with new reduced hash table
	this->minimalHashTable.clear();
	this->minimalHashTable.shrink_to_fit();
	this->minimalHashTable = collapsedMinimalHashTable;
	this->numberOfUniqueModelPPFs = this->minimalHashTable.size(); // update member

	return true;
}


template <typename PointNT> uint64_t
CMPHmodelHashTable<PointNT>::getNumberOfUniqueModelPFs(){
	return this->numberOfUniqueModelPPFs;
}


template <typename PointNT> uint64_t
CMPHmodelHashTable<PointNT>::getNumberOfModelPFs(){
	return this->numberOfModelPPFs;
}


template <typename PointNT> void
CMPHmodelHashTable<PointNT>::eraseKeys(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys){
	uint32_t hashValue = 0;
	for (auto const & key: keys){
		hashValue = getHashValue(key);
		// instead of deleting the whole row, we just delete all columns/entries of the row
		// that way we don't need to calculate a whole new hash function
		minimalHashTable[hashValue].second.clear();
	}
}

template <typename PointNT> void
CMPHmodelHashTable<PointNT>::getHighFrequencyPFs(std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>& keys, unsigned int minEntries){

	typename MinimalDiscretePF<PointNT>::featureHashKeyT key;
	for(auto& it : minimalHashTable){
		key = it.first;
		if(it.second.size() >= minEntries){
			keys.insert(key);
		}
	}
}

template <typename PointNT> void
CMPHmodelHashTable<PointNT>::weightByDistance(float power){

	for(auto& featureInfo: minimalHashTable){

		// get distance of feature
		MinimalDiscretePF<PointNT> tmpFeature(featureInfo.first, 0); // alpha = 0 is arbitrary
		float d = tmpFeature.discretePF[0] * this->d_dist_abs;

		// weight according to distance of points powf(n, -1 * power);
		for (auto& entry : featureInfo.second){
			entry.weight *= powf(d / this->modelDiameter, power);
		}
	}
}

template <typename PointNT> void
CMPHmodelHashTable<PointNT>::discardByDistance(float distThresh){
	auto featureIt = minimalHashTable.begin();
	while(featureIt != minimalHashTable.end()){

		// get distance of feature
		MinimalDiscretePF<PointNT> tmpFeature(featureIt->first, 0); // alpha = 0 is arbitrary
		float d = tmpFeature.discretePF[0] * this->d_dist_abs;

		// erase entries of feature if distance is small
		// do not erase feature itself, since that would require an update of the CMPH_CHD hash function
		if(d <= distThresh){
			featureIt->second.clear();
		}
	}
}

template <typename PointNT> void
CMPHmodelHashTable<PointNT>::weightByFrequency(float power){
	// weight each entry separately
	float n = 0;
	for(auto& feature : minimalHashTable){
		n = feature.second.size();
		for (auto& entry : feature.second){
			entry.weight *= powf(n, -1 * power);
		}
	}
}

template <typename PointNT> void
CMPHmodelHashTable<PointNT>::weightByValue(float weight){
	for(auto& feature: minimalHashTable){
		for (auto& entry : feature.second){
			entry.weight *= weight;
		}
	}
}

template <typename PointNT> void
CMPHmodelHashTable<PointNT>::voteInHoughSpaces(uint32_t& hashValue, int& feature, HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray, unsigned int alphaIndex, float alpha){

	accumulatorArray.vote(minimalHashTable[hashValue].second[feature].weight,        	// weight of feature
						  minimalHashTable[hashValue].second[feature].refPointIndex,
						  alphaIndex);
	alphaArray.vote(alpha * minimalHashTable[hashValue].second[feature].weight, 		// weighted alpha
					minimalHashTable[hashValue].second[feature].refPointIndex,
					alphaIndex);
}

// ========================================================================================================
// ===== hash table class with CMPH-CHD minimal perfect hash function for visibility context matching =====
// ========================================================================================================

/**
 * @class CMPHvisibilityContextModelHashTable
 * @brief In memory-usage minimized hash table with minimal-perfect-hash-function for storing information about calculated S2S features and their Visibility Context.
 *
 * The hash table key contains the quantized S2S information and its visibility-context. It has 64 bit.
 * The hash value is calculated from the hash key with the CHPH-CHD minimal perfect hash algorithm.
 * Therefore the hash value maps from the key directly to the index in the hash table, where the PPF-struct is stored.
 * Compared to the std::unordered_multimap, a bucket-structure is not needed and therefore the hash table should be of minimal size.
 */
template <typename PointNT>
CMPHvisibilityContextModelHashTable<PointNT>::CMPHvisibilityContextModelHashTable(featureType fType_,
																				  PFmodel<PointNT>& model):
modelHashTable<PointNT>(model),
CMPHmodelHashTable<PointNT>(fType_, model),
visibilityContextModelHashTable<PointNT>(model){
	this->fType = fType_;
	this->htType = 4;
	setParameters(model);
	ROS_INFO_STREAM("[CMPHvisibilityContextModelHashTable::CMPHvisibilityContextModelHashTable] CMPHvisibilityContextModelHashTable object constructed. htType = "<< this->htType);
}

template <typename PointNT> bool
CMPHvisibilityContextModelHashTable<PointNT>::constructFeatures( const typename pcl::PointCloud<PointNT>::ConstPtr& refCloudPtrIn,
																 const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
																 const bool refPointDirectionAmbiguous,
																 const bool targetPointDirectionAmbiguous,
																 const unsigned int stepSize){

	// check that clouds are not empty
    if(refCloudPtrIn->points.size() == 0 || targetCloudPtr->points.size() == 0){
        PCL_WARN("[CMPHVisibilityContextFeatureHashTable::constructFeatures] at least one of the clouds is empty.\n");
        return false;
    }

    // save reference cloud or make new one
    if(refPointDirectionAmbiguous){

        // make new reference point cloud with positive and negative reference point
        // orientation
        typename pcl::PointCloud<PointNT>::Ptr tmpCloudPtr(new pcl::PointCloud<PointNT>);
        pcl::copyPointCloud(*refCloudPtrIn, *tmpCloudPtr);
        for(auto& p: refCloudPtrIn->points){
            tmpCloudPtr->push_back(p);
            tmpCloudPtr->back().getNormalVector3fMap() *= -1.0;
        }
        this->refPointCloudPtr = tmpCloudPtr;
    }
    else{
        this->refPointCloudPtr = refCloudPtrIn;
    }

    // erase all previous content
    this->hashTableKeys.clear();
    this->minimalHashTable.clear();

	// reserve memory
	this->hashTableKeys.reserve( refCloudPtrIn->points.size() * targetCloudPtr->points.size() );

    // figure out normal directions to consider
    std::vector<float> targetPointNormalSigns = {1.0};
    if(targetPointDirectionAmbiguous){
        targetPointNormalSigns.push_back(-1.0);
    }

    // add all features to hash table
    // i_r -> index of reference point
    // i_t -> index of target point
    VisibilityContextDiscretePF<PointNT> modelFeature(this); //!< calling constructor and set parameters

    // collect all PPFs that occur in the current model
    collectAllModelFeatures(targetCloudPtr, modelFeature, targetPointNormalSigns, stepSize);

	// shrink to fit actual size
	this->hashTableKeys.shrink_to_fit();

	// store number of all found model PPFs for statistics
	this->numberOfModelPPFs = this->hashTableKeys.size();
    ROS_INFO_STREAM("[CMPHVisibilityContextFeatureHashTable::constructFeatures] The number of found model PPFs were: "<< this->numberOfModelPPFs );

    // remove all PPF duplicates that where found in the model
	sort( this->hashTableKeys.begin(), this->hashTableKeys.end() );
	this->hashTableKeys.erase( unique( this->hashTableKeys.begin(), this->hashTableKeys.end() ), this->hashTableKeys.end() );
	this->hashTableKeys.shrink_to_fit();

	// store number of all unique model PFFs (not only for statistics)
	this->numberOfUniqueModelPPFs = this->hashTableKeys.size();
	ROS_INFO_STREAM("[CMPHVisibilityContextFeatureHashTable::constructFeatures] The number of found model PPFs were (without duplicates): "<< this->numberOfUniqueModelPPFs );

	this->minimalHashTable.resize(this->numberOfUniqueModelPPFs);
	// create CMPH-CHD hash function from the now known model PFs
	if (!this->constructMinimalPerfectHashFunction("hashFunction_of_" + this->refPointCloudPtr->header.frame_id)){
		return false;
	}

	// calculate all model PFs again and store them in the hash table
	storeAllModelFeatures(targetCloudPtr, modelFeature, targetPointNormalSigns, stepSize);

	ROS_INFO_STREAM("[CMPHVisibilityContextFeatureHashTable::constructFeatures] The CMPH-model hash table contains unique PPFs: "<< this->minimalHashTable.size() );

    return true;
}


template <typename PointNT> void
CMPHvisibilityContextModelHashTable<PointNT>::collectAllModelFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
																	  VisibilityContextDiscretePF<PointNT> &modelFeature,
																	  std::vector<float> &targetPointNormalSigns,
																	  const unsigned int stepSize){

	// create PCLVisualizer
	boost::shared_ptr<pcl::visualization::PCLVisualizer> fancy_viewer;
#ifdef visualize_VisCon_feature
	if (this->visualizeVisibilityContextFeature){
		// show pointcloud in visualizer
		fancy_viewer = visualization::visualizePointCloud<PointNT>(this->refPointCloudPtr);//, m_r, m_t, n_r, n_t);

		// show center points of all occupied voxels:
		std::vector<PointNT, typename Eigen::aligned_allocator<PointNT> > voxelCenters;
		this->octreeSearchPtr->getOccupiedVoxelCenters (voxelCenters);
		typename pcl::PointCloud<PointNT>::Ptr voxelCentersCloudPtr (new typename pcl::PointCloud<PointNT>);
		voxelCentersCloudPtr->points = voxelCenters;
		visualization::visualizeOctreeCenterpoints<PointNT>(fancy_viewer, voxelCentersCloudPtr);
	}
#else
	fancy_viewer = nullptr;
#endif

	if (this->useNeighbourPPFsForTraining){ // just an info message
		ROS_INFO_STREAM("[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] Generating neighbour PPFs for model training too." );
	}

	// loop over all reference points
	for(unsigned int i_r = 0; i_r < this->refPointCloudPtr->points.size(); i_r += stepSize){

		// get Eigen-style point information of reference point
		Eigen::Vector3f m_r = this->refPointCloudPtr->points[i_r].getVector3fMap();
		Eigen::Vector3f n_r = this->refPointCloudPtr->points[i_r].getNormalVector3fMap();

		// pre-calculate transform Tm->g (align m_r and n_r to origin / x-axis)
		Eigen::Affine3f Tmg = aligningTransform(m_r, n_r);

		for(unsigned int i_t = 0; i_t < targetCloudPtr->points.size(); i_t++){

			// get Eigen-style point information of target point
			Eigen::Vector3f m_t = targetCloudPtr->points[i_t].getVector3fMap();
			Eigen::Vector3f n_t = targetCloudPtr->points[i_t].getNormalVector3fMap();

			// no feature of points at same location (will lead to d = 0 and therefore an
			// undefined feature vector)
			if(m_r != m_t){

#ifdef visualize_VisCon_feature
				if (this->visualizeVisibilityContextFeature){

					// show PPF (+ VisCon) in visualizer
					visualization::visualizeVisConFeature<PointNT>(fancy_viewer, m_r, m_t, n_r, n_t);
					// show PCA-results in visualizer
					visualization::visualizePCA<PointNT>(fancy_viewer, this->modelPtr->modelCentroid, this->modelPtr->pcaEigenValues, this->modelPtr->pcaEigenVectors, this->modelPtr->d_max, this->modelPtr->d_med, this->modelPtr->d_min);
				}
#endif

				// calculate feature for all normal directions to consider and add
				for(float& targetPointNormalSign: targetPointNormalSigns){

					if (modelFeature.calculateForTraining(m_r, n_r,
													      m_t, targetPointNormalSign * n_t,
														  Tmg,
														  fancy_viewer) == false){  // MFZ 13.09.2017 inserted fancy-viewer parameter for visualization

						ROS_ERROR_STREAM("[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] calculated Model-PF (BELOW if printed) is invalid... this should not happen.");
#ifdef print_feature
						ROS_INFO_STREAM("[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] " << modelFeature);
#endif
						continue; // overflow during quantization occurred, skip invalid model-PF
					}
#ifdef print_feature
					ROS_INFO_STREAM("[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] " << modelFeature);
#endif

					// counter to find out which of the V_out-directions result in V_out = 0 the most
					for (int i = 0; i < 3; i++){
						if (modelFeature.discreteVC[i] == 0){
							this->countVoutZero[i].second ++;
						}
					}

					if (!this->useNeighbourPPFsForTraining){ //!< MFZ 13.03.2018 include IF-statement, to check if neighbours should be calculated or not, because calculateNeighbourFeature() also calculates the original feature!
//						this->hashTableKeys.push_back((std::to_string(modelFeature.getDiscreteFeatureAsInt())).c_str());
						this->hashTableKeys.push_back(modelFeature.getDiscreteFeatureAsInt());
					}
					else{
						bool featureIsValid = false;
						for (int i = 0; i < 3; i++){
							for (int j = 0; j < 3; j++){
								for (int k = 0; k < 3; k++){
									for (int l = 0; l < 3; l++){
										for (int m = 0; m < 3; m++){
											for (int n = 0; n < 3; n++){
												for (int o = 0; o < 3; o++){
													for (int p = 0; p < 3; p++){
														featureIsValid = modelFeature.calculateNeighbourFeature(i,j,k,l,m,n,o,p);
#ifdef print_feature
														std::cout << "[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] Quantized neighbour feature: ("<< unsigned(modelFeature.discretePFneighbour[0]) << ", "
																																									<< unsigned(modelFeature.discretePFneighbour[1]) << ", "
																																									<< unsigned(modelFeature.discretePFneighbour[2]) << ", "
																																									<< unsigned(modelFeature.discretePFneighbour[3]) << " | "
																																									<< unsigned(modelFeature.discreteVCneighbour[0]) << ", "
																																									<< unsigned(modelFeature.discreteVCneighbour[1]) << ", "
																																									<< unsigned(modelFeature.discreteVCneighbour[2]) << ", "
																																									<< unsigned(modelFeature.discreteVCneighbour[3]) << ")" << std::endl;
#endif
														if (featureIsValid){ // distance d > 0 and no overflow occurred
//															this->hashTableKeys.push_back((std::to_string(modelFeature.getDiscreteNeighbourFeatureAsInt())).c_str());
															this->hashTableKeys.push_back(modelFeature.getDiscreteNeighbourFeatureAsInt());

															// counter to find out which of the V_out-directions result in V_out = 0 the most
															for (int i = 0; i < 3; i++){
																if (modelFeature.discreteVC[i] == 0){
																	this->countVoutZero[i].second ++;
																}
															}
														}
#ifdef print_feature
														else{
															std::cout << "[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] neighbour (ABOVE if printed) is INVALID!" << std::endl;
														}
#endif
													} // p
												} // o
											} // n
										} // m
									} // l
								} // k
							} // j
						} // i
					} // else use neighbour PFs
				} // targetPointsNormalSigns

#ifdef visualize_VisCon_feature
				if (this->visualizeVisibilityContextFeature){
					// Visualizer loop (runs in a separate thread)
					while ((!fancy_viewer->wasStopped ())){ // needs to be stopped by user 'closing' the window, clicking x.
						fancy_viewer->spinOnce (100);
						boost::this_thread::sleep (boost::posix_time::microseconds (100000));
					}

					char input;
					std::cout << "\nWould you like to visualize the next visibility feature too? [y/n]\nNote: Choosing 'n' is permanent for this Hash-Table-type." << std::endl;
					std::cin >> input;

					while ((input != 'y') && (input !='n'))
					{
						std::cout << "Input unknown. Would you like to visualize the next visibility feature too? [y/n]" << std::endl;
						std::cin >> input;
					}
					if (input == 'y'){
						fancy_viewer->resetStoppedFlag();
						fancy_viewer->removeAllShapes();
					}
					else { // input has to be 'n'
						modelFeature.visualizeVisibilityContextFeature = false;
						this->visualizeVisibilityContextFeature = false;  // member of hash table
						ROS_INFO_STREAM("[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] Set visualizeVisibilityContextFeature to FALSE");
						fancy_viewer->removeAllShapes();
						fancy_viewer->removeAllPointClouds();
						fancy_viewer->close(); //VTK bug prevents window from being closed.
						//fancy_viewer.reset();
					}
				} // visualizeVisibilityContextFeature
#endif
			} // if (m_t != m_r)
		} // target points
	} // reference points

	// counter to find out which of the V_out-directions result in V_out = 0 the most
	ROS_INFO_STREAM("[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] counting V_out_x = 0 for this model: V_out_0 = "<< this->countVoutZero[0].second <<
																												      ", V_out_1 = "<< this->countVoutZero[1].second <<
																													  ", V_out_2 = "<< this->countVoutZero[2].second <<
																													  ", V_out_3 = "<< this->countVoutZero[3].second);
	std::sort(this->countVoutZero.begin(), this->countVoutZero.end(), [](const std::pair<int,double> &left, const std::pair<int,double> &right) {return left.second > right.second;});
	ROS_INFO_STREAM("[CMPHvisibilityContextModelHashTable::collectAllModelFeatures] new order of calculating the V_out: "<< this->countVoutZero[0].first << ", "
																													     << this->countVoutZero[1].first << ", "
																														 << this->countVoutZero[2].first << ", "
																														 << this->countVoutZero[3].first );
}


template <typename PointNT> void
CMPHvisibilityContextModelHashTable<PointNT>::storeAllModelFeatures(const typename pcl::PointCloud<PointNT>::ConstPtr& targetCloudPtr,
																		 VisibilityContextDiscretePF<PointNT> &modelFeature,
																		 std::vector<float> &targetPointNormalSigns,
																		 const unsigned int stepSize){

	// loop over all reference points
	for(unsigned int i_r = 0; i_r < this->refPointCloudPtr->points.size(); i_r += stepSize){

		// get Eigen-style point information of reference point
		Eigen::Vector3f m_r = this->refPointCloudPtr->points[i_r].getVector3fMap();
		Eigen::Vector3f n_r = this->refPointCloudPtr->points[i_r].getNormalVector3fMap();

		// pre-calculate transform Tm->g (align m_r and n_r to origin / x-axis)
		Eigen::Affine3f Tmg = aligningTransform(m_r, n_r);

		for(unsigned int i_t = 0; i_t < targetCloudPtr->points.size(); i_t++){

			// get Eigen-style point information of target point
			Eigen::Vector3f m_t = targetCloudPtr->points[i_t].getVector3fMap();
			Eigen::Vector3f n_t = targetCloudPtr->points[i_t].getNormalVector3fMap();

			// no feature of points at same location (will lead to d = 0 and therefore an
			// undefined feature vector)
			if(m_r != m_t){

				// calculate feature for all normal directions to consider and add
				for(float& targetPointNormalSign: targetPointNormalSigns){

					if(modelFeature.calculateForTraining(m_r, n_r,
										   	   	   	     m_t, targetPointNormalSign * n_t,
														 Tmg) == false){
						continue; // overflow during quantization occurred, skip invalid model-PF
					}

					if (!this->useNeighbourPPFsForTraining){ //!< check if neighbours should be calculated or not, because calculateNeighbourFeature() also calculates the original feature!
						typename VisibilityContextDiscretePF<PointNT>::featureHashKeyT hashKey = modelFeature.getDiscreteFeatureAsInt();
						uint32_t bin = this->getHashValue(hashKey);

 						try {
							// TODO: here I need to place the PPF-key at .first and the ModelPFinfo struct at .second
							// for that I need to check if a pair already exists and if not make one.
							if (this->minimalHashTable[bin].second.empty()){
								this->minimalHashTable[bin].first = hashKey;
								this->minimalHashTable[bin].second.push_back(ModelPFinfo(modelFeature.alpha,i_r));
							}
							else{
								this->minimalHashTable[bin].second.push_back(ModelPFinfo(modelFeature.alpha,i_r));
							}
						}
						catch (const std::out_of_range& oor){
						    ROS_ERROR_STREAM("[CMPHvisibilityContextModelHashTable::storeAllModelFeatures] Requested Key leads to faulty hashvalue / bin! Error: " << oor.what() );
						}
					}
					else{
						bool featureIsValid = false;
						for (int i = 0; i < 3; i++){
							for (int j = 0; j < 3; j++){
								for (int k = 0; k < 3; k++){
									for (int l = 0; l < 3; l++){
										for (int m = 0; m < 3; m++){
											for (int n = 0; n < 3; n++){
												for (int o = 0; o < 3; o++){
													for (int p = 0; p < 3; p++){
														featureIsValid = modelFeature.calculateNeighbourFeature(i,j,k,l,m,n,o,p);

														if (featureIsValid){ // distance d > 0 and no overflow occurred
															typename VisibilityContextDiscretePF<PointNT>::featureHashKeyT hashKey = modelFeature.getDiscreteNeighbourFeatureAsInt();
															uint32_t bin = this->getHashValue(hashKey);

															try {
																// TODO: here I need to place the PPF-key at .first and the ModelPFinfo struct at .second
																// for that I need to check if a pair already exists and if not make one.
																if (this->minimalHashTable[bin].second.empty()){
																	this->minimalHashTable[bin].first = hashKey;
																	this->minimalHashTable[bin].second.push_back(ModelPFinfo(modelFeature.alpha,i_r));
																}
																else{
																	if(this->minimalHashTable[bin].first != hashKey){
																		ROS_ERROR_STREAM("[CMPHvisibilityContextModelHashTable::storeAllModelFeatures] key in hashtable: " << this->minimalHashTable[bin].first << " and key of current feature " << hashKey );
																		ROS_ERROR_STREAM("[CMPHvisibilityContextModelHashTable::storeAllModelFeatures] these keys should match but they don't. i.e. two keys got the same hash value. i.e. the hash fuction is not perfect!");
																	}
																	else{ // keys match. OK.
																		this->minimalHashTable[bin].second.push_back(ModelPFinfo(modelFeature.alpha,i_r));
																	}
																}
															}
															catch (const std::out_of_range& oor){
																ROS_ERROR_STREAM("[CMPHvisibilityContextModelHashTable::storeAllModelFeatures] Requested Key not part of Model! Error: " << oor.what() );
															}
														}
													}
												}
											}
										}
									}
								}
							}
						} // i
					} // else
				} // targetPointsNormalSigns
			} // if (m_r != m_t)
		} // target points
	} // reference points

	// now loop over the whole table again, trying to shrink each table-row to its actual size
	for (uint64_t row = 0 ; row < this->minimalHashTable.size(); row ++){
		for (uint64_t column = 0; column < this->minimalHashTable[row].second.size(); column ++){
			this->minimalHashTable[row].second.shrink_to_fit();
		}
	}
	ROS_INFO_STREAM("[CMPHvisibilityContextModelHashTable::storeAllModelFeatures] minimal hash table shrunk to its true size.");
}


template <typename PointNT> bool
CMPHvisibilityContextModelHashTable<PointNT>::setParameters(PFmodel<PointNT>& model){

    // all parameters were already set in modelHashTable::setParameters() and visibilityContextModelHashTable::setParameters()
    return true;
}


template <typename PointNT> void
CMPHvisibilityContextModelHashTable<PointNT>::weightByDistance(float power){

	for(auto& featureInfo: this->minimalHashTable){

		// get distance of feature
		VisibilityContextDiscretePF<PointNT> tmpFeature(featureInfo.first, 0); // alpha = 0 is arbitrary
		float d = tmpFeature.discretePF[0] * this->d_dist_abs;

		// weight according to distance of points powf(n, -1 * power);
		for (auto& entry : featureInfo.second){
			entry.weight *= powf(d / this->modelDiameter, power);
		}
	}
}


template <typename PointNT> void
CMPHvisibilityContextModelHashTable<PointNT>::discardByDistance(float distThresh){
	auto featureIt = this->minimalHashTable.begin();
	while(featureIt != this->minimalHashTable.end()){

		// get distance of feature
		VisibilityContextDiscretePF<PointNT> tmpFeature(featureIt->first, 0); // alpha = 0 is arbitrary
		float d = tmpFeature.discretePF[0] * this->d_dist_abs;

		// erase entries of feature if distance is small
		// do not erase feature itself, since that would require an update of the CMPH_CHD hash function
		if(d <= distThresh){
			featureIt->second.clear();
		}
	}
}


} /* namespace pf_matching */

