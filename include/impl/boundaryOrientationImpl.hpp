/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

// PCL
#include <pcl/common/pca.h>

#include "../boundaryOrientation.hpp"


namespace pf_matching{
namespace extensions{

template<typename PointNT> void
BoundaryOrientation<PointNT>::orientPoints(typename BoundaryOrientation<PointNT>::CloudNT::Ptr& boundaryPointsPtr,
                                           typename BoundaryOrientation<PointNT>::KdtreeT::Ptr tree){
            
    // can't operate on empty input
    if(boundaryPointsPtr->size() == 0){
        PCL_ERROR("[BoundaryOrientation::orientPoints] Input cloud is empty.\n");
        return;
    }
    
    // make a kdTree if none was provided
    if(!tree){
        PCL_DEBUG("[BoundaryOrientation::orientPoints] No valid kdTree given, building a new one.\n");
        tree = typename KdtreeT::Ptr(new KdtreeT);
        tree->setInputCloud(boundaryPointsPtr);
    }
}
    
template<typename PointNT> void
BoundaryOrientation<PointNT>::orientSinglePoint(typename BoundaryOrientation<PointNT>::CloudNT::Ptr boundaryPointsPtr,
                                                typename BoundaryOrientation<PointNT>::KdtreeT::Ptr tree,
                                                int i_p){
            
    // find neighbours
    std::vector<int> neighbourIndices;
    std::vector<float> neighbourSqrDists;
    bool foundNeighbours = tree->radiusSearch(*boundaryPointsPtr, i_p, searchRadius,
                                              neighbourIndices, neighbourSqrDists);

    // can only align if there were neighbours, else NaN
    if(foundNeighbours){
        
        // PCA of neighbour locations relative to interest point
        Eigen::Matrix3f eigenvectors;
        Eigen::Vector3f eigenvalues;
        Eigen::Matrix3f covarianceMatrix;
        pcl::computeCovarianceMatrix(*boundaryPointsPtr,
                                     neighbourIndices,
                                     boundaryPointsPtr->points[i_p].getVector4fMap(),
                                     covarianceMatrix);
        pcl::eigen33(covarianceMatrix, eigenvectors, eigenvalues);
        
        // orient point in direction of eigenvector that corresponds to largest eigenvalue
        // (principal component), eigenvectors are ordered by ascending eingenvalue, 2 = last
        boundaryPointsPtr->points[i_p].getNormalVector3fMap() = eigenvectors.col(2);
    }
    else{
        boundaryPointsPtr->points[i_p].getNormalVector3fMap() = Eigen::Vector3f(NAN, NAN, NAN);
        boundaryPointsPtr->is_dense = false;
    }

}

} // end namespace
} // end namespace
