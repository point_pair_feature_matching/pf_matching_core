/*
 * pairFeaturesInline.hpp
 *
 *  Created on: Jun 1, 2018
 *      Author: Markus Franz Ziegler
 */

#ifndef PF_MATCHING_CORE_INCLUDE_IMPL_PAIRFEATURESINLINE_HPP_
#define PF_MATCHING_CORE_INCLUDE_IMPL_PAIRFEATURESINLINE_HPP_

#include <ros/console.h>

//#define print_methods_name

namespace pf_matching{

// ========================================================================================================
// ======================================== inline member functions =======================================
// ========================================================================================================



// *****************************************
// the following two methods are not correct
// *****************************************
//template <typename PointNT> inline bool
//MinimalDiscretePF<PointNT>::invalidAngle(const float& angle){
//
//	return angle >= std::numeric_limits<MinimalDiscretePF<PointNT>::featureEntryT>::max() ? true : false;  // TODO: 19.06.2018 changed to >= form the former ==. think "equals" is rubbish
//}

//template <typename PointNT> inline bool
//VisibilityContextDiscretePF<PointNT>::invalidAngle(const float& angle){
//
//	return angle >= std::numeric_limits<VisibilityContextDiscretePF<PointNT>::featureEntryT>::max() ? true : false;  // TODO: 19.06.2018 changed to >= form the former ==. think "equals" is rubbish
//}

template <typename PointNT> inline bool
MinimalDiscretePF<PointNT>::invalidAngle(const float& angle){

	if (angle > std::numeric_limits<MinimalDiscretePF<PointNT>::featureEntryT>::max()){
		return true;
	}
	else if ( (angle < 0) || (angle > (M_PI/this->d_angle))) { // an angle < 0 is impossible and angle > 180° is impossible too
		return true;
	}
	else {
		return false; //  angle is valid
	}
}

template <typename PointNT> inline bool
VisibilityContextDiscretePF<PointNT>::invalidAngle(const float& angle){

	if (angle > std::numeric_limits<VisibilityContextDiscretePF<PointNT>::featureEntryT>::max()){
		return true;
	}
	else if ( (angle < 0) || (angle > (M_PI/this->d_angle))) { // an angle < 0 is impossible and angle > 180° is impossible too
		return true;
	}
	else {
		return false; //  angle is valid
	}
}

// forward declarations
template <typename PointNT> void EigenVecToPCLPointT ( const Eigen::Vector3f& EigenVector,
	      	  	  	  	  	  	  	  	  	  	  	   PointNT& PointTRef);

template <typename PointNT> PointNT EigenVecToPCLPointT ( const Eigen::Vector3f& EigenVector );

template <typename PointNT> inline void
VisibilityContextDiscretePF<PointNT>::calculateIntersectedVoxelCenters( const Eigen::Vector3f& origin,
																		const Eigen::Vector3f& direction,
																		const typename pcl::PointCloud<PointNT>::Ptr& voxelCentersPtr,
																		const std::string& label) {
#ifdef print_methods_name
	ROS_INFO_STREAM("[VisibilityContextDiscretePF::calculateIntersectedVoxelCenters] calculate intersected voxel centers."); // TODO: MFZ DEBUG 14/09/17 hinterher wieder entfernen
#endif
	int count = octreeSearchPtr->getIntersectedVoxelCenters (origin, direction, voxelCentersPtr->points, 0);
	ROS_DEBUG_STREAM(label << ": Voxels intersected: "<< count );
}


template <typename PointNT> inline PointNT
VisibilityContextDiscretePF<PointNT>::getCentroidOfVoxelAsPLCPointT( PointNT& searchPoint){

	return EigenVecToPCLPointT<PointNT>( getCentroidOfVoxelAsEigenVec3f(searchPoint) );
}


template <typename PointNT> inline PointNT
VisibilityContextDiscretePF<PointNT>::getNormalOfCentroidAsPCLPointT ( PointNT& searchPoint){

	return EigenVecToPCLPointT<PointNT>( getNormalOfCentroidAsEigenVec3f(searchPoint) );
}


template <typename PointNT> inline std::pair<typename VisibilityContextDiscretePF<PointNT>::featureEntryT, bool>
VisibilityContextDiscretePF<PointNT>::checkOverflow(const float& value){

	// TODO: 19.06.2018 changed to >= form the former ==. think "equals" is rubbish
	float max = std::numeric_limits<featureEntryT>::max();
	return value >= std::numeric_limits<featureEntryT>::max() ? std::make_pair(std::numeric_limits<VisibilityContextDiscretePF<PointNT>::featureEntryT>::max(), true) :
																std::make_pair(static_cast<featureEntryT>(value), false);
}

} // end namespace pf_matching

#endif /* PF_MATCHING_CORE_INCLUDE_IMPL_PAIRFEATURESINLINE_HPP_ */
