/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

#include "../pcDownsampler.hpp"


// other
#include <random>

// PCL
#include <pcl/filters/filter.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/linear_least_squares_normal.h>
#include <pcl/common/io.h>
#define PCL_NO_PRECOMPILE // so that tempalted mls algorithm can be used
#include <pcl/surface/mls.h>
#undef PCL_NO_PRECOMPILE // not required for other algorithms

namespace pf_matching{
namespace pre_processing{

    
template <typename PointT, typename PointN> void
PCnormalEstimator::estimateNormals(typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                                   typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr,
                                   bool flipNormals,
                                   bool deepCopyForNONE ){
    
    // check input plausability
    if(cloudPtr == cloudNormalsPtr){
        PCL_WARN("[PCnormalEstimator::estimateNormals] Input and output pointers are the "\
                 "same. Calculation might fail.\n");
    }
    
    if(cloudPtr->points.empty()){
        cloudNormalsPtr->points.clear();
        PCL_ERROR("[PCnormalEstimator::estimateNormals] Input is empty, returning empty cloud.\n");
        return;
    }

    // estimate normals according to stored setting
    bool orientNormalsToViewpoint = false;
    switch(method){
        
        case NONE:
            PCL_DEBUG("[PCnormalEstimator::estimateNormals] normal estimation method "\
                      "set to 'NONE'.\n");
            
            // copy over contents, either deep or just pointer
            if(cloudPtr != cloudNormalsPtr){
                if(deepCopyForNONE){
                    pcl::copyPointCloud(*cloudPtr, *cloudNormalsPtr);
                }
                else{
                    cloudNormalsPtr = cloudPtr;
                }
            }

            break;
        
        case LINEAR_ORGANIZED:
        
            // note: can only use organized method if cloud is organized
            if(cloudPtr->height > 1){
                normalEstimationLinearOrganized<PointT, PointN>(cloudPtr, cloudNormalsPtr);
            }
            else{
                PCL_WARN("[PCnormalEstimator::estimateNormals] normal estimation set to "\
                     "'LINEAR_ORGANIZED' but input cloud is not organized; Falling back "\
                     "to method 'LINEAR'.\n");
                normalEstimationLinear<PointT, PointN>(cloudPtr, cloudNormalsPtr);
            }
            
            break; 
            
        case LLS_ORGANIZED:

            // note: can only use organized method if cloud is organized
            if(cloudPtr->height > 1){
                normalEstimationLLS<PointT, PointN>(cloudPtr, cloudNormalsPtr);
            }
            else{
                PCL_WARN("[PCnormalEstimator::estimateNormals] normal estimation set to "\
                     "'LLS_ORGANIZED' but input cloud is not organized; Falling back "\
                     "to method 'LINEAR'.\n");
                normalEstimationLinear<PointT, PointN>(cloudPtr, cloudNormalsPtr);
            }
            
            break;
            
        case LINEAR:

            normalEstimationLinear<PointT, PointN>(cloudPtr, cloudNormalsPtr);

            break;
        
        case MLS_TANGENT:
            
            normalEstimationMLS<PointT, PointN>(cloudPtr, cloudNormalsPtr, false, 1);
            orientNormalsToViewpoint = true;
            break;
            
        case MLS_POLY_2:
        
            normalEstimationMLS<PointT, PointN>(cloudPtr, cloudNormalsPtr, true, 2);
            orientNormalsToViewpoint = true;
            break;
        
        default:
            PCL_ERROR("[PCnormalEstimator::estimateNormals] set estimation method is unknown.\n");
            
            // copy over contents, either deep or just pointer
            if(cloudPtr != cloudNormalsPtr){
                if(deepCopyForNONE){
                    pcl::copyPointCloud(*cloudPtr, *cloudNormalsPtr);
                }
                else{
                    cloudNormalsPtr = cloudPtr;
                }
            }

    }
    
    // consistently orient normals to viewpoint if required
    if(orientNormalsToViewpoint){
        Eigen::Vector4f vp = cloudNormalsPtr->sensor_origin_;
        for(typename pcl::PointCloud<PointN>::iterator it = cloudNormalsPtr->begin();
            it!= cloudNormalsPtr->end(); it++){
                
            pcl::flipNormalTowardsViewpoint(*it, vp.x(), vp.y(), vp.z(),
                                             it->normal_x, it->normal_y, it->normal_z);
            }
    }
    
    // flip the direction of all normals
    // TODO: could be easily parallelized
    if(flipNormals){
        for(typename pcl::PointCloud<PointN>::iterator it = cloudNormalsPtr->begin();
            it!= cloudNormalsPtr->end(); it++){
            it->normal_x = -1 * it->normal_x;
            it->normal_y = -1 * it->normal_y;
            it->normal_z = -1 * it->normal_z;
            }
    }


}


template <typename PointT, typename PointN> void
PCnormalEstimator::normalEstimationLinear(const typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                                                typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr){

    // objects
    pcl::NormalEstimationOMP<PointT, PointN> ne;
    typename pcl::search::KdTree<PointT>::Ptr tree(new pcl::search::KdTree<PointT>());
    ne.setSearchMethod(tree);
    
    // params
    ne.setRadiusSearch(searchRadius);
    
    // I/O
    ne.setInputCloud(cloudPtr);
    pcl::copyPointCloud(*cloudPtr, *cloudNormalsPtr); // need to copy xyz-values over
    ne.compute(*cloudNormalsPtr);
}


template <typename PointT, typename PointN> void 
PCnormalEstimator::normalEstimationLinearOrganized(const typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                                                         typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr){

    // objects
    pcl::IntegralImageNormalEstimation<PointT, PointN> ne;
    
    // params
    /* default values set by constructor, seem to work
    ne.setNormalEstimationMethod(ne.AVERAGE_3D_GRADIENT);
    ne.setMaxDepthChangeFactor(0.02f);
    ne.setNormalSmoothingSize(10.0f);
    ne.setDepthDependentSmoothing(false);
    */

    // I/O
    ne.setInputCloud(cloudPtr);
    pcl::copyPointCloud(*cloudPtr, *cloudNormalsPtr); // need to copy xyz-values over
    ne.compute(*cloudNormalsPtr);
}


template <typename PointT, typename PointN> void
PCnormalEstimator::normalEstimationLLS(const typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                                             typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr){

    // objects
    pcl::LinearLeastSquaresNormalEstimation<PointT, PointN> ne;

    // params
    /* default values set by constructor seem to work
    ne.setMaxDepthChangeFactor(1.0f);
    ne.setNormalSmoothingSize(9.0f);
    ne.setDepthDependentSmoothing(false);
    */

    // I/O
    ne.setInputCloud(cloudPtr);
    pcl::copyPointCloud(*cloudPtr, *cloudNormalsPtr); // need to copy xyz-values over
    ne.compute(*cloudNormalsPtr);
}


template <typename PointT, typename PointN> void
PCnormalEstimator::normalEstimationMLS(typename pcl::PointCloud<PointT>::Ptr& cloudPtr, 
                                       typename pcl::PointCloud<PointN>::Ptr& cloudNormalsPtr,
                                       bool usePolynomial,
                                       unsigned int polynomialOrder){
                             
    //TODO: newer PCL versions include MLS versions that use OpenMP for parallelization

    // remove point
    if(!cloudPtr->is_dense){
        std::vector<int> tmp; // dummy vector
        pcl::removeNaNNormalsFromPointCloud(*cloudPtr, *cloudPtr, tmp);
        pcl::removeNaNFromPointCloud(*cloudPtr, *cloudPtr, tmp);
        cloudPtr->is_dense = true;
        PCL_DEBUG("[PCnormalEstimator::normalEstimationMLS] removed NaN from input cloud.\n");
    }

    // objects
    pcl::MovingLeastSquares<PointT, PointN> mls;
    typename pcl::search::KdTree<PointT>::Ptr tree(new pcl::search::KdTree<PointT>);
    mls.setSearchMethod(tree);
    
    // params
    mls.setComputeNormals (true);
    mls.setSearchRadius(searchRadius);
    
    // estimation method
    if(usePolynomial){
        mls.setPolynomialFit(true);
        mls.setPolynomialOrder(polynomialOrder);
    }
    else{
        mls.setPolynomialFit(false);
    }

    // I/O
    mls.setInputCloud(cloudPtr);
    mls.process(*cloudNormalsPtr);
    
    if(cloudNormalsPtr->points.size() == 0){
        PCL_WARN("[PCnormalEstimator::normalEstimationMLS] MLS normal estimation returned"\
                 " empty. Your search radius might be too small.\n");
    }
}

} // end namespace
} // end namespace

