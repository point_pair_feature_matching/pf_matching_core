/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

#include "../boundingSphere.hpp"
#include <float.h> // for FLT_MAX and FLT_MIN
#include <random>  // for std::mt19937 random number generator
#include <algorithm> // for std::shuffle and std::sort

namespace pf_matching{
namespace tools{
    
/////////////////// local static functions //////////////////////////////////////////////
    


/**
 * @brief Compute the squared R3 distance between a PCL point and a Eigen::Vector using the first 3 components.
 * @param p1 Eigen point
 * @param p2 PCL point
 * @return squared distance between the two points.
 */
template <typename PointT> static float squaredDistance(const Eigen::Vector4f& p1, const PointT& p2){
    float diff_x = p1[0] - p2.x;
    float diff_y = p1[1] - p2.y;
    float diff_z = p1[2] - p2.z;
    
    return diff_x*diff_x + diff_y*diff_y + diff_z*diff_z;
}


/**
 * @brief Find the indices of points in a point cloud that have the extreme (maximum and minimum)
 *        values along one of the coordinate axes.
 * 
 * @details This is equal to projecting the points onto the unit axis vectors and finding the points with
 * the extreme projection values.
 * Points having NaN or inf values in x y or z will not be considered.
 * 
 * @param[in] cloud point cloud
 * @param[out] extremaIndices index of the points with the extreme projections in the point cloud;
 *             order is (xmin, xmax, ymin ymax, zmin zmax), any previous data will be deleted
 */
 
 
template <typename PointT> inline void
findExtremeXYZindices(const pcl::PointCloud<PointT>& cloud, std::vector<int>& extremaIndices){
    
    // clear index storage
    extremaIndices.clear();

    // initialize storage for extreme values
    Eigen::Array4f coordsMin, coordsMax;
    coordsMin.setConstant(FLT_MAX);
    coordsMax.setConstant(-FLT_MAX);
    
    // get number of points
    int n = cloud.points.size();
    
    // for dense data, don't need to check for inf or NaN values
    if(cloud.is_dense){
    
        // initialize extrema with 0th point
        coordsMin[0] = coordsMax[0] = cloud.points[0].x;
        coordsMin[1] = coordsMax[1] = cloud.points[0].y;
        coordsMin[2] = coordsMax[2] = cloud.points[0].z;
        extremaIndices.resize(6, 0);
        
        // loop over all points, find the points that have the extreme min / max projection
        // on an axis unity vector
        // only allow a point to be the extreme point in one axis direction, this ensures
        // that speical cases like an axis-aligned clube still produce at least 4 extreme values
        for(size_t i = 1; i < n; i++){
            
            if( coordsMin[0] > cloud.points[i].x){ // compare projection to x-axis
                coordsMin[0] = cloud.points[i].x;
                extremaIndices[0] = i;
            }
            else if(coordsMax[0] < cloud.points[i].x){
                    coordsMax[0] = cloud.points[i].x;
                    extremaIndices[1] = i;
            }
            else if( coordsMin[1] > cloud.points[i].y){  // compare projection to x-axis
                coordsMin[1] = cloud.points[i].y;
                extremaIndices[2] = i;
            }
            else if(coordsMax[1] < cloud.points[i].y){
                    coordsMax[1] = cloud.points[i].y;
                    extremaIndices[3] = i;
            }
            else if( coordsMin[2] > cloud.points[i].z){  // compare projection to z-axis
                coordsMin[2] = cloud.points[i].z;
                extremaIndices[4] = i;
            }
            else if(coordsMax[2] < cloud.points[i].z){
                    coordsMax[2] = cloud.points[i].z;
                    extremaIndices[5] = i;
            }
            
        }
        
    }
    // non-dense data: need to check for inf and NaN
    else{
        // initialize extrema with first valid point
        size_t j;
        for(j = 0; j < n; j++){
            // skip points with inf or NaN
            if(!pcl_isfinite (cloud.points[j].x) || 
               !pcl_isfinite (cloud.points[j].y) || 
               !pcl_isfinite (cloud.points[j].z)){
                continue;
            }
            
            // initialize with the first point that is valid
            coordsMin[0] = coordsMax[0] = cloud.points[j].x;
            coordsMin[1] = coordsMax[1] = cloud.points[j].y;
            coordsMin[2] = coordsMax[2] = cloud.points[j].z;
            extremaIndices.resize(6, j);
            
            break; // we initialized and can exit the loop
        }
        
        // loop over all points, find the points that have the extreme min / max projection
        // on an axis unity vector
        // only allow a point to be the extreme point in one axis direction, this ensures
        // that speical cases like an axis-aligned clube still produce at least 4 extreme values
        for(size_t i = 1; i < n; i++){
            
            if( coordsMin[0] > cloud.points[i].x){ // compare projection to x-axis
                coordsMin[0] = cloud.points[i].x;
                extremaIndices[0] = i;
            }
            else if(coordsMax[0] < cloud.points[i].x){
                    coordsMax[0] = cloud.points[i].x;
                    extremaIndices[1] = i;
            }
            else if( coordsMin[1] > cloud.points[i].y){  // compare projection to x-axis
                coordsMin[1] = cloud.points[i].y;
                extremaIndices[2] = i;
            }
            else if(coordsMax[1] < cloud.points[i].y){
                    coordsMax[1] = cloud.points[i].y;
                    extremaIndices[3] = i;
            }
            else if( coordsMin[2] > cloud.points[i].z){  // compare projection to z-axis
                coordsMin[2] = cloud.points[i].z;
                extremaIndices[4] = i;
            }
            else if(coordsMax[2] < cloud.points[i].z){
                    coordsMax[2] = cloud.points[i].z;
                    extremaIndices[5] = i;
            }
        }
    }
    
}


/**
 * @brief Find the minimum sphere that contains all points of the support set, only keep point of the
 *        suppot set that lie on the boundary of the sphere.
 * 
 * @param[in] cloud point cloud containing the support set
 * @param[out] sphereParameters parameters of the found sphere
 * @param[in,out] supportIndices indices of the support set points in cloud, betwwen 2 and 5 items long
 * @param[in] epsilon tolerance for checking if a point is within the sphere
 */
template <typename PointT>
static void updateSupportSet(const pcl::PointCloud<PointT>& cloud, Eigen::Vector4f& sphereParameters, std::vector<int>& supportIndices, float epsilon = 1.0e-20){
    
    // number of points in the support set (between 2 and 5)
    int n = supportIndices.size();
    
    // variables for looping
    std::vector<bool> useForModel;  // points to use for the sphere, if element i is true (value > 1),
                                    // we use supportIndices[i] for the sphere
    std::vector<int> useIndices;    // indices of points to use for the sphere
    std::vector<int> checkIndices;  // indices of points to check for being inside the sphere
    Eigen::Vector4f tmpSphere;      // sphere parameters calculated from useIndices
    bool sphereExists;
    bool pointOutside;
    
    // variables for result storage
    Eigen::Vector4f minSphere;      // current smallest sphere, initialized with max radius
    minSphere[3] = FLT_MAX;
    std::vector<int> minSphereIndices; // points supporting the current min. sphere

    
    // loop over number of points i to construct a sphere from
    for(unsigned int i = 2; i <= 4 && i <= n; i++){
        
        // initialize for permutations
        useForModel.clear();
        useForModel.resize(n-i, false);
        useForModel.resize(n, true);
        
        // loop over all possible combinations of i out of n points
        do{
            
            // figure out which points to use for constructing the sphere and which to check
            // against the sphere
            useIndices.clear();
            checkIndices.clear();
            for(int j=0; j < n; j++){
                if(useForModel[j]){
                    useIndices.push_back(supportIndices[j]);
                }
                else{
                    checkIndices.push_back(supportIndices[j]);
                }
            }
            
            // calculate the sphere
            sphereExists = false;
            switch(i){
                case 2:
                    sphereExists = sphereFrom2Points(cloud, useIndices, tmpSphere);
                    break;
                case 3:
                    sphereExists = sphereFrom3Points(cloud, useIndices, tmpSphere);
                    break;
                case 4:
                    sphereExists = sphereFrom4Points(cloud, useIndices, tmpSphere);
                    break;
            }
            
            // if sphere creation failed, try next combination
            if(!sphereExists){
                continue;
            }
            
            // check if all other points are within the sphere
            pointOutside = false;
            for(int j = 0; j < checkIndices.size(); j++){
                
                // check if point is outside the sphere
                if(squaredDistance(tmpSphere, cloud.points[checkIndices[j]]) > tmpSphere[3]*tmpSphere[3] + epsilon){

                    pointOutside = true;
                }
            }
            
            // if one of the points was outside, this is not the sphere, try the next points
            if(pointOutside){
                continue;
            }
            
            // arriving here means all points are within the tmpSphere
            // if the sphere is a new minimum, save it
            if(minSphere[3] > tmpSphere[3]){
                minSphere = tmpSphere;
                minSphereIndices = useIndices;
            }

        } while (std::next_permutation(useForModel.begin(), useForModel.end()));

    }
    
    // if we found a minimum sphere, return it
    if(minSphere[3] < FLT_MAX){
        sphereParameters = minSphere;
        supportIndices = minSphereIndices;  // the new support set consits only of points that
                                            // formed the sphere
        return;
    }
    
    // Arriving here means that no sphere was found that contained all points.
    // This is probably due to numeric roundoff errors, so try again with a bigger epsilon value.
    PCL_DEBUG("[welzlBoundingSphere] No sphere found that contained all points. Trying with a bigger epsilon\n"); 
    updateSupportSet(cloud, sphereParameters, supportIndices, epsilon*2);
    return;
}

///////////////////// implementation of provided functions ///////////////////////////////

template <typename PointT> bool
sphereFrom4Points(const pcl::PointCloud<PointT>& cloud, const std::vector<int>& indices, Eigen::Vector4f& boundingSphere){
  // Need 4 indices
  if (indices.size () != 4)
  {
    PCL_ERROR ("[sphereFrom4Points] Invalid set of indices given (%zu)!\n", indices.size ());
    return (false);
  }

  Eigen::Matrix4f temp;
  for (int i = 0; i < 4; i++)
  {
    temp (i, 0) = cloud.points[indices[i]].x;
    temp (i, 1) = cloud.points[indices[i]].y;
    temp (i, 2) = cloud.points[indices[i]].z;
    temp (i, 3) = 1;
  }
  float m11 = temp.determinant ();
  if (m11 == 0)
    return (false);             // the points don't define a sphere!

  for (int i = 0; i < 4; ++i)
    temp (i, 0) = (cloud.points[indices[i]].x) * (cloud.points[indices[i]].x) +
                  (cloud.points[indices[i]].y) * (cloud.points[indices[i]].y) +
                  (cloud.points[indices[i]].z) * (cloud.points[indices[i]].z);
  float m12 = temp.determinant ();

  for (int i = 0; i < 4; ++i)
  {
    temp (i, 1) = temp (i, 0);
    temp (i, 0) = cloud.points[indices[i]].x;
  }
  float m13 = temp.determinant ();

  for (int i = 0; i < 4; ++i)
  {
    temp (i, 2) = temp (i, 1);
    temp (i, 1) = cloud.points[indices[i]].y;
  }
  float m14 = temp.determinant ();

  for (int i = 0; i < 4; ++i)
  {
    temp (i, 0) = temp (i, 2);
    temp (i, 1) = cloud.points[indices[i]].x;
    temp (i, 2) = cloud.points[indices[i]].y;
    temp (i, 3) = cloud.points[indices[i]].z;
  }
  float m15 = temp.determinant ();

  // Center (x , y, z)
  boundingSphere.resize (4);
  boundingSphere[0] = 0.5f * m12 / m11;
  boundingSphere[1] = 0.5f * m13 / m11;
  boundingSphere[2] = 0.5f * m14 / m11;
  // Radius
  boundingSphere[3] = sqrtf (
                            boundingSphere[0] * boundingSphere[0] +
                            boundingSphere[1] * boundingSphere[1] +
                            boundingSphere[2] * boundingSphere[2] - m15 / m11);
                            
  return (true);
}


template <typename PointT> bool
sphereFrom3Points(const pcl::PointCloud<PointT>& cloud, const std::vector<int>& indices, Eigen::Vector4f& sphereParameters){
    
    // Need exactly 3 indices
    if (indices.size () != 3)
    {
        PCL_ERROR ("[sphereFrom3Points] Invalid number of indices given (%zu)!\n", indices.size ());
        return (false);
    }
    
    // extract the points
    Eigen::Vector3f p0, p1, p2;
    p0 = cloud.points[indices[0]].getVector3fMap();
    p1 = cloud.points[indices[1]].getVector3fMap();
    p2 = cloud.points[indices[2]].getVector3fMap();
    
    // calculate constants
    float k1 = pcl::L2_Norm_SQR(p0, p1, 3);
    float k2 = pcl::L2_Norm_SQR(p0, p2, 3);
    float k3 = -2.0 * (p0.squaredNorm() - p0.dot(p1) -p0.dot(p2) + p1.dot(p2));
    
    // can't calculate if the denominator becomes zero (points are colinear or there are identical
    // points)
    if ((4*k1*k2 - k3*k3) == 0.0){
        return (false);
    }
    
    // calculate the barycentric coordinates of the center
    float u1 = k2 * (2*k1 + k3) / (4*k1*k2 - k3*k3);
    float u2 = k1 * (2*k2 + k3) / (4*k1*k2 - k3*k3);
    float u0 = 1 - u1 - u2;
    
    // calculate the sphere center via the barycentric coordinates
    Eigen::Vector3f c = u0*p0 + u1*p1 + u2*p2;
    
    // calculate the radius
    float r = pcl::L2_Norm(c, p0, 3);
    
    // write results into parameter vector
    sphereParameters[0] = c[0];
    sphereParameters[1] = c[1];
    sphereParameters[2] = c[2];
    sphereParameters[3] = r;
    
    return true;
}


template <typename PointT> bool
sphereFrom2Points(const pcl::PointCloud<PointT>& cloud, const std::vector<int>& indices, Eigen::Vector4f& sphereParameters){
    
    // Need exactly 2 indices
    if (indices.size () != 2)
    {
        PCL_ERROR ("[sphereFrom2Points] Invalid number of indices given (%zu)!\n", indices.size ());
        return (false);
    }
    
    // extract the points
    Eigen::Vector3f p0, p1;
    p0 = cloud.points[indices[0]].getVector3fMap();
    p1 = cloud.points[indices[1]].getVector3fMap();
    
    // the sphere center is in th middle between the two points
    for(unsigned int i = 0; i < 3; i++){
        sphereParameters[i] = 0.5 * (p0[i] + p1[i]);
    }
    
    // the distance between the two points is the diameter, we want the radius
    sphereParameters[3] = 0.5 * pcl::geometry::distance(p0, p1);

    return true;
}


template <typename PointT> inline bool
epos6BoundingSphere(const pcl::PointCloud<PointT>& cloud, Eigen::Vector4f& sphereParameters){

    // extract the number of points
    unsigned int n = cloud.points.size();
    
    if(n <= 6){
        
        // for small clouds, passing directly to optimal algorithm is more efficient
        if(!welzlBoundingSphere(cloud, sphereParameters)){
            return false;
        }
    }
    else{
        // normal EPOS6 algorithm
        
        // find the points with extreme projection values onto the coordinate axis unit vectors
        std::vector<int> extremaIndices; // (xmin, xmax, ymin ymax, zmin zmax)
        findExtremeXYZindices(cloud, extremaIndices);
        
        //find the bounding sphere for the extreme points via Welzl's optimal algorithm
        if(!welzlBoundingSphere(cloud, extremaIndices, sphereParameters)){
            
            PCL_ERROR ("[epos6BoundingSphere] Can't find a bounding sphere for extracted extremaIndices");
            return false;
        }
        
        // iterate over all points again, move and enlarge the sphere if a point is outside
        // the sphere
        for(typename pcl::PointCloud<PointT>::const_iterator it = cloud.begin() ; it != cloud.end(); it++){
            
            // check if point is outside the sphere
            if(squaredDistance(sphereParameters, *it) > sphereParameters[3]*sphereParameters[3]){
                // if outside, we need to move and enlarge the sphere to contain both the old
                // sphere and the point

                // convert PoinT to Eigen for clearner code
                Eigen::Vector4f p((*it).x, (*it).y, (*it).z, 0); // 4th component arbitraty
                
                // save the radius of the old sphere
                float radiusOld = sphereParameters[3];
                
                // calculate direction, get vector length and normalize vector
                Eigen::Vector4f diff = p - sphereParameters;
                diff[3] = 0.0;  // set 4th component to 0 to get right norm in R3-space
                float distance = diff.norm();
                diff = diff / distance;
                
                // new radius
                float radiusNew = 0.5*(distance + radiusOld);
                
                // distance to translate the sphere center along the calculated vector direction
                float t = radiusNew - radiusOld;
                
                // translate center along normalized direction
                sphereParameters += diff * t;
                
                // save new radius
                sphereParameters[3] = radiusNew;
            }
        }
        return true;
        

    }
}


template <typename PointT> bool
welzlBoundingSphere(const pcl::PointCloud<PointT>& cloud,const std::vector<int>& indices_, Eigen::Vector4f& sphereParameters){
    
    // copy indices (for shuffling later) and determine given number of indices
    std::vector<int> indices = indices_;
    int n = indices.size();
    
    // check plausability of input
    if(n < 2){
        PCL_ERROR ("[welzlBoundingSphere] Invalid number of indices given (%zu), need at least 2!\n", indices.size ());
        return false;
    }
    
    // randomly shuffle the indices vector
    // This is done to prevent lots of algorithm passes on ordered point clouds.
    std::mt19937 randomNumberGenerator;
    std::shuffle(indices.begin(), indices.end(), randomNumberGenerator);
    
    // initialize bounding sphere with frist point and radius 0
    sphereParameters[0] = cloud.points[indices[0]].x;  // center
    sphereParameters[1] = cloud.points[indices[0]].y;
    sphereParameters[2] = cloud.points[indices[0]].z;
    sphereParameters[3] = 0.0;                         // radius
    
    // initialize support set of bounding sphere with the first point
    std::vector<int> supportIndices;
    supportIndices.push_back(indices[0]);

    // iterate over all given points and update the bounding sphere every time a point outisde
    // the current sphere is found
    int i = 1; // point index in indices to check
    while(i < n){
        
        // check if the point with index indices[i] is not in the support set
        if(std::find(supportIndices.begin(), supportIndices.end(), indices[i])
                     == supportIndices.end()){

            // check whether the point is not with the sphere ((distance to center)² > radius²)
            if(  squaredDistance(sphereParameters, cloud[indices[i]]) 
               > sphereParameters[3]* sphereParameters[3]){

                // add the point to the support set
                supportIndices.push_back(indices[i]);

                // find the new bounding sphere for the support set, reduce support set if necessary
                Eigen::Vector4f tmpParameters;
                updateSupportSet(cloud, tmpParameters, supportIndices);

                // check whether new sphere is bigger (prevent numeric problems)
                if(tmpParameters[3] > sphereParameters[3]){
                    
                    // assign the new, bigger sphere
                    sphereParameters = tmpParameters;
                }
                
                // restart algorithm
                i = 0;
                continue;

            }
            
        }
        i++;
    }
    
    // sphere was found
    return true;
}


template <typename PointT> bool
welzlBoundingSphere(const pcl::PointCloud<PointT>& cloud, Eigen::Vector4f& sphereParameters){
    
    // construct an index vector with indices of all points in the cloud
    std::vector<int> indices;
    for(int i = 0; i < cloud.points.size(); i++){
        indices.push_back(i);
    }
    
    // use index vector to call function overload
    return welzlBoundingSphere(cloud, indices, sphereParameters);
}


template <typename PointT>
octreeBoundingBox<PointT>::octreeBoundingBox(const typename pcl::octree::OctreePointCloudSearch<PointT>::Ptr &octreeSearchPtr){

	// get min and max coordinates of each dimension
	double min_x, min_y, min_z, max_x, max_y, max_z;
	octreeSearchPtr->getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);

	x_length = fabs(max_x - min_x);
	y_breadth = fabs(max_y - min_y);
	z_height = fabs(max_z - min_z);

	// sort the dimensions by size
	sortDimensions();
}


template <typename PointT> void
octreeBoundingBox<PointT>::sortDimensions(){
	sortedDimensions[0] = x_length;
	sortedDimensions[1] = y_breadth;
	sortedDimensions[2] = z_height;
	std::sort(std::begin(sortedDimensions), std::end(sortedDimensions));
}


} // end namespace
} // end namespace
