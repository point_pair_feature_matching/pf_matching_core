/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

#include "../unorganizedEdgeDetection.hpp"

// PCL
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/filter.h>

// others
#include <algorithm>

// TODO: test only
#define VTK_EXCLUDE_STRSTREAM_HEADERS
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>

// TODO: remove std::out outputs
// TODO: normals are assumed to be unit length?
// TODO: no NaNs / infs in points and normals

namespace pf_matching{
namespace extensions{


template<typename PointNT> void
UnorganizedEdgeDetection<PointNT>::setCurvatureThresholds(const float lowerThresh_,
                                                          const float upperThresh_){
    if(lowerThresh_ > upperThresh_){
        PCL_ERROR("[UnorganizedEdgeDetection::setCurvatureThresholds] lower threshold " 
                  "> upper threshold, not setting thresholds!.\n");
    }
    else{
        lowerThresh = lowerThresh_;
        upperThresh = upperThresh_;
    }
}


template<typename PointNT> void
UnorganizedEdgeDetection<PointNT>::compute(typename UnorganizedEdgeDetection<PointNT>::CloudNT::Ptr& edgesPtr,
                                           std::vector<int>& edgesIndices){
    
    // make space for results
    edgesPtr   = typename CloudNT::Ptr(new CloudNT);
    
    // check input conditions
    if(searchRadius <= 0){
        PCL_ERROR("[UnorganizedEdgeDetection::compute] Search radius not set or <= 0. "
                  "Returning empty.\n");
        return;
    }
    
    if( (!cloudPtr) || cloudPtr->size() == 0){
        PCL_ERROR("[UnorganizedEdgeDetection::compute] Input cloud not set or empty. "
                  "Returning empty.\n");
        return;
    }
    
    // make a kdTree if none was provided
    if(!tree){
        PCL_DEBUG("[UnorganizedEdgeDetection::compute] No valid kdTree given, building a new one.\n");
        tree = typename KdtreeT::Ptr(new KdtreeT);
        tree->setInputCloud(cloudPtr);
    }
    
    // find edge points
    calculatePrincipalCurvatures();
    findLocalCurvatureMaxima(0.25);
    //showCurvatureAndEdges();
    findEdges();
    //showCurvatureAndEdges();
    
    copyResults(edgesPtr, edgesIndices);
}


template<typename PointNT> void
UnorganizedEdgeDetection<PointNT>::compute(typename UnorganizedEdgeDetection<PointNT>::CloudNT::Ptr& edgesPtr,
                                           std::vector<int>& edgesIndices,
                                           typename UnorganizedEdgeDetection<PointNT>::CurvaturesCloudT::Ptr& curvaturesPtr){

    compute(edgesPtr, edgesIndices);
    curvaturesPtr = curvatures;
}


template<typename PointNT> void
UnorganizedEdgeDetection<PointNT>::calculatePrincipalCurvatures(){
    
    // set up
    pcl::PrincipalCurvaturesEstimation<PointNT, PointNT, CurvaturesT> pce;
    pce.setInputCloud(cloudPtr);
    pce.setInputNormals(cloudPtr);
    pce.setSearchMethod(tree);
    pce.setRadiusSearch(searchRadius);
    
    // calculate
    curvatures = typename CurvaturesCloudT::Ptr(new CurvaturesCloudT());
    pce.compute(*curvatures);
}


template<typename PointNT> void
UnorganizedEdgeDetection<PointNT>::findLocalCurvatureMaxima(float maxPerpendicularDistFactor){
    
    // make space for results
    isEdgePoint.resize(cloudPtr->size());
    edgeDirections = pcl::PointCloud<pcl::Normal>::Ptr(new pcl::PointCloud<pcl::Normal>);
    edgeDirections->resize(cloudPtr->size());
    
    // iterate over input points
    #pragma omp parallel for
    for(int i = 0; i < cloudPtr->size(); i++){
        
        // calculate direction of edge at this point
        Eigen::Vector3f pointVector = cloudPtr->points[i].getVector3fMap();
        Eigen::Vector3f normalVector = cloudPtr->points[i].getNormalVector3fMap();
        Eigen::Vector3f curvatureVector(curvatures->points[i].principal_curvature_x,
                                        curvatures->points[i].principal_curvature_y,
                                        curvatures->points[i].principal_curvature_z);
        Eigen::Vector3f edgeDirection = normalVector.cross(curvatureVector);
        edgeDirection.normalize();
        edgeDirections->points[i].getNormalVector3fMap() = edgeDirection;
        
        // check curvature against neighbours that are located approximately perpendicular
        // to the edge direction
        isEdgePoint[i] = true;
        std::vector<int> neighbourIndices;
        std::vector<float> neighbourSqrDists;
        if(tree->radiusSearch(*cloudPtr, i, searchRadius, neighbourIndices,
                              neighbourSqrDists)){

            // there are neighbours, check until we find one that is bigger
            for(auto& nIndex: neighbourIndices){
                
                // get distance to neighbour in edge-direction
                Eigen::Vector3f v = pointVector - cloudPtr->points[nIndex].getVector3fMap();
                float dist = fabs(v.dot(edgeDirection));
                
                // point within search area with higher curvature -> can stop searching
                if(dist <= searchRadius * maxPerpendicularDistFactor && 
                   fabs(curvatures->points[nIndex].pc1) > fabs(curvatures->points[i].pc1)){
                
                    isEdgePoint[i] = false;
                    break;
                }
            }
        }
        else{
            // no neighbour, won't be a maximum (curvature values are unreliable)
            isEdgePoint[i] = false;
        }
    }
}


template<typename PointNT> void
UnorganizedEdgeDetection<PointNT>::findEdges(){
    
    std::list<int> unresolvedEdgeCandidates; // holds indices to points with curvatures
                                             // between thresholds, which do not have a
                                             // definite edge point in the neighbourhood (yet)
    
    // first pass over points, determine all sure edge points
    #pragma omp parallel for
    for(int i = 0; i < cloudPtr->size(); i++){
        
        // only consider edge candiates
        if(isEdgePoint[i]){
            
            if(curvatures->points[i].pc1 < lowerThresh){
                
                // definitely no edge point
                isEdgePoint[i] = false;
            }
            else if(curvatures->points[i].pc1 < upperThresh){
                
                // curvature is in between thresholds, possible edge point
                // will be checked in subsequent passes
                #pragma omp critical
                unresolvedEdgeCandidates.push_back(i);
                isEdgePoint[i] = false;
            }
        }
        // points with curvature >= upper threshold won't be touched (already marked true)
    }
    
    // note: content of isEdgePoint at this point:
    //       true for all sure edges, false for all other points
    
    // subsequent passes to iteratively grow edges with unresolved candidates
    bool changesInLastPass = true;
    while(unresolvedEdgeCandidates.size() > 0 && changesInLastPass){
        changesInLastPass = false;
        
        auto candidateIt = unresolvedEdgeCandidates.begin();
        while(candidateIt != unresolvedEdgeCandidates.end()){
            bool candidateResolved = false;
            
            // check neighbourhood for sure edge points
            std::vector<int> neighbourIndices;
            std::vector<float> neighbourSqrDists;
            if(tree->radiusSearch(*cloudPtr, *candidateIt, searchRadius, neighbourIndices,
                                  neighbourSqrDists)){
                
                // got neighbours, need one sure edge point in neighbourhood
                for(int& nIndex: neighbourIndices){
                    if(isEdgePoint[nIndex]){
                        isEdgePoint[*candidateIt] = true;
                        candidateResolved = true;
                        break;
                    }
                }
            }
            else{
                // no neighbours, won't be classified as edge point
                candidateResolved = true;
            }
            
            // 
            if(candidateResolved){
                candidateIt = unresolvedEdgeCandidates.erase(candidateIt);
                changesInLastPass = true;
            }
            else{
                candidateIt++;
            }
            
        }
    }
}


template<typename PointNT> void
UnorganizedEdgeDetection<PointNT>::copyResults(typename UnorganizedEdgeDetection<PointNT>::CloudNT::Ptr& edgesPtr,
                                               std::vector<int>& edgesIndices){
    
    // reserve space in output clouds
    int nEdges = std::count(isEdgePoint.begin(), isEdgePoint.end(), true);
    edgesPtr->reserve(nEdges);
    edgesIndices.reserve(nEdges);
    
    // copy surface and edges into output clouds
    for(int i = 0; i < cloudPtr->size(); i++){
        if(isEdgePoint[i]){
            // edge: copy location and assign edge direction as new "normal" direction
            PointNT edgePoint = cloudPtr->points[i];
            edgePoint.getNormalVector3fMap() = edgeDirections->points[i].getNormalVector3fMap();
            edgesPtr->push_back(edgePoint);
            
            edgesIndices.push_back(i);
        }
    }
}


template<typename PointNT> void
UnorganizedEdgeDetection<PointNT>::showCurvatureAndEdges(){
                
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloudRGBPtr(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    pcl::copyPointCloud(*cloudPtr, *cloudRGBPtr);
    for(int i = 0; i < curvatures->size(); i++){
        float maxPrincipalCurvature = std::fabs(curvatures->points[i].pc1);
        
        uint8_t r, g, b;
        if(maxPrincipalCurvature > upperThresh){
            r = 0;
            g = 0;
            b = 255;
        }
        else{
            b = maxPrincipalCurvature / upperThresh * 255;
            r = 0;
            g = 0;
        }
        cloudRGBPtr->points[i].r = r;
        cloudRGBPtr->points[i].g = g;
        cloudRGBPtr->points[i].b = b;
    }
    
    for(int i = 0; i < isEdgePoint.size(); i++){
        if(isEdgePoint[i]){
            cloudRGBPtr->points[i].r = 0;
            cloudRGBPtr->points[i].g = 255;
            cloudRGBPtr->points[i].b = 0;
        }
    }
    
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    viewer->setBackgroundColor (255, 255, 255);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGBNormal> rgb(cloudRGBPtr);
    viewer->addPointCloud<pcl::PointXYZRGBNormal> (cloudRGBPtr, rgb, "sample cloud");
    
    while (!viewer->wasStopped ()){
        viewer->spinOnce (100);
    }
}
    
} // end namespace pf_matching
} // end namespace