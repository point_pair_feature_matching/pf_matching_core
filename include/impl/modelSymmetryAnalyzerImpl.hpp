/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

#include "../modelSymmetryAnalyzer.hpp"


// PCL
#include <pcl/common/transforms.h>
#include <pcl/common/geometry.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/filters/extract_indices.h>

// boost
#include <boost/pointer_cast.hpp>
#include <boost/shared_ptr.hpp>


namespace pf_matching{
namespace extensions{

/**
 * @brief transform a point and its normal to a new coordinate system
 * @param point the point to transform
 * @param transform transformation to apply
 * @return point after transformation
 */
template <typename PointNT> PointNT
transformPointWithNormal(const PointNT& point, const Eigen::Affine3f& transform){

    PointNT transformed = pcl::transformPoint(point, transform);
    transformed.getNormalVector3fMap() = transform.rotation() * point.getNormalVector3fMap();
    return transformed;
}


/**
 * @brief check if the normals of two points are aligned
 *
 * For speed the cosine of the angles between the normal vectors is compared.
 * @param p0 first point
 * @param p1 second point
 * @param normalAngleDiffCosMin cosine of the max. allowed angle between the normals (which must be
 *                              smaller than the cosine of the actual angle betwwen the normals)
 * @param normalDirectionAmbigous If true, compare the vectors as if they are lines (direction
 *                                does not matter) to allow for ambigous normal directions. If false,
 *                                compare vector angle.
 * @return true if the two normals are algined, false otherwise
 */
template <typename PointNT> bool
normalsAreAligned(const PointNT& p0, const PointNT& p1, float normalAngleDiffCosMin,
                  bool normalDirectionAmbigous){

    Eigen::Vector3f n0 = p0.getNormalVector3fMap();
    Eigen::Vector3f n1 = p1.getNormalVector3fMap();
    n0.normalize();
    n1.normalize();

    float angleDiffCos;
    if(normalDirectionAmbigous){
        // angle in [0, pi/2] (direction irrelevant, basically smallest angle between lines)
        angleDiffCos = std::fabs(n0.dot(n1));
    }
    else{
        // angle in [0, pi] (direction does matter, angle between vectors)
        angleDiffCos = n0.dot(n1);
    }

    if(angleDiffCos >= normalAngleDiffCosMin){
        return true;
    }
    else{
        return false;
    }
}


template <typename PointNT>
void ModelSymmetryAnalyzer<PointNT>::findEquivalentPoses(const PFmatcher<PointNT>& referenceMatcher,
                                                         float relativePoseWeightThresh,
                                                         float maxThresh,
                                                         featureType featuresToMatch
                                                         ){

    // set up matcher and match all available features
    PFmatcher<PointNT> matcher;
    matcher.copyMatchingParameters(referenceMatcher);
    matcher.maxThresh = maxThresh;
    matcher.setMiscParameters("");
    matcher.useEquivalentModelPoses = false;
    matcher.refPointStep = 1;
    matcher.setModel(modelPtr);
    matcher.setScene(modelPtr->surfaceCloudPtr, modelPtr->edgeCloudPtr);
    // MFZ 19.04.2018 switch off all matcher-extensions - they are not necessary for self matching
    matcher.switchOffExtensions();

    matcher.match(featuresToMatch);


    // find the pose that represents the identity transform (If it exists, matching was
    // successful.)
    unsigned int nPoses = (matcher.getNumberofValidPosesSimple(relativePoseWeightThresh))[0]; // at [0] because one uses no voting balls anyway
    std::vector<Pose*> poses = matcher.getBestPosesOfAllVotingBalls(nPoses);

    int identityPoseIndex = -1;
    for(unsigned int i = 0; i < poses.size(); i++){
        if(poses[i]->isSimilar(Pose(),
                               matcher.clusterTransThreshRelative,
                               matcher.clusterRotThresh)){

            identityPoseIndex = i;
            break;
        }
    }

    if(identityPoseIndex >= 0){
        // matching successful, write equivalent poses to model
        modelPtr->equivalentPoses.clear();
        for(unsigned int i = 0; i < poses.size(); i++){

            // do not add the identity pose (already contained implicitly)
            if(i == identityPoseIndex){
                continue;
            }

            // add other poses
            modelPtr->equivalentPoses.push_back(*(poses[i]));  // TODO: iNFO: object slicing happens here: a cluster is sliced into a pose
            modelPtr->equivalentPoses.back().setWeight(1.0); // reset pose weight
        }

        PCL_DEBUG("Added %d equivalent poses to model.\n", poses.size() - 1);
    }
    else{
        PCL_ERROR("[ModelSymmetryAnalyzer] No identity pose found while matching model to "\
                  "self, something went wrong. Model is not modified.\n");
        return;
    }
}

template <typename PointNT>
void ModelSymmetryAnalyzer<PointNT>::collapseRefPoints(float distThreshRelative,
                                                       float angleThreshRelative,
                                                       bool useInverseTransforms,
                                                       bool deleteUniquePoints){

    // return immediately if there are no equivalent transforms stored in the model
    if(modelPtr->equivalentPoses.size() == 0){
        PCL_WARN("[ModelSymmetryAnalyzer::collapseRefPoints] No equivalent poses stored " \
                 "in the model object. Not collapsing reference points.\n");
        return;
    }

    // calculate comparison values
    float distThresh = distThreshRelative * modelPtr->d_dist_abs;
    float angleThreshCos = cos(angleThreshRelative * modelPtr->d_angle);

    // calculate all transforms we need to check when searching for point equivalence
    std::vector<Eigen::Affine3f> equivalentTransforms;
    for(Pose& p: modelPtr->equivalentPoses){
        equivalentTransforms.push_back(p.asTransformation());
        if(useInverseTransforms){
            equivalentTransforms.push_back(p.asTransformation().inverse(Eigen::Affine));
        }
    }

    // go through hash tables separately
    for(auto& hashTablePtr: modelPtr->hashTables){

        bool rpNormalAmbigous = featureHasAmbiguousRefPointNormal(hashTablePtr->getFeatureType());
        unsigned int nPoints = hashTablePtr->refPointCloudPtr->points.size();
        pcl::KdTreeFLANN<PointNT> kdtree(false); // no sorting of required -> faster radiusSearch
        kdtree.setInputCloud(hashTablePtr->refPointCloudPtr);

        // structure to store how many other points a point is equivalent to after
        // transforming it with
        // equivalencies[i] = 0 -> point was not checked yet
        // equivalencies[i] > 0 -> number of other points equivalent to this one
        // equivalencies[i] < 0 -> point is already the transformed equivalent for the
        //                         previously checked point with index x, so that
        //                         equivalencies[i] = -(x + 1) (so that 0 is not ambiguous)
        std::vector<int> equivalencies;
        equivalencies.resize(nPoints, 0);

        // iterate over points to find correspondences to
        for(unsigned int i = 0; i < nPoints; i++){

            // skip checking for points that are already equivalent to some other point
            if(equivalencies[i] < 0){
                continue;
            }

            // check correspondences for each equivalent pose
            for(Eigen::Affine3f transform: equivalentTransforms){
                PointNT transformedPoint = transformPointWithNormal(hashTablePtr->refPointCloudPtr->points[i],
                                                                    transform);

                // find points that could be equivalent
                std::vector<int> possibleEquivalentsIndices;
                std::vector<float> sqrDistances;
                kdtree.radiusSearch(transformedPoint,
                                    distThresh,
                                    possibleEquivalentsIndices,
                                    sqrDistances);

                // check each possible correspondence in turn
                for(unsigned int j: possibleEquivalentsIndices){

                    // skip checking point with itself and points that were already found to
                    // be an equivalent to an earlier point
                    if(j == i || equivalencies[j] < 0){
                        continue;
                    }

                    // check for equivalent orientation
                    if(normalsAreAligned(transformedPoint,
                    					 hashTablePtr->refPointCloudPtr->points[j],
                                         angleThreshCos,
                                         rpNormalAmbigous)){

                        if(j < i){
                            // point corresponds to a point that was previously checked.
                            // Should only be due to rounding / imprecise rotation axes due
                            // to noise etc. -> save equivalence for the original equivalent
                            // point
                            int equivalentPointIndex = - equivalencies[j] - 1;
                            equivalencies[equivalentPointIndex]++;
                            equivalencies[j] = -(equivalentPointIndex + 1);
                        }
                        else{
                            // point corresponds to a so far unchecked point, save equivalence
                            equivalencies[i]++;
                            equivalencies[j] = -(i + 1);
                        }
                    }
                }
            }
        }

        // find all points that are still in use (were first point of correspondence)
        pcl::PointIndices::Ptr remainingPointsIndices(new pcl::PointIndices());
        for(unsigned int i = 0; i < equivalencies.size(); i++){
            bool keepPoint;
            if(deleteUniquePoints){
                keepPoint = (equivalencies[i] > 0);
            }
            else{
                keepPoint = (equivalencies[i] >= 0);
            }
            if(keepPoint){
                remainingPointsIndices->indices.push_back(i);
            }
        }

        // make new reference point cloud
        typename cloudNT::Ptr newRefPointCloudPtr(new cloudNT);
        newRefPointCloudPtr->header = hashTablePtr->refPointCloudPtr->header;
        pcl::ExtractIndices<PointNT> extract;
        extract.setInputCloud(hashTablePtr->refPointCloudPtr);
        extract.setIndices(remainingPointsIndices);
        extract.filter(*newRefPointCloudPtr);
        hashTablePtr->refPointCloudPtr = newRefPointCloudPtr;

        // remove unused features from hash table and weight remaining ones
        hashTablePtr->collapseHashTable(equivalencies, deleteUniquePoints, nPoints);
    }
}


} // end namespace
} // end namespace
