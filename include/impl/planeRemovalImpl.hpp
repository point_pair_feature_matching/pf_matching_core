/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * @author Xaver Kroischke
 */

#pragma once

#include "../planeRemoval.hpp"

#include <pcl/ModelCoefficients.h>              // for plane removal
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/sac_segmentation.h>


namespace pf_matching{
namespace pre_processing{
    

template<typename PointT>
void removeBiggestPlane(typename pcl::PointCloud<PointT>::Ptr& cloudInPtr,
                        typename pcl::PointCloud<PointT>::Ptr& cloudOutPtr,
                        float distanceTresh){

    // storage for plane model
    pcl::ModelCoefficients::Ptr plane(new pcl::ModelCoefficients);
    plane->values.resize (4); // plane equation parameters (ax+by+cz+d=0)
    pcl::PointIndices::Ptr inliers_plane(new pcl::PointIndices);
    
    // set up segmenter
    pcl::SACSegmentation<pcl::PointNormal> seg;
    seg.setOptimizeCoefficients(true); // makes no big runtime difference, so enable
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setDistanceThreshold (distanceTresh);
    seg.setInputCloud(cloudInPtr);

    // fit plane
    seg.segment(*inliers_plane, *plane);

    // check success
    if (inliers_plane->indices.size () == 0){
        // failed, just copy over
        cloudOutPtr = cloudInPtr;
    }
    else{
        // success, return model outliers (= the cloud without the plane)
        pcl::ExtractIndices<PointT> extract;
        extract.setInputCloud(cloudInPtr);
        extract.setIndices(inliers_plane);
        extract.setNegative(true);          // extract outliers instead of inliers
        extract.filter(*cloudOutPtr);
    }
}
        
} // end namespace
} // end namespace