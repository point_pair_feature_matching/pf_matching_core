/**
 * @copyright
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Markus Ziegler
 * @date 23.03.2018
 * @file flagArrayImpl.hpp
 * @brief Implementation of flag array member functions
 */

#pragma once

#include "../flagArray.hpp"

// STL
#include <utility> // for piecewise_costruct
#include <tuple>
#include <stdio.h> // for loading file

// project
#include <modelHashTable.hpp>

// debugging flags
//#define debug_level_II
//#define debug_level_III
//#define print_hash_table
#define check_reset_flags

namespace pf_matching{


/**
 * @brief getting the cacheline size of the processor on linux systems
 * @note to prevent "false sharing" during multi-threaded access of the flag array
 * @author Nick Strupat
 * @link https://stackoverflow.com/a/4049562
 * @return cache line size (in bytes) of the processor, or 0 on failure
 */
size_t
getCacheLineSize() {
    FILE * p = 0;
    p = fopen("/sys/devices/system/cpu/cpu0/cache/index0/coherency_line_size", "r");
    unsigned int i = 0;
    if (p) {
        fscanf(p, "%d", &i);
        fclose(p);
    }
    return i;
}


/**
 * @brief constructor that initializes the desired flag-array hash table
 * @param[in] hashAlgorithm_ desired hash table type [CHD or STL]
 * @param[in] scenePoints_ number of scene points
 * @param[in] modelPoints_ number of model points
 */
template <typename PointNT>
flagArray<PointNT>::flagArray(flagArrayHashTableType hashAlgorithm_,
							  uint64_t scenePoints_,
							  modelHashTable<PointNT>* modelHashTablePtr,
							  int maxNumberOfThreads_) : hashAlgorithm(hashAlgorithm_) {

  // assigning the correct address to the member-pointer according to the chosen hash function:
  switch (hashAlgorithm){

    case FA_NONE: // no flag array wanted
	  // do nothing
      resetFlags = nullptr;
      checkFlagAtPosition = nullptr;
      ROS_INFO_STREAM("[flagArray::flagArray] no flag array wanted, none initialized");
      break;

    case FA_STL: // STL unordered multimap
      // pointer = address of reset-function to use with this hash function
      resetFlags = &flagArray<PointNT>::resetFlags_withSTL;
      // pointer = address of setFlagAtPosition-function to use with this hash function
      checkFlagAtPosition = &flagArray<PointNT>::checkFlagAtPosition_withSTL;
      // initialize STL hash table:
      ROS_INFO_STREAM("[flagArray::flagArray] STL flag array about to be initialized...");
      stlData.init(scenePoints_, modelHashTablePtr, maxNumberOfThreads_);
      break;

    case FA_STL_PREBUILT: // TODO:STL unordered multimap Prebuilt
      // pointer = address of reset-function to use with this hash function
	  resetFlags = &flagArray<PointNT>::resetFlags_withSTLprebuilt;
	  // pointer = address of setFlagAtPosition-function to use with this hash function
	  checkFlagAtPosition = &flagArray<PointNT>::checkFlagAtPosition_withSTLprebuilt;
	  // initialize STL hash table:
	  ROS_INFO_STREAM("[flagArray::flagArray] STL-PREBUILT flag array about to be initialized...");
	  stlDataPrebuilt.init(scenePoints_, modelHashTablePtr, maxNumberOfThreads_);
	  break;

  	case FA_CMPH_CHD: // CMPH-CHD
      // pointer = address of reset-function to use with this hash function
      resetFlags = &flagArray<PointNT>::resetFlags_withCMPH;
      // pointer = address of setFlagAtPosition-function to use with this hash function
      checkFlagAtPosition = &flagArray<PointNT>::checkFlagAtPosition_withCMPH;
      // initialize CMPH hash function:
      ROS_INFO_STREAM("[flagArray::flagArray] CMPH_CHD flag array about to be initialized...");
      cmphData.init(modelHashTablePtr, maxNumberOfThreads_);
      break;

  	case FA_OWN_PERFECT_HASHER: // OWN_PERFECT_HASHER
  	  // TODO:
  	  // pointer = address of reset-function to use with this hash function
	  resetFlags = &flagArray<PointNT>::resetFlags_withOPH;
	  // pointer = address of setFlagAtPosition-function to use with this hash function
	  checkFlagAtPosition = &flagArray<PointNT>::checkFlagAtPosition_withOPH;
	  // initialize CMPH hash function:
	  ROS_INFO_STREAM("[flagArray::flagArray] OWN PERFECT HASHER flag array about to be initialized...");
	  ophData.init(modelHashTablePtr, maxNumberOfThreads_);
	  break;
  }
}


/**
 * @brief inits the STL flag array hash table
 * @note reserves a certain number of memory and sets the max_load factor
 */
template <typename PointNT> void
STLflagArray<PointNT>::init(uint64_t scenePoints, modelHashTable<PointNT>* modelHashTablePtr, int maxNumberOfThreads_){

	// consider the number of threads!
	maxNumberOfThreads = maxNumberOfThreads_;
	this->foundModelHashKeys.resize(maxNumberOfThreads_);

	//reserve enough space for the hash table
	numberOfUniqueModelPFs = 0;
	if (modelHashTablePtr->getHashTableType() == 3) {  // CMPH_CHD model hash table
		CMPHmodelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<CMPHmodelHashTable<PointNT>* >(modelHashTablePtr);
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getNumberOfUniqueModelPFs();
		this->reserve(numberOfUniqueModelPFs);
	}
	if (modelHashTablePtr->getHashTableType() == 4) {  // CMPH_CHD visibility context model hash table
		CMPHvisibilityContextModelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<CMPHvisibilityContextModelHashTable<PointNT>* >(modelHashTablePtr);
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getNumberOfUniqueModelPFs();
		this->reserve(numberOfUniqueModelPFs);
	}
	if (modelHashTablePtr->getHashTableType() == 1){  // STL model hash table
		STLmodelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<STLmodelHashTable<PointNT>* >(modelHashTablePtr);
		std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT> uniqueModelPFs;
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getNumberOfUniqueModelPFs();
		this->reserve(numberOfUniqueModelPFs);
	}
	if (modelHashTablePtr->getHashTableType() == 2){  // STL visibility context model hash table
		STLvisibilityContextModelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<STLvisibilityContextModelHashTable<PointNT>* >(modelHashTablePtr);
		std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT> uniqueModelPFs;
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getNumberOfUniqueModelPFs();
		this->reserve(numberOfUniqueModelPFs);
	}
	if ( modelHashTablePtr->getHashTableType() <= 0 || modelHashTablePtr->getHashTableType() >= 5 ){
		ROS_ERROR_STREAM("[STLflagArray::init] unknown model hash table type: " << modelHashTablePtr->getHashTableType()  );
	}

	// set the max load factor (number of bins per bucket) to 1
	this->max_load_factor( 1.0 );

	ROS_INFO_STREAM("[STLflagArray::init] STL flag array initialized, i.e. memory reserved.");

}


/**
 * @brief inits the STL flag array hash table
 * @note reserves a certain number of memory and sets the max_load factor
 */
template <typename PointNT> void
STLflagArrayPrebuilt<PointNT>::init(uint64_t scenePoints, modelHashTable<PointNT>* modelHashTablePtr, int maxNumberOfThreads_){

	// go through whole model and store all the flags
	// consider the number of threads!
	maxNumberOfThreads = maxNumberOfThreads_;
	numberOfUniqueModelPFs = 0;
	std::vector<typename flagArray<PointNT>::flagArrayResolution> emptyFlags (maxNumberOfThreads_, 0);
	typename MinimalDiscretePF<PointNT>::featureHashKeyT hashKey = 0;

	// loop over all unique model PFs to build a STL hash table with a 0-initialized flag array per PF and thread:

	if (modelHashTablePtr->getHashTableType() == 3) {  // CMPH_CHD model hash table
		CMPHmodelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<CMPHmodelHashTable<PointNT>* >(modelHashTablePtr);
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getNumberOfUniqueModelPFs();
//		std::cout << "number of unique model PFs: "<< numberOfUniqueModelPFs << std::endl;
		this->reserve(numberOfUniqueModelPFs);
		for (int row = 0; row < numberOfUniqueModelPFs; row ++){
//			std::cout << "row = " << row << std::endl;
			hashKey = modelHashTablePtr_casted->minimalHashTable[row].first;
			this->emplace(std::piecewise_construct,
						  std::forward_as_tuple(hashKey),
						  std::forward_as_tuple(emptyFlags));
		}
	}

	if (modelHashTablePtr->getHashTableType() == 4) {  // CMPH_CHD visibility context model hash table
		CMPHvisibilityContextModelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<CMPHvisibilityContextModelHashTable<PointNT>* >(modelHashTablePtr);
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getNumberOfUniqueModelPFs();
		this->reserve(numberOfUniqueModelPFs);
		for (int row = 0; row < numberOfUniqueModelPFs; row ++){
			hashKey = modelHashTablePtr_casted->minimalHashTable[row].first;
			this->emplace(std::piecewise_construct,
						  std::forward_as_tuple(hashKey),
						  std::forward_as_tuple(emptyFlags));
		}
	}
	if (modelHashTablePtr->getHashTableType() == 1){  // STL model hash table
		STLmodelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<STLmodelHashTable<PointNT>* >(modelHashTablePtr);
		std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT> uniqueModelPFs;
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getUniqueModelPFs(uniqueModelPFs);
		this->reserve(numberOfUniqueModelPFs);
		for (typename std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>::iterator it = uniqueModelPFs.begin(); it != uniqueModelPFs.end(); it++){
			hashKey = *it;
			this->emplace(std::piecewise_construct,
						  std::forward_as_tuple(hashKey),
						  std::forward_as_tuple(emptyFlags));
		}
	}
	if (modelHashTablePtr->getHashTableType() == 2){  // STL visibility context model hash table
		STLvisibilityContextModelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<STLvisibilityContextModelHashTable<PointNT>* >(modelHashTablePtr);
		std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT> uniqueModelPFs;
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getUniqueModelPFs(uniqueModelPFs);
		this->reserve(numberOfUniqueModelPFs);
		for (typename std::set<typename MinimalDiscretePF<PointNT>::featureHashKeyT>::iterator it = uniqueModelPFs.begin(); it != uniqueModelPFs.end(); it++){
			hashKey = *it;
			this->emplace(std::piecewise_construct,
						  std::forward_as_tuple(hashKey),
						  std::forward_as_tuple(emptyFlags));
		}
	}
	if ( modelHashTablePtr->getHashTableType() <= 0 || modelHashTablePtr->getHashTableType() >= 5 ){
		ROS_ERROR_STREAM("[STLflagArrayPrebuilt::init] unknown model hash table type: " << modelHashTablePtr->getHashTableType()  );
	}

	this->foundModelHashKeys.resize(maxNumberOfThreads_);

	ROS_INFO_STREAM("[STLflagArrayPrebuilt::init] prebuilt STL flag array initialized with "  << this->numberOfUniqueModelPFs << " rows for each of the " << this->maxNumberOfThreads << " threads. ");

}


/**
 * @brief inits the CMPH flag array hash table
 * @note using the already calculated hashValue from matching
 *       this should have the advantage, that one does not need to
 *       calculate two different hash values (as with STL flag array)
 *       and therefore save computation time
 */
template <typename PointNT> void
CMPHflagArray<PointNT>::init(modelHashTable<PointNT>* modelHashTablePtr, int maxNumberOfThreads_){

	if (modelHashTablePtr->getHashTableType() == 3) {  // CMPH_CHD model hash table
		CMPHmodelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<CMPHmodelHashTable<PointNT>* >(modelHashTablePtr);
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getNumberOfUniqueModelPFs();
	}

	if (modelHashTablePtr->getHashTableType() == 4) {  // CMPH_CHD visibility context model hash table
		CMPHvisibilityContextModelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<CMPHvisibilityContextModelHashTable<PointNT>* >(modelHashTablePtr);
		numberOfUniqueModelPFs = modelHashTablePtr_casted->getNumberOfUniqueModelPFs();
	}
	if ( modelHashTablePtr->getHashTableType() <= 2 || modelHashTablePtr->getHashTableType() >= 5 ){
		ROS_ERROR_STREAM("[CMPHflagArray::init] unknown model hash table type or not supported STL model hash table type");
	}

	// for single-threading:
	tableSize = numberOfUniqueModelPFs;

	// for multi-threading insert additional padding:
	// get size of cacheline in bytes
	unsigned int cacheLineSize = getCacheLineSize();
	ROS_WARN_STREAM("[OPHflagArray::init] the cacheline-size (in bytes) of this systems cpu's is " << cacheLineSize);
	if((cacheLineSize !=0) && (maxNumberOfThreads_ > 1)){
		// add padding to table, to prevent "false sharing" during multi-threaded flag-array access
		float modulo = tableSize * sizeof(flagArrayResolution) % cacheLineSize;
		tablePadding = (cacheLineSize - modulo)/sizeof(flagArrayResolution);
		tableSize = tableSize + tablePadding;
	}
	else{
		tablePadding = 0;
	}

	maxNumberOfThreads = maxNumberOfThreads_;

	// allocate memory with padding between the array parts of each thread, to prevent "false sharing" during multi-threading:
	// ||                     <--                    memory of flag array                          -->                          ||
	// || array-part for thread #1 | padding || array-part for thread #2 | padding || ... || array-part for thread #n | padding ||
	// ^^ aligned at a cacheline             ^^ aligned at a cacheline
	flagArrayPtr = (flagArrayResolution*) aligned_alloc(cacheLineSize, maxNumberOfThreads * tableSize * sizeof(flagArrayResolution)); //!< "multidimensional" 1-D array for multi-threading
	memset(flagArrayPtr, 0, maxNumberOfThreads * tableSize * sizeof(flagArrayResolution));

	std::cout << "address of first flag-array entry to check if correctly aligned: " << &flagArrayPtr[0] << std::endl;

	foundModelHashValues.resize(maxNumberOfThreads);

	float size_ = maxNumberOfThreads * (numberOfUniqueModelPFs + tablePadding) * (float)(sizeof(flagArrayResolution)/(float)1048576); /*megabytes*/
	if (flagArrayPtr != nullptr){
		ROS_INFO_STREAM("[CMPHflagArray::init] CMPH flag array initialized with " << numberOfUniqueModelPFs << " rows plus " << tablePadding << " padding rows for each of the "
				        << maxNumberOfThreads_<< " threads. That should result in " << std::setprecision(7) << size_ << " megabytes");
	}
	else{
		ROS_ERROR_STREAM("[CMPHflagArray::init] Error, flagArrayPtr is nullptr! Memory allocation failed!");
	}
}


template <typename PointNT> void
OPHflagArray<PointNT>::init(modelHashTable<PointNT>* modelHashTablePtr, int maxNumberOfThreads_){

	// set parameters
	setParameters(modelHashTablePtr);

	// calculating hash table size from max values
	if (visconBase == 0){
		  tableSize = (distanceBase-1)*pow(angleBase,3);
	  }
	else{
		  tableSize = (distanceBase-1)*pow(angleBase,3)*pow(visconBase,4);
	  }

	// creating hash table
	// for multi-threading:
	// get size of cacheline in bytes
	unsigned int cacheLineSize = getCacheLineSize();
	ROS_WARN_STREAM("[OPHflagArray::init] the cacheline-size (in bytes) of this systems cpu's is " << cacheLineSize);
	if((cacheLineSize !=0) && (maxNumberOfThreads_ > 1)){
		// add padding to table, to prevent "false sharing" during multi-threaded flag-array access
		float modulo = tableSize * sizeof(flagArrayResolution) % cacheLineSize;
		tablePadding = (cacheLineSize - modulo)/sizeof(flagArrayResolution);
		tableSize = tableSize + tablePadding;
	}
	else{
		tablePadding = 0;
	}
	maxNumberOfThreads = maxNumberOfThreads_;

	// allocate memory with padding between the array parts of each thread, to prevent "false sharing" during multi-threading:
	// ||                     <--                    memory of flag array                          -->                          ||
	// || array-part for thread #1 | padding || array-part for thread #2 | padding || ... || array-part for thread #n | padding ||
	// ^^ aligned at a cacheline             ^^ aligned at a cacheline
	flagArrayPtr = (flagArrayResolution*) aligned_alloc(cacheLineSize, maxNumberOfThreads * tableSize * sizeof(flagArrayResolution)); //!< "multidimensional" 1-D array for multi-threading
	memset(flagArrayPtr, 0, maxNumberOfThreads * tableSize * sizeof(flagArrayResolution));

	std::cout << "address of first flag-array entry to check if correctly aligned: " << &flagArrayPtr[0] << std::endl;

	foundModelHashValues.resize(maxNumberOfThreads);

	#ifdef debug_level_II
	      std::cout << "[OPHflagArray::init] Initialisieren aller "<< tableSize << " Hash Table Eintr��ge mit 0" << std::endl;
	#endif
	#ifdef print_hash_table
	      std::cout << "[OPHflagArray::init] und so sieht die initialisierte Hash-Table aus:" << std::endl;
	      for (int i = 0; i < maxNumberOfThreads * tableSize; i++){
	          std::cout << std::bitset<32>(flagArrayPtr[i]) << std::endl;
	        }
	#endif

	float size_ = maxNumberOfThreads * tableSize * (float)sizeof(flagArrayResolution)/(float)1024/(float)1024; /*megabytes*/

	if ( (modelHashTablePtr->getHashTableType() == 1) || (modelHashTablePtr->getHashTableType() == 3) ){ // no visibility context used
		if (distanceBase <= angleBase){
			// easiest case, in which no conversion is needed, because distanceBase is smaller than angleBase
			// pointer = address of own hash function
			hashFunction = &OPHflagArray<PointNT>::ownHashFunctionWithoutConversion;
		}
		else { // distanceBase > angleBase

			// handle according to maxpower
			if (maxpower > 1){ // more complex conversion is needed
				// pointer = address of own hash funtion
				hashFunction = &OPHflagArray<PointNT>::ownHashFunctionWithConversionWithLoop;
			}
			else { // easier conversion is sufficient
				// pointer = address of own hash funtion
				hashFunction = &OPHflagArray<PointNT>::ownHashFunctionWithConversion;
			}
		}
	}
	else { // visibility context is used
		if(distanceBase <= angleBase){
			// easiest case, in which no conversion is needed, because distanceBase is smaller than angleBase
			// pointer = address of own hash function
			hashFunction = &OPHflagArray<PointNT>::ownHashFunctionWithoutConversion_visCon;
		}
		else { // // distanceBase > angleBase
			// instead of getting max.power, we use my new improved hash function (which doesn't need max. power):
			hashFunction = &OPHflagArray<PointNT>::ownHashFunctionWithConversion_visCon;
		}
	}
	if (flagArrayPtr != nullptr){
		ROS_INFO_STREAM("[OPHflagArray::init] OPH flag array initialized with " << tableSize << " rows including " << tablePadding << " padding rows for each of the "
				        << maxNumberOfThreads_<< " threads. That should result in " << std::setprecision(7) << size_ << " megabytes");
	}
	else{
		ROS_ERROR_STREAM("[OPHflagArray::init] Error, flagArrayPtr is nullptr! Memory allocation failed!");
	}
}


/**
 * @brief checks whether the PF may vote in Hough space or not
 * @note version that uses the CMPH hash function
 * @param[in] alpha alpha alpha_scene of the PF
 * @param[in] hashValue the hash value of the PF that indicates directly the bin in the flagArrayPtr
 * @return true if PF is allowed vote in Hough-space, false if it shall not vote
 */
template <typename PointNT> bool
CMPHflagArray<PointNT>::checkFlagAtPosition(unsigned int alpha, uint32_t hashValue, int threadID){
	uint32_t flag = 0b00000000000000000000000000000001;
	flag = flag << alpha;
	uint64_t index = getIndex(hashValue, threadID);

	#ifdef debug_level_III
	std::cout << "[hashTable::setFlagAtPosition] desired slot bits: " << std::bitset<32>(flag) << std::endl;
	std::cout << "[hashTable::setFlagAtPosition] current slot bits: " << std::bitset<32>(flagArrayPtr[index]) << std::endl;
	#endif

	bool flagAlreadySet = false;

	#pragma omp critical (flagArrayPtr)
	{
		flagAlreadySet = flagArrayPtr[index] & flag;
	}

	if (flagAlreadySet){
		// there has already been a vote for a similar PF with a similar pose
		// do not allow another vote for this pose
		return false;
	}
	else {
		// no vote for this PF with this pose yet
		// allow a vote for this pose
		// and flag this pose from now on for the current reference point
		#pragma omp critical (flagArrayPtr)
		{
		flagArrayPtr[index] = flagArrayPtr[index] | flag;
		}
	#ifdef debug_level_III
	std::cout << "[hashTable::setFlagAtPosition] After flagging:    " << std::bitset<32>(flagArrayPtr[index]) << std::endl;
	#endif

		#pragma omp critical (foundModelHashValues)
		{
		foundModelHashValues[threadID].push_back(hashValue);
		}
		return true;
	}
}


/**
 * @brief checks whether the PF may vote in Hough space or not
 * @note version with STL hash table
 * @param[in] alpha alpha_scene of the PF
 * @param[in] hashKey the hash key of the PF
 * @return true if PF is allowed to vote in Hough-space, false if it shall not vote
 */
template <typename PointNT> bool
STLflagArray<PointNT>::checkFlagAtPosition(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF, int threadID){

	uint32_t flag = 0b00000000000000000000000000000001;
	flag = flag << alpha;
	typename hashTableT::iterator bin;
	typename MinimalDiscretePF<PointNT>::featureHashKeyT hashKey;

	// handle if either "neighbour" PF, or "original" PF should be checked
	if (!neighbourPF){
		hashKey = sceneFeaturePtr->getDiscreteFeatureAsInt();
	}
	else{
		hashKey = sceneFeaturePtr->getDiscreteNeighbourFeatureAsInt(); //!< neighbour PF calculation during matching is currently not implemented
	}

	#pragma omp critical (searchKey)
	{
	bin = this->find(hashKey); // iterator to the bin containing the flags
	}
	#ifdef debug_level_III
	std::cout << "[hashTable::setFlagAtPosition_withSTL] desired slot bits: " << std::bitset<32>(flag) << std::endl;
	#endif

	if (bin != this->end()){
		// key already known, i.e. a flag has already been set
		#ifdef debug_level_III
		std::cout << "[hashTable::setFlagAtPosition_withSTL] current slot bits: " << std::bitset<32>(bin->second[threadID]) << std::endl;
		#endif
		if (bin->second[threadID] & flag){
			// there has already been a vote for a similar PF with a similar pose
			// do not allow another vote for this pose
			return false;
		}
		else {
			// no vote for this PF with this pose yet
			// allow a vote for this pose
			// and flag this pose from now on for the current reference point
			#pragma omp critical (updateFlag)
			{
			bin->second[threadID] = bin->second[threadID] | flag;
			}
			#ifdef debug_level_III
			std::cout << "[hashTable::setFlagAtPosition_withSTL] new slot bits:     " << std::bitset<32>(bin->second[threadID]) << std::endl;
			#endif

			#pragma omp critical (foundModelHashKeys)
			{
			this->foundModelHashKeys[threadID].push_back(hashKey);
			}
			return true;
		}
	}
	else {
		// key not yet known, create object and store hashKey and flag in bin
		std::vector<typename flagArray<PointNT>::flagArrayResolution> emptyFlags (maxNumberOfThreads, 0);
		emptyFlags[threadID] = emptyFlags[threadID] | flag;
		#pragma omp critical (addKey)
		{
		this->emplace(std::piecewise_construct,
					  std::forward_as_tuple(hashKey),
					  std::forward_as_tuple(emptyFlags));
		}
		#ifdef debug_level_III
		std::cout << "[hashTable::setFlagAtPosition_withSTL] Key not yet known. Key = " << std::bitset<64>(hashKey)<< std::endl;
		bin = this->find(hashKey); // iterator to the bin containing the flags
		std::cout << "[hashTable::setFlagAtPosition_withSTL] Created new bin with:    " << std::bitset<32>(bin->second[threadID]) << std::endl;
		#endif

		this->foundModelHashKeys[threadID].push_back(hashKey);
		return true;
	}
}


template <typename PointNT> bool
STLflagArrayPrebuilt<PointNT>::checkFlagAtPosition(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF, int threadID){

	uint32_t flag = 0b00000000000000000000000000000001;
	flag = flag << alpha;
	typename MinimalDiscretePF<PointNT>::featureHashKeyT hashKey;

	// handle if either "neighbour" PF, or "original" PF should be checked
	if (!neighbourPF){
		hashKey = sceneFeaturePtr->getDiscreteFeatureAsInt();
	}
	else{
		hashKey = sceneFeaturePtr->getDiscreteNeighbourFeatureAsInt(); //!< neighbour PF calculation during matching is currently not implemented
	}

	// STL containers are thread safe for reading operations (this should be a reading operation!):
	auto bin = this->find(hashKey); // iterator to the bin containing the flags

	#ifdef debug_level_III
	std::cout << "[hashTable::setFlagAtPosition_withSTL] desired slot bits: " << std::bitset<32>(flag) << std::endl;
	#endif

	if (bin != this->end()){
		// key already known
		#ifdef debug_level_III
		std::cout << "[hashTable::setFlagAtPosition_withSTL] current slot bits: " << std::bitset<32>(bin->second[threadID]) << std::endl;
		#endif
		// STL containers are thread safe for reading operations:
		if (bin->second[threadID] & flag){
			// there has already been a vote for a similar PF with a similar pose
			// do not allow another vote for this pose
			return false;
		}
		else {
			// no vote for this PF with this pose yet
			// allow a vote for this pose
			// and flag this pose from now on for the current reference point
			// STL containers are not thread safe for writing operations:
			#pragma omp critical (updateFlag)
			{
			bin->second[threadID] = bin->second[threadID] | flag;
			}

			#ifdef debug_level_III
			std::cout << "[hashTable::setFlagAtPosition_withSTL] new slot bits:     " << std::bitset<32>(bin->second[threadID]) << std::endl;
			#endif

			#pragma omp critical (foundModelHashKeys)
			{
			this->foundModelHashKeys[threadID].push_back(hashKey);
			}
			return true;
		}
	}
	else {
		// key not known, error!
		ROS_ERROR_STREAM("[hashTable::setFlagAtPosition_withSTL] Prebuild STL Flag Array: ERROR! Key not prebuild!");
		return false;
	}
}

template <typename PointNT> bool
OPHflagArray<PointNT>::checkFlagAtPosition(unsigned int alpha, MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF, int threadID){

	uint32_t flag = 0b00000000000000000000000000000001;
	flag = flag << alpha;
	typename MinimalDiscretePF<PointNT>::featureHashKeyT hashKey;

	uint64_t hashValue = (this->*hashFunction)(sceneFeaturePtr, neighbourPF); /* be VERY careful calculating the hash value with the hash-function.
	                                                           	    			 as it turns out, it is highly complex and takes a lot of time. */
	uint64_t index = getIndex(hashValue, threadID);

	#ifdef debug_level_III
	std::cout << "[hashTable::setFlagAtPosition] desired slot bits: " << std::bitset<32>(flag) << std::endl;
	std::cout << "[hashTable::setFlagAtPosition] current slot bits: " << std::bitset<32>(flagArrayPtr[index]) << std::endl;
	#endif

	bool flagAlreadySet = false;

	#pragma omp critical (flagArrayPtr)
	{
		flagAlreadySet = flagArrayPtr[index] & flag;
	}

	if (flagAlreadySet){
		// there has already been a vote for a similar PF with a similar pose
		// do not allow another vote for this pose
		return false;
	}
	else {
		// no vote for this PF with this pose yet
		// allow a vote for this pose
		// and flag this pose from now on for the current reference point
		#pragma omp critical (flagArrayPtr)
		{
		flagArrayPtr[index] = flagArrayPtr[index] | flag;
		}
	#ifdef debug_level_III
	std::cout << "[hashTable::setFlagAtPosition] After flagging:    " << std::bitset<32>(flagArrayPtr[index]) << std::endl;
	#endif

		#pragma omp critical (foundModelHashValues)
		{
		foundModelHashValues[threadID].push_back(hashValue);
		}
		return true;
	}
}

template <typename PointNT> bool
CMPHflagArray<PointNT>::resetFlags(int threadID){

	if (flagArrayPtr != nullptr){
		/* instead of resetting all hash table entries to 0, it
		* might be more efficient to track all the changed
		* entries and reset only those.
		*/
		#ifdef print_hash_table
	    std::cout << "[CMPHflagArray::resetFlags] filled Hash-Table:" << std::endl;
	    for (int i = 0; i < numberOfUniqueModelPFs; i++){
	        std::cout << std::bitset<32>(flagArrayPtr[i]) << std::endl;
	      }
		#endif

		// maybe its even faster, if i somehow check the index and assign the
		// value only once. because this might save a lot of time.
		#ifdef debug_level_II
		std::cout << "[CMPHflagArray::resetFlags] there were " << foundModelHashValues.size() << " flag-array rows flagged. about to reset them..." << std::endl;
		#endif

		int numberOfHashValues = 0;
		#pragma omp critical (foundModelHashValues)
		{
		numberOfHashValues = foundModelHashValues[threadID].size();
		}
		try{
			for (int hashValue = 0; hashValue < numberOfHashValues; hashValue++ ){
				#pragma omp critical (flagArrayPtr)
				{
				flagArrayPtr[getIndex(foundModelHashValues[threadID][hashValue], threadID)] = 0b00000000000000000000000000000000;
				}
//			  foundModelHashValues[threadID].pop_back();
			}
		}
		catch (const std::out_of_range& oor){
			ROS_ERROR_STREAM("[CMPHflagArray::resetFlags] Error: " << oor.what() );
		}

		#ifdef print_hash_table
	    std::cout << "[CMPHflagArray::resetFlags] flags deleted" << std::endl;
	    for (int i = 0; i < numberOfUniqueModelPFs; i++){
	    	        std::cout << std::bitset<32>(flagArrayPtr[i]) << std::endl;
	    	      }
		#endif

		#pragma omp critical (foundModelHashValues)
	    {
	    foundModelHashValues[threadID].clear();
	    }

#ifdef check_reset_flags
	    uint64_t startIdx = getIndex(0,threadID);
	    uint64_t endIdx = getIndex(numberOfUniqueModelPFs, threadID);
	    bool resetFailed = false;
	    for (int entry = startIdx; entry < endIdx; entry ++){
	    	uint32_t flagArrayEntry = 0;
	    	#pragma omp atomic read
	    	flagArrayEntry = flagArrayPtr[entry];
//	    	std::cout << entry << ".FlagArrayEntry = " <<  std::bitset<32>(flagArrayEntry) << std::endl;
			if(flagArrayEntry != 0b00000000000000000000000000000000){
				PCL_ERROR("[CMPHflagArray::resetFlags] Error, entry %u was not reset to 0! Instead, it is %u.", entry, flagArrayEntry);
				resetFailed = true;
			}
	    }
	    if (resetFailed) return false; // error occurred!
#endif
	    return true; // no error occurred
	  }

	  else{
		  std::cout << "[CMPHflagArray::resetFlags] Error, flagArrayPtr is nullptr!" << std::endl;
		  return false; // error occurred
	  }
}


/**
 *
 * @return true if reset was successful
 */
template <typename PointNT> bool
STLflagArray<PointNT>::resetFlags(int threadID){
    // reset only flags that have been set

	#ifdef debug_level_II
	std::cout << "[STLflagArray::resetFlags] there were " << foundModelHashKeys[threadID].size() << " flag-array rows flagged. about to reset them..." << std::endl;
	#endif
	typename hashTableT::iterator bin;
    for (auto& key: this->foundModelHashKeys[threadID]){
    	bin = this->find(key);
        if (bin != this->end()){
            bin->second[threadID] = 0b00000000000000000000000000000000;
        }
        else {
            std::cout << "[STLflagArray::resetFlags] Error, slot to reset not found!" << std::endl;
            return false; // error occurred
		}
    }

	#ifdef print_hash_table
    std::cout << "[STLflagArray::resetFlags] Display reset Hash-Table:" << std::endl;
    auto it = this->begin();
    while(it != this->end()){
            std::cout << std::bitset<32>(it->second[threadID]) << std::endl;
            it ++;
        }
	#endif

    this->foundModelHashKeys[threadID].clear();

#ifdef check_reset_flags
	bool resetFailed = false;
	for (auto entryIt = this->begin(); entryIt != this->end(); entryIt ++){
		uint32_t flagArrayEntry = 0;
		#pragma omp atomic read
		flagArrayEntry = entryIt->second[threadID];
//	    std::cout << "FlagArrayEntry = " <<  std::bitset<32>(flagArrayEntry) << std::endl;
		if(flagArrayEntry != 0b00000000000000000000000000000000){
			PCL_ERROR("[STLflagArray::resetFlags] Error, entry was not reset to 0! Instead, it is %u.", flagArrayEntry);
			resetFailed = true;
		}
	}
	if (resetFailed) return false; // error occurred!
#endif

    return true; // no error occurred
}


template <typename PointNT> bool
STLflagArrayPrebuilt<PointNT>::resetFlags(int threadID){
    // reset only flags that have been set

	#ifdef debug_level_II
	std::cout << "[STLflagArray::resetFlags] there were " << this->foundModelHashKeys[threadID].size() << " flag-array rows flagged. about to reset them..." << std::endl;
	#endif
    typename hashTableT::iterator bin;
	for (auto& key: this->foundModelHashKeys[threadID]){
		bin = this->find(key);
		if (bin != this->end()){
			bin->second[threadID] = 0b00000000000000000000000000000000;
		}
		else {
			std::cout << "[STLflagArray::resetFlags] Error, slot to reset not found!" << std::endl;
			return false; // error occurred
		}
	}

	this->foundModelHashKeys[threadID].clear();

#ifdef check_reset_flags
	bool resetFailed = false;
	for (auto entryIt = this->begin(); entryIt != this->end(); entryIt ++){
		uint32_t flagArrayEntry = 0;
		#pragma omp atomic read
		flagArrayEntry = entryIt->second[threadID];
//	    std::cout << "FlagArrayEntry = " <<  std::bitset<32>(flagArrayEntry) << std::endl;
		if(flagArrayEntry != 0b00000000000000000000000000000000){
			PCL_ERROR("[STLflagArrayPrebuilt::resetFlags] Error, entry was not reset to 0! Instead, it is %u.", flagArrayEntry);
			resetFailed = true;
		}
	}
	if (resetFailed) return false; // error occurred!
#endif

	return true; // no error occurred
}


template <typename PointNT> bool
OPHflagArray<PointNT>::resetFlags(int threadID){

	if (flagArrayPtr != nullptr){
		/* instead of resetting all hash table entries to 0, it
		* might be more efficient to track all the changed
		* entries and reset only those.
		*/
		#ifdef print_hash_table
		std::cout << "[OPHflagArray::resetFlags] filled Hash-Table:" << std::endl;
		for (int i = 0; i < tableSize; i++){
			std::cout << std::bitset<32>(flagArrayPtr[getIndex(foundModelHashValues[threadID][i], threadID)]) << std::endl;
		  }
		#endif

		// maybe its even faster, if i somehow check the index and assign the
		// value only once. because this might save a lot of time.
		#ifdef debug_level_II
		std::cout << "[OPHflagArray::resetFlags] there were " << foundModelHashValues[threadID].size() << " flag-array rows flagged. about to reset them..." << std::endl;
		#endif

		int numberOfHashValues = 0;
		#pragma omp critical (foundModelHashValues)
		{
		numberOfHashValues = foundModelHashValues[threadID].size();
		}
		try{
			for (int hashValue = 0; hashValue < numberOfHashValues; hashValue++ ){
				#pragma omp critical (flagArrayPtr)
				{
				flagArrayPtr[getIndex(foundModelHashValues[threadID][hashValue], threadID)] = 0b00000000000000000000000000000000;
				}
//			  foundModelHashValues[threadID].pop_back();
			}
		}
		catch (const std::out_of_range& oor){
			ROS_ERROR_STREAM("[OPHflagArray::resetFlags] Error: " << oor.what() );
		}

		#ifdef print_hash_table
		std::cout << "[OPHflagArray::resetFlags] flags deleted" << std::endl;
		for (int i = 0; i < tableSize; i++){
					std::cout << std::bitset<32>(flagArrayPtr[getIndex(foundModelHashValues[threadID][i], threadID)]) << std::endl;
				  }
		#endif

		#pragma omp critical (foundModelHashValues)
		{
		foundModelHashValues[threadID].clear();
		}

#ifdef check_reset_flags
	    uint64_t startIdx = getIndex(0,threadID);
	    uint64_t endIdx = getIndex(tableSize, threadID);
	    bool resetFailed = false;
	    for (int entry = startIdx; entry < endIdx; entry ++){
	    	uint32_t flagArrayEntry = 0;
	    	#pragma omp atomic read
	    	flagArrayEntry = flagArrayPtr[entry];
//	    	std::cout << entry << ".FlagArrayEntry = " <<  std::bitset<32>(flagArrayEntry) << std::endl;
			if(flagArrayEntry != 0b00000000000000000000000000000000){
				PCL_ERROR("[OPHflagArray::resetFlags] Error, entry %u was not reset to 0! Instead, it is %u.", entry, flagArrayEntry);
				resetFailed = true;
			}
	    }
	    if (resetFailed) return false; // error occurred!
#endif

		return true; // no error occurred
	  }

	  else{
		  ROS_ERROR_STREAM("[OPHflagArray::resetFlags] Error, flagArrayPtr is nullptr!" );
		  return false; // error occurred
	  }
}


template <typename PointNT> uint64_t
OPHflagArray<PointNT>::ownHashFunctionWithConversionWithLoop(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF){
  // version for cases in which distanceBase is a lot bigger than angleBase

  typename MinimalDiscretePF<PointNT>::featureEntryT d, a1, a2, a3;
  if (!neighbourPF){
	  d = sceneFeaturePtr->discretePF[0];
	  a1 = sceneFeaturePtr->discretePF[1];
	  a2 = sceneFeaturePtr->discretePF[2];
	  a3 = sceneFeaturePtr->discretePF[3];
  }
  else{
	  d = sceneFeaturePtr->discretePFneighbour[0];
  	  a1 = sceneFeaturePtr->discretePFneighbour[1];
  	  a2 = sceneFeaturePtr->discretePFneighbour[2];
  	  a3 = sceneFeaturePtr->discretePFneighbour[3];
  }

  // ��berf��hren des distance-Wertes aus seiner Basis in einen Wert der Basis der Winkel-Werte
  int modulus = 0;
  int numberWithDezimalBase = 0;
  int numberWithDistanceBase = d;
  for (int i = 0; i < maxpower; i ++){
      // convert to angle base
      modulus = numberWithDistanceBase / pow(angleBase,maxpower-i);
      numberWithDistanceBase = fmod(numberWithDistanceBase, (pow(angleBase,maxpower-i)));
#ifdef debug_level_III
      std::cout << "[hashTable::ownHashFunctionWithConversionWithLoop] The modulus is " << modulus << std::endl;
      std::cout << "[hashTable::ownHashFunctionWithConversionWithLoop] The remainder is " << numberWithDistanceBase << std::endl;
#endif
      // convert to dezimal base
      numberWithDezimalBase = numberWithDezimalBase + (modulus * pow(angleBase,maxpower-i+3));
#ifdef debug_level_III
      std::cout << "[hashTable::ownHashFunctionWithConversionWithLoop] The number in dezimal base is " << numberWithDezimalBase << std::endl;
#endif
  }
  //  Subtrahieren von Werten kleiner gleich PFF(1,0,0,0),      Das entspricht der Subtraktion von (1,0,0,0)
  //  da Distanzen d=0 nicht m��glich sind.                                   |
  numberWithDezimalBase = numberWithDezimalBase + ((numberWithDistanceBase - 1) *pow(angleBase,3));

  // generell sollte hier noch ein Logik-Check hin, der ��berpr��ft, ob die Merkmalsauspr��gungen auch alle kleiner als ihre jeweilige Basis sind.
  // auch sollte ��berpr��ft werden, ob d >=0 bzw d<1 ist
  // Ich denke, das sollte lieber nicht hier gepr��ft werden, weil es nur unn��tig zeit frisst. d>0 sollte eigentlich immer gelten. oder immer sichergestellt werden.

  // ��berf��hren des PPFs -- nun komplett in Basis der Winkel-Werte ausgedr��ckt -- in einen Wert der Basis 10.
  uint64_t hashValue = a3 + a2*angleBase + a1*(pow(angleBase,2)) + numberWithDezimalBase ;

#ifdef debug_level_III
  //std::cout << "[hashTable::ownHashFunctionWithConversionWithLoop] Das von Xaver diskretisierte Feature sieht folgenderma��en aus: " << feature.getDiscreteFeatureAsInt() << std::endl;
  std::cout << "[hashTable::ownHashFunctionWithConversionWithLoop] Resolution of distanceBase is a lot bigger than of angleBase " << std::endl;
  std::cout << "[hashTable::ownHashFunctionWithConversionWithLoop] The Hash Value is " << hashValue << std::endl;
#endif

  return hashValue;
}


template <typename PointNT> uint64_t
OPHflagArray<PointNT>::ownHashFunctionWithConversion(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF){

  typename MinimalDiscretePF<PointNT>::featureEntryT d, a1, a2, a3;
  if (!neighbourPF){
	  d = sceneFeaturePtr->discretePF[0];
	  a1 = sceneFeaturePtr->discretePF[1];
	  a2 = sceneFeaturePtr->discretePF[2];
	  a3 = sceneFeaturePtr->discretePF[3];
  }
  else{
	  d = sceneFeaturePtr->discretePFneighbour[0];
	  a1 = sceneFeaturePtr->discretePFneighbour[1];
	  a2 = sceneFeaturePtr->discretePFneighbour[2];
	  a3 = sceneFeaturePtr->discretePFneighbour[3];
  }
  // ��berf��hren des distance-Wertes aus seiner Basis in einen Wert der Basis der Winkel-Werte
  int modulus = d / angleBase;
  int remainder = d % angleBase;

  // ��berf��hren des PPFs -- nun komplett in Basis der Winkel-Werte ausgedr��ckt -- in einen Wert der Basis 10.                                Das entspricht der Subtraktion von (1,0,0,0)
  // Subtrahieren von Werten kleiner gleich PFF(1,0,0,0), da Distanzen d=0 nicht m��glich sind.                                                                  |
  uint64_t hashValue = (a3 + a2*angleBase + a1*(pow(angleBase,2)) + (remainder - 1)*(pow(angleBase, 3)) + modulus*(pow(angleBase,4)) ); //- pow(angleBase,3) ;

#ifdef debug_level_III
  //std::cout << "[hashTable::ownHashFunctionWithConversion] Das von Xaver diskretisierte Feature sieht folgenderma��en aus: " << feature.getDiscreteFeatureAsInt() << std::endl;
  std::cout << "[hashTable::ownHashFunctionWithConversion] The modulus is " << modulus << std::endl;
  std::cout << "[hashTable::ownHashFunctionWithConversion] The remainder is " << remainder << std::endl;
  std::cout << "[hashTable::ownHashFunctionWithConversion] The Hash Value is " << hashValue << std::endl;
#endif

  return hashValue;
}

template <typename PointNT> uint64_t
OPHflagArray<PointNT>::ownHashFunctionWithoutConversion(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF){
  // version for the case in which the resolution of the distance is <= resolution of angle:
  // distanceBase <= angleBase
  // therefore no conversion from distanceBase to angleBase is needed.

  typename MinimalDiscretePF<PointNT>::featureEntryT d, a1, a2, a3;
  if (!neighbourPF){
	  d = sceneFeaturePtr->discretePF[0];
	  a1 = sceneFeaturePtr->discretePF[1];
	  a2 = sceneFeaturePtr->discretePF[2];
	  a3 = sceneFeaturePtr->discretePF[3];
  }
  else{
	  d = sceneFeaturePtr->discretePFneighbour[0];
	  a1 = sceneFeaturePtr->discretePFneighbour[1];
	  a2 = sceneFeaturePtr->discretePFneighbour[2];
	  a3 = sceneFeaturePtr->discretePFneighbour[3];
  }

  // ��berf��hren des PPFs -- nun komplett in Basis der Winkel-Werte ausgedr��ckt -- in einen Wert der Basis 10.
  // Subtrahieren von Werten kleiner gleich PFF(1,0,0,0), da Distanzen d=0 nicht m��glich sind.
 uint64_t hashValue = (a3 + a2*angleBase + a1*(pow(angleBase,2)) + (d - 1)*(pow(angleBase, 3))  ); // (d-1) entspricht der Subtraktion von (1,0,0,0)

#ifdef debug_level_III
  //std::cout << "[hashTable::ownHashFunctionWithoutConversion] Das von Xaver diskretisierte Feature sieht folgenderma��en aus: " << feature.getDiscreteFeatureAsInt() << std::endl;
  std::cout << "[hashTable::ownHashFunctionWithoutConversion] Resolution of the distance is <= resolution of angle " << std::endl;
  std::cout << "[hashTable::ownHashFunctionWithoutConversion] The Hash Value is " << hashValue << std::endl;
#endif

  return hashValue;
}


template <typename PointNT> uint64_t
OPHflagArray<PointNT>::ownHashFunctionWithoutConversion_visCon(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF){
    // distanceBase <= angleBase, that means, there is no transformation needed to express d in angleBase.

	// downcasting necessary
	VisibilityContextDiscretePF<PointNT>* sceneFeaturePtr_casted = dynamic_cast<VisibilityContextDiscretePF<PointNT>* >(sceneFeaturePtr);
	typename VisibilityContextDiscretePF<PointNT>::featureEntryT d, a1, a2, a3, vc1, vc2, vc3, vc4;
	  if (!neighbourPF){
		  d = sceneFeaturePtr_casted->discretePF[0];
		  a1 = sceneFeaturePtr_casted->discretePF[1];
		  a2 = sceneFeaturePtr_casted->discretePF[2];
		  a3 = sceneFeaturePtr_casted->discretePF[3];
		  vc1 = sceneFeaturePtr_casted->discreteVC[0];
		  vc2 = sceneFeaturePtr_casted->discreteVC[1];
		  vc3 = sceneFeaturePtr_casted->discreteVC[2];
		  vc4 = sceneFeaturePtr_casted->discreteVC[3];
	  }
	  else{
		  d = sceneFeaturePtr_casted->discretePFneighbour[0];
		  a1 = sceneFeaturePtr_casted->discretePFneighbour[1];
		  a2 = sceneFeaturePtr_casted->discretePFneighbour[2];
		  a3 = sceneFeaturePtr_casted->discretePFneighbour[3];
		  vc1 = sceneFeaturePtr_casted->discreteVCneighbour[0];
		  vc2 = sceneFeaturePtr_casted->discreteVCneighbour[1];
		  vc3 = sceneFeaturePtr_casted->discreteVCneighbour[2];
		  vc4 = sceneFeaturePtr_casted->discreteVCneighbour[3];
	  }

    // 1. transform PPF (d, angle_1, angle_2, angle_3) into number with dezimal base
    unsigned int PPFinDezimalBase = (d-1)*pow(angleBase,3)
                                  +  a1*pow(angleBase,2)
                                  +  a2*angleBase
                                  +  a3;
    //2. transform PPF into number with visconBase and directly into number with dezimal base:
    int power = 4;
    uint64_t hashValue = 0;
    do {
            // transform first into visconBase and then into dezimal base
            hashValue = hashValue + PPFinDezimalBase % visconBase * pow(visconBase, power);
            //                           -> number in visconBase   -> number in dezimal base
            power++;
            PPFinDezimalBase = PPFinDezimalBase / visconBase;
    }while(PPFinDezimalBase != 0);

    // 3. calculate hash value, which is the whole feature vector (PPF|VisCon) in dezimal base
    hashValue = hashValue + vc1*pow(visconBase, 3)
                          + vc2*pow(visconBase, 2)
                          + vc3*visconBase
                          + vc4;

#ifdef debug_level_III
  std::cout << "[hashTable::ownHashFunctionWithoutConversion_II] Resolution of the distance is <= resolution of angle " << std::endl;
  std::cout << "[hashTable::ownHashFunctionWithoutConversion_II] The Hash Value is " << hashValue << std::endl;
#endif

    return hashValue;
}

template <typename PointNT> uint64_t
OPHflagArray<PointNT>::ownHashFunctionWithConversion_visCon(MinimalDiscretePF<PointNT>* sceneFeaturePtr, bool neighbourPF){
    // distanceBase > angleBase, that means, there is a transformation needed to express d in angleBase.

	// downcasting necessary
	VisibilityContextDiscretePF<PointNT>* sceneFeaturePtr_casted = dynamic_cast<VisibilityContextDiscretePF<PointNT>* >(sceneFeaturePtr);
	typename VisibilityContextDiscretePF<PointNT>::featureEntryT d, a1, a2, a3, vc1, vc2, vc3, vc4;
	  if (!neighbourPF){
		  d = sceneFeaturePtr_casted->discretePF[0];
		  a1 = sceneFeaturePtr_casted->discretePF[1];
		  a2 = sceneFeaturePtr_casted->discretePF[2];
		  a3 = sceneFeaturePtr_casted->discretePF[3];
		  vc1 = sceneFeaturePtr_casted->discreteVC[0];
		  vc2 = sceneFeaturePtr_casted->discreteVC[1];
		  vc3 = sceneFeaturePtr_casted->discreteVC[2];
		  vc4 = sceneFeaturePtr_casted->discreteVC[3];
	  }
	  else{
		  d = sceneFeaturePtr_casted->discretePFneighbour[0];
		  a1 = sceneFeaturePtr_casted->discretePFneighbour[1];
		  a2 = sceneFeaturePtr_casted->discretePFneighbour[2];
		  a3 = sceneFeaturePtr_casted->discretePFneighbour[3];
		  vc1 = sceneFeaturePtr_casted->discreteVCneighbour[0];
		  vc2 = sceneFeaturePtr_casted->discreteVCneighbour[1];
		  vc3 = sceneFeaturePtr_casted->discreteVCneighbour[2];
		  vc4 = sceneFeaturePtr_casted->discreteVCneighbour[3];
	  }

    // 1. transform distance d into a number with angleBase and then directly into number in dezimal base
    unsigned int distanceInDezimalBase = d-1; //!< substract 1 to take into account, that there won't be PPFs with d=0
    unsigned int PPFinDezimalBase = 0;
    int power = 3; //!< because there are three angles
    do{
            PPFinDezimalBase = PPFinDezimalBase + distanceInDezimalBase % angleBase * pow(angleBase, power);
            power++;
            distanceInDezimalBase = distanceInDezimalBase / angleBase;
        }while(distanceInDezimalBase != 0);

    // 2. transform PPF (d, angle_1, angle_2, angle_3) into number with dezimal base
    PPFinDezimalBase = PPFinDezimalBase + a1*pow(angleBase, 2)
                                        + a2*angleBase
                                        + a3;

    //3. transform PPF into number with visconBase and then directly into number with dezimal base:
    power = 4;
    unsigned long int hashValue = 0;
    do {
            hashValue = hashValue + PPFinDezimalBase % visconBase * pow(visconBase, power);
            power++;
            PPFinDezimalBase = PPFinDezimalBase / visconBase;
    }while(PPFinDezimalBase != 0);

    // 4. calculate hash value, which is the whole feature vector (PPF|VisCon) in dezimal base
    hashValue = hashValue + vc1*pow(visconBase, 3)
                          + vc2*pow(visconBase, 2)
                          + vc3*visconBase
                          + vc4;
#ifdef debug_level_III
  std::cout << "[hashTable::ownHashFunctionWithConversion_II] Resolution of the distance is > resolution of angle " << std::endl;
  std::cout << "[hashTable::ownHashFunctionWithConversion_II] The Hash Value is " << hashValue << std::endl;
#endif

    return hashValue;
}

template <typename PointNT> void
OPHflagArray<PointNT>::setParameters(modelHashTable<PointNT>*  modelHashTablePtr){

	angleBase = round(M_PI/modelHashTablePtr->get_d_angle()) + 1; 									// d_angle is 0.05 * pi (= 9��) by default --> gives us 20+1 angle feature states [0,1, ..., 20]
	//          ^^^^^ this is essential, otherwise we will not get 21, but only 20 statuses!!
	distanceBase = round(modelHashTablePtr->get_modelDiameter()/modelHashTablePtr->get_d_dist_abs()) + 1; 	// distance feature states [0, 1, ... 20] for default d_dist_abs = 0.05*modelDiameter

	ROS_WARN_STREAM("[OPHflagArray::setParameters] The feature-bases are: d_dist -> " << distanceBase << " d_angle -> " << angleBase );

	if (modelHashTablePtr->getHashTableType() == 2 | modelHashTablePtr->getHashTableType() == 4){
		visibilityContextModelHashTable<PointNT>* modelHashTablePtr_casted = dynamic_cast<visibilityContextModelHashTable<PointNT>* >(modelHashTablePtr);
		visconBase = ceilf(modelHashTablePtr->get_modelDiameter()/(modelHashTablePtr_casted->get_d_VisCon_abs())) + 1;
	}
	else {
		visconBase = 0;
	}

	// get max. power
	float p = distanceBase/angleBase;
	maxpower = 1;
	while(p > angleBase){
		p = p/angleBase;
		maxpower ++;
	}
#ifdef debug_level_III
	std::cout << "[OPHflagArray::init] maxpower is "<< maxpower << std::endl;
#endif
}


} // namespace pf_matching

