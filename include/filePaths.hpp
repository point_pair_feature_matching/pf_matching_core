/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 03/02/16
 * @file filePaths.hpp
 * @brief Functions for setting / checking file paths
 */

#pragma once

// boost
#include <string>


namespace pf_matching{
namespace tools{

/**
 * @brief format the given path in a consistent way (remove trailing '/') and ensure that it
 *        exists (create if required)
 * @param path the directory path, empty paths are returned right away
 * @return the formatted directory path or "" if the path does not exist and can't be created
 */
std::string formatPathAndCreate(std::string path);

} // end namespace
} // end namespace