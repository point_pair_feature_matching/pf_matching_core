/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Franz Ziegler (MFZ)
 * @date 08.04.2019
 * @file pairFeatures.hpp
 * @brief Definition of pair features
 */

#pragma once

// eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

// PCL
#include <pcl/octree/octree_search.h> 		// for doing ray-tracing within an octree
#include <pcl/octree/octree_pointcloud.h>  	// for generating an octree of a pointcloud
#include <pcl/visualization/pcl_visualizer.h>

// STL
#include <string>
#include <iostream>
#include <utility>		// for std::pair
#include <functional>	// for std::pair

namespace pf_matching{


// forward declaration
template <typename PointNT> class modelHashTable;


/**
 * @brief calculate the angle in the range of [0, pi] (vectors do NOT have to be normalized)
 *
 * @param v0 first vector
 * @param v1 second vector
 * @return angle between vectors in rad
 */
float vectorAngle(const Eigen::Vector3f& v0,
                  const Eigen::Vector3f& v1);

/**
 * @brief Compute a transformation that aligns an oriented point with the origin and the x-axis
 * 
 * The point is transformed in the following way:
 *  * p_r is moved to the origin
 *  * n_r is aligned with the x-axis
 * 
 * @param p_r coordinates of the reference point
 * @param n_r normal direction of the reference point (does NOT have to be normalized)
 * 
 * @return transformaion as described above
 */
 Eigen::Affine3f aligningTransform(const Eigen::Vector3f& p_r,
                                   const Eigen::Vector3f& n_r);


/**
 * @class ModelPFinfo
 * @brief basic information about a pair feature that are required to build a model description
 */
struct ModelPFinfo{
    float alpha;                //!< angle for rotation around x-axis for feature alignment
     	 	 	 	 	 	 	// TODO: MFZ 30.03.2018 if possible, reduce the size ( float has 32 bit)
    uint16_t refPointIndex; //!< index of reference point that was used to construct the feature
    							// TODO: MFZ 30.03.2018 if possible, reduce the size ( unsigned int has 32 bit --> reduce to 16 bit = 65536 points)
    float weight;               //!< voting weight of feature

    /**
     * @brief construct by copying values, see members for parameter description
     * @param alpha_ alpha
     * @param refPointIndex_ index of reference point
     * @param weight_ weight of feature
     * @return 
     */
    ModelPFinfo(float& alpha_, unsigned int& refPointIndex_, float weight_ = 1);
    
    /**
     * @brief overload for stream output
     */
    friend std::ostream& operator<<(std::ostream& stream, const pf_matching::ModelPFinfo info);
};


/**
 * @class MinimalDiscretePF
 * @brief minimal information representing a point feature that has already been hashed
 * 
 * Quantization parameters are not stored in the class for efficiency reasons. You have to keep
 * track of them yourself if you need them later.
 */
template <typename PointNT>
class MinimalDiscretePF{
    
    public:

		// shared pointer aliases
		typedef boost::shared_ptr<MinimalDiscretePF<PointNT>> Ptr; 			//!< shared pointer definition for convenience

        typedef uint16_t featureEntryT;   //!< type for quantized value for single feature vector entry
        typedef uint64_t featureVectorT;  //!< type for representing whole discrete feature as int
        typedef uint64_t featureHashKeyT; //!< type for representing the hash key of a feature
    
        featureEntryT discretePF[4];			//!< the discrete value of the feature vector as multiples of d_dist or d_angle
         	 	 	 	 	 	 	 	 	 	//!  |d|, angle(n_r, d), angle(n_t, d), angle(n_r, n_t)
        featureEntryT discretePFneighbour[4];	//!< anti-noise method from [Hinterstoisser et al. 2016]

        float alpha;                      //!< angle for rotation around x-axis for feature alignment, [-pi,pi]

        // feature construction parameters
		float d_dist_abs;
		float d_angle;
        

		/**
		 * @brief construct uninitialized storage
		 */
		MinimalDiscretePF(){
		}

        /**
         * @brief construct by copying parameters from hash table, but with empty feature
         */
        MinimalDiscretePF(modelHashTable<PointNT>* hashTablePtr);
        
        /**
         * @brief construct by copying values
         * @param discreteFeature_ discrete feature vector
         * @param alpha_ value for alpha
         */
        MinimalDiscretePF(featureEntryT discretePF_[4], float alpha_);
        
        /**
         * @brief construct by copying values
         * @param intFeatureRepresentation discrete feature vector represented as one int
         * @param alpha_ value for alpha
         */
        MinimalDiscretePF(featureVectorT intFeatureRepresentation, float alpha_);
        
        virtual ~MinimalDiscretePF(){};

		virtual bool setParameters(modelHashTable<PointNT>* hashTablePtr);

        /**
         * @brief calculate from pre-calculated continuous feature vector
         * @param continousFeature not quantized feature
         * @param alpha_ continuous angle for rotation around x-axis for feature alignment
         * @param d_dist quantization steps for d-value
         * @param d_angle quantization steps for alpha in rad
         * @return true if no overflow occurred during quantization, else false
         */
        bool calculate(float continousFeature[4], const float& alpha_);

        /**
         * @brief calculate from Eigen-style data.
         * 
         * @note All passed point data is assumed to be valid (i.e. underlying cloud has is_dense == true).
         *       NaN-values in one or more of the points or normals will lead to NaN-values in the
         *       feature vector.
         * 
         * @note Identical point locations will also result in NaNs in the feature vector.
         * 
         * @param p_r reference point
         * @param n_r normal at reference point (does NOT need to be normalized)
         * @param p_t target point
         * @param n_t normal at reference point (does NOT need to be normalized)
         * @param p_r_aligningTransform rigid body transformation that aligns the reference point
         *                              and its normal with the origin and the x-axis
         * @return true if no overflow occured during quantization, else false
         */
        bool calculate(const Eigen::Vector3f& p_r, const Eigen::Vector3f& n_r, 
                       const Eigen::Vector3f& p_t, const Eigen::Vector3f& n_t,
                       const Eigen::Affine3f& p_r_aligningTransform);
        
		/**
		 * @brief Method as proposed by [Hinterstoi��er et al 2016]:
		 * 		  Calculating the 80 neighbour-PPFs of the reference PPF
		 * @param[in] i
		 * @param[in] j
		 * @param[in] k
		 * @param[in] l
		 * @return true if calculated neighbour is valid
		 */
		bool calculateNeighbourFeature(int i, int j, int k, int l);

        /**
         * @brief Convert the int-represented quantized feature vector back to human readable. Note that this will
         *        give you the quantized values.
         * @param[out] feature storage to write results to
         */
        void getHumanReadableFeature(float feature[4]);
        
        /**
         * @brief return the discrete feature vector interpreted as a single integer
         * @return 
         */
        virtual featureHashKeyT getDiscreteFeatureAsInt();
        
        /**
         * @brief return the discrete neighbour feature vector interpreted as a single integer
         * @return
         */
        virtual featureHashKeyT getDiscreteNeighbourFeatureAsInt();

        friend std::ostream& operator << (std::ostream& stream, MinimalDiscretePF<PointNT>& feature){
			stream << "Quantized feature: ("<< unsigned(feature.discretePF[0]) << ", "
											<< unsigned(feature.discretePF[1]) << ", "
											<< unsigned(feature.discretePF[2]) << ", "
											<< unsigned(feature.discretePF[3]) << ")\n";
			return stream;
		}

    private:
    
        /**
         * @brief compute a discrete point feature and store it in the member variable discreteFeature
         * 
         * @note NaN values get mapped to 0, overflowing d get mapped to max value of featureEntryT
         * 
         * @param[in] continousFeature the original feature
         * @return false if overflow in d occurred, else true
         */
        bool quantize(float continousFeature[4]);

    protected:

        /**
		 *
		 * @param angle
		 * @return
		 */
		float mapNeighbourAngle(const float& angle);

		/**
		 *
		 * @param value
		 * @return
		 */
		virtual bool invalidDistance(const float& distance);

		/**
		 *
		 * @param angle
		 * @return false if angle is valid
		 */
		virtual bool invalidAngle(const float& angle);
};

// ========================================================================================================
// ======================= extended MinimalDiscretePF class for visibility context ========================
// ========================================================================================================


/**
 * @class VisibilityContextDiscretePF
 * @brief Child of MinimalDiscretePF, supports Visibility Context for S2S features as proposed in [Kim and Medioni 2011]
 */
template <typename PointNT>
class VisibilityContextDiscretePF : public MinimalDiscretePF<PointNT>{
	public:

		// shared pointer aliases
	    typedef boost::shared_ptr<VisibilityContextDiscretePF<PointNT>> Ptr; 			//!< shared pointer definition for convenience

		typedef uint8_t featureEntryT; 				 				//!< type for quantized value for single feature vector entry
		typedef uint32_t featureVectorT;  			 				//!< type for representing a discrete feature (PF or VisCon) as int
		using typename MinimalDiscretePF<PointNT>::featureHashKeyT; //!< type for representing the hash key of a feature (PF+VisCon)

		featureEntryT discretePF[4]; 		  //!< the discrete value of the feature vector as multiples of d_dist or d_angle
	                                          //!  |d|, angle(n_r, d), angle(n_t, d), angle(n_r, n_t)
		featureEntryT discreteVC[4]; 		  //!< the discrete value of the visibility feature as multiples of d_VisCon_abs
		 	 	 	 	 	 	 			  //!  V_out_0, V_out_1, V_out_2, V_out_3
		featureEntryT discretePFneighbour[4]; //!< anti-noise method from [Hinterstoisser et al. 2016]
		featureEntryT discreteVCneighbour[4]; //!< anti-noise method extended to include visibility context features from [Kim and Medioni 2011]

		// Hinterstoisser methods parameters
		bool useVotingBalls = false; 		//!< for handling overflow checks during quantize()

		// Visibility Context: feature construction parameters
		float modelDiameter;
		float d_VisCon_abs;
		float voxelSize_intersectDetect; 		 				//!< MFZ 12/09/17 voxel size for intersection detection (visibility context lengths)
		float ignoreFactor_intersectClassific; 	 				//!< MFZ 12/09/17 factor for intersection detection (visibility context lengths). intersections within (factor * voxelDiameter) will be ignored.
		float gapSizeFactor_allCases_intersectClassific; 		//!< MFZ 12/04/18 Factor for intersection detection: Closing gaps of size (factor * voxelDiameter) between intersected voxel centers regardless of ray-surface-intersection-case.
		float gapSizeFactor_surfaceCase_intersectClassific;  	//!< MFZ 12/04/18 Factor for intersection detection: Closing gaps of size (factor * voxelDiameter) between intersected voxel centers in case 'ray on surface'. This requires extra calculations. Value = 0 deactivates feature.
		float alongSurfaceThreshold_intersectClassific;			//!< MFZ 11/10/17 Threshold for intersection detection in [degrees]. Tolerance for classifying a ray as _|_ to surface-NORMAL.
		float othorgonalToSurfaceThreshold_intersectClassific; 	//!< MFZ 11/10/17 Threshold for intersection detection in [degrees]. Tolerance for classifying a ray as || to surface-NORMAL.
		bool advancedIntersectionClassification; 				//!< MFZ 11/10/17 Toggle advanced classification of the intersected voxels. Might result in more precise V_out features.
		bool visualizeVisibilityContextFeature;  				//!< MFZ 11/10/17 Toggle PCLVisualizer on/off to show visibility context feature

		// visibility context: using search-commands of OctreePointCloudSearch for feature construction
		typename pcl::octree::OctreePointCloudSearch<PointNT>::Ptr octreeSearchPtr; //!< MFZ 13/09/17 shared pointer to octree for visibility context ray casting and other octree based searching

		typename pcl::octree::OctreePointCloud<PointNT>::PointCloudConstPtr cloudPtr;

		VisibilityContextDiscretePF(modelHashTable<PointNT>* hashTable );

		VisibilityContextDiscretePF(): MinimalDiscretePF<PointNT>(){
			// do nothing
		}

		/**
		 * @brief construct by copying values
		 * @param discreteFeature_ discrete feature vector
		 * @param alpha_ value for alpha
		 */
		VisibilityContextDiscretePF(featureEntryT discretePF_[4], featureEntryT discreteVC_[4], float alpha_);

		/**
		 * @brief construct by copying values
		 * @param intFeatureRepresentation discrete feature vector represented as one int
		 * @param alpha_ value for alpha
		 */
		VisibilityContextDiscretePF(featureHashKeyT intFeatureRepresentation, float alpha_);

		/**
		 *
		 * @param hashTable
		 * @return true if parameters are valid
		 */
		bool setParameters(modelHashTable<PointNT>* hashTablePtr );

		/**
         * @brief calculate PPF and Visibility Context from Eigen-style data.
         *
         * @note All passed point data is assumed to be valid (i.e. underlying cloud has is_dense == true).
         *       NaN-values in one or more of the points or normals will lead to NaN-values in the
         *       feature vector.
         *
         * @note Identical point locations will also result in NaNs in the feature vector.
         *
         * @param p_r reference point
         * @param n_r normal at reference point (does NOT need to be normalized)
         * @param p_t target point
         * @param n_t normal at reference point (does NOT need to be normalized)
         * @param p_r_aligningTransform rigid body transformation that aligns the reference point
         *                              and its normal with the origin and the x-axis
         * @return true if no overflow occurred during quantization, else false
         */
		bool calculateForTraining(const Eigen::Vector3f& p_r, const Eigen::Vector3f& n_r,
							      const Eigen::Vector3f& p_t, const Eigen::Vector3f& n_t,
								  const Eigen::Affine3f& p_r_aligningTransform,
								  const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer = nullptr );

		bool calculateForMatching(const Eigen::Vector3f& p_r, const Eigen::Vector3f& n_r,
								  const Eigen::Vector3f& p_t, const Eigen::Vector3f& n_t,
								  const Eigen::Affine3f& p_r_aligningTransform,
								  const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
								  const std::vector<std::pair<int,double>> &countVoutZero);

		/**
		 * @brief enabling access to the calculateNeighbourFeature() member function of the base class
		 */
		using MinimalDiscretePF<PointNT>::calculateNeighbourFeature;

		/**
		 * @brief Method-Extension for VisibilityContext Feature
		 * 		  Calculating the 6560 neighbour-PPFs of the reference PPF
		 * @param[in] i
		 * @param[in] j
		 * @param[in] k
		 * @param[in] l
		 * @param[in] m
		 * @param[in] n
		 * @param[in] o
		 * @param[in] p
		 * @return true if calculated neighbour is valid
		 */
		bool calculateNeighbourFeature(int i, int j, int k, int l, int m, int n, int o, int p);

		/**
		 * @brief transform the discretePFfeature- and discreteVCfeature-members into an integer
		 */
		featureHashKeyT getDiscreteFeatureAsInt();

		/**
		 * @brief transform the discretePFfeature- and discreteVCfeature-members into an integer
		 * @return integer that is the key for the hash table/hash function. NOT the hash value! Hash key is transformed by the hash function into the hash value
		 */
		featureHashKeyT getDiscreteNeighbourFeatureAsInt();

		friend std::ostream& operator << (std::ostream& stream, VisibilityContextDiscretePF<PointNT>& feature){
			stream << "Quantized feature: ("<< unsigned(feature.discretePFfeature[0]) << ", "
											<< unsigned(feature.discretePFfeature[1]) << ", "
											<< unsigned(feature.discretePFfeature[2]) << ", "
											<< unsigned(feature.discretePFfeature[3]) << " | "
											<< unsigned(feature.discreteVCfeature[0]) << ", "
											<< unsigned(feature.discreteVCfeature[1]) << ", "
											<< unsigned(feature.discreteVCfeature[2]) << ", "
											<< unsigned(feature.discreteVCfeature[3]) << ")\n";
			return stream;
		}


	private:

		/**
		 * @brief calculate the intersected voxels of a casted ray
		 *
		 * @details casts a ray from the given origin into the given direction. on its way, the ray intersects whith the
		 * 			voxels of the given octree. the voxel centers of the intersected voxels are stored in the submitted shared pointer
		 *
		 * @param[in] origin origin of the ray
		 * @param[in] direction direction of the ray
		 * @param[in] octreeSearchPtr octree/voxelgrid of the point cloud one would like to check for ray intersections
		 * @param[out] voxelCentersPtr cloud that contains points, which represent those voxel centers whose voxels were intersected
		 * @param[in] label name of the direction (for debugging)
		 */
		void calculateIntersectedVoxelCenters(	const Eigen::Vector3f& origin,
												const Eigen::Vector3f& direction,
												const typename pcl::PointCloud<PointNT>::Ptr& voxelCentersPtr,
												const std::string& label);

		/**
		 * @brief returns the centroid of a voxel, given a point within the voxel
		 * @details member that calculates the centroid of the
		 *          voxel by calulating the mean of all points, that
		 *          lay withing the voxel, that is indicated by searchPoint
		 * @param[in] octreeSearchPtr representing the point cloud
		 * @param[in] searchPoint point of point cloud, indicating a voxel
		 * @return centroid of voxel ( = mean of all points in
		 *         voxel indicated by searchPoint)
		 */
		Eigen::Vector3f getCentroidOfVoxelAsEigenVec3f( PointNT& searchPoint);

		/**
		 * @brief returns the centroid of a voxel, given a point within the voxel
		 * @details member that calculates the centroid of the
		 *          voxel by calulating the mean of all points, that
		 *          lay withing the voxel, that is indicated by searchPoint
		 * @param[in] octreeSearchPtr representing the point cloud
		 * @param[in] searchPoint point of point cloud, indicating a voxel
		 * @return centroid of voxel ( = mean of all points in
		 *         voxel indicated by searchPoint)
		 */
		PointNT getCentroidOfVoxelAsPLCPointT( PointNT& searchPoint);

		/**
		 * @brief returns the nearest normal for searchPoint
		 * @details meant for use with getCentroidOfVoxel()
		 * 			searches for the nearest neighbour of the
		 * 			searchPoint and returns its normal
		 * @note no normal estimation takes place
		 * @param[in] octreeSearchPtr representing the point cloud
		 * @param[in] searchPoint point of point cloud for which the nearest surface normal has to be found
		 * @return normal of the point, which is the nearest neighbour of searchPoint
		 */
		Eigen::Vector3f	getNormalOfCentroidAsEigenVec3f ( PointNT& searchPoint);

		/**
		 * @brief returns the nearest normal for searchPoint
		 * @details meant for use with getCentroidOfVoxel()
		 * 			searches for the nearest neighbour of the
		 * 			searchPoint and returns its normal
		 * @note no normal estimation takes place
		 * @param[in] octreeSearchPtr representing the point cloud
		 * @param[in] searchPoint point of point cloud for which the nearest surface normal has to be found
		 * @return normal of the point, which is the nearest neighbour of searchPoint
		 */
		PointNT	getNormalOfCentroidAsPCLPointT ( PointNT& searchPoint);

		/**
		 * @brief categorizes an input angle into one of the three
		 * 		  angle-classes 0��, 45�� and 90�� and increments the
		 * 		  counter at the according place in the array
		 * @note thresholds for classification are class members
		 * @note array[0] = counter for 0��, array[1] = counter for 45��, array[2] = counter for 90��
		 * @param[in] angle to be classified
		 * @param[in] array of angle-classes
		 */
		void incrementCounter( float& angle,
		 	 	 	    	   int array[3] );

		/**
		 * @brief categorizes an input angle into one of the three
		 * 		  angle-classes 0��, 45�� and 90�� and increments the
		 * 		  counter at the according place in the array
		 * @note thresholds for classification are class members
		 * @note array[0] = counter for 0��, array[1] = counter for 45��, array[2] = counter for 90��
		 * @param[in] angle to be classified
		 * @param[in] array of angle-classes
		 */
		void decrementCounter( float& angle,
							   int array[3] );

		/**
		 * @brief checks if the angle falls into the category of a 90�� intersection of V_out and the surface-normal
		 * @note threshold for classification is class member
		 * @param[in] angle to get checked
		 * @return true if input-angle falls into the 90�� class (with tolerance threshold)
		 */
		bool angleEquals90Deg( float angle );

		/**
		 * @brief finds the point with the minimal distance to V_out within
		 *        the voxel specified by searchPoint
		 * @details projects the vector (point-referredPoint) onto the
		 * 			vector of (V_out-direction-referredPoint).
		 * 			The distance between both vectors is calculated for
		 * 			all points of the voxel indicated by the searchPoint.
		 * 			The point with the minimal distance is retrieved.
		 * @note the nearest point is the one with the highest probability to
		 *       be the true point of intersection of V_out with the surface.
		 * @param[in] octreeSearchPtr of the base point cloud
		 * @param[in] referredPoint of the PPF
		 * @param[in] VoutDirection of the visibility context
		 * @param[in] searchPoint point indicating the voxel within which the nearest point has to be found
		 * @return pair of nearest point in the voxel and the measured distance to V_out
		 */
		std::pair<Eigen::Vector3f,float> PointWithMinimalDistance ( const Eigen::Vector3f& referredPoint,
																	const Eigen::Vector3f& VoutDirection,
																	const PointNT& searchPoint );

		/**
		 * @brief calculates the length of the specified V_out-direction
		 * @param[in] referredPoint of the PPF
		 * @param[in] VoutDirection of the visibility context
		 * @param[in] octreeSearchPtr of the base point cloud
		 * @param[in] voxelCentersPtr vector of the voxel centers intersected by the V_out ray
		 * @param[in] label Name of the V_out-direction for the visualizer
		 * @param[in] factor * voxel-diameter is the area arround referredPoint within which intersections are ignored
		 * @param[in] visualizationRequired flag for visualizing the PPF+Visibility Context
		 * @return V_out-value in the specified V_out-direction. Returns -1 if V_out-value > modelDiameter, PF is invalid then
		 */
		float getDistanceForVout(const Eigen::Vector3f& referredPoint,
								 const Eigen::Vector3f& VoutDirection,
								 const typename pcl::PointCloud<PointNT>::Ptr& voxelCentersPtr,
								 const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
								 const std::string& label);


        /**
         * @brief compute a discrete point feature + its visibility context and store it in the member variables
         *
         * @note NaN values get mapped to 0, overflowing d get mapped to max value of featureEntryT
         *
         * @param[in] continousPFfeature the original PFF
         * @param[in] continousVCfearure the original Visibility Context lengths
         * @param[in] d_dist quantization step for distance
         * @param[in] d_angle quantization step for angle
         * @param[in] d_VisCon_abs quantization step for Visibility Context lengths
         * @return false if overflow in feature occurred, else true
         */
		bool quantize(float continousPFfeature[4],
			      	  float continousVCfeature[4] );

		/**
		 * @brief check if quantization resulted in an overflow
		 * @param[in] value Quantized value to check for overflow
		 * @return returns a pair. first entry is the quantized value, second entry is a flag. flag = true if overflow occurred.
		 */
		std::pair<featureEntryT, bool> checkOverflow(const float& value);


		/**
		 *
		 * @param distance
		 * @return
		 */
		bool invalidVout(const float& distance);

		bool invalidAngle(const float& angle);

		bool invalidDistance(const float& distance);

		// TODO: 01/06/2018 I still don't get it, why I have to make these memberfunctions here visible: Shouldn't they be accessible without that?
		// same thing with the "alpha" member variable, and all the others.

		using MinimalDiscretePF<PointNT>::mapNeighbourAngle;



};

} // end namespace pf_matching

#include "./impl/pairFeaturesInline.hpp"
//#include "../src/pairFeatures.cpp"
