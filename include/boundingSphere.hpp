/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Ziegler
 * @date 08.04.2019
 * @file boundingSphere.hpp
 * @brief Function templates for fitting minimal or thight fitting approximate bounding spheres
 * around point clouds. + class to estimate bounding box from an octree of the model point cloud (octree is used anyway)
 */

#pragma once

// PCL
#include <pcl/point_types.h>
#include <pcl/point_cloud.h> //PointCloud template
#include <pcl/common/geometry.h>
#include <pcl/common/norms.h> 
#include <pcl/octree/octree_search.h> 		// MFZ 29/03/18 getting bounding box of an pcl octree
#include <pcl/octree/octree_pointcloud.h>  	// MFZ 29/03/18 getting bounding box of an pcl octree


#include <algorithm> // for find operations on vector


namespace pf_matching{
    
namespace tools{


    
/**
 * @brief Compute a sphere so that all 4 point lie on the sphere boundary.
 * 
 * Assumes that the points passed are valid (no Inf or NaN values). The code for sphere calculation
 * was taken from the implementation of
 * pcl::SampleConsensusModelSphere<PointT>::computeModelCoefficients in 
 * pcl-1.7/pcl/sample_consensus/impl/sac_model_sphere.hpp.
 * Input values and naming were slightly modified.
 * 
 * @param[in] cloud point cloud containing the 4 points
 * @param[in] indices indices of the 4 points to construct a sphere from
 * @param[out] boundingSphere parameters of the found sphere (center_x, center_y, center_z, radius)
 * @return true if a sphere was found, else false
 */
template <typename PointT> bool
sphereFrom4Points(const pcl::PointCloud<PointT>& cloud, const std::vector<int>& indices, Eigen::Vector4f& boundingSphere);


/**
 * @brief Compute a sphere so that all 3 point lie on the sphere boundary and the radius is minimal.
 *
 * Assumes that the points passed are valid (no Inf or NaN values).
 * The sphere center will lie on the plane defined by the 3 points. If the points are colinear
 * or two or more points are identical, no sphere will be calculated.
 * See http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/welzl-r2484
 * for the equations that are solved.
 * 
 * @param[in] cloud point cloud containing the 3 points
 * @param[in] indices of the 3 points to construct a sphere from
 * @param[out] sphereParameters parameters of the found sphere (center_x, center_y, center_z, radius)
 * @return true if a sphere was found, else false
 */
template <typename PointT> bool
sphereFrom3Points(const pcl::PointCloud<PointT>& cloud, const std::vector<int>& indices, Eigen::Vector4f& sphereParameters);


/**
 * @brief Compute a sphere so that both points lie on the sphere boundary and the radius is minimal.
 *
 * Assumes that the points passed are valid (no Inf or NaN values).
 * The resulting sphere is centered midway between the two points and has a radius of 0.5*distance.
 * For identical points, this results into a sphere of radius 0.
 * @param[in] cloud point cloud containing the 2 points
 * @param[in] indices of the 2 points to construct a sphere from
 * @param[out] sphereParameters parameters of the found sphere (center_x, center_y, center_z, radius)
 * @return true if a sphere was found, else false
 */
template <typename PointT> bool
sphereFrom2Points(const pcl::PointCloud<PointT>& cloud, const std::vector<int>& indices, Eigen::Vector4f& sphereParameters);


/**
 * @brief Find an approximate but tight fitting bounding sphere around a point cloud using the 
 *        EPOS6-Algorithm (Extremal Points optimal Sphere with 6 extremal points).
 * 
 * Used algorithm:
 * Larson, Fast and Tight Fitting Bounding Spheres
 * found at http://www.ep.liu.se/ecp/034/009/ecp083409.pdf
 * 
 * Very small test about performance:
 * Compared to the Welzl-algorithm, EPOS6 increases speed approx. 4-8 times while the radius usually
 * is < 5% bigger than the minimal bounding sphere. However, in bad cases some radii were up to
 * 15% bigger during testing.
 * 
 * @param[in] cloud point cloud to calculate the bounding sphere around
 * @param[out] sphereParameters parameters of the found sphere (center_x, center_y, center_z, radius)
 * @return true if a sphere was found, else false
 */
template <typename PointT> bool
epos6BoundingSphere(const pcl::PointCloud<PointT>& cloud, Eigen::Vector4f& sphereParameters);

/**
 * @brief Calculate the minimum bounding sphere around a point cloud, using the algorithm of Welzl.
 *        The function assumes that all passed points are valid (i.e. contain no NaN or Inf values).
 * 
 * Used algorithm:
 * Welzl, Smallest enclosing disks (balls and ellipsoids) 
 * "New Results and New Trends in Computer Science", (H. Maurer, Ed.),
 * Lecture Notes in Computer Science 555 (1991) 359-370.
 * 
 * Further references: http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/welzl-r2484
 * 
 * @param[in] cloud point cloud to calculate the bounding sphere around
 * @param[out] sphereParameters parameters of the found sphere (center_x, center_y, center_z, radius)
 * @return true if a sphere was found, else false
 */
template <typename PointT> bool
welzlBoundingSphere(const pcl::PointCloud<PointT>& cloud, Eigen::Vector4f& sphereParameters);

/**
 * @brief Calculate the minimum bounding sphere around a point cloud, using the algorithm of Welzl.
 *        The function assumes that all passed points are valid (i.e. contain no NaN or Inf values).
 * 
 * Used algorithm:
 * [Welzl91] - Smallest enclosing disks (balls and ellipsoids) 
 * "New Results and New Trends in Computer Science", (H. Maurer, Ed.),
 * Lecture Notes in Computer Science 555 (1991) 359-370.
 * 
 * Further references: http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/welzl-r2484
 * 
 * @param[in] cloud point cloud containning the points to calculate the bounding sphere around
 * @param[in] indices indices of the points to use for calculation
 * @param[out] sphereParameters parameters of the found sphere (center_x, center_y, center_z, radius)
 * @return true if a sphere was found, else false
 */
template <typename PointT> bool
welzlBoundingSphere(const pcl::PointCloud<PointT>& cloud,const std::vector<int>& indices, Eigen::Vector4f& sphereParameters);

/**
 * @brief a pcl octree builds a bounding box around a point cloud,
 * 		  this struct uses this information and processes them
 * 		  to provide the bounding box information in a more convenient form
 * @note this is not used in the current algorithm, because the bounding box
 *       seemed to have a non-deterministic behaviour.
 */
template <typename PointT>
struct octreeBoundingBox{

	float x_length;
	float y_breadth;
	float z_height;
	float sortedDimensions[3];
	octreeBoundingBox(const typename pcl::octree::OctreePointCloudSearch<PointT>::Ptr &octreeSearchPtr);

	void sortDimensions();
};

} // end namespace
} // end namespace

#include "./impl/boundingSphereImpl.hpp"
