/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Franz Ziegler
 * @date 15/01/16, 12/09/17
 * @file featureTypes.hpp
 * @brief definition of feature types and related checking functions
 */

#pragma once

#include <map>
#include <pcl/console/print.h>

namespace pf_matching{

//! Possible types of pair features as defined by Choi 2012: "Voting-based Pose Estimation for Robotic
//! Assembly Using a 3D Sensor" as bit-mask
//! MFZ: Added further type 'S2SVisCon' which was proposed in [Kim and Medioni 2011]
enum featureType{
    NONE = 0,      //!< no known feature
    S2S = 1 << 0,  //!< surface-to-surface feature
    B2B = 1 << 1,  //!< boundary-to-boundary feature (edge-to-edge)
    S2B = 1 << 2,  //!< surface-to-boundary feature (surface-to-edge)
    B2S = 1 << 3,  //!< boundary-to-surface feature (edge-to-surface)
    S2SVisCon = 1 << 4, //!< MFZ 12/09/2017 surface-to-surface feature extended by visibility context feature as proposed in [Kim and Medioni 2011]
    ALL_FEATURES = S2S | B2B | S2B | B2S //!< all known feature types
};

//! conversion map from feature type enums to human readable
static std::map< featureType, const char*> featureNames = {
   {S2S, "S2S"},
   {B2B, "B2B"},
   {S2B, "S2B"},
   {B2S, "B2S"},
   {S2SVisCon, "S2SVisCon"} //!< MFZ 12/09/2017 S2SVisCon added
};

enum modelHashTableType{
	MH_NONE = 0,		//!< no known model hash table type
	MH_STL = 1, 			//!< Use STL::unordered_multimap, a bucketized hash table. Use this with standard PF-matching. Faster, but significantly higher memory usage.
	MH_CMPH_CHD = 2		//!< Use CMPH_CHD minimal perfect hash function and a vector-contruct as table. Use this with Visibility Context and storing neighbour PPFs in model.
};

enum flagArrayHashTableType{
	FA_NONE = 0,			//!< Do not use flag array-extension during matching
	FA_STL = 1,			//!< Use STL::unordered_multimap, a bucketized hash table. add the bins on the fly. slow, not recommended.
	FA_STL_PREBUILT = 2,	//!< Use STL::unordered_multimap, a bucketized hash table, with pre-built bins. i.e. with bins for all model-PFs.
	 	 	 	 	 	//!  Should be fast when used for standard PF-matching, without visibility context and without storing neighbour PPFs in model.
	FA_CMPH_CHD = 3,		//!< Use CMPH_CHD minimal perfect hash function and an uint32_t array. Minimal memory usage.
	 	 	 	 	 	//!  Should be chosen, when using visibility context and storing neighbour PPFs in model.
	FA_OWN_PERFECT_HASHER = 4 //!< Fastest algorithm if used with standard PF-matching without Visibility Context. Needs the most memory space of all hash table types.
};

/**
 * @brief overload for "|" operator for type "featureType"
 * @param a left argument
 * @param b right argument
 * @return bitwise or
 */
inline featureType operator|(featureType a, featureType b)
    {return static_cast<featureType>(static_cast<int>(a) | static_cast<int>(b));}

/**
 * @brief overload for "&" operator for type "featureType"
 * @param a left argument
 * @param b right argument
 * @return bitwise or
 */
inline featureType operator&(featureType a, featureType b)
    {return static_cast<featureType>(static_cast<int>(a) & static_cast<int>(b));}

/**
 * @brief check if a feature or combination of features requires points from a surface cloud
 * @param f features to check
 * @return true if surface cloud is requierd, else false
 */
inline bool featureRequiresSurfaceCloud(featureType f){
    return (f & (S2S | S2B | B2S | S2SVisCon)); //!< MFZ 12/09/2017 S2SVisCon added
}

/**
 * @brief check if a feature or combination of features requires points from an edge cloud
 * @param f features to check
 * @return true if edge cloud is requierd, else false
 */
inline bool featureRequiresEdgeCloud(featureType f){
    return (f & (B2B | S2B | B2S));
}

/**
 * @brief check if a feature of this type may have ambigous normal directions for the reference point
 * @param f feature type to check
 * @return true if normals are ambigous or the feature type is unknown, false otherwise
 */
bool featureHasAmbiguousRefPointNormal(featureType f){
    if(f & (B2B | B2S)){
        return true;
    }
    else if(f & (S2S | S2B | S2SVisCon)){
        return false;
    }
    else{
        PCL_ERROR("[featureHasAmbigousRefPointNormal]: Feature type unknown. Returning 'true'.\n");
    }
}

/**
 * @brief check if a feature of this type may have ambigous normal directions for the target point
 * @param f feature type to check
 * @return true if normals are ambigous or the feature type is unknown, false otherwise
 */
bool featureHasAmbiguousTargetPointNormal(featureType f){
    if(f & (B2B | S2B)){
        return true;
    }
    else if(f & (S2S | B2S | S2SVisCon)){
        return false;
    }
    else{
        PCL_ERROR("[featureHasAmbigousTargetPointNormal]: Feature type unknown. Returning 'true'.\n");
    }
}

/**
 * @brief convert a feature type or multiple feature types to a string for output
 * @param fTypes bitfield with feature types
 * @return string representation
 */
std::string featureToStr(featureType fTypes){
    std::string output;
    
    for(auto& typeNamePair : featureNames){
        if(fTypes & typeNamePair.first){
            if(!output.empty()){
                output.append(" | ");
            }
        output.append(typeNamePair.second);
        }
    }
    
    return output;
}

/**
 * @brief human readable console output of feature types
 */
inline std::ostream& operator<<(std::ostream& stream, featureType& fTypes){
    
    stream << featureToStr(fTypes);
    return stream;
}



} // end namespace
