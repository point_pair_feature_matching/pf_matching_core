/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 15/07/16
 * @file boundaryOrientation.hpp
 * @brief processing of boundary points
 */
 
 
#pragma once

// PCL
#include <pcl/kdtree/kdtree.h>

namespace pf_matching{
namespace extensions{

/**
 * @class BoundaryOrientation
 * @brief class for orienting the normals of boundary points into the direction of neighboring
 *        boundary points
 */
template<typename PointNT>
class BoundaryOrientation{
    
    public:

        // internal data types
        typedef pcl::PointCloud<PointNT> CloudNT;     //!< type of clouds to process
        typedef pcl::search::KdTree<PointNT> KdtreeT; //!< kdtree type

        /**
         * @brief set radius around point for nearest neighbour search
         * @param searchRadius_ search radius
         */
        void setRadiusSearch(float searchRadius_){
            searchRadius = searchRadius_;
        }

        /**
         * @brief orient the normal vectors of the given boundary points along the boundary
         * 
         * The boundary direction at a point is obtained by fitting a line through the nearest
         * neighbour locations using PCA. If no neighbours are found, the normal vector of the
         * point is set to NaN.
         * @param[in,out] boundaryPointsPtr point that will be re-oriented
         * @param[in,out] tree kdTree for nearest neighbour search, if an empty pointer is passed,
         *                a new tree will be built
         */
        void orientPoints(typename CloudNT::Ptr& boundaryPointsPtr,
                          typename KdtreeT::Ptr tree = NULL);
        
    protected:
    
        float searchRadius = -1;
        typename KdtreeT::Ptr tree;    //!< of input cloud, self-build or provided by user
        typename CloudNT::Ptr boundaryPointsPtr; //!< boundary points to orient
        
        /**
         * @brief orient the normal vector of a single boundary point in the boundary direction by
         *        via PCA of the nearest neighbour locations
         * @param boundaryPointsPtr cloud with boundary points
         * @param tree kdTree for nearest neighbour search
         * @param i_p index of point to orient
         */
        void orientSinglePoint(typename CloudNT::Ptr boundaryPointsPtr,
                               typename KdtreeT::Ptr tree,
                               int i_p);
    
};

} // end namespace
} // end namespace

#include "./impl/boundaryOrientationImpl.hpp"
