/**
 * @author Markus Ziegler
 * @date 08.04.2019
 * @file octreeVoxelGridFilter.hpp
 * @brief Classes related to down-sampling a point cloud with an octree based voxel grid filter.
 * @note Code based on code from Julius Kammerl and Robert Huitl:
 *       http://www.pcl-users.org/VoxelGridFilter-Leaf-size-is-too-small-td4025570.html
 */

#pragma once


// PCL
#include <pcl/octree/octree.h>
// Include the following implementation files to solve linking-error that occurs if we don't.
// Similar to: http://www.pcl-users.org/OctreePointCloud-isn-t-recognize-td2890100.html
// This way we do not have to edit the CMakeLists.txt which I was not able to
// modify correctly. Linking PCL against ROS-nodes can be tricky.
// Might be a result of me having both, PCL 1.7.2 and PCL 1.8.1, installed.
#include <pcl/octree/impl/octree_base.hpp>
#include <pcl/octree/impl/octree_pointcloud.hpp>


namespace pf_matching{
namespace pre_processing{

template<typename PointT>
class OctreeCentroidContainer : public pcl::octree::OctreeContainerBase
{
	public:
		/** \brief Class initialization. */
		OctreeCentroidContainer();

		/** \brief Empty class destructor. */
		virtual ~OctreeCentroidContainer()
		{
		};

		/** \brief deep copy function */
		virtual OctreeCentroidContainer * deepCopy() const;

		/**
		 * \brief Add new point to voxel.
		 * \param[in] new_point the new point to add
		 */
		void addPoint(const PointT& new_point);

		/**
		 * \brief Calculate centroid of voxel.
		 * \param[out] centroid_arg the resultant centroid of the voxel
		 */
		void getCentroid(PointT& centroid_arg) const;

		/** \brief Reset leaf container. */
		void reset();

	private:
		unsigned int point_counter_;
		PointT pt_sum_;
};


template<typename PointT>
class OctreeVoxelGrid : public pcl::octree::OctreePointCloudVoxelCentroid< PointT, OctreeCentroidContainer<PointT> >
{
	public:
		/**
		 * @brief construct an octree with a specified leaf-size
		 * @param resolution_arg edge length of the cubic octree leafs
		 */
		OctreeVoxelGrid(double edgeLength);

		/**
		 * @brief apply voxel grid filter
		 * @details generates outputCloud that contains only the voxel centroids
		 *          of the inputCloud
		 * @param outputCloud filtered point cloud
		 */
		void filter(pcl::PointCloud<PointT>& outputCloud);
};


} // end namespace
} // end namespace

#include "./impl/octreeVoxelGridFilterImpl.hpp"
