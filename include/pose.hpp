/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Franz Ziegler
 * @date 08.04.2019
 * @file pose.hpp
 * @brief classes for representing model poses and clusters of those
 */
#pragma once

// Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>

// boost
#include <boost/dynamic_bitset.hpp>
#include <bitset>

#include <iostream>


namespace pf_matching{


/**
 * @class Pose
 * @brief store a weighted model pose in an efficent way for comparison with other poses
 */
class Pose{
    
    friend class PoseCluster;
    friend class PoseWithAssociatedModelPoint;
    friend class PoseClusterWithAssociatedModelPoints;
    
    public:
        /**
         * @brief construct from a precalculated transformation
         * @param transformation affine transformation so that s = T * m with s the scene point and
                                 m the model point
         * @param weight_ weight associated with the pose
         */
        Pose(const Eigen::Affine3f& transformation, const float weight_);

        /**
         * @brief construct from a precalculated translation and rotation
         * @param translation translation part
         * @param rotation rotation part
         * @param weight_ weight associated with the pose
         */
        Pose(const Eigen::Vector3f& translation, Eigen::Quaternionf& rotation, const float weight_);

        /**
         * @brief construct as identitiy with weight 0
         */
        Pose();

        /**
         * @brief construct from intermediate transforms as inv(Tsg) * Rx(alpha) * Tmg
         * @param Tsg transformation that aligns the scene reference point to the origin / x-axis
         * @param Tmg transformation that aligns the model reference point to the origin / x-axis
         * @param alpha rotation angle for alignment
         * @param  weight_ weight associated with the pose
         */
        Pose(const Eigen::Affine3f& Tsg, const Eigen::Affine3f& Tmg, float alpha, float weight_, int sceneRefPointIdx_,  int maxIdx_);
        

        virtual ~Pose(){};

        /**
         * @brief get pose represented as a transformation
         * @return 
         */
        const Eigen::Affine3f asTransformation() const;
        /**
         * @brief determine if two poses are similar to each other within certain threshold levels
         * 
         * * The translation and rotation parts are checked seperately.
         * * The translation part is comparted using the Euclidean distance. E.g. two translations
         *   are similar, if \f$ threshold > |t_1 - t_2| \f$
         * * The rotations are compared using the angle theta needed to get from one orientation
         *   to the other. E.g. two orientations are similar, if 
             \f$ threshold > \theta = 2 \cos^{-1} (|<q_1, q_2>|) \f$.
             Geometrically, if we transform an abritary point with both rotations, the vectors
             from the origin to the resulting points will enclose an angle <= theta. (theta gives
             the maximum angular measure of separation.)
             (see "Metrics for 3D Rotations: Comparison and Analysis" by Du Q. Huynh, 2009)
         * @param otherPose the pose to compare to
         * @param transThresh threshold value for comparing the translation parts
         * @param rotThresh threshold value for comparing the rotation parts, must be in [0, pi]
         *                  with 0 = no difference and pi = max. difference;
         * @return true if both translation and rotation are within the set thresholds, else false
         */
        bool isSimilar(const Pose& otherPose, const float transThresh, const float rotThresh);
        
        /**
         * @brief determine if the translation of two poses is similar within a threshold
         * 
         * Two translations are similar, if \f$ threshold > |t_1 - t_2| \f$.
         * @param otherPose the pose to compare to
         * @param transThresh threshold value for comparing the translation
         * @return true if translation is within threshold, otherwise false
         */
        bool translationIsSimilar(const Pose& otherPose, const float transThresh);
        
        /**
         * @brief Multiplying poses returns a pose eqivalent to the product of the transform 
         *        matrices and weighted by the product of the weights
         * @param p1 other pose
         * @return the new pose
         */
        const Pose operator* (const Pose& p1) const {
            
            Eigen::Affine3f poseTransform = asTransformation() * p1.asTransformation();
            return Pose(poseTransform, weight * p1.weight);
        }
        
        /**
         * @brief overload for "<" operator, poses are compared in terms of their associated weight
         */
        const inline bool operator< (const Pose& p1) const {
            return weight < p1.weight;
        }
        
        /**
         * @brief overload for ">" operator, poses are compared in terms of their associated weight
         */
        const inline bool operator> (const Pose& p1) const {
            return weight > p1.weight;
        }
        
        /**
         * @brief overload for stream output
         */
        friend std::ostream& operator<<(std::ostream& stream, const Pose p);

        /**
         * @brief get the rotation part of the pose as a quaternion
         * @return unit quaternion representing pose orientation
         */
        const Eigen::Quaternionf getRotationPart() const{
            return q;
        }
        
        /**
         * @brief get the translation part of the pose
         * @return xyz-vector
         */
        const Eigen::Vector3f getTranslationPart() const{
            return t;
        }
        
        /**
         * @brief set the weight of the pose manually
         * @param weight_
         */
        void setWeight(float weight_){
            weight = weight_;
        }
        
        /**
         * @brief get the weight of the pose
         * @return weight
         */
        const float getWeight() const{
            return weight;
        }
        

        int sceneRefPointIdx; 	//!< index of associated reference point. for unambiguously sorting the poses during clustering
		int maxIdx;			//!< number of associated accumulator maximum. for unambiguously sorting the poses during clustering

    private:
        Eigen::Vector3f t;      //!< translation part of the pose
        Eigen::Quaternionf q;   //!< rotation part of the pose represented as a unit quaternion
        float weight = 0;       //!< weight of this pose

};



/**
 * @brief Multiplying a pose p with a transform T returns a pose equivalent
 *        to the product of the homogeneous transformation matrices with the weight of the pose.
 * @param T transformation
 * @param p pose
 * @return pose with transform equivalent to the matrix product
 */
inline Pose operator* (const Eigen::Affine3f& T, const Pose& p){
    return Pose(T * p.asTransformation(), p.getWeight());
}

/**
 * @brief Multiplying a pose p with a transform T returns a pose equivalent
 *        to the product of the homogeneous transformation matrices with the weight of the pose.
 * @param p pose
 * @param T transformation
 * @return pose with transform equivalent to the matrix product
 */
inline Pose operator* (const Pose& p, const Eigen::Affine3f& T){
    return Pose(p.asTransformation() * T, p.getWeight());
}

// TODO: MFZ 14/0917 es fehlt m�glicherweise eine �berladung des * operators f�r meine Anwendung

//Eigen::Vector3f operator* (const float& a, const Eigen::Vector3f& b){
//	return a*b;
//}


/**
 * @class PoseCluster
 * @brief represent the center of a cluster of poses
 */
class PoseCluster: virtual public Pose{
    

    public:
		/**
		 * @brief initialize pose cluster as identity with weight = 0,
		 * 		  by calling the standard constructor of base class
		 */
		PoseCluster(unsigned int votingBallIdx_ = 0);

        /**
         * @brief construct from a pose
         * @param p pose to construct from
         */
        PoseCluster(const Pose& p, unsigned int votingBallIdx_ = 0);

//        virtual ~PoseCluster();
    
        /**
         * @brief add an additional pose to the cluster and update the cluster weight and center
         * @param p pose to be added
         */
        virtual void addPose(Pose* p);

        const inline unsigned int getVotingBallIdx(){
        	return votingBallIdx;
        };

    private:
        unsigned int votingBallIdx;
};

/**
 * @class PoseWithRefPoint
 * @brief child-class of Pose class for Hinterstoisser-Methods.
 * @note additionally storing the index i_r of the model-reference point that is associated with this pose.
 */
class PoseWithAssociatedModelPoint : virtual public Pose{

	friend class PoseClusterWithAssociatedModelPoints;

	public:

		/**
		 * @brief construct as identity with weight = 0 and i_r = 0
		 */
		PoseWithAssociatedModelPoint();

		/**
		 * @brief construct from a precalculated transformation
		 * @param transformation affine transformation so that s = T * m with s the scene point and
								 m the model point
		 * @param weight_ weight associated with the pose
		 * @param i_r model reference point associated with the pose, i.e. associated with the scene reference point
		 */
		PoseWithAssociatedModelPoint(const Eigen::Affine3f& transformation, const float weight_, const unsigned int i_r_);

		/**
		 * @brief construct from intermediate transforms as inv(Tsg) * Rx(alpha) * Tmg
		 * @param Tsg transformation that aligns the scene reference point to the origin / x-axis
		 * @param Tmg transformation that aligns the model reference point to the origin / x-axis
		 * @param alpha rotation angle for alignment
		 * @param weight_ weight associated with the pose
		 * @param i_r model reference point associated with the pose, i.e. associated with the scene reference point
		 */
		PoseWithAssociatedModelPoint(const Eigen::Affine3f& Tsg, const Eigen::Affine3f& Tmg, float alpha, float weight_, int sceneRefPointIdx_,  int maxIdx_, const unsigned int i_r_);

//		virtual ~PoseWithAssociatedModelPoint();

//		/**
//		 * @brief Multiplying poses returns a pose eqivalent to the product of the transform
//		 *        matrices and weighted by the product of the weights
//		 * @param p1 other pose
//		 * @return the new pose
//		 */
//		const PoseWithAssociatedModelPoint operator* (const Pose& p1) const {
//
//			Eigen::Affine3f poseTransform = asTransformation() * p1.asTransformation();
//			return PoseWithAssociatedModelPoint(poseTransform, weight * p1.weight, 0);
//		}

	private:
		int i_r;
};


class PoseClusterWithAssociatedModelPoints : public PoseCluster, public PoseWithAssociatedModelPoint {

        public:

		PoseClusterWithAssociatedModelPoints(const PoseWithAssociatedModelPoint& p, unsigned int numberOfModelPoints_, unsigned int votingBallIdx_ = 0);

//		virtual ~PoseClusterWithAssociatedModelPoints();

		virtual void addPose(Pose* p);// WithAssociatedModelPoint& p);

		inline bool isModelPointAlreadyPresent(const PoseWithAssociatedModelPoint& p){
//			if (p.i_r > numberOfModelPoints){ // if model point indices are bigger than the number of model points (due to downsampling)
//				FlagArrayOfModelPoints.resize(p.i_r, false);
//				return false;
//			}
			return (FlagArrayOfModelPoints[p.i_r] == true) ? true : false;
		}

	private:
		int numberOfModelPoints;
		boost::dynamic_bitset<> FlagArrayOfModelPoints;	//!< flag array to flag model-reference point indices of those points
														//! that are already associated with one of the clusters poses
//		std::bitset<3000>FlagArrayOfModelPoints;
};

} // end namespace
