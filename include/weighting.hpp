/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 13/05/16
 * @file weighting.hpp
 * @brief weighting of features in an existing model
 */

#pragma once

//std
#include <utility>
#include <vector>

// project
//#include <pairFeatureModel.hpp>  // now forward declared
#include <featureTypes.hpp>

namespace pf_matching{

// forward declarations
template<typename PointNT> class PFmodel;

namespace extensions{

/**
 * @class ModelWeighter
 * @brief for weighting the features of a previously build model according to different aspects
 */
template <typename PointNT>
class ModelWeighter{
    
    public:
        typedef std::pair<featureType, float> typeWeightPair;   //!< pair of feature type and weight
        typedef std::vector<typeWeightPair>   typeWeightVector; //!< vector of pairs with feature type and weight
        typedef PFmodel<PointNT> ModelT;                        //!< type of model representation
    
        /**
         * @brief set the model to weight
         * @param modelPtr_ pointer to the model
         */
        void setModel(typename ModelT::Ptr modelPtr_){
            modelPtr = modelPtr_;
        }
        
        /**
         * @brief weight each feature with d^power where d is the distance of the feature's two
         *        points normalized with the model diameter
         * @param power power to apply do normalized distance, > 0
         */
        void weightByDistance(float power);
        
        /**
         * @brief erase all features with a distance of the two point of less than or equal to
         *        thresh * model diameter
         * @param thresh ratio of model diameter, in (0, 1]
         */
        void discardByDistance(float thresh);
        
        /**
         * @brief weight the model's feautures according to how many entries for a feature exist
         *        in the hash table
         * 
         * For a feature leading to n entries, all entries are weighted with a factor of 1 / n^power
         * @param power power in weighting function, in (0, 1]
         */
        void weightByFrequency(float power);
        
        /**
         * @brief cell-wise erase features that occur very frequently (big cells) until erasing
         *        the next cells would mean there are lesss than x % of the original features
         *        left
         * @param minRemaining fraction of features to keep at least (0,1)
         */
        void discardByFrequency(float minRemaining);
        
        /**
         * @brief weight the features in the hash tables of the model according to what feature
         *        type the hash table holds
         * @param typeWeightPairs pairs of feature type and weighting factor to apply to features of
         *        that type; The weights must be > 0!
         */
        void weightByFeatureType(typeWeightVector& typeWeightPairs);

        
    private:
        typename PFmodel<PointNT>::Ptr modelPtr; //!< pointer to the model to weight
};


} // end namespace
} // end namespace

#include "./impl/weightingImpl.hpp"
