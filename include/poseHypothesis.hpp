/**
 * @copyright
Copyright (c) 2019
TU Berlin, Institut f�r Werkzeugmaschinen und Fabrikbetrieb
Fachgebiet Industrielle Automatisierungstechnik
Author: Sebastian Krone
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

DISCLAIMER
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @author Sebastian Krone, Markus Ziegler
 * @date 08.04.2019
 * @file poseHypothesis.hpp
 * @brief matching of scenes against stored models
 */

#pragma once

#include <pose.hpp>
#include <std_msgs/Header.h>

namespace pf_matching{

class PoseHypothesis{
    public:

        // shared pointer aliases
        typedef boost::shared_ptr<PoseHypothesis> Ptr; //!< shared pointer definition for convenience
        typedef boost::shared_ptr<const PoseHypothesis> ConstPtr; //!< shared pointer definition for convenience

        /*
         * @brief set model for pose hypothesis
         * @param modelName_ Name of Model to set for PoseHypothesis
         */
        void setModel(std::string modelName_)
        {
            modelName = modelName_;
        }

        /*
         * @brief set hypothetical pose
         * @param hypothetic_pose_ hypothetical pose belonging to PoseHypothesis
         */
        void setHypotheticalPose(Pose hypothetical_pose_)
        {
            hypothetical_pose = hypothetical_pose_;
        }

        /**
         * @brief set scene in which hypothetical pose of model was found
         * @param scene_ scene Header
         */
        void setScene(std_msgs::Header scene_)
        {
            scene = scene_;
        }

        /**
         * @brief set vector of indices of explained scene points near the hypothesis model points
         * @param explainedSceneIndices_ vector of scene indices
         */
        void setUsedSceneIndices(std::vector<int> usedSceneIndices_)
        {
            usedSceneIndices = usedSceneIndices_;
        }

        /**
         * @brief set weight of pose hypothesis (weight of vertex in conflict graph)
         * @param weight_ weight of the pose hypothesis
         */
        void setWeight(float weight_)
        {
            weight = weight_;
        }

        /**
         * @brief set the number of explaining model points divided by the number of explained scene points
         * @param modelpoints_per_scenepoints_ explaining model points per explained scenepoints
         */
        void setMpsPerSps(float modelpoints_per_scenepoints_)
        {
            modelpoints_per_scenepoints = modelpoints_per_scenepoints_;
        }

        /**
         * @brief set the info if this pose (-cluster) ( = hypothesis) was generated with a voting ball, and if so, with which one
         * @param votingBallIdx_ 0 if no voting balls were used, 1 if poseCluster belongs to small voting ball, 2 if to big voting ball, -1 if no info was available (raw pose)
         */
        inline void setVotingBallIdx(int votingBallIdx_){
			votingBallIdx = votingBallIdx_;
		}

        /**
         * @brief get the model for hypothetical pose
         * @return Name of the Model
         */
        std::string getModel()
        {
            return modelName;
        }

        /**
         * @brief get the hypothetical pose
         * @return Pose belonging to the PoseHypothesis
         */
        Pose getHypotheticalPose()
        {
            return hypothetical_pose;
        }

        /**
         * @brief get the scene in which hypothetical pose of model was found
         * @return scene Header
         */
        std_msgs::Header getScene()
        {
            return scene;
        }

        /**
         * @brief get the indices of scene points which are nearby the hypothesis model points
         * @return explainedSceneIndices vector of scene point indices near the hypothesis model points
         */
        std::vector<int> getUsedSceneIndices()
        {
            return usedSceneIndices;
        }

        /**
         * @brief get the weight of pose hypothesis
         * @return weight of pose hypothesis
         */
        float getWeight()
        {
            return weight;
        }

        /**
         * @brief get the number of explaining model points divided by the number of explained scene points
         * @return modelpoints_per_scenepoint explaining model points per explained scenepoints
         */
        float getMpsPerSps()
        {
            return modelpoints_per_scenepoints;
        }

        /**
		 * @brief get the info if this hypothesis was generated with a voting ball, and if so, with which one
		 * @param votingBallIdx_ 0 if no voting balls were used, 1 if hypothesis belongs to small voting ball, 2 if to big voting ball, -1 if no info was available (raw pose)
		 */
		inline int getVotingBallIdx(){
			return votingBallIdx;
		}

        /**
         * @brief simple method to get a string with Models Name and the Translative Position of a hypothetical pose
         * @return string with model name and translative position
         */
        std::string toString()
        {
            std::ostringstream ret;
            ret << modelName << "("
                    << "x: " << hypothetical_pose.getTranslationPart().x() << ", "
                    << "y: " << hypothetical_pose.getTranslationPart().y() << ", "
                    << "z: " << hypothetical_pose.getTranslationPart().z() << "| "
                    << "weight: " << weight << ", "
					<< "voting ball: " << votingBallIdx << ")";
            return ret.str();
        }


    private:

        std::string modelName;
        Pose hypothetical_pose;
        std_msgs::Header scene;
        std::vector<int> usedSceneIndices;
        float weight;
        float modelpoints_per_scenepoints;
        int votingBallIdx; //!< 0 if no voting balls were used,
         	 	 	 	   //! 1 if poseCluster belongs to small voting ball,
         	 	 	 	   //! 2 if poseCluster belongs to big voting ball,
         	 	 	 	   //! -1 if no info was available (hypothesis is a raw pose)

};
} // end namespace
