/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Ziegler
 * @date 08.04.2019
 * @file pairFeatureMatcher.hpp
 * @brief matching of scenes against stored models
 */

#pragma once


// PCL
#include <pcl/point_types.h>
#include <pcl/point_cloud.h> //PointCloud template
#include <pcl/octree/octree_search.h> 		// TODO: MFZ 18/10/17 added for doing ray-tracing within an octree
#include <pcl/octree/octree_pointcloud.h>  	// TODO: MFZ 18/10/17 added for generating an octree of a pointcloud

// project
#include <pose.hpp>
#include <pairFeatureModel.hpp>
#include <houghSpace2D.hpp>
#include <visibilityContextVisualization.hpp> // TODO: MFZ 18/10/17 added
#include <pairFeatures.hpp> // TODO: MFZ 18/10/17 added
#include <flagArray.hpp>	// TODO: MFZ 23/03/18 added
#include <modelHashTable.hpp> // TODO: MFZ 25/05/2018 added
#include <featureTypes.hpp> // TODO: MFZ 28/05/2018 added

// debugging flag generating (binary) files
//#define write_alphas_to_binary_file
//#define write_counters_to_file


namespace pf_matching{

// declaration to use friend-class accross namespaces
namespace extensions{
    template <typename PointT> class ModelSymmetryAnalyzer;
}


/**
 * @class PFmatcher
 * @brief Worker class for matching a pre-calculated model to an input scene via point pair features
          to retrieve the pose of the model in the scene (i.e. the transformation T that transforms
          the model points \f$ m_i \f$ to their position in the scene via \f$ T \cdot m_i \f$. 
 * 
 * //TODO: literature references
 * Depending on the input, matching can be done relying on different point features. The features
 * are calculated separately, while pose clustering works simultaneously on all raw poses calculated
 * for all features.
 * 
 * //TODO: find a way to eliminate false positives / judge the quality of the match
 *         (e.g. ratio between best and second best pose, ratio between pose cluster score and 
 *          sum of scores of all pose clusters).
 */
template <typename PointNT>
class PFmatcher{
    
    template <typename PointT>
    friend class extensions::ModelSymmetryAnalyzer;
	friend class modelHashTable<PointNT>;

    public:
    	// MFZ 17/10/17 added:
    	// visibility context: using search-commands of OctreePointCloudSearch for feature construction
		typename pcl::octree::OctreePointCloudSearch<PointNT>::Ptr octreeSearchPtr; //!< MFZ 17/10/17 shared pointer to octree for visibility context ray casting and other octree based searching

        /**
         * @brief set an already created model as the target for matching
         * @param modelPtr_ shared pointer to the model to be added
         */
        void setModel(const typename PFmodel<PointNT>::ConstPtr& modelPtr_);

        /**
         * @brief delete stored model
         */
        void clearModel(){
            modelPtr = NULL;
        }

        /**
         * @brief set the (downsampled) input scene for matching
         * 
         * assumes that the clouds contain no invalid points
         * 
         * @param surfaceCloudPtr_ cloud with surface points, set to NULL if not available
         * @param edgeCloudPtr_ cloud with edge points, set to NULL if not available
         */
        void setScene(const typename pcl::PointCloud<PointNT>::ConstPtr& surfaceCloudPtr_,
                      const typename pcl::PointCloud<PointNT>::ConstPtr& edgeCloudPtr_);
        
        /**
         * @brief set parameters used for matching
         * @param d_alpha_ discretization steps for aligning angle alpha in rad
         * @param refPointStep_ only construct features at every refPointStep_-th reference point
         * @param maxThresh_ calculate raw poses for all Hough space points with a score of more 
         *                   than maxThresh_ * global maximum, should be in (0,1]
         * @param clusterTransThreshRelative_ threshold value for translation for clustering similar poses,
         *                            geometrically, the max. Euclidean distance between translation
         * @param clusterRotThresh_ threshold value for rotation for clustering similar poses,
         *                         geometrically, the maximum angular measure of separation between
         *                         rotation results, see Pose::isSimilar() for further description
         * @param useEquivalentModelPoses_ if true, try to merge equivalent model poses
         */
        void setMatchingParameters(float d_alpha_,
                                   unsigned int refPointStep_,
                                   float maxThresh_,
                                   float clusterTransThreshRelative_,
                                   float clusterRotThresh_,
								   bool use_neighbour_PPFs_for_matching,
								   bool use_voting_balls,
								   bool use_hinterstoisser_clustering,
								   bool use_hypothesis_verification_with_visibility_context,
								   bool vote_for_adjacent_rotation_angles,
								   flagArrayHashTableType flag_array_hash_table_type,
								   unsigned int flag_array_quantization_steps,
                                   bool useEquivalentModelPoses_ = true);
        
        /**
         * @brief copy matching parameters from an other matcher object that should already be
         *        set up
         * @param otherMatcher the other matcher
         */
        template<typename PointT> void
        copyMatchingParameters(const PFmatcher<PointT>& otherMatcher);
        
        /**
         * @brief set miscellaneous parameters not related to the matching algorithm
         * 
         * @warning Do not enable saving Hough space images if performance is critical, it will slow
         *          down the matching process!
         * 
         * @param HSsaveDir_ directory to save images of the Hough spaces to;
         *                   It will be created if not existing; If the directory can not be
         *                   accessed, or the string is empty, no images will be saved.
         */
        void setMiscParameters(std::string HSsaveDir_);
        
        void switchOffExtensions();

        /**
         * @brief match the model to the scene with the set parameters, use other member functions
         *        to retrieve results.
         * @param featuresToMatch bitfield of features to match, defaults to all supported
         */
        void match(featureType featuresToMatch = ALL_FEATURES);
        
        /**
         * @brief get at most the n best model poses, pass 0 to get all stored poses
         * 
         * @note If no matching was performed or the matching failed completely, the returned vector
         *       will be empty.
         * 
         * @param n number of poses to get
         * @return sorted vector with poses in descending order (element 0 = best pose)
         */
        std::vector<Pose*> getBestPosesOfAllVotingBalls(unsigned int n = 0);
        std::vector<std::vector<Pose*>> getBestPosesOfEachVotingBall(unsigned int n = 0);
        std::vector<Pose*> getBestPosesOfVotingBall(unsigned int n, unsigned int votingBall);
        
        /**
         * @brief assuming that there are valid poses in the pose clusters, figure out how many 
         *        valid poses exist in the scene by simple thresholding of the pose cluster weights
         * 
         * A pose is considered valid if it's weight relative to that of the best pose cluster is 
         * above a certain threshold value. Note that this is a very simple test that only makes
         * sense if:
         * * The first pose is always valid (i.e. there is at least one object in the scene and it 
         *   will be found).
         * * There might be other poses (additional object instances) that are visible almost as
         *   good as the first one.
         * This method is recommended for matching models to themselves to analyze self-similarities.
         * @param thresh relative weight a pose must at least have to be considered "valid",
         *        in (0, 1]
         * @return k with the first k pose clusters representing valid poses
         */
        std::vector<float> getNumberofValidPosesSimple(float thresh = 0.8);
        
        /**
         * @brief number of pose clusters
         * @return length of pose cluster vector
         */
        std::vector<float> getNumberOfPoseClusters();
        
        /**
         * @brief get the weights of the first n pose clusters
         * @param n max. number of pose clusters to consider, n = 0 returns weights for all clusters
         * @return vector of weights, sorted like pose clusters in descending order of weight
         */
        std::vector<std::vector<float>> getPoseClusterWeights(unsigned int n = 0);
        
        /**
         * @brief get the median of the pose clusters' weights
         * @return the median
         */
        std::vector<float> getPoseClusterMedianWeight();
        
        /**
         * @brief get the vector of all raw poses found during matching
         * @return the raw poses
         */
        std::vector<std::vector<Pose*>> getRawPoses(){
            return rawPoses;
        }
        
        /**
         * @brief number of raw poses extracted from the Hough spaces
         * @return length of raw pose vector
         */
        std::vector<float> getNumberOfRawPoses();
        
        /**
         * @brief get the weights of the raw poses
         * @return vector of weights, no particular sorting
         */
        std::vector<std::vector<float>> getRawPosesWeights();
        
        /**
         * @brief get the average weight of all raw poses
         * @return the average
         */
        std::vector<float> getRawPosesMeanWeight();
        
        /**
         * @brief get the median of the raw poses' weights
         * @return the median
         */
        std::vector<float> getRawPosesMedianWeight();
        
        /**
         * @brief get the weight of the first raw pose (= maximum weight)
         * @return the max. weight
         */
        std::vector<float> getRawPosesMaxWeight();

        /**
         * @brief get the number of voting balls
         * @note makes only sense after clustering, before this function will return 0.
         * @return returns number of voting balls
         */
        unsigned int getNumberOfVotingBalls();

        void freeMemory();

        void writePosesToFile(std::vector<Pose*> poses, std::string filename);

        void writeToFile(std::string lineContent, std::string filename);

        void save_matching_debug_data_to_files(const typename modelHashTable<PointNT>::Ptr& htPtr,
				  	  	  	  	  	  	  	   const typename pcl::PointCloud<PointNT>::ConstPtr& sceneRefCloudPtr);
    
    private:
    
        // model and scene
        typename PFmodel<PointNT>::ConstPtr modelPtr; //!< model for matching
        typename pcl::PointCloud<PointNT>::ConstPtr sceneSurfaceCloudPtr;
        typename pcl::PointCloud<PointNT>::ConstPtr sceneEdgeCloudPtr;
        
        // matching parameters
        float d_alpha;              //!< quantization step for alpha
        unsigned int n_alpha;       //!< number of quantization steps for alpha
        unsigned int refPointStep;  //!< use every x'th reference point when matching
        float maxThresh;

        // pose clustering parameters
        float clusterTransThreshRelative;
        float clusterRotThresh;
        
        // analysis of self-symmetry
        bool useEquivalentModelPoses;
        
        // misc parameters
        bool saveHoughSpaceToFile = false;
        std::string saveDir;

    public:
        // timers for statistics-messages
    	float msElapsedWithFlagArrayBuilding;
		float msElapsedWithoutFlagArrayBuilding;
		float msElapesdWithVoxelGridForVotingBallBuilding;

#ifdef write_counters_to_file
        // TODO : 19.06.2018 delete the following debugging counters
        double invalidPFcounter = 0, validPFcounter = 0, overFlowCounter = 0;
#endif
#ifdef write_alphas_to_binary_file
        std::vector<float> accumulatorArrayAlphas;
#endif
        // TODO: 11.06.2018 make those private again, after debugging
        // EXTENSION methods from [Hinterstoisser et al. 2016] parameters
		bool useNeighbourPPFsForMatching;
        bool useVotingBalls;
		bool useHinterstoisserClustering;
		bool usePoseVerification;
		bool voteForAdjacentRotationAngles;
		flagArrayHashTableType faHashTableType;
		unsigned int flagArrayQuantizationSteps;
    // TODO: make private again after debugging
//		private:
        // set check for parameters
        bool matchingParamsSet = false;
        bool miscParamsSet = false;
        
        // poses
        std::vector<std::vector<Pose*>> rawPoses;      //!< after feature matching
        std::vector<std::vector<PoseCluster*>> poseClusters;  //!< after clustering of similar poses


        /**
         * @brief match a single feature type between a model given point clouds
         * @param typesForMatching bitset of feature types to match for this model
         * @param thisType feature type that this function call matches
         * @param sceneRefCloudPtr shared pointer to point cloud with oriented reference points
         * @param sceneTargetCloudPtr shared pointer to point cloud with oriented target points
         * @return false if no matching took place because of unset inputs, true otherwise;
         *         Note that if !(typesForMatching & thisType), true is returned 
         */
        bool matchFeature(const featureType typesForMatching,
                          const featureType thisType,
                          const typename pcl::PointCloud<PointNT>::ConstPtr& sceneRefCloudPtr,
                          const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr);
        
        /**
         *
         * @param typesForMatching
         * @param thisType
         * @param sceneRefCloudPtr
         * @param sceneTargetCloudPtr
         * @return
         */
        bool matchVisConFeature(const featureType typesForMatching,
                                const featureType thisType,
								const typename pcl::PointCloud<PointNT>::ConstPtr& sceneRefCloudPtr,
								const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr);

        /**
         * @brief for a given scene reference point, build features with all other scene points and 
         *        vote in the provided arrays via local coordinates
         * @param[in] s_r location of scene reference point
         * @param[in] n_r normal of scene reference point
         * @param[in] Tsg pre-calculated transformation that aligns s_r and n_r to the origin / x-axis
         * @param[in,out] accumulatorArray for voting, assumed to be initialized to right size
         * @param[in,out] alphaArray for keeping track of continuous alpha values, assumed to be
         *                           initialized to the right size
         * @param[in] sceneTargetCloudPtr pointer to the cloud with all target points to use
         * @param[in] ht model hash table with model features
         */
        void voteWithTargetPoints(const Eigen::Vector3f& s_r, const Eigen::Vector3f& n_r, 
                                  const Eigen::Affine3f& Tsg,
                                  HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
                                  const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr,
								  const typename modelHashTable<PointNT>::Ptr& htPtr,
								  MinimalDiscretePF<PointNT>& sceneFeature,
								  flagArray<PointNT>* refPointFlagArrayPtr);
        
        /**
         *
         * @param s_r
         * @param n_r
         * @param Tsg
         * @param accumulatorArray
         * @param alphaArray
         * @param sceneTargetCloudPtr
         * @param ht
         * @param sceneFeature
         */
        void voteWithTargetPoints(const Eigen::Vector3f& s_r, const Eigen::Vector3f& n_r,
								  const Eigen::Affine3f& Tsg,
								  HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
								  const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr,
								  const typename visibilityContextModelHashTable<PointNT>::Ptr& htPtr,
								  VisibilityContextDiscretePF<PointNT>& sceneFeature,
								  const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
								  flagArray<PointNT>* refPointFlagArrayPtr);

        void voteWithVotingBall(const std::vector<int> &targetPointIndices,
								const Eigen::Vector3f& s_r, const Eigen::Vector3f& n_r,
								const Eigen::Affine3f& Tsg,
								HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
								const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr,
								const typename modelHashTable<PointNT>::Ptr& htPtr,
								MinimalDiscretePF<PointNT>& sceneFeature,
								flagArray<PointNT>* refPointFlagArrayPtr);

        /**
         * @brief variant of voteWithTargetPoints, that loops over a given set of
         * 		  targetPoints instead of looping over the whole scene
         * @param [in] targetPointIndicesWithinRadius set of targetPoints specified by their indices
         * @param [in] numberOfTargetPoints number of targetPoints
         * @param [in] s_r
         * @param [in] n_r
         * @param [in] Tsg
         * @param [in] accumulatorArray
         * @param [in] alphaArray
         * @param [in] sceneTargetCloudPtr
         * @param [in] ht
         * @param [in] sceneFeature
         * @param [in] fancy_viewer
         * @param [in] refPointFlagArrayPtr
         */
        void voteWithVotingBall(const std::vector<int> &targetPointIndices,
								const Eigen::Vector3f& s_r, const Eigen::Vector3f& n_r,
								const Eigen::Affine3f& Tsg,
								HoughSpace2D& accumulatorArray, HoughSpace2D& alphaArray,
								const typename pcl::PointCloud<PointNT>::ConstPtr& sceneTargetCloudPtr,
								const typename visibilityContextModelHashTable<PointNT>::Ptr& htPtr,
								VisibilityContextDiscretePF<PointNT>& sceneFeature,
								const boost::shared_ptr<pcl::visualization::PCLVisualizer>& fancy_viewer,
								flagArray<PointNT>* refPointFlagArrayPtr);

        /**
         * @brief cluster similar raw poses into pose clusters;
         * 		  switches between different clustering methods
         */
        void clusterRawPoses();

        /**
         * @brief pose clusters are sorted by weight in descending order
         */
        void choiClustering();

        /**
         * @brief pose clusters are sorted by weight in ascending order
         */
        void hinterstoisserClustering();

        /**
         * @brief return the weights of poses in a vector
         * @param n get the weight of the first n poses or all if n == 0 or n > number of poses
         * @param poses vector holding the poses
         * @return vector with weights, sorted like pose vector
         */
        template <typename PType>
        std::vector<float> getWeights(unsigned int n, std::vector<PType*>& poses); // MFZ 18.04.2018 changed to PType*
        
        /**
         * @brief get the median of the pose weights
         * @param sortedPoses vector of poses, sorted by weight
         * @return the median
         */
        template <typename PType>
        float getMedianWeight(std::vector<PType*>& sortedPoses); // MFZ 18.04.2018 changed to PType*
};
    
} // end namespace

# include "./impl/pairFeatureMatcherImpl.hpp"
