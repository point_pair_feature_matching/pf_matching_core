/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 04/04/16
 * @file modelSymmetryAnalyzer.hpp
 * @brief find rotational symmetries in models
 */

#pragma once

// project
#include <pairFeatureMatcher.hpp>
#include <pairFeatureModel.hpp>


namespace pf_matching{
namespace extensions{

/**
 * @class ModelSymmetryAnalyzer
 * @brief class for analyzing a created model for poses that are equivalent to the identity pose
 *        due to rotational symmetries, adding those poses to the model and reducing the model by
 *        exploiting the symmetries
 */
template <typename PointNT>
class ModelSymmetryAnalyzer{

    public:

        // aliases for derived data types
        typedef pcl::PointCloud<PointNT> cloudNT; //!< point cloud type
        /**
         * @brief set the model to be analyzed
         * @param modelPtr_ pointer to model object
         */
        void setModel(typename PFmodel<PointNT>::Ptr modelPtr_){
            modelPtr = modelPtr_;
        }

        /**
         * @brief analyze a model for poses that are approximately equivalent to the identity pose
         *        due to self-similarities or rotational symmetries of the model;
         *        If any equivalent poses are found, they are added to the analyzed model object.
         *
         * The analysis is done through matching the model to itself and extracting valid object
         * poses from the matching process. To prevent the identity pose from having too much weight
         * relative to the other poses (because it would match to itself perfectly), a little noise
         * is added before matching.
         *
         * @note The matching process always uses the following parameters regardless of what is
         *       set up in the referenceMatcher:
         *       * refPointStep = 1
         *       * uses all available feature types
         *       * does not save any Hough space images
         *
         * @param referenceMatcher  the matcher object used for matching the model to scenes; We
         *                          copy the matching parameters for the internal matching, so this
         *                          object should be set up completely already.
         * @param relativePoseWeightThresh All poses with a relative weight to the best pose of more
         *                                 than this value will be considered valid equivalent
         *                                 poses. Must be in (0, 1).
         * @param maxThresh value for the maxThresh-setting to use for matching (replaces value
         *                  of reference matcher)
         * @param featuresToMatch bitfield specifying one or more features to use for the matching
         *                        stage of equivalent pose detection; Note that the requested 
         *                        features have to exist in the model description
         */
        void findEquivalentPoses(const PFmatcher<PointNT>& referenceMatcher,
                                 float relativePoseWeightThresh = 0.3,
                                 float maxThresh = 0.4,
                                 featureType featuresToMatch = S2S
                                 );

        /**
         * @brief using any model similarities found via findEquivalentPoses(), reduce the reference
         *        point clouds and the hash tables by removing any reference points that can be
         *        objtained by transforming an other point of the cloud with one of the equivalent
         *        poses.
         *
         * All features of a removed point are deleted from the hash table. The features of the
         * remaining points will receive stronger weight depending on how many equivalences their
         * reference point has among the deleted points.
         *
         * @param distThreshRelative max. distance between transformed reference points as fraction
         *                           of model's absolute d_dist-value
         * @param angleThreshRelative max. angle between transformed reference point normals as
         *                            fraction of model's d_angle-value
         * @param useInverseTransforms if true, also check point equivalencies by using the inverse
         *                             transforms of the model's equivalent poses
         * @param deleteUniquePoints if true, delete any points for which no correspondences were
         *                           found (usually noisy points that have wrong normal directions
         *                           or are located wrongly; can also apply to a point on a rotation
         *                           axis if the model has just one)
         */
        void collapseRefPoints(float distThreshRelative = 0.5, float angleThreshRelative = 0.5,
                               bool useInverseTransforms = true, bool deleteUniquePoints = true);

    private:
        typename PFmodel<PointNT>::Ptr modelPtr; //!< the model to analyze and modify

};

} // end namespace
} // end namespace

#include "./impl/modelSymmetryAnalyzerImpl.hpp"