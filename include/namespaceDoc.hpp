/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke
 * @date 20/08/16
 * @file namespaceDoc.hpp
 * @brief Central documentation of namespaces 
 */

// throw error on include
#error Documentation only.

//! Code for pair feature matching
namespace pf_matching{
    
    //! basic functions and classes not directly related to matching
    namespace tools{}
    
    //! founctionality for pre-processing point clouds
    namespace pre_processing{}
    
    //! functionality related to extensions of the pair feature matching algorithm
    namespace extensions{}
    
    //! functionality for visualization
    namespace visualization{}
    
    //! functionality directly related to ROS 
    namespace ROS{}
}