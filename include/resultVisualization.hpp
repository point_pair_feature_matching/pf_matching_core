/**
 * @copyright 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * @author Xaver Kroischke, Markus Ziegler
 * @date 08.04.2019
 * @file resultVisualization.hpp
 * @brief Functions for displaying matching results
 */

#pragma once

#include <pcl/point_cloud.h>
#include <string>
#include <pose.hpp>

namespace pf_matching{
namespace visualization{

/**
 * @brief create a unique RGB-color for index i out of N
 * @param[out] r red-value in range [0...255]
 * @param[out] g green-value in range [0...255]
 * @param[out] b blue-value in range [0...255]
 * @param[in] i index, 0-based in range [0...N-1]
 * @param[in] N number of colors to create
 */
inline void createRGBcolor(float& r, float& g, float& b, const unsigned int i, const unsigned int N);

/**
 * @brief transform and show point clouds in a PCLVisualizer-window until the window is closed
 * @param cloudPtrs shared pointer to the point clouds
 * @param cloudPoses poses of the clouds' coordinate frames relative to the visualization origin
 * @param names text to display in the legend
 * @param windowName title of the visualization window
 * @param pngSavePath path for saving a png screenshot of the first rendered view, if empty (default),
 *                    no screenshot will be saved
 */
template<typename PointT>
inline void visualizeClouds(const std::vector< typename pcl::PointCloud<PointT>::ConstPtr> cloudPtrs,
                     const std::vector<Pose> cloudPoses,
                     const std::vector<std::string> names,
                     const std::string windowName,
                     const std::string pngSavePath = "");



} // end namespace
} // end namespace


#include "./impl/resultVisualizationImpl.hpp"
