# you need to specify the following variables for your case:
bitbucket_username="phil_rudd"          		# username of your bitbucket accout, from where to download the git-repositories
git_username="Markus Ziegler"				# for the git config. not so important.
git_useremail="markus.f.ziegler@campus.tu-berlin.de"  	# for the git config. not so important.
# you also need to have the metslib files on your machine. for the following it is assumed, that the folder containing them lies in the ~ directory
# you need them for a successful compilation, but unfortunately
# they are not included in the ros-perception package
# you can download them from the git repo of PCL


## Prerequisites: make sure you have 
##                                   - set the above parameters
##                                   - the "metslib-0.5" located in your home directory ~
##

# some static stuff to get some colors in here
black=`tput setaf 0`
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
magenta=`tput setaf 5`
cyan=`tput setaf 6`
white=`tput setaf 7`
reset=`tput sgr0 `
# tput setab will do the trick for changing the backgroundcolor of the text
bold=`tput bold`


# 1. installing ros indigo as discribed in:	
# http://wiki.ros.org/indigo/Installation/Ubuntu
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 installing ros indigo
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"

#	1.1 Configure Ubuntu repositories to include universe, multiverse and restricted:
sudo add-apt-repository 'deb http://archive.ubuntu.com/ubuntu trusty main restricted universe multiverse' 

#	1.2 setup the sources.list:
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

#	1.3 setup the keys:
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116

#	1.4 update the Debian package index:
sudo apt-get update

#   	1.5 install ROS:
sudo apt-get install ros-indigo-desktop-full
sudo apt-get install ros-indigo-perception-pcl
	
#   	1.6 initialize rosdep:
sudo rosdep init
rosdep update
	
#   	1.7 set up environment, so that bash can find the ros-commands:
echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
source ~/.bashrc


# 2. install cmph
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 installing cmph
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
mkdir ~/Downloads
cd ~/Downloads
wget "https://sourceforge.net/projects/cmph/files/cmph/cmph-2.0.tar.gz"
gunzip cmph-2.0.tar.gz 
tar xvf cmph-2.0.tar
cd cmph-2.0
./configure
make
make check
sudo make install
sudo make installcheck
make clean
make distclean
cd ..
rm -r cmph-2.0 cmph-2.0.tar 
cd ~
echo '# bugfix for CMPH lib:' >> ~/.profile
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/' >> ~/.profile # to prevent error "could not find libcmph.so.O"

# 3. install git and make some config-changes:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 installing git
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
sudo apt-get install git
git config --global user.name $git_username
git config --global user.email $git_useremail

# 4. create the "catkin_workspace" workspace, which will contain all the source-code from the ROS-programming-project:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 creating the 'catkin workspace'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
#	4.1 create the folder-structure. src is the place, where the git-code is cloned to:	
mkdir -p ~/ROS/catkin_workspace/src 

#	4.2 get to the src folder		
cd ~/ROS/catkin_workspace/src/

#	4.3 init the current path as the current (?) workspace. as a result, a CMakeLists.txt link shows up (don't know where exactly):
catkin_init_workspace

# 6. clone the pf_matching_core repository from your OWN gitbucket-account. make sure to be in ~/ROS/catkin_workspace/src:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 downloading 'pf_matching_core'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
git clone https://"$bitbucket_username"@bitbucket.org/pointpairfeatureteam/pf_matching_core.git
cd ~/ROS/catkin_workspace/src/pf_matching_core/
git checkout matching_of_geometrically_simple_objects_PCL_1.7.2

# 7. move the missing metslib files from home directory to their real destination
sudo mv ~/metslib-0.5/metslib /usr/include/pcl-1.7/pcl/recognition/3rdparty/
sudo chmod -R +r /usr/include/pcl-1.7/pcl/recognition/3rdparty/metslib
sudo rm -r ~/metslib-0.5 

# 8. build the files. as the result the devel- and build-folders should be created in the workspace "catkin_workspace":
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 building 'pf_matching_core'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/ 
catkin_make -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS_TYPE=-O2


# 9. add the commands to the bash permanently, so that they are available from every terminal right from the start:
echo "source ~/ROS/catkin_workspace/devel/setup.bash" >> ~/.bashrc 
echo "echo 'catkin environment sourced'" >> ~/.bashrc

# 10. clone the pf_matching_tools repository from your OWN gitbucket-account. make sure to be in ~/ROS/catkin_workspace/src:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 downloading 'pf_matching_tools'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/src/
git clone https://"$bitbucket_username"@bitbucket.org/pointpairfeatureteam/pf_matching_tools.git
cd ~/ROS/catkin_workspace/src/pf_matching_tools/
git checkout matching_of_geometrically_simple_objects

# 11. build the files. as the result the devel- and build-folders should be created in the workspace "catkin_workspace":
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 building 'pf_matching_tools'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/
catkin_make
source ~/ROS/catkin_workspace/devel/setup.bash 
export LC_ALL="C"   #this is needed for rospack profile command to work within ssh session
rospack profile

# 12. clone the pf_matching repository from your OWN gitbucket-account. make sure to be in ~/ROS/catkin_workspace/src:
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 downlaoding 'pf_matching'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/src/
git clone https://"$bitbucket_username"@bitbucket.org/pointpairfeatureteam/pf_matching.git
cd ~/ROS/catkin_workspace/src/pf_matching/
git checkout matching_of_geometrically_simple_objects

# 13. build the files. as the result the devel- and build-folders should be created in the workspace "catkin_workspace"
echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 building 'pf_matching'
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
cd ~/ROS/catkin_workspace/ 
catkin_make 
source ~/ROS/catkin_workspace/devel/setup.bash 
export LC_ALL="C"
rospack profile

# 14. add lines to bashrc to enable autocompletion for ros
echo 'export LC_ALL="C"' >> ~/.bashrc

echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 fixing ros permissions
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
sudo rosdep fix-permissions

echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 installing rqt_graph and dynamic reconfigure
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
# 15. install ros node network visualization tool rqt_graph
sudo apt-get install ros-indigo-rqt ros-indigo-rqt-common-plugins
rosdep install dynamic_reconfigure # might not be necessary, could have been installed already with standard ROS


echo "${green} ${bold}
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 PPF-matching system installed.
 next step: manually add the dataset files to ~/ROS/datasets/
 **** NOTE THAT YOU HAVE TO RESTART THE COMPUTER BEFORE YOU CAN USE THE SYSTEM
 **** IF YOU DON'T RESTART THE libcmph.so WILL NOT BE FOUND AND THE SYSTEM CRASHES
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ${reset}"
