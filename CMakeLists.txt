cmake_minimum_required(VERSION 2.8.3)
project(pf_matching_core)

set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS} -O3")
# -O1 / -02 / -O3 option helps with PCL bug causing segmentation faut when compiling with C++11
# see http://stackoverflow.com/a/28857548

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(
    catkin REQUIRED COMPONENTS 
    pcl_ros
    roscpp
    sensor_msgs
    std_msgs
    geometry_msgs
    std_srvs
    message_generation
    nodelet
    dynamic_reconfigure
    pcl_conversions    # MFZ 09/15/17 added
)

#FIND_PACKAGE(PCL 1.7.2 REQUIRED COMPONENTS common octree) # MFZ 07.Apr.2018 added

## add open multi-processing compile flags if compiler supports it
# see http://stackoverflow.com/a/12404666
FIND_PACKAGE(OpenMP REQUIRED)
if (OPENMP_FOUND)
    message (WARNING "OpenMP found ${OpenMP_INCLUDE_DIRS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
else(OPENMP_FOUND)
    message (FATAL_ERROR "OpenMP not found")
endif()

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)
find_package(Boost REQUIRED COMPONENTS timer )

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
add_message_files(
    FILES
    WeightedPose.msg
    WeightedPoseArray.msg
    Statistics.msg
    StatisticsArray.msg
    ProcessedClouds.msg
)

## Generate services in the 'srv' folder
#add_service_files(
#    FILES
#)

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
generate_messages(
    DEPENDENCIES
    #std_msgs
    geometry_msgs
    sensor_msgs
)

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
generate_dynamic_reconfigure_options(
    cfg/matcher.cfg
    cfg/preprocessor.cfg
    cfg/verifier.cfg
)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
    INCLUDE_DIRS include
    LIBRARIES CoreBinaries
    CATKIN_DEPENDS message_runtime sensor_msgs std_msgs roscpp dynamic_reconfigure pcl_ros
    #DEPENDS PCL # MFZ 11/22/18 added
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
set ( cmph_LIBRARIES libcmph.so )
set ( cmph_INCLUDE_DIRS /usr/local/lib/ )

include_directories(
    include
    ${catkin_INCLUDE_DIRS}
    ${cmph_INCLUDE_DIRS}
    #${PCL_INCLUDE_DIRS} # MFZ 11/22/18 added
    ${metslib_INCLUDE_DIRS}
)

#link_directories(${PCL_LIBRARY_DIRS}) # MFZ 11/22/18 added
#add_definitions(${PCL_DEFINITIONS})   # MFZ 11/22/18 added

## binaries
add_library(CoreBinaries src/filePaths.cpp src/houghSpace2D.cpp src/pairFeatures.cpp src/pose.cpp)
target_link_libraries(CoreBinaries
    ${catkin_LIBRARIES}
    #${PCL_LIBRARIES} # MFZ 11/22/18 added
)

## nodelets
add_library(NodeletWithStatistics src/nodeletWithStatistics.cpp)
target_link_libraries(NodeletWithStatistics
    ${catkin_LIBRARIES}
    #${PCL_LIBRARIES} # MFZ 11/22/18 added
)
add_dependencies(NodeletWithStatistics ${PROJECT_NAME}_generate_messages_cpp)

add_library(CoreNodelets src/pcPreprocessorNodelet.cpp src/matcherNodelet.cpp src/verifierNodelet.cpp src/processedCloudsSplitterNodelet.cpp)
target_link_libraries(CoreNodelets
    ${catkin_LIBRARIES}
    ${BOOST_LIBRARIES} libboost_timer.so
    ##${Boost_INCLUDE_DIRS}
    NodeletWithStatistics
    CoreBinaries
    ${cmph_LIBRARIES}
    #${PCL_LIBRARIES}  # MFZ 11/22/18 added
)
add_dependencies(CoreNodelets ${PROJECT_NAME}_gencfg ${PROJECT_NAME}_generate_messages_cpp)



## Node instantiation of nodelets
add_executable(pcPreprocessor src/pcPreprocessorNode.cpp)
target_link_libraries(pcPreprocessor ${catkin_LIBRARIES} ) #${PCL_LIBRARIES}) # MFZ 11/22/18 added

add_executable(matcher src/matcherNode.cpp)
target_link_libraries(matcher ${catkin_LIBRARIES} ${cmph_LIBRARIES} ) # ${PCL_LIBRARIES}) # MFZ 11/22/18 added

add_executable(verifier src/verifierNode.cpp)
target_link_libraries(verifier ${catkin_LIBRARIES})

add_executable(splitter src/processedCloudsSplitterNode.cpp)
target_link_libraries(splitter ${catkin_LIBRARIES})

## Add cmake target dependencies of the executable
## same as for the library above
# add_dependencies(pf_matching_research_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
#target_link_libraries(
#
#    ${catkin_LIBRARIES}
#)

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark executables and/or libraries for installation
# install(TARGETS pf_matching_research pf_matching_research_node
#   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

## Mark cpp header files for installation
install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.hpp"
  )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_pf_matching_research.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
