#ifndef _METSLIB_METSLIB_AH_HH
#define _METSLIB_METSLIB_AH_HH 1
 
/* metslib/metslib_ah.hh. Generated automatically at end of configure. */
/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if you have the <inttypes.h> header file. */
#ifndef METSLIB_HAVE_INTTYPES_H
#define METSLIB_HAVE_INTTYPES_H 1
#endif

/* Define to 1 if you have the <memory.h> header file. */
#ifndef METSLIB_HAVE_MEMORY_H
#define METSLIB_HAVE_MEMORY_H 1
#endif

/* Define to 1 if you have the <stdint.h> header file. */
#ifndef METSLIB_HAVE_STDINT_H
#define METSLIB_HAVE_STDINT_H 1
#endif

/* Define to 1 if you have the <stdlib.h> header file. */
#ifndef METSLIB_HAVE_STDLIB_H
#define METSLIB_HAVE_STDLIB_H 1
#endif

/* Define to 1 if you have the <strings.h> header file. */
#ifndef METSLIB_HAVE_STRINGS_H
#define METSLIB_HAVE_STRINGS_H 1
#endif

/* Define to 1 if you have the <string.h> header file. */
#ifndef METSLIB_HAVE_STRING_H
#define METSLIB_HAVE_STRING_H 1
#endif

/* Define to 1 if you have the <sys/stat.h> header file. */
#ifndef METSLIB_HAVE_SYS_STAT_H
#define METSLIB_HAVE_SYS_STAT_H 1
#endif

/* Define to 1 if you have the <sys/types.h> header file. */
#ifndef METSLIB_HAVE_SYS_TYPES_H
#define METSLIB_HAVE_SYS_TYPES_H 1
#endif

/* Define to 1 if you have the <tr1/unordered_map> header file. */
#ifndef METSLIB_HAVE_TR1_UNORDERED_MAP
#define METSLIB_HAVE_TR1_UNORDERED_MAP 1
#endif

/* Define to 1 if you have the <unistd.h> header file. */
#ifndef METSLIB_HAVE_UNISTD_H
#define METSLIB_HAVE_UNISTD_H 1
#endif

/* Define to 1 if you have the <unordered_map> header file. */
/* #undef HAVE_UNORDERED_MAP */

/* Define to the address where bug reports for this package should be sent. */
#ifndef METSLIB_PACKAGE_BUGREPORT
#define METSLIB_PACKAGE_BUGREPORT "mirko.maischberger@gmail.com"
#endif

/* Define to the full name of this package. */
#ifndef METSLIB_PACKAGE_NAME
#define METSLIB_PACKAGE_NAME "metslib"
#endif

/* Define to the full name and version of this package. */
#ifndef METSLIB_PACKAGE_STRING
#define METSLIB_PACKAGE_STRING "metslib 0.5.3"
#endif

/* Define to the one symbol short name of this package. */
#ifndef METSLIB_PACKAGE_TARNAME
#define METSLIB_PACKAGE_TARNAME "metslib"
#endif

/* Define to the home page for this package. */
#ifndef METSLIB_PACKAGE_URL
#define METSLIB_PACKAGE_URL ""
#endif

/* Define to the version of this package. */
#ifndef METSLIB_PACKAGE_VERSION
#define METSLIB_PACKAGE_VERSION "0.5.3"
#endif

/* Define to 1 if you have the ANSI C header files. */
#ifndef METSLIB_STDC_HEADERS
#define METSLIB_STDC_HEADERS 1
#endif

/* Description */
/* #undef TR1_MIXED_NAMESPACE */
 
/* once: _METSLIB_METSLIB_AH_HH */
#endif
