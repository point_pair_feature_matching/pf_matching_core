This package contains the core functionality to match models point clouds
to scene point clouds via pair feature matching. It was developed as part
of the master's thesis of Xaver Kroischke, the bachelor's thesis of Sebastian
Krone and the master's thesis of Markus Ziegler. For the theoretical
background of the algorithms and their implementations take a look at these theses.

For a copy of Xaver's thesis, please contact him at xk.coding@gmail.com
For a copy of Markus's thesis, please contact him at m.f.z@t-online.de


# INSTALLATION

Please read the user_manual.odt or the user_manual.pdf.


It uses the installation script
`
installation_script_for_locally_setting_up_ros_system.sh
`.


The manual also includes the installation of the other two packages
`
pf_matching
`
and 
`
pf_matching_tools
`
and explains how to use the system.

# PATENTS

Please note that the pair feature matching algorithm is protected by at least
the following patents:

* US 8830229B2
* EP 2385483B1

If you are using this code for purposes other than researching the properties
of pair feature matching, please make sure that you acquire a suitable license
from the patent owners.
